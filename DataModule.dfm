object dmMain: TdmMain
  OldCreateOrder = False
  Left = 302
  Height = 860
  Width = 877
  object dbTicker: TADOConnection
    LoginPrompt = False
    Provider = 'SQLOLEDB'
    Left = 16
    Top = 24
  end
  object tblTicker_Groups: TADOTable
    Connection = dbTicker
    IndexFieldNames = 'Playlist_Description'
    TableName = 'Ticker_Groups'
    Left = 104
    Top = 80
  end
  object tblTicker_Elements: TADOTable
    Connection = dbTicker
    TableName = 'Ticker_Elements'
    Left = 104
    Top = 24
  end
  object dsTicker_Groups: TDataSource
    DataSet = tblTicker_Groups
    Left = 256
    Top = 144
  end
  object Query1: TADOQuery
    Connection = dbTicker
    Parameters = <>
    Left = 424
    Top = 29
  end
  object dsQuery1: TDataSource
    DataSet = Query1
    Left = 544
    Top = 27
  end
  object tblScheduled_Ticker_Groups: TADOTable
    Connection = dbTicker
    TableName = 'Scheduled_Ticker_Groups'
    Left = 104
    Top = 144
  end
  object dsScheduled_Ticker_Groups: TDataSource
    DataSet = tblScheduled_Ticker_Groups
    Left = 257
    Top = 208
  end
  object tblSponsor_Logos: TADOTable
    Connection = dbTicker
    TableName = 'Sponsor_Logos'
    Left = 426
    Top = 460
  end
  object dsSponsor_Logos: TDataSource
    DataSet = tblSponsor_Logos
    Left = 546
    Top = 461
  end
  object tblGame_Notes: TADOTable
    Connection = dbTicker
    TableName = 'Game_Notes'
    Left = 424
    Top = 393
  end
  object tblLeagues: TADOTable
    Connection = dbTicker
    TableName = 'Leagues'
    Left = 426
    Top = 596
  end
  object tblPromo_Logos: TADOTable
    Connection = dbTicker
    TableName = 'Promo_Logos'
    Left = 426
    Top = 532
  end
  object dsPromo_Logos: TDataSource
    DataSet = tblPromo_Logos
    Left = 546
    Top = 533
  end
  object dbSportbase: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;User ID=sa;Data S' +
      'ource=SportBase'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 664
    Top = 29
  end
  object SportbaseQuery: TADOQuery
    Connection = dbSportbase
    Parameters = <>
    Left = 664
    Top = 93
  end
  object dsSportbaseQuery: TDataSource
    DataSet = SportbaseQuery
    Left = 760
    Top = 93
  end
  object Query2: TADOQuery
    Connection = dbTicker
    Parameters = <>
    Left = 424
    Top = 85
  end
  object dsQuery2: TDataSource
    DataSet = Query2
    Left = 544
    Top = 83
  end
  object Query3: TADOQuery
    Connection = dbTicker
    Parameters = <>
    Left = 424
    Top = 149
  end
  object SportbaseQuery2: TADOQuery
    Connection = dbSportbase
    Parameters = <>
    Left = 664
    Top = 157
  end
  object tblBreakingNews_Elements: TADOTable
    Connection = dbTicker
    TableName = 'BreakingNews_Elements'
    Left = 104
    Top = 208
  end
  object tblBreakingNews_Groups: TADOTable
    Connection = dbTicker
    TableName = 'BreakingNews_Groups'
    Left = 104
    Top = 264
  end
  object tblScheduled_BreakingNews_Groups: TADOTable
    Connection = dbTicker
    TableName = 'Scheduled_BreakingNews_Groups'
    Left = 104
    Top = 328
  end
  object dsBreakingNews_Groups: TDataSource
    DataSet = tblBreakingNews_Groups
    Left = 256
    Top = 328
  end
  object dsScheduled_BreakingNews_Groups: TDataSource
    DataSet = tblScheduled_BreakingNews_Groups
    Left = 257
    Top = 392
  end
  object tblExtraline_Elements: TADOTable
    Connection = dbTicker
    TableName = 'Extraline_Elements'
    Left = 104
    Top = 392
  end
  object SportbaseQuery3: TADOQuery
    Connection = dbSportbase
    Parameters = <>
    Left = 664
    Top = 221
  end
  object dsSportbaseQuery3: TDataSource
    DataSet = SportbaseQuery3
    Left = 760
    Top = 221
  end
  object tblTemplate_Defs: TADOTable
    Connection = dbTicker
    TableName = 'Template_Defs'
    Left = 104
    Top = 456
  end
  object tblTeams: TADOTable
    Connection = dbTicker
    TableName = 'Teams'
    Left = 430
    Top = 668
  end
  object dsTeams: TDataSource
    DataSet = tblTeams
    Left = 534
    Top = 669
  end
  object tblGame_Phase_Codes: TADOTable
    Connection = dbTicker
    TableName = 'Game_Phase_Codes'
    Left = 662
    Top = 372
  end
  object dsGame_Phase_Codes: TDataSource
    DataSet = tblGame_Phase_Codes
    Left = 782
    Top = 373
  end
  object tblCustom_Headers: TADOTable
    Connection = dbTicker
    TableName = 'Custom_Headers'
    Left = 104
    Top = 592
  end
  object dsCustom_Headers: TDataSource
    DataSet = tblCustom_Headers
    Left = 256
    Top = 592
  end
  object GameDataQuery: TADOQuery
    Connection = dbTicker
    Parameters = <>
    Left = 424
    Top = 333
  end
  object WeatherQuery: TADOQuery
    Connection = dbTicker
    Parameters = <>
    Left = 544
    Top = 333
  end
  object tblDefault_Templates: TADOTable
    Connection = dbTicker
    TableName = 'Default_Templates'
    Left = 104
    Top = 664
  end
  object Query4: TADOQuery
    Connection = dbTicker
    Parameters = <>
    Left = 424
    Top = 213
  end
  object dsQuery4: TDataSource
    DataSet = Query2
    Left = 544
    Top = 211
  end
  object TeamsQuery: TADOQuery
    Connection = dbTicker
    Parameters = <>
    Left = 432
    Top = 725
  end
  object dsWeather_Cities: TDataSource
    DataSet = tblWeather_Cities
    Left = 256
    Top = 731
  end
  object tblWeather_Cities: TADOTable
    Connection = dbTicker
    TableName = 'Weather_Cities'
    Left = 104
    Top = 728
  end
  object dbTraffic: TADOConnection
    LoginPrompt = False
    Provider = 'SQLOLEDB'
    Left = 24
    Top = 792
  end
  object tblTraffic_Times: TADOTable
    Connection = dbTraffic
    TableName = 'Traffic_Times'
    Left = 104
    Top = 792
  end
  object tblTraffic_Incidents: TADOTable
    Connection = dbTraffic
    Filter = '(IncidentDescription <> '#39#39') AND (ImpactType > 2)'
    Filtered = True
    TableName = 'Traffic_Incidents'
    Left = 104
    Top = 848
  end
  object dsTraffic_Times: TDataSource
    DataSet = tblTraffic_Times
    Left = 256
    Top = 792
  end
  object dsTraffic_Incidents: TDataSource
    DataSet = tblTraffic_Incidents
    Left = 256
    Top = 856
  end
  object TrafficQuery: TADOQuery
    Connection = dbTraffic
    Parameters = <>
    Left = 368
    Top = 795
  end
  object dsTemplate_Defs: TDataSource
    DataSet = tblTemplate_Defs
    Left = 258
    Top = 457
  end
  object tblTemplate_Fields: TADOTable
    Connection = dbTicker
    TableName = 'Template_Fields'
    Left = 104
    Top = 520
  end
  object dsTemplate_Fields: TDataSource
    DataSet = tblTemplate_Fields
    Left = 258
    Top = 521
  end
  object tblTop_Region_Image_Wheel: TADOTable
    Connection = dbTicker
    TableName = 'Top_Region_Image_Wheel'
    Left = 104
    Top = 928
  end
  object dsTop_Region_Image_Wheel: TDataSource
    DataSet = tblTop_Region_Image_Wheel
    Left = 256
    Top = 928
  end
  object Query5: TADOQuery
    Connection = dbTicker
    Parameters = <>
    Left = 424
    Top = 269
  end
  object DBLoadQuery: TADOQuery
    Connection = dbTicker
    Parameters = <>
    Left = 488
    Top = 269
  end
end
