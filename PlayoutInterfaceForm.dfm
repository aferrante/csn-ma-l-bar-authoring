object PlayoutInterface: TPlayoutInterface
  Left = 652
  Top = 408
  Width = 132
  Height = 128
  Caption = 'PlayoutInterface'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object PlayoutConnection: TClientSocket
    Active = False
    ClientType = ctNonBlocking
    Port = 0
    OnConnect = PlayoutConnectionConnect
    OnDisconnect = PlayoutConnectionDisconnect
    OnRead = PlayoutConnectionRead
    OnError = PlayoutConnectionError
    Left = 40
    Top = 16
  end
  object ReconnectTimer: TTimer
    Enabled = False
    OnTimer = ReconnectTimerTimer
    Left = 40
    Top = 64
  end
end
