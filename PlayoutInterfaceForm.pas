unit PlayoutInterfaceForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ScktComp, Globals, TsGrid, ExtCtrls;

type
  TPlayoutInterface = class(TForm)
    PlayoutConnection: TClientSocket;
    ReconnectTimer: TTimer;
    procedure PlayoutConnectionError(Sender: TObject;
      Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
      var ErrorCode: Integer);
    procedure PlayoutConnectionConnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnectionDisconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure PlayoutConnectionRead(Sender: TObject;
      Socket: TCustomWinSocket);
    function ParseReceivedData(CommandIn: String): CommandRec;
    procedure SendCommand(CommandInfo: CommandRec);
    procedure ReconnectTimerTimer(Sender: TObject);
  private
    { Private declarations }
  public
     procedure SetPlayoutSlaveMode(Enable: Boolean);
     procedure SetAutoPlayMode(Enable: Boolean);
     procedure LoadPlaylist(PlaylistID: Double);
     procedure RefreshCurrentPlaylist(PlaylistID: Double);
     procedure UnloadCurrentPlaylist;
     procedure PlayNextPage(FieldID: SmallInt);
     procedure PlaySpecifiedPage(FieldID, PageIndex: SmallInt);
     procedure TriggerTicker;
     procedure PauseTicker;
     procedure ResetTickerToTop;
     procedure ClearAllGraphics;
     procedure RemoveAllFields;
     procedure SetRemoteClockMode(ClockIn: Boolean);
  end;

var
  PlayoutInterface: TPlayoutInterface;

implementation

uses Main, EngineIntf;

{$R *.dfm}

////////////////////////////////////////////////////////////////////////////////
// HANDLERS FOR SOCKET CONNECT/DISCONNECT
////////////////////////////////////////////////////////////////////////////////
//Playout Controller socket
procedure TPlayoutInterface.PlayoutConnectionConnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Make sure re-connection timer is disabled
  ReconnectTimer.Enabled := FALSE;

  //Set UI controls
  with MainForm do
  begin
    StatusBar.Color := clBtnFace;
    StatusBar.SimpleText := ' CONNECTED TO PLAYOUT CONTROLLER';
  end;

  //Set Connected flag
  PlayoutControllerInfo.Connected := TRUE;
  //Set status LEDs
  MainForm.PlayoutConnectLED.Lit := TRUE;
end;
procedure TPlayoutInterface.PlayoutConnectionDisconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  //Reset if in remote playout mode
  if (InRemotePlayoutMode) then
  begin
    //Set flag
    InRemotePlayoutMode := FALSE;
    //Set UI controls
    with MainForm do
    begin
      GotoRemotePlayoutModeMenu.Checked := FALSE;
      ExitRemotePlayoutModeMenu.Checked := TRUE;
      //PlaylistGridPanel.Color := clBtnFace;
      PlayoutPanel.Visible := FALSE;
      PlaylistModeLabel.Caption := 'L-BAR PLAYLIST MODE';
      StatusBar.Color := clYellow;
      StatusBar.SimpleText := ' LOST CONNECTION TO PLAYOUT CONTROLLER';
      //Set the grid modes to multiple rows
      MainForm.PlaylistGrid.RowSelectMode := rsMulti;
      MainForm.PlaylistGrid2.RowSelectMode := rsMulti;
    end;
    //Enable re-connection timer
    ReconnectTimer.Enabled := TRUE;
  end;

  //Clear Connected flag
  PlayoutControllerInfo.Connected := FALSE;
  //Set status LEDs
  MainForm.PlayoutConnectLED.Lit := FALSE;
end;
procedure TPlayoutInterface.PlayoutConnectionError(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  //Clear Connected flag
  PlayoutControllerInfo.Connected := FALSE;
  MainForm.PlayoutConnectLED.Lit := FALSE;
  //Set UI controls
  with MainForm do
  begin
    GotoRemotePlayoutModeMenu.Checked := FALSE;
    ExitRemotePlayoutModeMenu.Checked := TRUE;
    //PlaylistGridPanel.Color := clBtnFace;
    PlayoutPanel.Visible := FALSE;
    PlaylistModeLabel.Caption := 'L-BAR PLAYLIST MODE';
    StatusBar.Color := clYellow;
    StatusBar.SimpleText := ' ERROR WITH CONNECTION TO PLAYOUT CONTROLLER';
    //Set the grid mode to multiple rows
    MainForm.PlaylistGrid.RowSelectMode := rsMulti;
  end;
  //Clear error code to prevent runtime exception
  ErrorCode := 0;
end;

////////////////////////////////////////////////////////////////////////////////
// FUNCTIONS FOR RECEIVING AND PROCESSING COMMANDS FROM PLAYOUT CONTROLLERS
////////////////////////////////////////////////////////////////////////////////
//General function to parse data received from Playout Controller
function TPlayoutInterface.ParseReceivedData(CommandIn: String): CommandRec;
var
  Tokens: Array[1..7] of String;
  i, Cursor, TokenIndex: SmallInt;
  ParamVal: SmallInt;
  OutRec: CommandRec;
begin
  try
    //Parse the string for the command and any parameters
    //First, check for valid command framing; should contain at least one SOH, one ETX and one EOT;
    //If not, send back error code 1 = Invalid command syntax received
    //Need to at least have framing characters; also check for SOH at start and EOT at end
    if (Length(CommandIn) > 3) AND (CommandIn[1] = SOH) AND (CommandIn[Length(CommandIn)] = EOT) then
    begin
      //Strip off SOT and EOT
      CommandIn := Copy (CommandIn, 2, Length(CommandIn)-2);
      //Parse for tokens
      for i := 1 to 7 do Tokens[i] := '';
      Cursor := 1;
      TokenIndex :=1;
      repeat
        if (CommandIn[Cursor] = ETX) then
        begin
          Inc(Cursor);
          Inc(TokenIndex);
        end;
        Tokens[TokenIndex] := Tokens[TokenIndex] + CommandIn[Cursor];
        Inc(Cursor);
      until (Cursor > Length(CommandIn));
    end;
    //Set output record
    OutRec.Command := Tokens[1];
    OutRec.Param[1] := Tokens[2];
    OutRec.Param[2] := Tokens[3];
    OutRec.Param[3] := Tokens[4];
    OutRec.Param[4] := Tokens[5];
    ParseReceivedData := OutRec;
  except
    EngineInterface.WriteToErrorLog('Error occurred while trying to parse data received from Playout Controller');
  end;
end;

//Data packet from Playout Controller - Messages are status/error and current playlist index
procedure TPlayoutInterface.PlayoutConnectionRead(Sender: TObject;
  Socket: TCustomWinSocket);
var
  i: SmallInt;
  Data: String;
  PlayoutControllerRecPtr: ^PlayoutControllerRec;
  EOTLocation: SmallInt;
  CommandStr: String;
  CommandData: CommandRec;
  AllCommandsProcessed: Boolean;
  ParamVal, ParamVal2: SmallInt;
  ErrorMessage: String;
begin
  try
    //Process commands
    if (PlayoutControllerInfo.Connected) then
    begin
      Data := Socket.ReceiveText;
      //Init
      AllCommandsProcessed := FALSE;
      //Process commands
      if (Data[1] = SOH) AND (Data[Length(Data)] = EOT) then
      repeat
        //Copy command from data string & parse
        //Find end of command
        EOTLocation := Pos(EOT, Data);
        //Copy from start to end of command & parse
        CommandStr := Copy(Data, 1, EOTLocation);
        CommandData := ParseReceivedData(CommandStr);
        //Delete command from data string for next iteration
        if (EOTLocation < Length(Data)) then
          Data := Copy(Data, EOTLocation+1, Length(Data))
        else
          AllCommandsProcessed := TRUE;

        //Check for specific commands
        //Check for Status/Error Code
        if (CommandData.Command = 'CS') then
        begin
          //Call function to trigger ticker; pass in -1 if invalid integer
          ParamVal := StrToIntDef(CommandData.Param[1], -1);
          if (ParamVal >= 0) then
          begin
            //Set error code if applicable
            Case ParamVal of
             -1: ErrorMessage := ' Undefined status/error code reported by Playout Controller';
              1: ErrorMessage := ' Improperly framed command reported by Playout Controller';
              2: ErrorMessage := ' Incorrect remote control mode specified';
              3: ErrorMessage := ' Incorrect auto-sequence mode specified';
              4: ErrorMessage := ' Invalid playlist ID specified';
              5: ErrorMessage := ' Invalid ticker entry index specified';
            end;
            //MainForm.StatusBar.Color := clYellow;
            MainForm.StatusBar.SimpleText := UpperCase(ErrorMessage);
          end;
        end

        //Check for current ticker entry index
        else if (CommandData.Command = 'PI') and (InRemotePlayoutMode) {and (InRemoteAutoSequenceMode)} then
        begin
          //Call function to trigger ticker; pass in -1 if invalid integer
          ParamVal := StrToIntDef(CommandData.Param[1], -1);
          ParamVal2 := StrToIntDef(CommandData.Param[2], -1);
          if (ParamVal2 >= 0) then
          begin
            //Param 1 = Playlist ID; Param 2 = Playlist entry index
            Case ParamVal of
              PLAYLIST_1: begin
                //Set current index in grid
                //MainForm.PlaylistGrid.CurrentDataRow := ParamVal2+1;
                //MainForm.PlaylistGrid.SetTopLeft(1, MainForm.PlaylistGrid.CurrentDataRow);
                //Set current Playout playlist index for Grid #2
                CurrentPlayoutPlaylistIndex1 := ParamVal2;
                //Set color to red for that row only
                if (MainForm.PlaylistGrid.Rows > 0) then
                begin
                  for i := 1 to MainForm.PlaylistGrid.Rows do
                  begin
                    if (i = (ParamVal2+1)) then
                      MainForm.PlaylistGrid.RowColor[i] := clRed
                    else
                      MainForm.PlaylistGrid.RowColor[i] := clWindow;
                  end;
                end;
               end;
              PLAYLIST_2: begin
                //Set current index in grid
                //MainForm.PlaylistGrid2.CurrentDataRow := ParamVal2+1;
                //MainForm.PlaylistGrid2.SetTopLeft(1, MainForm.PlaylistGrid2.CurrentDataRow);
                //Set current Playout playlist index for Grid #2
                CurrentPlayoutPlaylistIndex2 := ParamVal2;
                //Set color to red for that row only
                if (MainForm.PlaylistGrid2.Rows > 0) then
                begin
                  for i := 1 to MainForm.PlaylistGrid2.Rows do
                  begin
                    if (i = (ParamVal2+1)) then
                      MainForm.PlaylistGrid2.RowColor[i] := clRed
                    else
                      MainForm.PlaylistGrid2.RowColor[i] := clWindow;
                  end;
                end;
              end;
            end;
          end;
        end

        //Check for Playlist auto-sequence status code
        else if (CommandData.Command = 'PS') then
        begin
          //Call function to trigger ticker; pass in -1 if invalid integer
          ParamVal := StrToIntDef(CommandData.Param[1], -1);
          if (ParamVal >= 0) then
          begin
            //Set error code if applicable
            Case ParamVal of
                 //Not auto-sequencing
             -1: begin
                   MainForm.PlaylistRunningLED.Lit := FALSE;
                   MainForm.PlaylistHaltedLED.Lit := FALSE;
                 end;
                 //Halted
              0: begin
                   MainForm.PlaylistRunningLED.Lit := FALSE;
                   MainForm.PlaylistHaltedLED.Lit := TRUE;
                   MainForm.PlayoutSequencingLED.Lit := FALSE;
                 end;
                 //Running
              1: begin
                   MainForm.PlaylistRunningLED.Lit := TRUE;
                   MainForm.PlaylistHaltedLED.Lit := FALSE;
                   MainForm.PlayoutSequencingLED.Lit := TRUE;
                 end;
            end;
          end;
        end

      until (AllCommandsProcessed);
    end;
  except
    EngineInterface.WriteToErrorLog('Error occurred while trying to process parsed data received from Playout Controller');
  end;
end;

//Procedure to send a command to the Playout Controller application
procedure TPlayoutInterface.SendCommand(CommandInfo: CommandRec);
var
  i: SmallInt;
  CmdStr: String;
begin
  if (PlayoutConnection.Active) then
  begin
    CmdStr := SOH + CommandInfo.Command;
    for i := 1 to 4 do
    begin
      if (Trim(CommandInfo.Param[i]) <> '') then
        CmdStr := CmdStr + ETX + Trim(CommandInfo.Param[i]);
    end;
    CmdStr := CmdStr + EOT;
    //Send command
    if (PlayoutConnection.Active) then
      PlayoutConnection.Socket.SendText(CmdStr);
  end;
end;

////////////////////////////////////////////////////////////////////////////////
//FUNCTIONS TO SEND COMMANDS TO PLAYOUT CONTROLLER
////////////////////////////////////////////////////////////////////////////////
//Set Playout Controller slave mode
procedure TPlayoutInterface.SetPlayoutSlaveMode(Enable: Boolean);
var
  i: SmallInt;
  CmdOut: CommandRec;
  ParamStr: String;
begin
  //Setup command
  if (Enable) then ParamStr := '1'
  else ParamStr := '0';
  CmdOut.Command := 'SM';
  CmdOut.Param[1] := ParamStr;
  for i := 2 to 4 do CmdOut.Param[i] := '';
  //Send command
  SendCommand(CmdOut);
end;

//Set auto-play (sequence) mode
procedure TPlayoutInterface.SetAutoPlayMode(Enable: Boolean);
var
  i: SmallInt;
  CmdOut: CommandRec;
  ParamStr: String;
begin
  //Setup command
  if (Enable) then ParamStr := '1'
  else ParamStr := '0';
  CmdOut.Command := 'AP';
  CmdOut.Param[1] := ParamStr;
  for i := 2 to 4 do CmdOut.Param[i] := '';
  //Send command
  SendCommand(CmdOut);
end;

//Load specfied playlist on Playout Controller
procedure TPlayoutInterface.LoadPlaylist(PlaylistID: Double);
var
  i: SmallInt;
  CmdOut: CommandRec;
  ParamStr: String;
begin
  //Setup command
  ParamStr := FloatToStr(PlaylistID);
  CmdOut.Command := 'PL';
  CmdOut.Param[1] := ParamStr;
  for i := 2 to 4 do CmdOut.Param[i] := '';
  //Send command
  SendCommand(CmdOut);
end;

//Unload current playlist on Playout Controller
procedure TPlayoutInterface.UnloadCurrentPlaylist;
var
  i: SmallInt;
  CmdOut: CommandRec;
begin
  //Setup command
  CmdOut.Command := 'PU';
  for i := 1 to 4 do CmdOut.Param[i] := '';
  //Send command
  SendCommand(CmdOut);
end;

//Reload current playlist
procedure TPlayoutInterface.RefreshCurrentPlaylist(PlaylistID: Double);
var
  i: SmallInt;
  CmdOut: CommandRec;
  ParamStr: String;
begin
  //Setup command
  ParamStr := FloatToStr(PlaylistID);
  CmdOut.Command := 'RP';
  CmdOut.Param[1] := ParamStr;
  for i := 2 to 4 do CmdOut.Param[i] := '';
  //Send command
  SendCommand(CmdOut);
end;

//Play next page in playlist
procedure TPlayoutInterface.PlayNextPage(FieldID: SmallInt);
var
  i: SmallInt;
  CmdOut: CommandRec;
  ParamStr: String;
begin
  //Setup command
  CmdOut.Command := 'PN';
  CmdOut.Param[1] := IntToStr(FieldID);
  for i := 2 to 4 do CmdOut.Param[i] := '';
  //Send command
  SendCommand(CmdOut);
end;

//Play selected page in playlist
procedure TPlayoutInterface.PlaySpecifiedPage(FieldID, PageIndex: SmallInt);
var
  i: SmallInt;
  CmdOut: CommandRec;
  ParamStr: String;
begin
  //Setup command
  ParamStr := IntToStr(PageIndex);
  CmdOut.Command := 'PS';
  CmdOut.Param[1] := IntToStr(FieldID);
  CmdOut.Param[2] := ParamStr;
  for i := 3 to 4 do CmdOut.Param[i] := '';
  //Send command
  SendCommand(CmdOut);
end;

//Trigger ticker (when in auto-sequence mode)
procedure TPlayoutInterface.TriggerTicker;
var
  i: SmallInt;
  CmdOut: CommandRec;
begin
  //Setup command
  CmdOut.Command := 'TS';
  for i := 1 to 4 do CmdOut.Param[i] := '';
  //Send command
  SendCommand(CmdOut);
end;

//Pause ticker (when in auto-sequence mode)
procedure TPlayoutInterface.PauseTicker;
var
  i: SmallInt;
  CmdOut: CommandRec;
begin
  //Setup command
  CmdOut.Command := 'TP';
  for i := 1 to 4 do CmdOut.Param[i] := '';
  //Send command
  SendCommand(CmdOut);
end;

//Reset ticker to top (when in auto-sequence mode)
procedure TPlayoutInterface.ResetTickerToTop;
var
  i: SmallInt;
  CmdOut: CommandRec;
begin
  //Setup command
  CmdOut.Command := 'TR';
  for i := 1 to 4 do CmdOut.Param[i] := '';
  //Send command
  SendCommand(CmdOut);
end;

//Clear all graphics on on-air Channel Box
procedure TPlayoutInterface.ClearAllGraphics;
var
  i: SmallInt;
  CmdOut: CommandRec;
begin
  //Setup command
  CmdOut.Command := 'CG';
  for i := 1 to 4 do CmdOut.Param[i] := '';
  //Send command
  SendCommand(CmdOut);
end;

//Clear all "Field" data panels
procedure TPlayoutInterface.RemoveAllFields;
var
  i: SmallInt;
  CmdOut: CommandRec;
begin
  //Setup command
  CmdOut.Command := 'CD';
  for i := 1 to 4 do CmdOut.Param[i] := '';
  //Send command
  SendCommand(CmdOut);
end;

//Set the state of the clock on the on-air Channel Box
procedure TPlayoutInterface.SetRemoteClockMode(ClockIn: Boolean);
var
  i: SmallInt;
  StateStr: String;
  CmdOut: CommandRec;
begin
  //Set the state of the clock
  if (ClockIn) then StateStr := '1'
  else StateStr := '0';
  //Setup command
  CmdOut.Command := 'SC';
  CmdOut.Param[1] := StateStr;
  for i := 2 to 4 do CmdOut.Param[i] := '';
  //Send command
  SendCommand(CmdOut);
end;

procedure TPlayoutInterface.ReconnectTimerTimer(Sender: TObject);
begin
  //Attempt re-connect to the Playout Controller for remote control if enabled
  if (PlayoutControllerInfo.Enabled = TRUE) then
  begin
    try
      EngineParameters.Enabled := TRUE;
      PlayoutInterface.PlayoutConnection.Address := PlayoutControllerInfo.IPAddress;
      PlayoutInterface.PlayoutConnection.Port := PlayoutControllerInfo.Port;
      PlayoutInterface.PlayoutConnection.Active := TRUE;
      MainForm.StatusBar.SimpleText := ' ATTEMPTING RE-CONNECTION TO PLAYOUT CONTROLLER';
    except

    end;
  end;
end;

end.
