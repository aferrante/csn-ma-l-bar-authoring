unit TemplateFieldLengthEditor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBCtrls, StdCtrls, Grids_ts, TSGrid, TSDBGrid, ExtCtrls, Buttons;

type
  TTemplateFieldLengthEditorDlg = class(TForm)
    Panel1: TPanel;
    tsDBGrid1: TtsDBGrid;
    Label1: TLabel;
    DBNavigator1: TDBNavigator;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    BitBtn1: TBitBtn;
    Panel2: TPanel;
    Label6: TLabel;
    Label7: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    tsDBGrid2: TtsDBGrid;
    DBNavigator2: TDBNavigator;
    procedure tsDBGrid2RowChanged(Sender: TObject; OldRow,
      NewRow: Variant);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TemplateFieldLengthEditorDlg: TTemplateFieldLengthEditorDlg;

implementation

uses DataModule;


{$R *.dfm}

procedure TTemplateFieldLengthEditorDlg.tsDBGrid2RowChanged(Sender: TObject; OldRow, NewRow: Variant);
begin
  dmMain.tblTemplate_Fields.Filtered := TRUE;
  dmMain.tblTemplate_Fields.Filter := 'Template_ID = ' + IntToStr(dmMain.tblTemplate_Defs.FieldByName('Template_ID').AsInteger) + ' AND Field_Type = 2';
end;

end.
