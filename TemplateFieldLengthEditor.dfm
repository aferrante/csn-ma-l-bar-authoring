object TemplateFieldLengthEditorDlg: TTemplateFieldLengthEditorDlg
  Left = 510
  Top = 76
  BorderStyle = bsDialog
  Caption = 'Template Field Length Editor'
  ClientHeight = 730
  ClientWidth = 627
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 16
    Top = 315
    Width = 593
    Height = 358
    BevelWidth = 2
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 14
      Width = 128
      Height = 20
      Caption = 'Template Fields'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 35
      Top = 286
      Width = 37
      Height = 20
      Caption = 'First'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 102
      Top = 286
      Width = 38
      Height = 20
      Caption = 'Prior'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 172
      Top = 286
      Width = 37
      Height = 20
      Caption = 'Next'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 242
      Top = 286
      Width = 36
      Height = 20
      Caption = 'Last'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 313
      Top = 286
      Width = 33
      Height = 20
      Caption = 'Edit'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 379
      Top = 286
      Width = 37
      Height = 20
      Caption = 'Post'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 439
      Top = 286
      Width = 56
      Height = 20
      Caption = 'Cancel'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 508
      Top = 286
      Width = 65
      Height = 20
      Caption = 'Refresh'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object tsDBGrid1: TtsDBGrid
      Left = 16
      Top = 40
      Width = 561
      Height = 241
      CellSelectMode = cmNone
      CheckBoxStyle = stCheck
      Cols = 4
      DatasetType = dstStandard
      DataSource = dmMain.dsTemplate_Fields
      DefaultRowHeight = 18
      ExactRowCount = True
      ExportDelimiter = ','
      FieldState = fsCustomized
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HeadingFont.Charset = DEFAULT_CHARSET
      HeadingFont.Color = clWindowText
      HeadingFont.Height = -13
      HeadingFont.Name = 'MS Sans Serif'
      HeadingFont.Style = [fsBold]
      HeadingHeight = 20
      HeadingParentFont = False
      ParentFont = False
      ParentShowHint = False
      RowChangedIndicator = riAutoReset
      RowMoving = False
      ShowHint = False
      TabOrder = 0
      Version = '2.20.26'
      XMLExport.Version = '1.0'
      XMLExport.DataPacketVersion = '2.0'
      DataBound = True
      ColProperties = <
        item
          DataCol = 1
          FieldName = 'Template_ID'
          Col.FieldName = 'Template_ID'
          Col.Heading = 'Template ID'
          Col.Width = 94
          Col.AssignedValues = '?'
        end
        item
          DataCol = 2
          FieldName = 'Field_ID'
          Col.FieldName = 'Field_ID'
          Col.Heading = 'Field ID'
          Col.Width = 71
          Col.AssignedValues = '?'
        end
        item
          DataCol = 3
          FieldName = 'Field_Label'
          Col.FieldName = 'Field_Label'
          Col.Heading = 'Field Description'
          Col.Width = 268
          Col.AssignedValues = '?'
        end
        item
          DataCol = 4
          FieldName = 'Field_Max_Length'
          Col.FieldName = 'Field_Max_Length'
          Col.Heading = 'Max Length'
          Col.Width = 90
          Col.AssignedValues = '?'
        end>
    end
    object DBNavigator1: TDBNavigator
      Left = 16
      Top = 312
      Width = 560
      Height = 33
      DataSource = dmMain.dsTemplate_Fields
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbEdit, nbPost, nbCancel, nbRefresh]
      TabOrder = 1
    end
  end
  object BitBtn1: TBitBtn
    Left = 258
    Top = 683
    Width = 105
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Kind = bkClose
  end
  object Panel2: TPanel
    Left = 16
    Top = 11
    Width = 593
    Height = 302
    BevelWidth = 2
    TabOrder = 2
    object Label6: TLabel
      Left = 16
      Top = 14
      Width = 84
      Height = 20
      Caption = 'Templates'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 66
      Top = 230
      Width = 37
      Height = 20
      Caption = 'First'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label12: TLabel
      Left = 205
      Top = 230
      Width = 38
      Height = 20
      Caption = 'Prior'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label13: TLabel
      Left = 341
      Top = 231
      Width = 37
      Height = 20
      Caption = 'Next'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label14: TLabel
      Left = 480
      Top = 230
      Width = 36
      Height = 20
      Caption = 'Last'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object tsDBGrid2: TtsDBGrid
      Left = 16
      Top = 40
      Width = 561
      Height = 185
      CellSelectMode = cmNone
      CheckBoxStyle = stCheck
      Cols = 2
      DatasetType = dstStandard
      DataSource = dmMain.dsTemplate_Defs
      DefaultRowHeight = 18
      ExactRowCount = True
      ExportDelimiter = ','
      FieldState = fsCustomized
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HeadingFont.Charset = DEFAULT_CHARSET
      HeadingFont.Color = clWindowText
      HeadingFont.Height = -13
      HeadingFont.Name = 'MS Sans Serif'
      HeadingFont.Style = [fsBold]
      HeadingHeight = 20
      HeadingParentFont = False
      ParentFont = False
      ParentShowHint = False
      RowChangedIndicator = riAutoReset
      RowMoving = False
      ShowHint = False
      TabOrder = 0
      Version = '2.20.26'
      XMLExport.Version = '1.0'
      XMLExport.DataPacketVersion = '2.0'
      OnRowChanged = tsDBGrid2RowChanged
      DataBound = True
      ColProperties = <
        item
          DataCol = 1
          FieldName = 'Template_ID'
          Col.FieldName = 'Template_ID'
          Col.Heading = 'Template ID'
          Col.Width = 94
          Col.AssignedValues = '?'
        end
        item
          DataCol = 2
          FieldName = 'Template_Description'
          Col.FieldName = 'Template_Description'
          Col.Heading = 'Template Description'
          Col.Width = 431
          Col.AssignedValues = '?'
        end>
    end
    object DBNavigator2: TDBNavigator
      Left = 16
      Top = 256
      Width = 552
      Height = 33
      DataSource = dmMain.dsTemplate_Defs
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
      TabOrder = 1
    end
  end
end
