unit Preferences;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, StBase, {StShBase, StBrowsr,} Spin, Mask;

type
  TPrefs = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel2: TPanel;
    Label2: TLabel;
    Edit2: TEdit;
    Panel1: TPanel;
    Label1: TLabel;
    SpinEdit1: TSpinEdit;
    Panel3: TPanel;
    Label3: TLabel;
    Edit1: TEdit;
    Edit3: TEdit;
    Label7: TLabel;
    Label8: TLabel;
    GraphicsEngineEnable: TPanel;
    EngineEnable: TCheckBox;
    EngineIPAddress: TEdit;
    Label4: TLabel;
    Label5: TLabel;
    EnginePort: TEdit;
    Panel4: TPanel;
    ForceUpperCaseCheckbox: TCheckBox;
    RadioGroup1: TRadioGroup;
    ForceUpperCaseGameInfoCheckbox: TCheckBox;
    DefaultToSingleLoopCheckbox: TCheckBox;
    Panel5: TPanel;
    ManualRankings_NCAAB: TCheckBox;
    ManualRankings_NCAAW: TCheckBox;
    ManualRankings_NCAAF: TCheckBox;
    Label6: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Prefs: TPrefs;

implementation

{$R *.DFM}

end.
