object TopBarImageWheelEditDlg: TTopBarImageWheelEditDlg
  Left = 650
  Top = 223
  Width = 1055
  Height = 475
  Caption = 'Top Bar Image Wheel Editor'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 16
    Top = 8
    Width = 1009
    Height = 369
    BevelWidth = 2
    TabOrder = 0
    object Label1: TLabel
      Left = 284
      Top = 320
      Width = 143
      Height = 16
      Caption = 'Dell Time (Seconds)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 52
      Top = 313
      Width = 100
      Height = 16
      Caption = 'Graphic Index '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 52
      Top = 329
      Width = 115
      Height = 16
      Caption = '(Must be unique)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object AddEntryBtn: TBitBtn
      Left = 518
      Top = 314
      Width = 210
      Height = 41
      Caption = 'Add Image to Wheel'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = AddEntryBtnClick
      Glyph.Data = {
        E6000000424DE60000000000000076000000280000000E0000000E0000000100
        0400000000007000000000000000000000001000000000000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3300333333333333330033333333333333003333300033333300333330F03333
        3300333330F033333300330000F000033300330FFFFFFF033300330000F00003
        3300333330F033333300333330F0333333003333300033333300333333333333
        33003333333333333300}
    end
    object DeleteEntryBtn: TBitBtn
      Left = 782
      Top = 314
      Width = 210
      Height = 41
      Cancel = True
      Caption = 'Delete Image from Wheel'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = DeleteEntryBtnClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333333333000033338833333333333333333F333333333333
        0000333911833333983333333388F333333F3333000033391118333911833333
        38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
        911118111118333338F3338F833338F3000033333911111111833333338F3338
        3333F8330000333333911111183333333338F333333F83330000333333311111
        8333333333338F3333383333000033333339111183333333333338F333833333
        00003333339111118333333333333833338F3333000033333911181118333333
        33338333338F333300003333911183911183333333383338F338F33300003333
        9118333911183333338F33838F338F33000033333913333391113333338FF833
        38F338F300003333333333333919333333388333338FFF830000333333333333
        3333333333333333333888330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object DwellTime: TSpinEdit
      Left = 432
      Top = 315
      Width = 60
      Height = 26
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxValue = 300
      MinValue = 2
      ParentFont = False
      TabOrder = 2
      Value = 5
    end
    object TeamsGrid: TtsDBGrid
      Left = 15
      Top = 16
      Width = 978
      Height = 289
      CellSelectMode = cmNone
      CheckBoxStyle = stCheck
      Cols = 3
      DatasetType = dstStandard
      DataSource = dmMain.dsTop_Region_Image_Wheel
      DefaultRowHeight = 18
      ExactRowCount = True
      ExportDelimiter = ','
      FieldState = fsCustomized
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      GridMode = gmListBox
      HeadingFont.Charset = DEFAULT_CHARSET
      HeadingFont.Color = clWindowText
      HeadingFont.Height = -13
      HeadingFont.Name = 'MS Sans Serif'
      HeadingFont.Style = [fsBold]
      HeadingHeight = 20
      HeadingParentFont = False
      ParentFont = False
      ParentShowHint = False
      RowChangedIndicator = riAutoReset
      RowMoving = False
      ShowHint = False
      TabOrder = 3
      Version = '2.20.26'
      XMLExport.Version = '1.0'
      XMLExport.DataPacketVersion = '2.0'
      DataBound = True
      ColProperties = <
        item
          DataCol = 1
          FieldName = 'GraphicIndex'
          Col.FieldName = 'GraphicIndex'
          Col.Heading = 'Index'
          Col.WordWrap = wwOn
          Col.AssignedValues = '?'
        end
        item
          DataCol = 2
          FieldName = 'DwellTime'
          Col.FieldName = 'DwellTime'
          Col.Heading = 'Dwell (secs)'
          Col.Width = 94
          Col.AssignedValues = '?'
        end
        item
          DataCol = 3
          FieldName = 'GraphicFilePath'
          Col.FieldName = 'GraphicFilePath'
          Col.Heading = 'Graphic File path'
          Col.Width = 1363
          Col.WordWrap = wwOn
          Col.AssignedValues = '?'
        end>
    end
    object GraphicIndex: TSpinEdit
      Left = 200
      Top = 315
      Width = 60
      Height = 26
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxValue = 25
      MinValue = 1
      ParentFont = False
      TabOrder = 4
      Value = 1
    end
  end
  object BitBtn3: TBitBtn
    Left = 463
    Top = 384
    Width = 113
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Kind = bkClose
  end
  object OpenPictureDialog: TOpenPictureDialog
    Filter = 
      'All (*.tga;*.tif;*.png,*)|*.tga;*.tif|TGA Format (*.tga)|*.tga|T' +
      'IF Format (*.tif)|*.tif|PNG Format (*.png)|*.png'
    InitialDir = 
      'C:\VDS_Projects\Comcast_2010\CSN_STATS_INC_INTEGRATION\CSN Phase' +
      ' 2 Release\CSN Ticker Engine\Images\sponsors'
    Options = [ofReadOnly, ofHideReadOnly, ofEnableSizing]
    Title = 'Image File Selector'
    Left = 448
    Top = 16
  end
end
