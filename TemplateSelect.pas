unit TemplateSelect;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, CheckLst, ExtCtrls, Buttons, Globals;

type
  TTemplateSelectDlg = class(TForm)
    Panel1: TPanel;
    TemplateCheckListbox: TCheckListBox;
    OkBtn: TBitBtn;
    CancelBtn: TBitBtn;
    BitBtn1: TBitBtn;
    procedure TemplateCheckListboxClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TemplateSelectDlg: TTemplateSelectDlg;

implementation

{$R *.dfm}

//Handler for change in checkmark status
procedure TTemplateSelectDlg.TemplateCheckListboxClick(Sender: TObject);
var
  i: SmallInt;
  SelectedTemplatesRecPtr: ^DefaultTemplatesRec;
begin
  if (Selected_Templates_Collection.Count > 0) then
  begin
    for i := 0 to Selected_Templates_Collection.Count-1 do
    begin
      SelectedTemplatesRecPtr := Selected_Templates_Collection.At(i);
      if (TemplateCheckListbox.Checked[i]) then
        SelectedTemplatesRecPtr^.Enabled := TRUE
      else
        SelectedTemplatesRecPtr^.Enabled := FALSE;
    end;
  end;
end;

end.
