unit EnableDateTimeEditor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Spin, ExtCtrls, ComCtrls;

type
  TEnableDateTimeEditorDlg = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label41: TLabel;
    EntryStartEnableDate: TDateTimePicker;
    EntryStartEnableTime: TDateTimePicker;
    Label42: TLabel;
    EntryEndEnableDate: TDateTimePicker;
    EntryEndEnableTime: TDateTimePicker;
    Label38: TLabel;
    EntryNote: TEdit;
    NoteChangeEnable: TCheckBox;
    DwellTimeEnable: TCheckBox;
    Label7: TLabel;
    EntryDwellTime: TSpinEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EnableDateTimeEditorDlg: TEnableDateTimeEditorDlg;

implementation

{$R *.dfm}

end.
