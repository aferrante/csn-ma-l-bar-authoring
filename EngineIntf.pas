// Rev: 02/04/10  M. Dilworth  Video Design Software Inc.
// Modified to use Stats Inc. as data source
unit EngineIntf;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  OoMisc, ADTrmEmu, AdPacket, AdPort, AdWnPort, ExtCtrls, Globals, FileCtrl,
  ScktComp;

type
  TEngineInterface = class(TForm)
    AdTerminal1: TAdTerminal;
    PacketEnableTimer: TTimer;
    TickerCommandDelayTimer: TTimer;
    PacketIndicatorTimer: TTimer;
    TickerPacketTimeoutTimer: TTimer;
    JumpToNextTickerRecordTimer: TTimer;
    BugCommandDelayTimer: TTimer;
    ExtraLineCommandDelayTimer: TTimer;
    JumpToNextBugRecordTimer: TTimer;
    JumpToNextExtraLineRecordTimer: TTimer;
    BugPacketTimeoutTimer: TTimer;
    ExtraLinePacketTimeoutTimer: TTimer;
    EnginePort: TClientSocket;
    TopGraphicDwellTimer: TTimer;
    procedure FormActivate(Sender: TObject);
    //Timer related
    procedure PacketEnableTimerTimer(Sender: TObject);
    procedure TickerCommandDelayTimerTimer(Sender: TObject);
    procedure PacketIndicatorTimerTimer(Sender: TObject);
    procedure TickerPacketTimeoutTimerTimer(Sender: TObject);
    procedure JumpToNextTickerRecordTimerTimer(Sender: TObject);
    //Engine connection port
    procedure EnginePortConnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure EnginePortDisconnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure EnginePortError(Sender: TObject; Socket: TCustomWinSocket; ErrorEvent: TErrorEvent; var ErrorCode: Integer);
    procedure EnginePortRead(Sender: TObject; Socket: TCustomWinSocket);
    //Logging
    procedure WriteToErrorLog (ErrorString: String);
    procedure WriteToAsRunLog (AsRunString: String);
    procedure TopGraphicDwellTimerTimer(Sender: TObject);
  private
    function ProcessStyleChips(CmdStr: String): String;
  public
    procedure SendTickerRecord(PlaylistID: SmallInt; NextRecord: Boolean; RecordIndex: SmallInt);

    function GetLineupText(CurrentTickerEntryIndex: SmallInt; TickerMode: SmallInt): LineupText;
    function GetMixedCase(InStr: String): String;
    function GetSponsorLogoFileName(LogoName: String): String;
    function LoadTempTemplateFields(TemplateID: SmallInt): TemplateDefsRec;

    function CheckForActiveSponsorLogo: Boolean;
    function GetAlternateTemplateID(CurrentTemplateID: SmallInt): SmallInt;

    //For Channel Box
    procedure SendCommand(CommandString: String);
    procedure OpenScene(SceneID: LongInt; SendParams: Boolean; Params: ANSIString);
    procedure LoadScene(SceneID: LongInt; SendParams: Boolean; Params: ANSIString);
    procedure PlayScene(SceneID: LongInt);
    procedure StopScene(SceneID: LongInt);
    procedure CloseScene(SceneID: LongInt);
    procedure UpdateScene(SceneID: LongInt; Params: ANSIString);
    procedure QuerySceneStatus(SceneID: LongInt);
    procedure SetupInitialScenes;
    procedure SetClockState(ClockIn: Boolean);

    //Version 2.0 Functions for top graphic region
    procedure RefreshTopGraphicCollection;
    procedure SetNextTopGraphic;

  end;

const
  SingleLineWinningLarge: String[1] = #180;
  SingleLineLosingLarge: String[1]  = #181;
  SingleLineWinningSmall: String[1]  = #182;
  SingleLineLosingSmall: String[1]  = #183;

var
  EngineInterface: TEngineInterface;
  DisableCommandTimer: Boolean;

const
 UseDataPacket = TRUE;

implementation

uses Main, //Main form
     DataModule, //Data module
     GameDataFunctions;

{$R *.DFM}

//Init
procedure TEngineInterface.FormActivate(Sender: TObject);
begin
end;

////////////////////////////////////////////////////////////////////////////////
// CHANNEL BOX SPECIFIC FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Procedure to send a command to the Playout Controller application
procedure TEngineInterface.SendCommand(CommandString: String);
begin
  EnginePort.Socket.SendText(CommandString + #13#10);
end;

//Open the specified scene
procedure TEngineInterface.OpenScene(SceneID: LongInt; SendParams: Boolean; Params: ANSIString);
var
  CmdStr: String;
begin
  CmdStr := 'B\OP\' + IntToStr(SceneId);
  if (SendParams) then CmdStr := CmdStr + '\' + Params;
  CmdStr := CmdStr + '\\';
  SendCommand(CmdStr);
end;

//Load the specified scene
procedure TEngineInterface.LoadScene(SceneID: LongInt; SendParams: Boolean; Params: ANSIString);
var
  CmdStr: String;
begin
  CmdStr := 'B\LO\' + IntToStr(SceneId);
  if (SendParams) then CmdStr := CmdStr + '\' + Params;
  CmdStr := CmdStr + '\\';
  SendCommand(CmdStr);
end;

//Play the specified scene
procedure TEngineInterface.PlayScene(SceneID: LongInt);
var
  CmdStr: String;
begin
  CmdStr := 'B\PL\' + IntToStr(SceneId) + '\\';
  SendCommand(CmdStr);
end;

//Stop the specified scene
procedure TEngineInterface.StopScene(SceneID: LongInt);
var
  CmdStr: String;
begin
  CmdStr := 'B\ST\' + IntToStr(SceneId) + '\\';
  SendCommand(CmdStr);
end;

//Close the specified scene
procedure TEngineInterface.CloseScene(SceneID: LongInt);
var
  CmdStr: String;
begin
  CmdStr := 'B\CL\' + IntToStr(SceneId) + '\\';
  SendCommand(CmdStr);
end;

//Update the specified scene with the specified name/value pairs
procedure TEngineInterface.UpdateScene(SceneID: LongInt; Params: ANSIString);
var
  CmdStr: String;
begin
  CmdStr := 'B\UP\' + IntToStr(SceneId) + '\' + Params + '\\';
  SendCommand(CmdStr);
end;

//Query the status of the specified scene
procedure TEngineInterface.QuerySceneStatus(SceneID: LongInt);
var
  CmdStr: String;
begin
  CmdStr := 'B\QS\' + IntToStr(SceneId) + '\\';
  SendCommand(CmdStr);
end;

//Setup the initial scenes - Load & Play
procedure TEngineInterface.SetupInitialScenes;
var
  i, j: LongInt;
  SceneRecPtr: ^SceneRec;
begin
  //Set status on main form
  MainForm.StatusBar.Color := clYellow;
  MainForm.StatusBar.SimpleText := ' PLEASE WAIT WHILE THE CHANNEL BOX SCENES ARE LOADED.';

  if (Scene_Collection.Count > 0) then
  begin
    //Load the scenes
    for i := 0 to Scene_Collection.Count-1 do
    begin
      //Get status & load scene if needed
      SceneRecPtr := Scene_Collection.At(i);
      //If seen not stopped, loaded or playing, load it
      if (SceneRecPtr^.Load_At_Startup) and (SceneRecPtr^.Scene_Status <> SCENE_STOPPED) and
         (SceneRecPtr^.Scene_Status <> SCENE_LOADED) and (SceneRecPtr^.Scene_Status <> SCENE_PLAYING) then
      begin
        LoadScene(SceneRecPtr^.Scene_ID, FALSE, '');
        MainForm.StatusBar.SimpleText := ' LOADING SCENE ID: ' + IntToStr(SceneRecPtr^.Scene_ID);
        //Wait for scene to be loaded
        j := 0;
        Repeat
          //for j := 1 to 5 do
          //begin
            Sleep(500);
            Application.ProcessMessages;
            QuerySceneStatus(SceneRecPtr^.Scene_ID);
          //end;
          inc(j);
        //Timeout set for 40*500mS = 20 seconds
        Until (SceneRecPtr^.Scene_Status = SCENE_LOADED) or (j >= 40);
        //Check for timeout
        if (j >= 40) then
        begin
          MainForm.StatusBar.Color := clRed;
          MainForm.StatusBar.SimpleText := ' TIMEOUT OCCURRED WHILE TRYING TO LOAD SCENE ID: ' + IntToStr(SceneRecPtr^.Scene_ID);
          Exit;
        end;
      end;
    end;

    //Delay
    for j := 1 to 20 do
    begin
      Sleep(100);
      Application.ProcessMessages;
    end;

    //Play the scenes
    for i := 0 to Scene_Collection.Count-1 do
    begin
      //Get status ad play scene if needed
      SceneRecPtr := Scene_Collection.At(i);
      QuerySceneStatus(SceneRecPtr^.Scene_ID);

      //If seen not playing, loaded or playing, load it
      if (SceneRecPtr^.Load_At_Startup) and (SceneRecPtr^.Play_At_Startup = TRUE) and ((SceneRecPtr^.Scene_Status <> SCENE_PLAYING) or (AlwaysPlayAllScenesAtStartup)) then
      begin
        PlayScene(SceneRecPtr^.Scene_ID);
        MainForm.StatusBar.SimpleText := ' PLAYING SCENE ID: ' + IntToStr(SceneRecPtr^.Scene_ID);
        //Wait for scene to be loaded
        j := 0;
        Repeat
          //for j := 1 to 10 do
          //begin
            Sleep(100);
            Application.ProcessMessages;
            QuerySceneStatus(SceneRecPtr^.Scene_ID);
          //end;
          inc (j);
        //Timeout set for 100*100mS = 10 seconds
        Until (SceneRecPtr^.Scene_Status = SCENE_PLAYING) or (j >= 100);
        //Check for timeout
        if (j >= 100) then
        begin
          MainForm.StatusBar.Color := clRed;
          MainForm.StatusBar.SimpleText := ' TIMEOUT OCCURRED WHILE TRYING TO PLAY SCENE ID: ' + IntToStr(SceneRecPtr^.Scene_ID);
          Exit;
        end;
      end

      //If seen not flagged to play at startup, stop it
      else if (SceneRecPtr^.Load_At_Startup) and (SceneRecPtr^.Play_At_Startup = FALSE) and ((SceneRecPtr^.Scene_Status = SCENE_PLAYING) or (AlwaysPlayAllScenesAtStartup)) then
      begin
        StopScene(SceneRecPtr^.Scene_ID);
        MainForm.StatusBar.SimpleText := ' STOPPING SCENE ID: ' + IntToStr(SceneRecPtr^.Scene_ID);
        //Wait for scene to be loaded
        j := 0;
        Repeat
          //for j := 1 to 10 do
          //begin
            Sleep(100);
            Application.ProcessMessages;
            QuerySceneStatus(SceneRecPtr^.Scene_ID);
          //end;
          inc (j);
        //Timeout set for 100*100mS = 10 seconds
        Until (SceneRecPtr^.Scene_Status = SCENE_STOPPED) or (SceneRecPtr^.Scene_Status = SCENE_LOADED) or (j >= 100);
        //Check for timeout
        if (j >= 100) then
        begin
          MainForm.StatusBar.Color := clRed;
          MainForm.StatusBar.SimpleText := ' TIMEOUT OCCURRED WHILE TRYING TO STOP SCENE ID: ' + IntToStr(SceneRecPtr^.Scene_ID);
          Exit;
        end;
      end;
    end;

    //Delay
    for j := 1 to 20 do
    begin
      Sleep(100);
      Application.ProcessMessages;
    end;

    //Send any initial commands
    for i := 0 to Scene_Collection.Count-1 do
    begin
      SceneRecPtr := Scene_Collection.At(i);
      if (SceneRecPtr^.Run_Startup_Command = TRUE) then
      begin
        UpdateScene(SceneRecPtr^.Scene_ID, SceneRecPtr^.Startup_Command);
      end;
    end;
  end;

  //Set status on main form
  MainForm.StatusBar.Color := clBtnFace;
  MainForm.StatusBar.SimpleText := ' ';

  //Set flag
  //InitialSceneLoadComplete := TRUE;
end;

//These are the graphics engine control procedures
////////////////////////////////////////////////////////////////////////////////
// UTILITY FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Function to get a parsed token from a delited string
function GetItem(TokenNo: Integer; Data: string; Delimiter: Char = '|'): string;
var
  Token: string;
  StrLen, TEnd, TNum: Integer;
begin
  StrLen := Length(Data);
  //Init
  TNum := 1;
  TEnd := StrLen;
  //Walk through string and parse for delimiter
  while ((TNum <= TokenNo) and (TEnd <> 0)) do
  begin
    TEnd := Pos(Delimiter, Data);
    //Not last value
    if TEnd <> 0 then
    begin
      //Copy & delete data from input string; increment token counter
      Token := Copy(Data, 1, TEnd - 1);
      Delete(Data, 1, TEnd);
      Inc(TNum);
    end
    //Last value
    else begin
      Token := Data;
    end;
  end;
  //Return data
  if TNum >= TokenNo then
  begin
    Result := Token;
  end
  //Return error code
  else begin
    Result := #27;
  end;
end;

//Function to do style chip substitutions
function TEngineInterface.ProcessStyleChips(CmdStr: String): String;
var
  i,j: SmallInt;
  StyleChipRecPtr: ^StyleChipRec;
  OutStr: String;
  SwitchPos: SmallInt;
begin
  //Check for presence of style chips
  //NOTE: Conversion to mixed case/upper case done in function to get field contents
  OutStr := CmdStr;
  if (StyleChip_Collection.Count > 0) AND (Length(CmdStr) >= 4) then
  begin
    for i := 0 to StyleChip_Collection.Count-1 do
    begin
      StyleChipRecPtr := StyleChip_Collection.At(i);
      //Check style chip type and substiture accordingly
      //Type 1 = Use substitution string
      if (StyleChipRecPtr^.StyleChip_Type = 1) then
        OutStr := StringReplace(OutStr, StyleChipRecPtr^.StyleChip_Code,
                  StyleChipRecPtr^.StyleChip_String, [rfReplaceAll])
      //Type 2 = Use substitution font & character
      else if (StyleChipRecPtr^.StyleChip_Type = 2) then
        OutStr := StringReplace(OutStr, StyleChipRecPtr^.StyleChip_Code,
                  '[f ' + IntToStr(StyleChipRecPtr^.StyleChip_FontCode) + ']' +
                  Char(StyleChipRecPtr^.StyleChip_CharacterCode), [rfReplaceAll])
    end;
  end;
  ProcessStyleChips := OutStr;
end;

// Function to take upper case string & return mixed-case string
function TEngineInterface.GetMixedCase(InStr: String): String;
var
  i: SmallInt;
  OutStr: String;
begin
  OutStr := ANSILowerCase(InStr);
  if (Length(OutStr) > 0) then
  begin
    //Set first character to upper case if it's not a digit
    if (Ord(OutStr[1]) < 48) OR (Ord(OutStr[1]) > 57) then  OutStr[1] := Char(Ord(OutStr[1])-32);
    //Check if two words in state name, and set 2nd word to upper case
    if (Length(OutStr) > 2) then
    begin
      for i := 2 to Length(OutStr) do
      begin
        if (OutStr[i-1] = ' ') then OutStr[i] := Char(Ord(OutStr[i])-32);
      end;
    end;
  end;
  GetMixedCase := OutStr;
end;

//Function to get sponsor logo file name based on logo description
function TEngineInterface.GetSponsorLogoFilename(LogoName: String): String;
var
  i: SmallInt;
  OutStr: String;
  SponsorLogoPtr: ^SponsorLogoRec;
begin
  for i := 0 to SponsorLogo_Collection.Count-1 do
  begin
    SponsorLogoPtr := SponsorLogo_Collection.At(i);
    if (SponsorLogoPtr^.SponsorLogoName = LogoName) then
    begin
      OutStr := SponsorLogoPtr^.SponsorLogoFileName;
    end;
  end;
  GetSponsorLogoFilename := OutStr;
end;

//Procedure to load temporary collection for fields (for current template); also returns
//associated template information
function TEngineInterface.LoadTempTemplateFields(TemplateID: SmallInt): TemplateDefsRec;
var
  i: SmallInt;
  TemplateFieldsRecPtr, NewTemplateFieldsRecPtr: ^TemplateFieldsRec;
  TemplateRecPtr: ^TemplateDefsRec;
  OutRec: TemplateDefsRec;
  FoundMatch: Boolean;
begin
  //Clear the existing collection
  Temporary_Fields_Collection.Clear;
  Temporary_Fields_Collection.Pack;
  //First, get the engine template ID
  if (Template_Defs_Collection.Count > 0) then
  begin
    i := 0;
    FoundMatch := FALSE;
    repeat
      TemplateRecPtr := Template_Defs_Collection.At(i);
      if (TemplateID = TemplateRecPtr^.Template_ID) then
      begin
        OutRec.Template_ID := TemplateRecPtr^.Template_ID;
        OutRec.Template_Type := TemplateRecPtr^.Template_Type;
        OutRec.Template_Description := TemplateRecPtr^.Template_Description;
        OutRec.Record_Type := TemplateRecPtr^.Record_Type;
        OutRec.TemplateSponsorType := TemplateRecPtr^.TemplateSponsorType;
        OutRec.Scene_ID_1 := TemplateRecPtr^.Scene_ID_1;
        OutRec.Scene_ID_2 := TemplateRecPtr^.Scene_ID_2;
        OutRec.Transition_In := TemplateRecPtr^.Transition_In;
        OutRec.Transition_Out := TemplateRecPtr^.Transition_Out;
        OutRec.Default_Dwell := TemplateRecPtr^.Default_Dwell;
        OutRec.UsesGameData := TemplateRecPtr^.UsesGameData;
        OutRec.RequiredGameState := TemplateRecPtr^.RequiredGameState;
        OutRec.Use_Alert_Background := TemplateRecPtr^.Use_Alert_Background;
        FoundMatch := TRUE;
      end;
      Inc(i);
    until (FoundMatch) OR (i = Template_Defs_Collection.Count);
  end;
  //Now, get the fields for the template
  for i := 0 to Template_Fields_Collection.Count-1 do
  begin
    TemplateFieldsRecPtr := Template_Fields_Collection.At(i);
    if (TemplateFieldsRecPtr^.Template_ID = TemplateID) then
    begin
      GetMem (NewTemplateFieldsRecPtr, SizeOf(TemplateFieldsRec));
      With NewTemplateFieldsRecPtr^ do
      begin
        Template_ID := TemplateFieldsRecPtr^.Template_ID;
        Field_ID := TemplateFieldsRecPtr^.Field_ID;
        Field_Type := TemplateFieldsRecPtr^.Field_Type;
        Field_Is_Manual := TemplateFieldsRecPtr^.Field_Is_Manual;
        Field_Label := TemplateFieldsRecPtr^.Field_Label;
        Field_Contents := TemplateFieldsRecPtr^.Field_Contents;
        Field_Format_Prefix := TemplateFieldsRecPtr^.Field_Format_Prefix;
        Field_Format_Suffix := TemplateFieldsRecPtr^.Field_Format_Suffix;
        Field_Max_Length := TemplateFieldsRecPtr^.Field_Max_Length;
        Scene_Field_Name := TemplateFieldsRecPtr^.Scene_Field_Name;
      end;
      if (Temporary_Fields_Collection.Count <= 100) then
      begin
        //Add to collection
        Temporary_Fields_Collection.Insert(NewTemplateFieldsRecPtr);
        Temporary_Fields_Collection.Pack;
      end;
    end;
  end;
  LoadTempTemplateFields := OutRec;
end;

//Utility function to strip off any ASCII characters above ASCII 127
function StripPrefix(InStr: String): String;
var
  i: SmallInt;
  OutStr: String;
begin
  OutStr := '';
  if (Length(InStr) > 0) then
  begin
    for i := 1 to Length(InStr) do
    begin
      if (Ord(InStr[i]) <=127) then OutStr := OutStr + InStr[i];
    end;
  end;
  StripPrefix := OutStr;
end;


//FOR ODDS PAGES
const
  DELIMITER:STRING = Chr(160);

////////////////////////////////////////////////////////////////////////////////
// ENGINE COMMAND STRING FORMATTING FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Function to get & return text strings to be used for line-ups
function TEngineInterface.GetLineupText(CurrentTickerEntryIndex: SmallInt; TickerMode: SmallInt): LineupText;
var
  i,j: SmallInt;
  OutRec: LineupText;
  TickerRecPtrCurrent, TickerRecPtrPrevious,TickerRecPtrNext: ^TickerRec;
  PreviousEntryIndex, NextEntryIndex: SmallInt;
  CurrentEntryLeague, PreviousEntryLeague, NextEntryLeague: String;
  HeaderIndex, CollectionItemCounter: SmallInt;
  OneLeagueOnly: Boolean;
  FirstLeague: String;
  CurrentLineupTickerEntryIndex: SmallInt;
begin
  //Always set flag to do line-up
  OutRec.UseLineup := TRUE;

  //Get text for line-ups
  if (OutRec.UseLineup = TRUE) AND (CurrentTickerEntryIndex <> NOENTRYINDEX) then
  begin
    //Init
    HeaderIndex := 1;
    CollectionItemCounter := 1;

    //Get current entry types
    CurrentLineupTickerEntryIndex := CurrentTickerEntryIndex;
    TickerRecPtrCurrent := Ticker_Collection.At(CurrentLineupTickerEntryIndex);

    //Make sure record is enabled and within time window
    j := 0;
    While ((TickerRecPtrCurrent^.Enabled = FALSE) OR (TickerRecPtrCurrent^.StartEnableDateTime > Now) OR
          (TickerRecPtrCurrent^.EndEnableDateTime <= Now) OR (TickerRecPtrCurrent^.League = 'NONE')) AND
          (j < Ticker_Collection.Count) do
    begin
      Inc(CurrentLineupTickerEntryIndex);
      if (CurrentLineupTickerEntryIndex = Ticker_Collection.Count) then
      begin
        CurrentLineupTickerEntryIndex := 0;
      end;
      TickerRecPtrCurrent := Ticker_Collection.At(CurrentLineupTickerEntryIndex);
      Inc(j);
    end;

    //Special case to just show MLB for all MLB leagues
    CurrentEntryLeague :=  TickerRecPtrCurrent^.League;
    if (CurrentEntryLeague = 'AL') OR (CurrentEntryLeague = 'NL') OR (CurrentEntryLeague = 'ML') OR (CurrentEntryLeague = 'IL') then
      CurrentEntryLeague := 'MLB';

    NextEntryIndex := CurrentLineupTickerEntryIndex+1;
    //Get type of next entry
    if (NextEntryIndex = Ticker_Collection.Count) then NextEntryIndex := 0;

    //Set first entry
    OutRec.TextFields[HeaderIndex] := CurrentEntryLeague;

    //Get remaining entries
    repeat
      TickerRecPtrNext := Ticker_Collection.At(NextEntryIndex);
      NextEntryLeague := TickerRecPtrNext^.League;
      if (NextEntryLeague = 'AL') OR (NextEntryLeague = 'NL') OR (NextEntryLeague = 'ML') OR (NextEntryLeague = 'IL') then
        NextEntryLeague := 'MLB';

      //If new league or child to prior parent, set text field; otherwise, continue iterating through
      if (NextEntryLeague <> CurrentEntryLeague) AND
         (TickerRecPtrNext^.Enabled = TRUE) AND (TickerRecPtrNext^.StartEnableDateTime <= Now) AND
         (TickerRecPtrNext^.EndEnableDateTime > Now) AND (j < Ticker_Collection.Count) AND
         (TickerRecPtrNext^.League <> 'NONE') then
      begin
        Inc(HeaderIndex);
        //Use parent league for field and clear flag
        OutRec.TextFields[HeaderIndex] := NextEntryLeague;
        CurrentEntryLeague := NextEntryLeague;
      end;
      if (NextEntryIndex = Ticker_Collection.Count-1) then NextEntryIndex := 0
      else Inc(NextEntryIndex);
      Inc(CollectionItemCounter);
    until (HeaderIndex = 4) OR (CollectionItemCounter = Ticker_Collection.Count*3);

    //Repeat fields if not enough after iterating
    if (HeaderIndex = 1) then
    begin
      OutRec.TextFields[2] :=  OutRec.TextFields[1];
      OutRec.TextFields[3] :=  OutRec.TextFields[1];
      OutRec.TextFields[4] :=  OutRec.TextFields[1];
    end
    else if (HeaderIndex = 2) then
    begin
      OutRec.TextFields[3] :=  OutRec.TextFields[1];
      OutRec.TextFields[4] :=  OutRec.TextFields[2];
    end
    else if (HeaderIndex = 3) then
    begin
      OutRec.TextFields[4] :=  OutRec.TextFields[1];
    end;
  end
  else
    for i := 1 to 4 do OutRec.TextFields[i] := ' ';
  //Return
  for i := 1 to 4 do if (OutRec.TextFields[i] = '') then OutRec.TextFields[i] := ' ';
  GetLineupText := OutRec;
end;

////////////////////////////////////////////////////////////////////////////////
// ENGINE CONTROL FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//V1.2 Procedure to set the state of the clock at the top of the L-Bar
procedure TEngineInterface.SetClockState(ClockIn: Boolean);
begin
  if (SocketConnected) then
  begin
    //Clock is currently in so bring it out
    if (ClockIn = CLOCK_OUT) then
    begin
      //Clear graphic with clock
      if (TopRegionToggleState = 1) then
      begin
        //Trigger current graphic #1 out
        UpdateScene(BaseScene_SceneID, 'Image_1_Out_Btn' + '�' + 'T');
      end
      else begin
        //Trigger current graphic #2 out
        UpdateScene(BaseScene_SceneID, 'Image_2_Out_Btn' + '�' + 'T');
      end;
      //Take out logo only & bring clock in
      UpdateScene(BaseScene_SceneID, 'Clock_Logo_Out_Btn�T');
      ClockIsIn := FALSE;
    end
    //Clock is currently out so bring it in
    else if (ClockIn = CLOCK_IN) then
    begin
      //Clear graphic with clock
      if (TopRegionToggleState = 1) then
      begin
        //Trigger current graphic #1 out
        UpdateScene(BaseScene_SceneID, 'Mktg_1_No_Clock_Out_Btn' + '�' + 'T');
      end
      else begin
        //Trigger current graphic #2 out
        UpdateScene(BaseScene_SceneID, 'Mktg_2_No_Clock_Out_Btn' + '�' + 'T');
      end;
      //Take out clock + logo & bring logo only in
      UpdateScene(BaseScene_SceneID, 'Clock_Logo_In_Btn�T');
      ClockIsIn := TRUE;
    end;
    //Set flag to prevent double out transition
    ClockStateChanged := TRUE;
  end;
end;

//Function to take a template ID and return the alternate template ID for 1-line vs.
//2-line mode
function TEngineInterface.GetAlternateTemplateID(CurrentTemplateID: SmallInt): SmallInt;
var
  i: SmallInt;
  TemplateDefsRecPtr: ^TemplateDefsRec;
  OutVal: SmallInt;
begin
  OutVal := -1;
  if (Template_Defs_Collection.Count > 0) then
  begin
    //Walk through templates to find alternate mode ID
    for i := 0 to Template_Defs_Collection.Count-1 do
    begin
      TemplateDefsRecPtr := Template_Defs_Collection.At(i);
      //Check for match between ticker element template ID and entry in template defs
      if (TemplateDefsRecPtr^.Template_ID = CurrentTemplateID) then
      begin
        //Alternate template ID found, so re-assign template ID
        if (TemplateDefsRecPtr^.AlternateModeTemplateID > 0) then
        begin
          OutVal := TemplateDefsRecPtr^.AlternateModeTemplateID;
        end;
      end;
    end;
  end;
  GetAlternateTemplateID := OutVal;
end;

////////////////////////////////////////////////////////////////////////////////
// TICKER CONTROL FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//FUNCTIONS FOR ADVANCING RECORDS
//Handler for packet requesting more data for ticker
procedure TEngineInterface.EnginePortRead(Sender: TObject; Socket: TCustomWinSocket);
var
  i: SmallInt;
  SceneRecPtr:^SceneRec;
  CurrentTemplateIDNum: SmallInt;
  CurrentTemplateIDNumStr: String;
  RequiredDelay: SmallInt;
  Data: String;
  HeaderStrPos: SmallInt;
  SceneStatus: SmallInt;
  CommandStr: String;
  SceneID: LongInt;
  SceneIDStr: String;
  AllCommandsProcessed: Boolean;
  EOTLocation: SmallInt;
begin
  AllCommandsProcessed := FALSE;

  //Get data
  Data := Trim(Socket.ReceiveText);

  //Parse out status if scene status info
  HeaderStrPos := Pos('B\QS\', Data);
  if (HeaderStrPos > 0) and (Pos(EOT_CB, Data) > 0) then
  repeat
    SceneIDStr := '';
    //Find start of command
    HeaderStrPos := Pos('B\QS\', Data);
    //Find end of command
    EOTLocation := Pos(EOT_CB, Data);
    //Copy from start to end of command & parse
    CommandStr := Trim(Copy(Data, HeaderStrPos, EOTLocation+1));

    SceneIDStr := '';
    i := 6;
    if (HeaderStrPos > 0) and (Length(CommandStr) > 5) then
    repeat
      SceneIDStr := SceneIDStr + CommandStr[i];
      inc(i);
    until (CommandStr[i] = CB_DELIMITER) or (CommandStr[i] = '\') or (i > Length(CommandStr));
    if (Length(SceneIDStr) > 0) then SceneID := StrToIntDef(SceneIDStr, 0);

    //Update status if valid scene ID
    if (SceneID > 0) and (Scene_Collection.Count > 0) then
    begin
      //Get status
      if (Pos('Opened', CommandStr) > 0) then SceneStatus := SCENE_OPENED
      else if (Pos('Loaded', CommandStr) > 0) then SceneStatus := SCENE_LOADED
      else if (Pos('Loading', CommandStr) > 0) then SceneStatus := SCENE_LOADING
      else if (Pos('Playing', CommandStr) > 0) then SceneStatus := SCENE_PLAYING
      else if (Pos('Stopped', CommandStr) > 0) then SceneStatus := SCENE_STOPPED
      else if (Pos('Closed', CommandStr) > 0) then SceneStatus := SCENE_CLOSED
      else SceneStatus := SCENE_NONEXISTENT;

      //Lookup scene in collection
      for i := 0 to Scene_Collection.Count-1 do
      begin
        SceneRecPtr := Scene_Collection.At(i);
        if (SceneRecPtr^.Scene_ID = SceneID) then
          SceneRecPtr^.Scene_Status := SceneStatus;
      end;
    end;

    //Delete command from data string for next iteration
    if (Pos(EOT_CB, Data) > 0) and (EOTLocation+1 < Length(Data)) then
      Data := Copy(Data, EOTLocation+2, Length(Data))
    else
      AllCommandsProcessed := TRUE;
  until (AllCommandsProcessed);

  //Handling for normal events - next ticker record
  if (Trim(Data) = '*') then
  begin
    if (RunningTicker = TRUE) AND (DisableCommandTimer = FALSE) then
    begin
      if (USEDATAPACKET) then TickerCommandDelayTimer.Enabled := TRUE;
      TickerPacketTimeoutTimer.Enabled := FALSE;
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// DISPLAY DWELL TIMERS
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// TICKER
////////////////////////////////////////////////////////////////////////////////
//Handler for timer to send next command
procedure TEngineInterface.TickerCommandDelayTimerTimer(Sender: TObject);
begin
  if (Ticker_Collection.Count > 0) then
  begin
    //Prevent retrigger
    TickerCommandDelayTimer.Enabled := FALSE;
    //Send next record
    SendTickerRecord(PLAYLIST_1, TRUE, 0);
  end;
end;
//Handler for 1 mS timer used to send next command when jumping over stats headers
procedure TEngineInterface.JumpToNextTickerRecordTimerTimer(Sender: TObject);
begin
  if (Ticker_Collection.Count > 0) then
  begin
    //Prevent retrigger
    JumpToNextTickerRecordTimer.Enabled := FALSE;
    //Send next record
    SendTickerRecord(PLAYLIST_1, TRUE, 0);
  end;
end;

//Function to check the ticker collection for any sponsor logo with a valid time window
function TEngineInterface.CheckForActiveSponsorLogo: Boolean;
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  OutVal: Boolean;
begin
  //Init
  OutVal := FALSE;
  //Check collection for any sponsor logo with a valid time window
  if (Ticker_Collection.Count > 0) then
  begin
    for i := 0 to Ticker_Collection.Count-1 do
    begin
      TickerRecPtr := Ticker_Collection.At(i);
      if ((TickerRecPtr^.Template_ID = ONE_LINE_SPONSOR_TAGLINE) OR
         (TickerRecPtr^.Template_ID = ONE_LINE_SPONSOR_FULL_PAGE) OR
         (TickerRecPtr^.Template_ID = TWO_LINE_SPONSOR_TAGLINE) OR
         (TickerRecPtr^.Template_ID = TWO_LINE_SPONSOR_FULL_PAGE))
      AND (TickerRecPtr^.StartEnableDateTime <= Now) AND
         (TickerRecPtr^.EndEnableDateTime > Now) then OutVal := TRUE;
    end;
  end;
  CheckForActiveSponsorLogo := OutVal;
end;

////////////////////////////////////////////////////////////////////////////////
// TICKER
////////////////////////////////////////////////////////////////////////////////
//Procedure to send next record after receipt of packet
procedure TEngineInterface.SendTickerRecord(PlaylistID: SmallInt; NextRecord: Boolean; RecordIndex: SmallInt);
var
  i,j,k: SmallInt;
  TempStr, CmdStr: String;
  TickerRecPtr, TickerRecPtrNew: ^TickerRec;
  FoundGame: Boolean;
  VisitorScore: SmallInt;
  HomeScore: SmallInt;
  VisitorColor: SmallInt;
  HomeColor: SmallInt;
  HomeRank: String;
  VisitorRank: String;
  HomeName: String;
  Visitorname: String;
  IsLast: Boolean;
  OverlayIndex, OverlayIndexNext: SmallInt;
  IsNewOverlay, PadText: Boolean;
  InGameStatsRequired: Boolean;
  FoundGameInDatamart: Boolean;
  PhaseStr, TimeStr: String;
  VLeader, HLeader: Boolean;
  GameHasStarted, GameIsFinal, GameIsHalf: Boolean;
  GTIME: String;
  GPHASE: SmallInt;
  CurrentGamePhaseRec: GamePhaseRec;
  CurrentGameState: SmallInt;
  RecordIsSponsor: Boolean;
  CurrentLeague: String;
  LineupData: LineupText;
  TemplateInfo: TemplateDefsRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  CurrentGameData: GameRec;
  OkToGo, OKToDisplay: Boolean;
  TokenCounter: SmallInt;
  TickerEntryIndex: SmallInt;
  CollectionCount: SmallInt;
  SceneToggleState: SmallInt;
begin
  //Init flags
  OKToDisplay := TRUE;
  GameHasStarted := FALSE;
  GameIsFinal:= FALSE;
  GameIsHalf := FALSE;
  //Init FoundGame flag - used to prevent inadverant commands from being sent if game not found
  FoundGame := TRUE;
  RecordIsSponsor := FALSE;

  //Turn on indicator
  PacketIndicatorTimer.Enabled := TRUE;
  MainForm.ApdStatusLight2.Lit := TRUE;

  //Disable packets and enable timer to re-enable packets
  //PacketEnable := FALSE;
  PacketEnableTimer.Enabled := TRUE;
  try
    //Init
    IsLast := FALSE;

    //Make sure we haven't dumped put
    if ((EngineParameters.Enabled) AND (TickerAbortFlag = FALSE) AND (Ticker_Collection.Count > 0)) then
    begin
      OKToGo := TRUE;

      //Triggering specific record, so set current entry index
      TickerEntryIndex := RecordIndex;
      Case PlaylistID of
        PLAYLIST_1: begin
          CurrentTickerEntryIndex1 := RecordIndex;
          CollectionCount := Ticker_Collection.Count;
        end;
        PLAYLIST_2: begin
          CurrentTickerEntryIndex2 := RecordIndex;
          CollectionCount := Ticker_Collection2.Count;
        end;
      end;
      //Disble command timer
      DisableCommandTimer := TRUE;

      //Proceed if not at end or in looping mode and at beggining
      if (TickerEntryIndex <= CollectionCount-1) AND (OKToGo) then
      begin
        //Get pointer to current record
        Case PlaylistID of
          PLAYLIST_1: TickerRecPtr := Ticker_Collection.At(TickerEntryIndex);
          PLAYLIST_2: TickerRecPtr := Ticker_Collection2.At(TickerEntryIndex);
        end;

        ////////////////////////////////////////////////////////////////////////
        //MAIN PROCESSING LOOP FOR TICKER FIELDS
        ////////////////////////////////////////////////////////////////////////
        //Get template info and load the termporary fields collection
        TemplateInfo := LoadTempTemplateFields(TickerRecPtr^.Template_ID);

        //Get game data if template requires it
        if (TemplateInfo.UsesGameData) then
        begin
          try
            //Use full game data
            CurrentGameData := GetGameData(TRIM(TickerRecPtr^.League), TRIM(TickerRecPtr^.GameID));
          except
            if (ErrorLoggingEnabled = True) then
              WriteToErrorLog('Error occurred while trying to get game data for ticker');
          end;
          FoundGame := CurrentGameData.DataFound;
        end;

         //Set the default dwell time - minimum = 2000mS; set packet timeout interval
        if (TickerRecPtr^.DwellTime > 2000) then
        begin
          //If sponsor logo dwell is specified, it's a sponsor logo, so use that for the dwell time
          if (TickerRecPtr^.SponsorLogo_Dwell > 0) AND ((TemplateInfo.TemplateSponsorType = 1) OR (TemplateInfo.TemplateSponsorType = 2)) then
          begin
            TickerCommandDelayTimer.Interval := (TickerRecPtr^.SponsorLogo_Dwell*1000);
            TickerPacketTimeoutTimer.Interval := (TickerRecPtr^.SponsorLogo_Dwell*1000) + 5000;
            //Set the current sponsor logo name
            CurrentSponsorLogoName[1] := TickerRecPtr^.SponsorLogo_Name;
          end
          else begin
            TickerCommandDelayTimer.Interval := TickerRecPtr^.DwellTime;
            TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 5000;
          end;
        end
        else begin
          TickerCommandDelayTimer.Interval := 2000;
          TickerPacketTimeoutTimer.Interval := TickerRecPtr^.DwellTime + 7000;
        end;

        //Add the fields from the temporary fields collection
        if (Temporary_Fields_Collection.Count > 0) then
        begin
          for j := 0 to Temporary_Fields_Collection.Count-1 do
          begin
            TemplateFieldsRecPtr := Temporary_Fields_Collection.At(j);
            //Version 2.1
            //Check for single fields (not compound fields required for 2-line, new look engine)
            if (TemplateFieldsRecPtr^.Field_Type > 0) then
            begin
              //Filter out fields ID values of -1 => League designator
              //if (TemplateFieldsRecPtr^.Engine_Field_ID >= 0) then
              begin
                //Add region command
                //If not a symbolic fields, send field contents directly
                if (Pos('$', TemplateFieldsRecPtr^.Field_Contents) = 0) then
                begin
                  TempStr := TemplateFieldsRecPtr^.Field_Contents;
                  //Add prefix and/or suffix if applicable
                  if (TemplateFieldsRecPtr^.Field_Format_Prefix <> '') AND (EnableTemplateFieldFormatting) then
                    TempStr := TemplateFieldsRecPtr^.Field_Format_Prefix + TempStr;
                  if (TemplateFieldsRecPtr^.Field_Format_Suffix <> '') AND (EnableTemplateFieldFormatting) then
                    TempStr := TempStr + TemplateFieldsRecPtr^.Field_Format_Suffix;
                  if (Trim(TempStr) = '') then TempStr := ' ';

                  //Append field update to command string
                  CmdStr := CmdStr + TemplateFieldsRecPtr^.Scene_Field_Name + '�' + TempStr;
                  //Append command delimiter if not last field
                  if (j < Temporary_Fields_Collection.Count-1) then CmdStr := CmdStr + '\'
                end
                //It's a symbolic name, so get the field contents
                else begin
                  try
                    TempStr := GetValueOfSymbol(PlaylistID, TICKER, TemplateFieldsRecPtr^.Field_Contents, TickerEntryIndex,
                      CurrentGameData, TickerDisplayMode, TickerRecPtr^.UserData[21]).SymbolValue;
                    //Add previx and/or suffix if applicable
                    if (TemplateFieldsRecPtr^.Field_Format_Prefix <> '') AND (EnableTemplateFieldFormatting) then
                      TempStr := TemplateFieldsRecPtr^.Field_Format_Prefix + TempStr;
                    if (TemplateFieldsRecPtr^.Field_Format_Suffix <> '') AND (EnableTemplateFieldFormatting) then
                      TempStr := TempStr + TemplateFieldsRecPtr^.Field_Format_Suffix;
                  except
                    if (ErrorLoggingEnabled = True) then
                      WriteToErrorLog('Error occurred while trying to get value for main ticker data field');
                  end;
                  if (Trim(TempStr) = '') then TempStr := ' ';

                  //Process any style chips
                  TempStr := ProcessStyleChips(TempStr);

                  //Append field update to command string
                  CmdStr := CmdStr + TemplateFieldsRecPtr^.Scene_Field_Name + '�' + TempStr;
                  //Append command delimiter if not last field
                  if (j < Temporary_Fields_Collection.Count-1) then CmdStr := CmdStr + '\'
                end;
              end;
            end;
          end;
          //Write out to the as-run log if it's a sponsor logo
          if (Trim(TickerRecPtr^.SponsorLogo_Name) <> '') then
            WriteToAsRunLog('Started display of sponsor logo: ' + CurrentSponsorLogoName[1]);
        end;

        //Transition out the prior scene
        //Transition out existing sponsor if new page is a sponsor logo
        if (TickerRecPtr^.SponsorLogo_Dwell <> 0) then
        begin
          EngineInterface.UpdateScene(SPONSOR_LOGO_SCENE_ID, SPONSOR_LOGO_OUT_COMMAND + '�' + 'T');
        end
        //Not a sponsor
        else begin
          if (LastTemplateInfo[PlaylistID].Scene_ID <> 0) and
             (LastTemplateInfo[PlaylistID].Scene_ID <> SPONSOR_LOGO_SCENE_ID) and
             (LastTemplateInfo[PlaylistID].OutTransition_Command <> '') then
            EngineInterface.UpdateScene(LastTemplateInfo[PlaylistID].Scene_ID, LastTemplateInfo[PlaylistID].OutTransition_Command + '�' + 'T');
        end;

        //Added for Version 1.1.0  10/20/11
        //Play the new scene
        Case PlaylistID of
          PLAYLIST_1: PlayScene(TemplateInfo.Scene_ID_1);
          PLAYLIST_2: PlayScene(TemplateInfo.Scene_ID_2);
        end;

        //Delay
        if (TickerRecPtr^.SponsorLogo_Dwell <> 0) then Sleep (1000)
        else Sleep (200);

        //Send the new data
        Case PlaylistID of
          PLAYLIST_1: UpdateScene(TemplateInfo.Scene_ID_1, CmdStr);
          PLAYLIST_2: UpdateScene(TemplateInfo.Scene_ID_2, CmdStr);
        end;

        //Delay
        Sleep (100);

        //Transition the data in & store info on the last scene & transition
        Case PlaylistID of
          PLAYLIST_1: begin
            //Check field state and change backplates if needed (Field 1&2 vs. Field 3)
            Case TemplateInfo.Template_Type of
              //Fields 1 & 2
              FIELDS_1_2: begin
                //Transition in Field 1 backplate if startup
                if (LastTemplateInfo[PLAYLIST_1].LastFieldTypeID = STARTUP) then
                begin
                  EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_1_In_Command + '�' + 'T');
                end
                //Take out Field 3 if current state and bring in Field 1
                else if (LastTemplateInfo[PLAYLIST_1].LastFieldTypeID = FIELD_3) then
                begin
                  EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_3_Out_Command + '�' + 'T');
                  EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_1_In_Command + '�' + 'T');
                end;
              end;
              //Field 3
              FIELD_3: begin
                //Take out Fields 1 & 2 if current state and bring in Field 3
                if (LastTemplateInfo[PLAYLIST_1].LastFieldTypeID = FIELDS_1_2) then
                begin
                  //Take out field 2 if it was in
                  if (LastTemplateInfo[PLAYLIST_2].LastFieldTypeID = FIELDS_1_2) then
                  begin
                    EngineInterface.UpdateScene(LastTemplateInfo[PLAYLIST_2].Scene_ID, LastTemplateInfo[PLAYLIST_2].OutTransition_Command + '�' + 'T');
                    EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_2_Out_Command + '�' + 'T');
                    //Clear fields so Field 2 starts from scratch the next time around
                    LastTemplateInfo[PLAYLIST_2].LastFieldTypeID := STARTUP;
                    LastTemplateInfo[PLAYLIST_2].Scene_ID := 0;
                  end;
                  //Take out field 1
                  EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_1_Out_Command + '�' + 'T');
                  //Bring in Field 3
                  EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_3_In_Command + '�' + 'T');
                end
                //Added for Version 2.0
                else if (LastTemplateInfo[PLAYLIST_1].Scene_ID = Full_Page_Promo_Scene_ID) then
                begin
                  EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_3_In_Command + '�' + 'T');
                end
                //Transition in Field 3 backplate if startup
                else if (LastTemplateInfo[PLAYLIST_1].LastFieldTypeID = STARTUP) then
                begin
                  EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_3_In_Command + '�' + 'T');
                end;
              end;
            end;
            //Transition the new scene in
            EngineInterface.UpdateScene(TemplateInfo.Scene_ID_1, TemplateInfo.Transition_In + '�' + 'T');

            //Stop the prior scene if it's a different scene number
            //Added for Version 1.1.0  10/20/11
            if (TickerRecPtr^.SponsorLogo_Dwell = 0) and (TemplateInfo.Scene_ID_1 <> LastTemplateInfo[PLAYLIST_1].Scene_ID) then
              StopScene(LastTemplateInfo[PLAYLIST_1].Scene_ID);

            //Store the last template info
            if (TickerRecPtr^.SponsorLogo_Dwell = 0) then
            begin
              LastTemplateInfo[PLAYLIST_1].Scene_ID := TemplateInfo.Scene_ID_1;
              LastTemplateInfo[PLAYLIST_1].OutTransition_Command := TemplateInfo.Transition_Out;
              LastTemplateInfo[PLAYLIST_1].LastFieldTypeID := TemplateInfo.Template_Type;
            end;
          end;
          PLAYLIST_2: begin
            //Check field state and change backplates if needed (Field 1&2 vs. Field 3)
            Case TemplateInfo.Template_Type of
              //Fields 1 & 2
              FIELDS_1_2: begin
                //Take out Field 3 if current state and bring in Field 2
                if (LastTemplateInfo[PLAYLIST_1].LastFieldTypeID = FIELD_3) then
                begin
                  //Take out the text from scene 1
                  if (LastTemplateInfo[PLAYLIST_1].Scene_ID <> 0) and (LastTemplateInfo[PLAYLIST_1].OutTransition_Command <> '') then
                    EngineInterface.UpdateScene(LastTemplateInfo[PLAYLIST_1].Scene_ID, LastTemplateInfo[PLAYLIST_1].OutTransition_Command + '�' + 'T');

                  //Swap the backplates
                  EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_3_Out_Command + '�' + 'T');
                  //EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_2_In_Command + '�' + 'T');

                  //Clear fields so Field 1 starts from scratch the next time around
                  LastTemplateInfo[PLAYLIST_1].LastFieldTypeID := STARTUP;
                  LastTemplateInfo[PLAYLIST_1].Scene_ID := 0;
                end;
                //Update Field 2 data
                if (LastTemplateInfo[PLAYLIST_2].LastFieldTypeID = STARTUP) then
                  EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_2_In_Command + '�' + 'T');
              end;
              //Field 3
              {FIELD_3: begin
                //Transition in Field 3 backplate if startup
                if (LastTemplateInfo[PLAYLIST_2].LastFieldTypeID = STARTUP) then
                begin
                  EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_2_In_Command + '�' + 'T');
                end
                //Take out Fields 1 & 2 if current state and bring in Field 3
                else if (LastTemplateInfo[PLAYLIST_2].LastFieldTypeID = FIELD_3) then
                begin
                  EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_2_Out_Command + '�' + 'T');
                  EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_3_In_Command + '�' + 'T');
                  //Take out field 1 if it was in
                  if (LastTemplateInfo[PLAYLIST_1].LastFieldTypeID = FIELDS_1_2) then
                  begin
                    EngineInterface.UpdateScene(LastTemplateInfo[PLAYLIST_1].Scene_ID, LastTemplateInfo[PLAYLIST_2].OutTransition_Command + '�' + 'T');
                    EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_1_Out_Command + '�' + 'T');
                  end;
                end;
              end;}
            end;
            //Transition the new scene in
            EngineInterface.UpdateScene(TemplateInfo.Scene_ID_2, TemplateInfo.Transition_In + '�' + 'T');

            //Stop the prior scene if it's a different scene number
            //Added for Version 1.1.0  10/20/11
            if (TickerRecPtr^.SponsorLogo_Dwell = 0) and (TemplateInfo.Scene_ID_2 <> LastTemplateInfo[PLAYLIST_2].Scene_ID) then
              StopScene(LastTemplateInfo[PLAYLIST_2].Scene_ID);

            //Store the last template info
            LastTemplateInfo[PLAYLIST_2].Scene_ID := TemplateInfo.Scene_ID_2;
            LastTemplateInfo[PLAYLIST_2].OutTransition_Command := TemplateInfo.Transition_Out;
            LastTemplateInfo[PLAYLIST_2].LastFieldTypeID := TemplateInfo.Template_Type;
          end;
        end;
      end;
    end;
  except
    if (ErrorLoggingEnabled = True) then
    begin
      Error_Condition := True;
      MainForm.Label5.Caption := 'ERROR';
      //WriteToErrorLog
      if (ErrorLoggingEnabled = True) then
        WriteToErrorLog('Error occurred while trying to send additional records to ticker');
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// SOCKET EVENT HANDLES
////////////////////////////////////////////////////////////////////////////////
//Handler for packet re-enable timer
procedure TEngineInterface.PacketEnableTimerTimer(Sender: TObject);
begin
  //Disable to prevent retrigger
  PacketEnableTimer.Enabled := FALSE;
  //Re-enable packets
  PacketEnable := TRUE;
end;

//Handler to turn off packet recevied indicator after 250 mS
procedure TEngineInterface.PacketIndicatorTimerTimer(Sender: TObject);
begin
  PacketIndicatorTimer.Enabled := FALSE;
  MainForm.ApdStatusLight2.Lit := FALSE;
end;

////////////////////////////////////////////////////////////////////////////////
// PACKET TIMEOUT TIMERS
////////////////////////////////////////////////////////////////////////////////
//Handler for ticker packet timeout timer - sends next command if status command not
//received
procedure TEngineInterface.TickerPacketTimeoutTimerTimer(Sender: TObject);
begin
  if (EngineParameters.Enabled) then
  begin
    //Disable timer to prevent retriggering
    TickerPacketTimeoutTimer.Enabled := FALSE;
    //Enable delay timer
    if (RunningTicker = TRUE) then
      //Send next record
      SendTickerRecord(PLAYLIST_1, TRUE, 0);
    //Log the timeout
    if (ErrorLoggingEnabled = True) then
    begin
      //WriteToErrorLog
      if (ErrorLoggingEnabled = True) then
        WriteToErrorLog('A timeout occurred while waiting for a Main Ticker command return status from the graphics engine.');
    end;
  end;
end;

//Handler to light indicator
procedure TEngineInterface.EnginePortConnect(Sender: TObject; Socket: TCustomWinSocket);
begin
  MainForm.ApdStatusLight1.Lit := TRUE;
  SocketConnected := TRUE;
end;

//Handler to turn off indicator
procedure TEngineInterface.EnginePortDisconnect(Sender: TObject; Socket: TCustomWinSocket);
begin
  MainForm.ApdStatusLight1.Lit := FALSE;
  SocketConnected := FALSE;
  MessageDlg('The connection to the graphics engine was lost. ' +
             'You will not be able to control the graphics engine. If you wish to try ' +
             'reconnecting to the engine, please select Utilities | Reconnect to Graphics Engine ' +
             'from the main program menu.', mtError, [mbOK], 0);
  if (ErrorLoggingEnabled = True) then
  begin
    Error_Condition := True;
    MainForm.Label5.Caption := 'ERROR';
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('Error occurred with connection to ticker graphics engine');
  end;
end;

//Handler for socket error
procedure TEngineInterface.EnginePortError(Sender: TObject; Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  MainForm.ApdStatusLight1.Lit := FALSE;
  SocketConnected := FALSE;
  MessageDlg('The connection to the graphics engine was lost. ' +
             'You will not be able to control the graphics engine. If you wish to try ' +
             'reconnecting to the engine, please select Utilities | Reconnect to Graphics Engine ' +
             'from the main program menu.', mtError, [mbOK], 0);
  if (ErrorLoggingEnabled = True) then
  begin
    Error_Condition := True;
    MainForm.Label5.Caption := 'ERROR';
    //WriteToErrorLog
    if (ErrorLoggingEnabled = True) then
      WriteToErrorLog('Error occurred with connection to ticker graphics engine');
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// BREAKING NEWS FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

//Version 2.0 Functions for top graphic region
////////////////////////////////////////////////////////////////////////////////
// TOP REGION FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Send next update
procedure TEngineInterface.TopGraphicDwellTimerTimer(Sender: TObject);
begin
  TopGraphicDwellTimer.Enabled := FALSE;
  SetNextTopGraphic;
end;

//Refresh the collection from the database
procedure TEngineInterface.RefreshTopGraphicCollection;
var
  i: SmallInt;
  TopGraphicRecPtr: ^TopGraphicRec;
begin
  with dmMain.Query5 do
  try
    Close;
    SQL.Clear;
    SQL.Add('SELECT * FROM Top_Region_Image_Wheel');
    Open;
    if (RecordCount > 0) then
    begin
      First;
      repeat
        //Add item to teams collection
        GetMem (TopGraphicRecPtr, SizeOf(TopGraphicRec));
        With TopGraphicRecPtr^ do
        begin
          GraphicIndex := FieldByName('GraphicIndex').AsInteger;
          GraphicFilePath := FieldByName('GraphicFilePath').AsString;
          DwellTime := FieldByName('DwellTime').AsInteger;
          if (Top_Graphic_Collection.Count <= 50) then
          begin
            Top_Graphic_Collection.Insert(TopGraphicRecPtr);
            Top_Graphic_Collection.Pack;
          end;
        end;
        //Got to next record
        Next;
      until (EOF = TRUE); //Repeat until end of dataset
    end;
  except
    if (ErrorLoggingEnabled = True) then
    begin
      Error_Condition := True;
      //WriteToErrorLog
      WriteToErrorLog('Error occurred while trying refresh top region wheel from database');
    end;
  end;
end;

//Send the next graphic to the region
procedure TEngineInterface.SetNextTopGraphic;
var
  TopGraphicRecPtr: ^TopGraphicRec;
begin
  if (Top_Graphic_Collection.Count = 0) then RefreshTopGraphicCollection;

  if (Top_Graphic_Collection.Count > 0) then
  begin
    //Set values and transition in new graphic based on toggle state
    TopGraphicRecPtr := Top_Graphic_Collection.At(0);

    if (ClockIsIn) then
    begin
      //Check for start of playlist
      if (TopRegionToggleState = -1) then //Bring Image #1 in
      begin
        //Set next value for Image #1 based on toggle state
        UpdateScene(BaseScene_SceneID, 'Top_Image_File_1' + '�' + TopGraphicRecPtr^.GraphicFilePath);

        //Trigger next graphic #1 in
        UpdateScene(BaseScene_SceneID, 'Image_1_In_Btn' + '�' + 'T');
      end
      //Last displayed was image region #1; display image #2
      else if (TopRegionToggleState = 1) then //Bring Image #2 in
      begin
        //Set next value for Image #1 based on toggle state
        UpdateScene(BaseScene_SceneID, 'Top_Image_File_2' + '�' + TopGraphicRecPtr^.GraphicFilePath);

        //Trigger current graphic #1 out if not a result of a clock state change
        if not (ClockStateChanged) then
          UpdateScene(BaseScene_SceneID, 'Image_1_Out_Btn' + '�' + 'T');

        //Trigger next graphic #2 in
        UpdateScene(BaseScene_SceneID, 'Image_2_In_Btn' + '�' + 'T');
      end
      //Last displayed was image region #2; display image #1
      else begin //Bring image #2 in
        //Set next value for Image #2 based on toggle state
        UpdateScene(BaseScene_SceneID, 'Top_Image_File_1' + '�' + TopGraphicRecPtr^.GraphicFilePath);

        //Trigger current graphic #2 out if not a result of a clock state change
        if not (ClockStateChanged) then
          UpdateScene(BaseScene_SceneID, 'Image_2_Out_Btn' + '�' + 'T');

        //Trigger next graphic #1 in
        UpdateScene(BaseScene_SceneID, 'Image_1_In_Btn' + '�' + 'T');
      end;
    end
    //Clock is out
    else begin
      //Check for start of playlist
      if (TopRegionToggleState = -1) then //Bring Image #1 in
      begin
        //Set next value for Image #1 based on toggle state
        UpdateScene(BaseScene_SceneID, 'Top_Image_File_1_No_Clock' + '�' + TopGraphicRecPtr^.GraphicFilePath);

        //Trigger next graphic #1 in
        UpdateScene(BaseScene_SceneID, 'Mktg_1_No_Clock_In_Btn' + '�' + 'T');
      end
      //Last displayed was image region #1; display image #2
      else if (TopRegionToggleState = 1) then //Bring Image #2 in
      begin
        //Set next value for Image #1 based on toggle state
        UpdateScene(BaseScene_SceneID, 'Top_Image_File_2_No_Clock' + '�' + TopGraphicRecPtr^.GraphicFilePath);

        //Trigger current graphic #1 out if not a result of a clock state change
        if not (ClockStateChanged) then
          UpdateScene(BaseScene_SceneID, 'Mktg_1_No_Clock_Out_Btn' + '�' + 'T');

        //Trigger next graphic #2 in
        UpdateScene(BaseScene_SceneID, 'Mktg_2_No_Clock_In_Btn' + '�' + 'T');
      end
      //Last displayed was image region #2; display image #1
      else begin //Bring image #2 in
        //Set next value for Image #2 based on toggle state
        UpdateScene(BaseScene_SceneID, 'Top_Image_File_1_No_Clock' + '�' + TopGraphicRecPtr^.GraphicFilePath);

        //Trigger current graphic #2 out if not a result of a clock state change
        if not (ClockStateChanged) then
        UpdateScene(BaseScene_SceneID, 'Mktg_2_No_Clock_Out_Btn' + '�' + 'T');

        //Trigger next graphic #1 in
        UpdateScene(BaseScene_SceneID, 'Mktg_1_No_Clock_In_Btn' + '�' + 'T');
      end;
    end;

    //Set timer dwell value and trigger
    TopGraphicDwellTimer.Enabled := FALSE;
    TopGraphicDwellTimer.Interval := TopGraphicRecPtr^.DwellTime*1000;
    TopGraphicDwellTimer.Enabled := TRUE;

    //Delete the top entry in the collection
    Top_Graphic_Collection.AtDelete(0);
    Top_Graphic_Collection.Pack;
  end;

  //Refresh if needed
  if (Top_Graphic_Collection.Count = 0) then
    RefreshTopGraphicCollection;

  //Toggle the value for next iteration
  if (TopRegionToggleState = -1) or (TopRegionToggleState = 2) then TopRegionToggleState := 1
  else TopRegionToggleState := 2;

  //Clear flag
  ClockStateChanged := FALSE;
end;

////////////////////////////////////////////////////////////////////////////////
// LOGGING FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
// This procedure opens the current log file and writes out an error message
// with a time/date stamp and servide ID indicator
procedure TEngineInterface.WriteToErrorLog (ErrorString: String);
var
   FileName: String;
   DateStr: String;
   TimeStr: String;
   HoursStr: String[2];
   MinutesStr: String[2];
   SecondsStr: String[2];
   DayStr: String[2];
   MonthStr: String[2];
   TestStr: String;
   ErrorLogStr: String;
   Present: TDateTime;
   Year, Month, Day, Hour, Min, Sec, MSec: Word;
begin
   {Check for error log directory and create if it doesn't exist}
   if (DirectoryExists('c:\CSN_Error_LogFiles') = FALSE) then CreateDir('c:\CSN_Error_LogFiles');
   {Build date and time strings}
   Present:= Now;
   DecodeTime (Present, Hour, Min, Sec, MSec);
   HoursStr := IntToStr(Hour);
   If (Length(HoursStr) = 1) then HoursStr := '0' + HoursStr;
   MinutesStr := IntToStr(Min);
   If (Length(MinutesStr) = 1) then MinutesStr := '0' + MinutesStr;
   SecondsStr := IntToStr(Sec);
   If (Length(SecondsStr) = 1) then SecondsStr := '0' + SecondsStr;
   TimeStr := HoursStr + ':' + MinutesStr + ':' + SecondsStr;
   DecodeDate (Present, Year, Month, Day);
   DayStr := IntToStr(Day);
   If (Length(DayStr) = 1) then DayStr := '0' + DayStr;
   MonthStr := IntToStr(Month);
   If (Length(MonthStr) = 1) then MonthStr := '0' + MonthStr;
   DateStr := MonthStr + '-' + DayStr + '-' + IntToStr(Year);
   ErrorLogStr := TimeStr + '    ' + DateStr + '    ' + ErrorString;
   {Construct filename using current date - files are numbered 01 through 31}
   FileName := 'c:\TSTN_Error_Logfiles\TSTNErrorLog' + DayStr + '.txt';
   AssignFile (ErrorLogFile, FileName);
   //Write error information to one line in file
   //If file doesn't exist create a new one. Otherwise, check to see if it's one
   //month old. If so, delete & create a new one. Otherwise, append error to file
   if (Not (FileExists(FileName))) then
   begin
      ReWrite (ErrorLogFile);
      try
         Writeln (ErrorLogFile, ErrorLogStr);      finally         CloseFile (ErrorLogFile);      end;   end   else begin      {Check file to see if it's a month old. If so, erase and create a new one}      Reset (ErrorLogFile);      try         Readln (ErrorLogFile, TestStr);      finally         CloseFile (ErrorLogFile);      end;      If ((TestStr[16] <> DayStr[1]) OR (TestStr[17] <> DayStr[2])) then begin         {Date in 1st entry in error log does not match today's date, so erase}         Erase (ErrorLogFile);         {Create a new logfile}         ReWrite (ErrorLogFile);         try
            Writeln (ErrorLogFile, ErrorLogStr);         finally            CloseFile (ErrorLogFile);         end;      end      else begin         {File exists and not a month old, so append to existing log file}         Append (ErrorLogFile);         try
            Writeln (ErrorLogFile, ErrorLogStr);         finally            CloseFile (ErrorLogFile);         end;      end;   end;end;

// This procedure opens the current log file and writes out an error message
// with a time/date stamp and servide ID indicator
procedure TEngineInterface.WriteToAsRunLog (AsRunString: String);
var
   FileName: String;
   DateStr: String;
   TimeStr: String;
   HoursStr: String[2];
   MinutesStr: String[2];
   SecondsStr: String[2];
   DayStr: String[2];
   MonthStr: String[2];
   TestStr: String;
   AsRunLogStr: String;
   Present: TDateTime;
   Year, Month, Day, Hour, Min, Sec, MSec: Word;
   DirectoryStr: String;
begin
   {Check for error log directory and create if it doesn't exist}
   if (DirectoryExists(AsRunLogFileDirectoryPath) = FALSE) then
   begin
     if (DirectoryExists('c:\CSN_AsRun_LogFiles') = FALSE) then
       CreateDir('c:\CSN_AsRun_LogFiles');
     DirectoryStr := 'c:\CSN_AsRun_LogFiles\';
   end
   else DirectoryStr := AsRunLogFileDirectoryPath;
   {Build date and time strings}
   Present:= Now;
   DecodeTime (Present, Hour, Min, Sec, MSec);
   HoursStr := IntToStr(Hour);
   If (Length(HoursStr) = 1) then HoursStr := '0' + HoursStr;
   MinutesStr := IntToStr(Min);
   If (Length(MinutesStr) = 1) then MinutesStr := '0' + MinutesStr;
   SecondsStr := IntToStr(Sec);
   If (Length(SecondsStr) = 1) then SecondsStr := '0' + SecondsStr;
   TimeStr := HoursStr + ':' + MinutesStr + ':' + SecondsStr;
   DecodeDate (Present, Year, Month, Day);
   DayStr := IntToStr(Day);
   If (Length(DayStr) = 1) then DayStr := '0' + DayStr;
   MonthStr := IntToStr(Month);
   If (Length(MonthStr) = 1) then MonthStr := '0' + MonthStr;
   DateStr := MonthStr + '-' + DayStr + '-' + IntToStr(Year);
   AsRunLogStr := TimeStr + '    ' + DateStr + '    ' + AsRunString;
   {Construct filename using current date - files are numbered 01 through 31}
   FileName := DirectoryStr + 'TSTNAsRunLog' + DayStr + '.txt';
   AssignFile (AsRunLogFile, FileName);
   {If (FileExists (ErrorLogFile)) then Erase (ErrorLogFile);}
   {Write error information to one line in file}
   {If file doesn't exist create a new one. Otherwise, check to see if it's one }
   {month old. If so, delete & create a new one. Otherwise, append error to file}
   if (Not (FileExists(FileName))) then begin
      ReWrite (AsRunLogFile);
      try
         Writeln (AsRunLogFile, AsRunLogStr);      finally         CloseFile (AsRunLogFile);      end;   end   else begin      {Check file to see if it's a month old. If so, erase and create a new one}      Reset (AsRunLogFile);      try         Readln (AsRunLogFile, TestStr);      finally         CloseFile (AsRunLogFile);      end;      If ((TestStr[16] <> DayStr[1]) OR (TestStr[17] <> DayStr[2])) then      begin         {Date in 1st entry in error log does not match today's date, so erase}         Erase (AsRunLogFile);         {Create a new logfile}         ReWrite (AsRunLogFile);         try
            Writeln (AsRunLogFile, AsRunLogStr);         finally            CloseFile (AsRunLogFile);         end;      end      else begin         {File exists and not a month old, so append to existing log file}         Append (AsRunLogFile);         try
            Writeln (AsRunLogFile, AsRunLogStr);         finally            CloseFile (AsRunLogFile);         end;      end;   end;end;

end.


