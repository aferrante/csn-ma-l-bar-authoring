Base English Dictionary for use with Addict Spell Check & Thesaurus
(c)1994-2000 Addictive Software
v2.02 Released 5 June 2000

http://www.addictive.net/
mailto:addictsw@kagi.com
Support: mailto:addictsupport@esbconsult.com.au

This dictionary contains all the words that are common to the various 
English Dictionaries and whilst it can be used by itself it is better
to use one of its descendants such as American or British.
 
This dictionary is made freely available to all users of Addictive 
Software's Addict Spell Check & Thesaurus. A trial version can be 
obtained from:

http://www.addictive.net/

No guarantees are made as to the accuracy and reliability of this 
dictionary, but we would encourage you to notify us of any problems 
your find or additions you would like to see.

Addictive Software and support are native English speakers and we
are reliant on third party sources for information regarding the
quality of non-English dictionaries.

You are permitted to distribute this dictionary with your application
provided you do not charge for the dictionary itself.

http://www.addictive.net/
mailto:addictsw@kagi.com
Support: mailto:addictsupport@esbconsult.com.au