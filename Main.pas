////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// CSNMA L-Bar Authoring Tool - Original Release
// V1.0.0  08/23/11  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support up to 50 user-defined text fields.
// V1.0.1  09/28/11  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to play scenes only when needed (to address performance issues)
// V1.1.0  10/20/11  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to support controls for Clock In/Out.
// V1.2.0  08/27/13  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Modified to suport new headshot/logo composition scheme, automated scores &
// stats and other misc. modifications.
// V2.0.0  03/26/14  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Fixed bug with playlist insert function.
// V2.0.1  04/14/14  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////
// Fixed bug with co-mingling of sponsor logos with playlist entries.
// V2.0.2  04/15/14  M. Dilworth  Video Design Software
////////////////////////////////////////////////////////////////////////////////

unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Spin, Buttons, ExtCtrls, Menus, Mask, ComCtrls, StBase,
  TSImageList, Grids_ts, TSGrid, TSDBGrid, DBCtrls, JPEG, StColl, MPlayer, OleCtrls,
  OleServer, Excel97, Grids, PBJustOne, ClipBrd, TSMask, DBGrids, OoMisc, AdStatLt,
  Globals, INIFiles, Variants;

{H+} //ANSI strings

//Record type defs
type
  //Main progam object def
  TMainForm = class(TForm)
    //Object defs
    ProgramModePageControl: TPageControl;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Exit1: TMenuItem;
    Prefs1: TMenuItem;
    SetPrefs1: TMenuItem;
    Help1: TMenuItem;
    About1: TMenuItem;
    TabSheet2: TTabSheet;
    TabSheet4: TTabSheet;
    DBMaintPanel: TPanel;
    Login1: TMenuItem;
    LogintoSystem1: TMenuItem;
    N2: TMenuItem;
    LogoutfromSystem1: TMenuItem;
    PBJustOne1: TPBJustOne;
    Database1: TMenuItem;
    RepopulateTeamsCollection1: TMenuItem;
    TimeOfDayTimer: TTimer;
    TabSheet7: TTabSheet;
    SchedulePanel: TPanel;
    N3: TMenuItem;
    PurgeScheduledEvents1DayOld1: TMenuItem;
    N4: TMenuItem;
    ShowSponsorLogoDataEntryPage1: TMenuItem;
    HideSponsorLogoDataEntryPage1: TMenuItem;
    tsMaskDefs1: TtsMaskDefs;
    PlaylistSaveControlsPnl: TPanel;
    Label4: TLabel;
    BitBtn6: TBitBtn;
    BitBtn24: TBitBtn;
    Edit1: TEdit;
    ExpandButton: TBitBtn;
    PlaylistSelectTabControl: TTabControl;
    PlaylistGridPanel: TPanel;
    Label34: TLabel;
    Label69: TLabel;
    NumEntries: TLabel;
    BitBtn20: TBitBtn;
    BitBtn18: TBitBtn;
    Panel4: TPanel;
    Label38: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    EntryEnable: TCheckBox;
    EntryNote: TEdit;
    EntryStartEnableDate: TDateTimePicker;
    EntryStartEnableTime: TDateTimePicker;
    EntryEndEnableDate: TDateTimePicker;
    EntryEndEnableTime: TDateTimePicker;
    Panel16: TPanel;
    Label14: TLabel;
    TemplateDescription: TLabel;
    EntryFieldsGrid: TtsGrid;
    PlaylistGrid: TtsGrid;
    PlaylistPopupMenu: TPopupMenu;
    Copy1: TMenuItem;
    Paste1: TMenuItem;
    Duplicate1: TMenuItem;
    Delete1: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    Cut1: TMenuItem;
    Enable1: TMenuItem;
    Disable1: TMenuItem;
    N7: TMenuItem;
    AppendtoEnd1: TMenuItem;
    DuplicateandAppendtoEnd1: TMenuItem;
    N8: TMenuItem;
    EditStartEndEnableTimes1: TMenuItem;
    Panel27: TPanel;
    Label13: TLabel;
    Label15: TLabel;
    Label19: TLabel;
    Label37: TLabel;
    Label68: TLabel;
    ScheduleEntryStartDate: TDateTimePicker;
    ScheduleEntryEndDate: TDateTimePicker;
    Label24: TLabel;
    AvailablePlaylistsGrid: TtsDBGrid;
    Label25: TLabel;
    ScheduledPlaylistsGrid: TtsDBGrid;
    Panel26: TPanel;
    Label36: TLabel;
    BitBtn44: TBitBtn;
    BitBtn43: TBitBtn;
    Panel19: TPanel;
    Label40: TLabel;
    DatabaseMaintPlaylistGrid: TtsDBGrid;
    DBNavigator1: TDBNavigator;
    BitBtn32: TBitBtn;
    BitBtn34: TBitBtn;
    Panel1: TPanel;
    PlaylistModeLabel: TLabel;
    DataEntryPanel: TPanel;
    Panel5: TPanel;
    Label1: TLabel;
    Label11: TLabel;
    LeagueTab: TTabControl;
    GamesDBGrid: TtsDBGrid;
    SponsorsDBGrid: TtsDBGrid;
    AvailableTemplatesDBGrid: TtsDBGrid;
    AddToPlaylistBtn: TBitBtn;
    InsertIntoPlaylistBtn: TBitBtn;
    SponsorDwellPanel: TPanel;
    Label5: TLabel;
    SponsorLogoDwell: TSpinEdit;
    N9: TMenuItem;
    SelectAll1: TMenuItem;
    N10: TMenuItem;
    DeSelectAll1: TMenuItem;
    Panel2: TPanel;
    ApdStatusLight1: TApdStatusLight;
    Label6: TLabel;
    ApdStatusLight2: TApdStatusLight;
    N11: TMenuItem;
    ReconnecttoGraphicsEngine1: TMenuItem;
    ScheduleEntryStartTime: TDateTimePicker;
    ScheduleEntryEndTime: TDateTimePicker;
    Label2: TLabel;
    BitBtn42: TBitBtn;
    PromptForData: TCheckBox;
    Label7: TLabel;
    EntryDwellTime: TSpinEdit;
    Panel3: TPanel;
    DateTimeLabel: TLabel;
    PlaylistNameLabel: TLabel;
    DBMaintPopupMenu: TPopupMenu;
    BitBtn1: TBitBtn;
    Label8: TLabel;
    LastSaveTimeLabel: TLabel;
    ManualLeaguePanel: TPanel;
    Label3: TLabel;
    ManualLeagueSelect: TComboBox;
    Image1: TImage;
    ManualOverridePanel: TPanel;
    ManualOverrideBtn: TBitBtn;
    ShowOddsOnly: TCheckBox;
    OddsDBGrid: TtsDBGrid;
    ImmediateModeBreakingNews: TCheckBox;
    N12: TMenuItem;
    ClearGraphicsOutput1: TMenuItem;
    N13: TMenuItem;
    ClearGraphicsOutput2: TMenuItem;
    TemplateTimeEditPopupMenu: TPopupMenu;
    EditDefaultTemplateStartEndEnableTimes1: TMenuItem;
    N14: TMenuItem;
    EditSponsorLogoDefinitions1: TMenuItem;
    EditTeamInformation1: TMenuItem;
    EditGamePhaseData1: TMenuItem;
    BreakingNewsIterations: TSpinEdit;
    EnableBreakingNews: TCheckBox;
    Label9: TLabel;
    EditCustomHeaders1: TMenuItem;
    SingleLoopEnable: TCheckBox;
    Label16: TLabel;
    DoubleLineCheck: TCheckBox;
    SingleLineCheck: TCheckBox;
    RegionSelect: TComboBox;
    RegionSelectMode: TRadioGroup;
    RegionLabel: TLabel;
    RegionIDNum: TEdit;
    IDLabel: TLabel;
    DatabaseMaintBreakingNewsPlaylistGrid: TtsDBGrid;
    TemplateListSaveControlsPnl: TPanel;
    SaveTemplateListBtn: TBitBtn;
    LoadTemplateListBtn: TBitBtn;
    SaveCloseTemplateListBtn: TBitBtn;
    DefaultTemplatesGrid: TtsGrid;
    DefaultTemplatesPopupMenu: TPopupMenu;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    MenuItem18: TMenuItem;
    MenuItem19: TMenuItem;
    Label10: TLabel;
    Enable2: TMenuItem;
    DisablebyDefault1: TMenuItem;
    N1: TMenuItem;
    PlaylistAutoSaveTimer: TTimer;
    Options1: TMenuItem;
    UseRankingsforNCAAB: TMenuItem;
    UseSeedforNCAAB: TMenuItem;
    ScheduledPlaylistsPopupMenu: TPopupMenu;
    DeleteSelectedScheduleEntry1: TMenuItem;
    DeletePlaylist1: TMenuItem;
    Label12: TLabel;
    PlayoutConnectLED: TApdStatusLight;
    PlayoutPanel: TPanel;
    AutoPlaybackEnableCheckbox: TCheckBox;
    LoadPlaylistBtn: TBitBtn;
    PlayNextField1Btn: TBitBtn;
    PlaySelectedField1Btn: TBitBtn;
    UnloadCurrentPlaylistBtn: TBitBtn;
    N15: TMenuItem;
    GotoRemotePlayoutModeMenu: TMenuItem;
    ExitRemotePlayoutModeMenu: TMenuItem;
    TriggerTickerBtn: TBitBtn;
    AbortPlaylistBtn: TBitBtn;
    ResetTickerBtn: TBitBtn;
    N16: TMenuItem;
    ReconnnecttoPlayoutController1: TMenuItem;
    StatusBar: TStatusBar;
    Label17: TLabel;
    EntryFieldsGrid2: TtsGrid;
    Label18: TLabel;
    Label20: TLabel;
    AddToPlaylist2Btn: TBitBtn;
    InsertIntoPlaylist2Btn: TBitBtn;
    Playlist2PopupMenu: TPopupMenu;
    Enable_2: TMenuItem;
    Disable_2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    MenuItem10: TMenuItem;
    Cut2: TMenuItem;
    Copy2: TMenuItem;
    MenuItem13: TMenuItem;
    Paste2: TMenuItem;
    AppendToEnd2: TMenuItem;
    MenuItem16: TMenuItem;
    Duplicate2: TMenuItem;
    DuplicateAndAppendToEnd2: TMenuItem;
    MenuItem21: TMenuItem;
    Delete2: TMenuItem;
    MenuItem23: TMenuItem;
    MenuItem25: TMenuItem;
    StartupTimer: TTimer;
    PlayNextField2Btn: TBitBtn;
    PlaySelectedField2Btn: TBitBtn;
    PlayNextBothBtn: TBitBtn;
    PlaySelectedBothBtn: TBitBtn;
    CitiesDBGrid: TtsDBGrid;
    ExitRemoteModeBtn: TBitBtn;
    GoToRemoteModeBtn: TBitBtn;
    N17: TMenuItem;
    ClearAllTemplatesOnAir1: TMenuItem;
    PlaylistGrid2: TtsGrid;
    PlaylistRunningLED: TApdStatusLight;
    PlaylistHaltedLED: TApdStatusLight;
    ReloadAllScenes1: TMenuItem;
    PlayoutSequencingLED: TApdStatusLight;
    Label21: TLabel;
    RemoveAllFieldsBtn: TBitBtn;
    DebounceTimer: TTimer;
    Traffic: TTabSheet;
    Panel6: TPanel;
    Panel7: TPanel;
    ViewTrafficTimesBtn: TBitBtn;
    ViewTrafficIncidentsBtn: TBitBtn;
    Label22: TLabel;
    Panel8: TPanel;
    Label23: TLabel;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    Panel9: TPanel;
    Label26: TLabel;
    ClearIncidentsBtn: TBitBtn;
    Label27: TLabel;
    NumEntries2: TLabel;
    EditTemplateFieldLengths: TMenuItem;
    Edit_Top_region_Image_Wheel: TMenuItem;
    Panel10: TPanel;
    ClockOnBtn: TBitBtn;
    ClockOffBtn: TBitBtn;
    Label28: TLabel;
    Label29: TLabel;

    //General program functions
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure LoadPrefs;
    procedure StorePrefs;

    //Dialog functions
    procedure About1Click(Sender: TObject);
    procedure SetPrefs1Click(Sender: TObject);

    //Handlers for UI objects
    procedure Exit1Click(Sender: TObject);
    procedure ProgramModePageControlChange(Sender: TObject);
    procedure ScheduledPlaylistsGridDblClick(Sender: TObject);
    procedure PlaylistGridDblClick(Sender: TObject);
    procedure PurgeScheduledEvents1DayOld1Click(Sender: TObject);
    procedure LeagueTabChange(Sender: TObject);
    procedure ExpandButtonClick(Sender: TObject);
    procedure DoubleLineCheckClick(Sender: TObject);
    procedure SingleLineCheckClick(Sender: TObject);
    procedure AddToPlaylistBtnClick(Sender: TObject);
    procedure InsertIntoPlaylistBtnClick(Sender: TObject);
    procedure AvailableTemplatesDBGridDblClick(Sender: TObject);
    procedure PlaylistGridRowChanged(Sender: TObject; OldRow, NewRow: Integer);
    procedure ScheduleSelectTabChange(Sender: TObject);
    procedure DatabaseMaintSelectTabChange(Sender: TObject);
    procedure PlaylistSelectTabControlChange(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn24Click(Sender: TObject);
    procedure BitBtn32Click(Sender: TObject);
    procedure BitBtn34Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn42Click(Sender: TObject);
    procedure BitBtn44Click(Sender: TObject);
    procedure BitBtn43Click(Sender: TObject);
    procedure Delete1Click(Sender: TObject);
    procedure Enable1Click(Sender: TObject);
    procedure Disable1Click(Sender: TObject);
    procedure Copy1Click(Sender: TObject);
    procedure Cut1Click(Sender: TObject);
    procedure Paste1Click(Sender: TObject);
    procedure AppendtoEnd1Click(Sender: TObject);
    procedure Duplicate1Click(Sender: TObject);
    procedure DuplicateandAppendtoEnd1Click(Sender: TObject);
    procedure SaveCloseTemplateListBtnClick(Sender: TObject);
    procedure MenuItem7Click(Sender: TObject);
    procedure MenuItem18Click(Sender: TObject);
    procedure MenuItem6Click(Sender: TObject);
    procedure Enable2Click(Sender: TObject);
    procedure DisablebyDefault1Click(Sender: TObject);
    procedure EditStartEndEnableTimes1Click(Sender: TObject);
    procedure UseRankingsforNCAABClick(Sender: TObject);
    procedure UseSeedforNCAABClick(Sender: TObject);
    procedure AvailablePlaylistsGridDblClick(Sender: TObject);
    procedure DeleteSelectedScheduleEntry1Click(Sender: TObject);
    procedure DeletePlaylist1Click(Sender: TObject);
    procedure GamesDBGridMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure RepopulateTeamsCollection1Click(Sender: TObject);
    procedure ReloadGamesfromGametrak1Click(Sender: TObject);
    procedure PlaylistGridKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SelectAll1Click(Sender: TObject);
    procedure DeSelectAll1Click(Sender: TObject);
    procedure PlaylistGridClick(Sender: TObject);
    procedure ReconnecttoGraphicsEngine1Click(Sender: TObject);
    procedure AvailableTemplatesDBGridRowChanged(Sender: TObject; OldRow, NewRow: Variant);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ShowOddsOnlyClick(Sender: TObject);
    procedure ImmediateModeBreakingNewsClick(Sender: TObject);
    procedure ClearGraphicsOutput1Click(Sender: TObject);
    procedure ClearGraphicsOutput2Click(Sender: TObject);
    procedure EditDefaultTemplateStartEndEnableTimes1Click(Sender: TObject);
    procedure EditSponsorLogoDefinitions1Click(Sender: TObject);
    procedure EditTeamInformation1Click(Sender: TObject);
    procedure EditGamePhaseData1Click(Sender: TObject);
    procedure EditCustomHeaders1Click(Sender: TObject);
    procedure GamesDBGridDblClick(Sender: TObject);
    procedure SingleLoopEnableClick(Sender: TObject);
    procedure DoubleLineCheckMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure SingleLineCheckMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure RegionSelectModeClick(Sender: TObject);
    procedure RegionSelectChange(Sender: TObject);
    procedure AddToPlaylist2BtnClick(Sender: TObject);
    procedure InsertIntoPlaylist2BtnClick(Sender: TObject);
    procedure Enable_2Click(Sender: TObject);
    procedure Disable_2Click(Sender: TObject);
    procedure PlaylistGrid2KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure PlaylistGrid2RowChanged(Sender: TObject; OldRow, NewRow: Integer);
    procedure PlaylistGrid2DblClick(Sender: TObject);

    //Playlist operations
    procedure LoadZipperCollection;
    procedure LoadPlaylistCollection(PlaylistType: SmallInt; AppendMode: SmallInt; PlaylistID: Double);
    function SaveTickerPlaylist(PlaylistType: SmallInt; ClearCollection: Boolean): Boolean;
    procedure SavePlaylistWithoutClear;
    procedure AddPlaylistEntry(CurrentTemplateID: SmallInt; AddAllDefaultTemplates: Boolean; GridID: SmallInt; var DataGrid: TtsGrid);
    procedure InsertPlaylistEntry(CurrentTemplateID: SmallInt; AddAllDefaultTemplates: Boolean; GridID: SmallInt; var DataGrid: TtsGrid);
    procedure RefreshEntryFieldsGrid(TickerMode: SmallInt; GridID: SmallInt; var DataGrid: TtsGrid);
    procedure RefreshPlaylistGrid(GridID: SmallInt; var DataGrid: TtsGrid);
    procedure EnableDisablePlaylistEntries(EnableState: Boolean; GridID: SmallInt; var DataGrid: TtsGrid);
    procedure RefreshScheduleGrid;
    procedure EditCurrentScheduleEntry;
    procedure AddScheduleEntry;
    procedure SetToSingleLineMode;
    procedure SetToDoubleLineMode;
    procedure DeleteSelectedScheduleEntry;
    procedure DeleteSelectedPlaylist;
    procedure DeleteZipperPlaylist (PlaylistType: SmallInt; Playlist_ID: Double; Playlist_Description: String);
    procedure DeletePlaylistFromSchedule(PlaylistType: SmallInt; Start_Enable_Time: Double; Station_ID: SmallInt);
    procedure DeleteGraphicFromZipperPlaylist(Confirm: Boolean; GridID: SmallInt; var DataGrid: TtsGrid);
    procedure EditZipperPlaylistEntry(GridID: SmallInt; var DataGrid: TtsGrid);

    //Utility functions
    function ScrubText (InText: String):String;
    procedure ReloadDataFromDatabase;
    function GetGamePhaseRec(League: String; GPhase: SmallInt): GamePhaseRec;
    function GetGameTimeString(GTimeStr: String): String;
    function GetStatInfo (StoredProcedureName: String): StatRec;
    function GetAutomatedLeagueDisplayMnemonic (League: String): String;
    function GetGameState(GTIME: String; GPHASE: SmallInt): Integer;
    function GetTemplateFieldsCount(Template_ID: SmallInt): SmallInt;
    function GetRecordTypeFromTemplateID(Template_ID: SmallInt): SmallInt;
    function GetTemplateInformation(TemplateID: SmallInt): TemplateDefsRec;
    function GetRecordTypeDescription (Playlist_Type: SmallInt; Record_Type: SmallInt): String;

    //Timer functions
    procedure TimeOfDayTimerTimer(Sender: TObject);
    procedure PlaylistAutoSaveTimerTimer(Sender: TObject);

    //Procedures for hotkey operations
    procedure EditStartEndEnableTimes(GridID: SmallInt; var DataGrid: TtsGrid);
    procedure Cut(GridID: SmallInt; var DataGrid: TtsGrid);
    procedure Copy(GridID: SmallInt; var DataGrid: TtsGrid);
    procedure Paste(GridID: SmallInt; var DataGrid: TtsGrid);
    procedure PasteEnd(GridID: SmallInt; var DataGrid: TtsGrid);
    procedure Duplicate(GridID: SmallInt; var DataGrid: TtsGrid);
    procedure DuplicateAndAppendToEnd(GridID: SmallInt; var DataGrid: TtsGrid);
    procedure SelectForCopyOrCut(GridID: SmallInt; var DataGrid: TtsGrid);
    procedure PasteEntries(AppendToEnd: Boolean; GridID: SmallInt; var DataGrid: TtsGrid);
    procedure DuplicateEntries(AppendToEnd: Boolean; DuplicationCount: SmallInt; GridID: SmallInt; var DataGrid: TtsGrid);
    procedure SelectDeselectAll(SelectMode: Boolean; GridID: SmallInt; var DataGrid: TtsGrid);
    procedure MoveRecord(Direction: SmallInt; GridID: SmallInt; var DataGrid: TtsGrid);
    procedure PreviewCurrentPlaylistRecord(GridID: SmallInt);
    procedure ManualGameOverride;
    function CheckForInvalidTemplates(CurrentDisplayMode: SmallInt): Boolean;
    procedure ReAssignTemplateIDsOnModeChange;
    function GetRecordTypeForTemplate(TemplateID: SmallInt): SmallInt;

    //Default templates functions
    procedure RefreshDefaultTemplatesGrid;
    procedure AddDefaultTemplateEntry(CurrentTemplateID: SmallInt);
    procedure InsertDefaultTemplateEntry(CurrentTemplateID: SmallInt);
    procedure LeagueTabChanging(Sender: TObject; var AllowChange: Boolean);
    procedure SaveDefaultTemplatesList(Category_ID: SmallInt; ClearCollection: Boolean);
    procedure LoadTemplateListBtnClick(Sender: TObject);
    procedure SaveTemplateListBtnClick(Sender: TObject);

    //Remote Playout Mode functions
    procedure GoToRemotePlayoutMode;
    procedure ExitRemotePlayoutMode;
    procedure GotoRemotePlayoutModeMenuClick(Sender: TObject);
    procedure ExitRemotePlayoutModeMenuClick(Sender: TObject);
    procedure AutoPlaybackEnableCheckboxClick(Sender: TObject);
    procedure TriggerTickerBtnClick(Sender: TObject);
    procedure AbortPlaylistBtnClick(Sender: TObject);
    procedure ResetTickerBtnClick(Sender: TObject);
    procedure LoadPlaylistBtnClick(Sender: TObject);
    procedure UnloadCurrentPlaylistBtnClick(Sender: TObject);
    procedure ReconnnecttoPlayoutController1Click(Sender: TObject);
    procedure PlayNextField1BtnClick(Sender: TObject);
    procedure PlaySelectedField1BtnClick(Sender: TObject);

    //For function key processing
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ProcessFunctionKey (Key: Word; IsCtrlKey: Boolean);
    procedure EditStartEndEnableTimes2Click(Sender: TObject);
    procedure SelectAll2Click(Sender: TObject);
    procedure DeSelectAll2Click(Sender: TObject);
    procedure Cut2Click(Sender: TObject);
    procedure Copy2Click(Sender: TObject);
    procedure Paste2Click(Sender: TObject);
    procedure AppendToEnd2Click(Sender: TObject);
    procedure Duplicate2Click(Sender: TObject);
    procedure DuplicateAndAppendToEnd2Click(Sender: TObject);
    procedure Delete2Click(Sender: TObject);
    procedure StartupTimerTimer(Sender: TObject);
    procedure PlaylistGrid2Click(Sender: TObject);

    //Channel Box functions
    procedure RunStartupCommands;
    procedure ExitRemoteModeBtnClick(Sender: TObject);
    procedure GoToRemoteModeBtnClick(Sender: TObject);
    procedure ClearAllTemplates;
    procedure PlayNextField2BtnClick(Sender: TObject);
    procedure PlaySelectedField2BtnClick(Sender: TObject);
    procedure PlayNextBothBtnClick(Sender: TObject);
    procedure PlaySelectedBothBtnClick(Sender: TObject);
    procedure ClearAllTemplatesOnAir1Click(Sender: TObject);
    procedure ReloadAllScenes1Click(Sender: TObject);
    procedure RemoveAllFieldsBtnClick(Sender: TObject);

    //For switch de-bounce
    procedure EnableRemoteControlButtons;
    procedure DisableRemoteControlButtons;
    procedure DebounceTimerTimer(Sender: TObject);
    procedure ViewTrafficTimesBtnClick(Sender: TObject);
    procedure ViewTrafficIncidentsBtnClick(Sender: TObject);
    procedure ClearIncidentsBtnClick(Sender: TObject);
    procedure EditTemplateFieldLengthsClick(Sender: TObject);
    procedure Edit_Top_region_Image_WheelClick(Sender: TObject);

    procedure ClockOnBtnClick(Sender: TObject);
    procedure ClockOffBtnClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

uses Preferences,     //Prefs dialog
     AboutBox,        //About box
     DataModule,      //Data module
     SearchDataEntry, //Dialog used to enter text to search for
     GeneralFunctions,//General purpose functions
     PlaylistSelect,  //Dialog for selecting playlist to load
     PlaylistGraphicsViewer,  //Viewer for Zipper playlists
     ZipperEntryEditor,  //Editor dialog for playlist entries
     ScheduleEntryTimeEditor, //Editor for schedule entry times
     NoteEntryEditor, //Dialog for game note entry & editing
     NCAAManualGameEntry, //Dialog for manual NCAA entries
     ZipperEntry, //Dialog for basic playlist element entry
     DuplicationCountSelect, //Dialog for selecting the number of duplicate records to generate
     EnableDateTimeEditor,
     EngineConnectionPreferences,
     EngineIntf,
     ManualGameEditor,
     SponsorLogoEditor,
     DatabaseEditor,
     GamePhaseEditor,
     CustomCategoryEditor,
     BreakingNewsPlaylistSelect,
     GameDataFunctions,
     TemplateSelect,
     PlayoutInterfaceForm,
     TrafficTimesEditor,
     TrafficIncidentsEditor,
     //Added for Version 2.0
     TemplateFieldLengthEditor,
     TopImageWheelEditor;

{$R *.DFM}
////////////////////////////////////////////////////////////////////////////////
//Procedures to delete data pointers in each collection node
////////////////////////////////////////////////////////////////////////////////
procedure Ticker_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TickerRec));
end;
procedure Sponsor_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(BreakingNewsRec));
end;
procedure Temp_Ticker_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TickerRec));
end;
procedure Temp_Sponsor_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(BreakingNewsRec));
end;
procedure Team_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TeamRec));
end;
procedure Ticker_Playout_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TickerRec));
end;
procedure SponsorLogo_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(SponsorLogoRec));
end;
procedure PromoLogo_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(PromoLogoRec));
end;
procedure Stat_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(StatRec));
end;
procedure Temp_Stat_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(StatRec));
end;
procedure Game_Phase_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(GamePhaseRec));
end;
procedure Template_Defs_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TemplateDefsRec));
end;
procedure Child_Template_ID_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(ChildTemplateIDRec));
end;
procedure Temporary_Fields_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TemplateFieldsRec));
end;
procedure Template_Fields_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TemplateFieldsRec));
end;
procedure Categories_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(CategoryRec));
end;
procedure Category_Templates_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(CategoryTemplatesRec));
end;
procedure RecordType_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(RecordTypeRec));
end;
procedure StyleChip_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(StyleChipRec));
end;
procedure LeagueCode_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(LeagueCodeRec));
end;
procedure Automated_League_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(AutomatedLeagueRec));
end;
procedure PlayoutStationInfo_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(PlayoutStationInfoRec));
end;
procedure Default_Templates_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(DefaultTemplatesRec));
end;
procedure Selected_Templates_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(DefaultTemplatesRec));
end;
procedure Scene_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(SceneRec));
end;
procedure LBar_Weather_Icon_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(LBarWeatherIconRec));
end;
procedure Top_Graphic_Collection_DisposeData(Data : pointer); far;
begin
  FreeMem(Data,SizeOf(TopGraphicRec));
end;

////////////////////////////////////////////////////////////////////////////////
// GENERAL PROGRAM PROCEDURES, FUNCTIONS AND HANDLERS
////////////////////////////////////////////////////////////////////////////////
//Handler for form activiation - inits done here
procedure TMainForm.FormActivate(Sender: TObject);
var
  i: SmallInt;
  Year, Month, Day: Word;
  SceneRecPtr: ^SceneRec;
begin
  //Set size
  //MainForm.Height := 1024;
  //MainForm.Width := 1290;
  //MainForm.Height := 900;
  //MainForm.Width := 1171;
  MainForm.Height := 913;
  MainForm.Width := 1450;

  //Set defaults
  LastPageIndex := 0;
  ProgramModePageControl.ActivePageIndex := 0;
  LeagueTab.TabIndex := 0;
  FormatForSNY := TRUE;
  PlaylistRunningLED.Visible := FALSE;
  PlaylistHaltedLED.Visible := FALSE;

  //Init for 2-line mode ticker, looping
  TickerDisplayMode := 4;

  //Load pereferences file
  LoadPrefs;

  //Set button de-bounce duration
  DebounceTimer.Interval := DebounceDuration;

  //Set menu options for seeding
  if (UseSeedingForNCAABGames) then
  begin
    UseRankingsforNCAAB.Checked := FALSE;
    UseSeedforNCAAB.Checked := TRUE;
  end
  else begin
    UseRankingsforNCAAB.Checked := TRUE;
    UseSeedforNCAAB.Checked := FALSE;
  end;

  //Create collections
  Ticker_Collection := TStCollection.Create(1500);
  Ticker_Collection2 := TStCollection.Create(1500);
  Ticker_Collection3 := TStCollection.Create(1500);
  BreakingNews_Collection := TStCollection.Create(500);
  Temp_Ticker_Collection := TStCollection.Create(1500);
  Temp_BreakingNews_Collection := TStCollection.Create(500);
  Team_Collection := TStCollection.Create(5000);
  Ticker_Playout_Collection := TStCollection.Create(1500);
  Overlay_Collection := TStCollection.Create(250);
  SponsorLogo_Collection := TStCollection.Create(100);
  PromoLogo_Collection := TStCollection.Create(100);
  Stat_Collection := TStCollection.Create(250);
  Temp_Stat_Collection := TStCollection.Create(250);
  Game_Phase_Collection := TStCollection.Create(500);
  Template_Defs_Collection := TStCollection.Create(500);
  Child_Template_ID_Collection := TStCollection.Create(250);
  Template_Fields_Collection := TStCollection.Create(5000);
  Temporary_Fields_Collection := TStCollection.Create(100);
  Categories_Collection := TStCollection.Create(50);
  Category_Templates_Collection := TStCollection.Create(500);
  Sports_Collection := TStCollection.Create(50);
  RecordType_Collection := TStCollection.Create(250);
  StyleChip_Collection := TStCollection.Create(100);
  LeagueCode_Collection := TStCollection.Create(1000);
  Automated_League_Collection := TStCollection.Create(50);
  PlayoutStationInfo_Collection := TStCollection.Create(10);
  Default_Templates_Collection := TStCollection.Create(500);
  Selected_Templates_Collection := TStCollection.Create(250);
  Scene_Collection := TStCollection.Create(100);
  LBar_Weather_Icon_Collection := TStCollection.Create(100);
  //Added for Version 2.0
  Top_Graphic_Collection := TStCollection.Create(50);

  Ticker_Collection.DisposeData := Ticker_Collection_DisposeData;
  Ticker_Collection2.DisposeData := Ticker_Collection_DisposeData;
  Ticker_Collection3.DisposeData := Ticker_Collection_DisposeData;
  BreakingNews_Collection.DisposeData := Sponsor_Collection_DisposeData;
  Temp_Ticker_Collection.DisposeData := Temp_Ticker_Collection_DisposeData;
  Temp_BreakingNews_Collection.DisposeData := Temp_Sponsor_Collection_DisposeData;
  Team_Collection.DisposeData := Team_Collection_DisposeData;
  Ticker_Playout_Collection.DisposeData := Ticker_Playout_Collection_DisposeData;
  SponsorLogo_Collection.DisposeData := SponsorLogo_Collection_DisposeData;
  PromoLogo_Collection.DisposeData := PromoLogo_Collection_DisposeData;
  Stat_Collection.DisposeData := Stat_Collection_DisposeData;
  Temp_Stat_Collection.DisposeData := Temp_Stat_Collection_DisposeData;
  Game_Phase_Collection.DisposeData := Game_Phase_Collection_DisposeData;
  Template_Defs_Collection.DisposeData := Template_Defs_Collection_DisposeData;
  Child_Template_ID_Collection.DisposeData := Child_Template_ID_Collection_DisposeData;
  Template_Fields_Collection.DisposeData := Template_Fields_Collection_DisposeData;
  Temporary_Fields_Collection.DisposeData := Temporary_Fields_Collection_DisposeData;
  Categories_Collection.DisposeData := Categories_Collection_DisposeData;
  Category_Templates_Collection.DisposeData := Category_Templates_Collection_DisposeData;
  RecordType_Collection.DisposeData := RecordType_Collection_DisposeData;
  StyleChip_Collection.DisposeData := StyleChip_Collection_DisposeData;
  LeagueCode_Collection.DisposeData := LeagueCode_Collection_DisposeData;
  Automated_League_Collection.DisposeData := Automated_League_Collection_DisposeData;
  PlayoutStationInfo_Collection.DisposeData := PlayoutStationInfo_Collection_DisposeData;
  Default_Templates_Collection.DisposeData := Default_Templates_Collection_DisposeData;
  Selected_Templates_Collection.DisposeData := Selected_Templates_Collection_DisposeData;
  Scene_Collection.DisposeData := Scene_Collection_DisposeData;
  LBar_Weather_Icon_Collection.DisposeData := LBar_Weather_Icon_Collection_DisposeData;
  //Added for Version 2.0
  Top_Graphic_Collection.DisposeData := Top_Graphic_Collection_DisposeData;

  //Activate database & tables
  With dmMain do
  begin
    dbTicker.Connected := FALSE;
    dbTicker.ConnectionString := DBConnectionString;
    dbTicker.Connected := TRUE;
    tblTicker_Groups.Active := TRUE;
    tblTicker_Elements.Active := TRUE;
    tblScheduled_Ticker_Groups.Active := TRUE;
    tblBreakingNews_Groups.Active := TRUE;
    tblBreakingNews_Elements.Active := TRUE;
    tblWeather_Cities.Active := TRUE;
    //tblScheduled_BreakingNews_Groups.Active := TRUE;
    tblSponsor_Logos.Active := TRUE;
    dbSportbase.Connected := FALSE;
    dbSportbase.ConnectionString := DBConnectionString2;
    dbSportbase.Connected := TRUE;
    dbTraffic.Connected := FALSE;
    dbTraffic.ConnectionString := DBConnectionString3;
    dbTraffic.Connected := TRUE;
  end;

  //Call procedure to load data from database
  ReloadDataFromDatabase;

  //Init checkbox for one loop through if enabled
  if (DefaultToOneLoopThrough) then SingleLoopEnable.Checked := TRUE
  else SingleLoopEnable.Checked := FALSE;

  //Init local playlist controls
  if (EnableLocalPlaylistControls) then
  begin
    RegionSelectMode.Visible := TRUE;
    RegionLabel.Visible := TRUE;
    IDLabel.Visible := TRUE;
    RegionSelect.Visible := TRUE;
    RegionIDNum.Visible := TRUE;
  end
  else begin
    RegionSelectMode.Visible := FALSE;
    RegionLabel.Visible := FALSE;
    IDLabel.Visible := FALSE;
    RegionSelect.Visible := FALSE;
    RegionIDNum.Visible := FALSE;
  end;

  //Init date/time pickers & combo boxes
  ScheduleEntryStartDate.Date := Now;
  ScheduleEntryEndDate.Date := Now+1;
  ScheduleEntryStartTime.Time := 0;
  ScheduleEntryEndTime.Time := 0;
  EntryStartEnableDate.Date := Now;
  EntryStartEnableTime.Time := 0;
  DecodeDate(Now, Year, Month, Day);
  //Allow for leap year
  if (Month = 2) AND (Day = 29) then Day := 28;
  EntryEndEnableDate.Date := EncodeDate(Year+10, Month, Day);
  EntryEndEnableTime.Time := 1-(1/(24*60));

  //Force update of games grid
  LeagueTab.OnChange(self);

  //Set playlist type tab names
  PlaylistSelectTabControl.Tabs.Clear;
  //Modified for L-Bar
  //PlaylistSelectTabControl.Tabs.Add('Ticker');
  //PlaylistSelectTabControl.Tabs.Add('Alerts');
  //if (EnableDefaultTemplates) then
  //  PlaylistSelectTabControl.Tabs.Add('Templates');
  PlaylistSelectTabControl.Tabs.Add('Side Panel');

  //Connect to the graphics engine if it's enabled
  if (EngineParameters.Enabled = TRUE) then
  begin
    try
      EngineParameters.Enabled := TRUE;
      EngineInterface.EnginePort.Address := EngineParameters.IPAddress;
      EngineInterface.EnginePort.Port := StrToIntDef(EngineParameters.Port, 7795);
      EngineInterface.EnginePort.Active := TRUE;
    except
      if (ErrorLoggingEnabled = True) then
      begin
        Error_Condition := True;
        Label16.Caption := 'ERROR';
        //WriteToErrorLog
        EngineInterface.WriteToErrorLog('Error occurred while trying connect to Channel Box');
      end;
    end;
  end;

  //Connect to the Playout Controller for remote control if enabled
  if (PlayoutControllerInfo.Enabled = TRUE) then
  begin
    try
      EngineParameters.Enabled := TRUE;
      PlayoutInterface.PlayoutConnection.Address := PlayoutControllerInfo.IPAddress;
      PlayoutInterface.PlayoutConnection.Port := PlayoutControllerInfo.Port;
      PlayoutInterface.PlayoutConnection.Active := TRUE;
    except
      if (ErrorLoggingEnabled = True) then
      begin
        Error_Condition := True;
        Label16.Caption := 'ERROR';
        //WriteToErrorLog
        EngineInterface.WriteToErrorLog('Error occurred while trying connect to Playout Controller');
      end;
    end;
  end;

  //Set default ticker mode
  if (DefaultTickerMode = 2) then DoubleLineCheck.Checked := TRUE
  else SingleLineCheck.Checked := TRUE;

  //Setup playlist auto-save
  if (PlaylistAutoSaveEnable) then
  begin
    PlaylistAutoSaveTimer.Interval := PlaylistAutoSaveInterval*60000;
    PlaylistAutoSaveTimer.Enabled := TRUE;
  end;

  //Setup status bar
  StatusBar.Font.Style := [fsBold];

  //Setup timer to call function that loads & plays scenes at startup
  StartupTimer.Enabled := TRUE;
end;

//Function to run scene setup commands at startup
procedure TMainForm.RunStartupCommands;
var
  i: SmallInt;
  SponsorLogoRecPtr: ^SponsorLogoRec;
  SponsorLogoPath: String;
begin
  //Load in startup command information
  with dmMain.Query1 do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT * FROM Startup_Commands');
    Open;
    if (RecordCount > 0) then
    begin
      First;
      repeat
        //Send command
        EngineInterface.UpdateScene(FieldByName('SceneID').AsInteger, FieldByName('Command').AsString);
        //Dwell
        Sleep (FieldByName('Dwell').AsInteger);
        //Go to next record
        dmMain.Query1.Next;
      until (dmMain.Query1.EOF = TRUE); //Repeat until end of dataset
    end;
    //Load the default sponsor logo
    if (SponsorLogo_Collection.Count > 0) then
    begin
      for i := 0 to SponsorLogo_Collection.Count-1 do
      begin
        SponsorLogoRecPtr := SponsorLogo_Collection.At(i);
        //If it's the default logo, update the sponsor logo region
        if (SponsorLogoRecPtr^.IsDefaultLogo) then
        begin
          SponsorLogoPath := Sponsor_Logo_Base_Path + '\' + SponsorLogoRecPtr^.SponsorLogoFilename;
          SponsorLogoPath := StringReplace(SponsorLogoPath, '\', '/', [rfReplaceAll]);
          EngineInterface.UpdateScene(SPONSOR_LOGO_SCENE_ID, SPONSOR_LOGO_FILE_UPDATE_COMMAND + '�' + SponsorLogoPath);
          EngineInterface.UpdateScene(SPONSOR_LOGO_SCENE_ID, SPONSOR_LOGO_IN_COMMAND + '�' + 'T');
        end;
      end;
    end;
  end;
end;

//Handler for startup timer
procedure TMainForm.StartupTimerTimer(Sender: TObject);
var
  i: SmallInt;
  SceneRecPtr: ^SceneRec;
begin
  //Disable to prevent re-triggering
  StartupTimer.Enabled := FALSE;

  //Check if connected to engine
  if (EngineInterface.EnginePort.Active) then
  begin
    //Query scenes for initial status
    if (Scene_Collection.Count > 0) then
    begin
      for i := 0 to Scene_Collection.Count-1 do
      begin
        SceneRecPtr := Scene_Collection.At(i);
        //Clear status
        SceneRecPtr^.Scene_Status := SCENE_NOSTATUS;
        //Request current status
        EngineInterface.QuerySceneStatus(SceneRecPtr^.Scene_ID);
      end;
    end;

    //Delay
    for i := 1 to 200 do
    begin
      Sleep(10);
      Application.ProcessMessages;
    end;

    //Load scenes
    if (LoadScenesAtStartup) then EngineInterface.SetupInitialScenes;
    //Run startup commands
    RunStartupCommands;
    //Clear all graphics
    ClearAllTemplates;

    //Added for Version 2.0
    EngineInterface.SetNextTopGraphic;
  end
  else MessageDlg('Not connected to graphics engine - scenes will not be loaded.',
                  mtInformation, [mbOk], 0);
end;

//Handler for program exit from main menu
procedure TMainForm.Exit1Click(Sender: TObject);
begin
  Close;
end;

//Handler for main program form close
procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
var
  OKToClose: Boolean;
begin
  //Init
  OkToClose := TRUE;
  //Confirm with operator}
  if (MessageDlg('Are you sure you want to exit the Comcast L-Bar Authoring application?',
                  mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
  begin
    //Prompt for save of playlist before exiting
    if (Ticker_Collection.Count > 0) then
    begin
      if (MessageDlg('Do you wish to save the contents of the current L-Bar playlist before exiting?',
                      mtWarning, [mbYes, mbNo], 0) = mrYes) then
      begin
        //Save the playlist
        if (SaveTickerPlaylist(TICKER, TRUE) = FALSE) then OKToClose := FALSE;
      end;
    end;
    //Prompt for save of playlist before exiting
    if (BreakingNews_Collection.Count > 0) AND (OKToClose = TRUE) then
    begin
      if (MessageDlg('Do you wish to save the contents of the current Alerts playlist before exiting?',
                      mtWarning, [mbYes, mbNo], 0) = mrYes) then
      begin
        //Save the playlist
        if (SaveTickerPlaylist(BREAKINGNEWS, TRUE) = FALSE) then OKToCLose := FALSE;
      end;
    end;
    //Set action for main form
    if (OKToClose) then
    begin
      //Close database connections
      dmMain.dbTicker.Connected := FALSE;
      dmMain.dbSportbase.Connected := FALSE;
      //Set playout to OFFLINE (not remote) mode
      ExitRemotePlayoutMode;
      //Close connection to Playout
      PlayoutInterface.PlayoutConnection.Active := FALSE;
      Action := caFree;
    end
    else Action := caNone;
  end
  else
     Action := caNone;
end;

//Procedure to load user preferences
procedure TMainForm.LoadPrefs;
var
  PrefsINI : TINIFile;
begin
  try
    //Create the INI file
    PrefsINI := TINIFile.Create(ExtractFilePath(Application.EXEName) + 'Comcast_Authoring.ini');
    //Read the settings
    StationID := PrefsINI.ReadInteger('General Settings', 'StationID', 1);
    ForceUpperCase := PrefsINI.ReadBool('General Settings', 'Force Upper Case', FALSE);
    AutoTrimText := PrefsINI.ReadBool('General Settings', 'Auto Trim Edited Text', TRUE);
    ForceUpperCaseGameInfo := PrefsINI.ReadBool('General Settings', 'Force Upper Case Game Info', TRUE);
    SpellCheckerDictionaryDir := PrefsINI.ReadString('General Settings', 'Spellchecker Dictionary Path',
      ExtractFilePath(Application.EXEName) + '\Dictionaries');
    //Get game start time offset - expressed in hours, so divide by 24 to get value
    GameStartTimeOffset := (PrefsINI.ReadInteger('General Settings', 'Game Start Time Offset from ET (hours)', 0))/24;
    TimeZoneSuffix := PrefsINI.ReadString('General Settings', 'Time Zone Suffix', 'ET');
    DefaultTickerMode := PrefsINI.ReadInteger('General Settings', 'Default Ticker Mode', 2);
    EnablePersistentSponsorLogo := PrefsINI.ReadBool('General Settings', 'Enable Persistent Sponsor Logo', TRUE);
    EnableTemplateFieldFormatting := PrefsINI.ReadBool('General Settings', 'Enable Template Field Formatting', TRUE);
    DefaultToOneLoopThrough  := PrefsINI.ReadBool('General Settings', 'Default to One Loop Through', TRUE);
    BlankOutLineupForSponsors := PrefsINI.ReadBool('General Settings', 'Blank Out Lineup For Sponsors', TRUE);
    EnableLocalPlaylistControls := PrefsINI.ReadBool('General Settings', 'Enable Local Playlist Controls', TRUE);
    DebugMode := PrefsINI.ReadBool('General Settings', 'Debug Mode', FALSE);
    AllowAlertBackgroundsForNews := PrefsINI.ReadBool('General Settings', 'Allow Alert Backgrounds for News', FALSE);
    EnableDefaultTemplates := PrefsINI.ReadBool('General Settings', 'Enable Default Templates', TRUE);
    ShowTimeAndDay := PrefsINI.ReadBool('General Settings', 'Show Time and Day for Schedule', FALSE);
    EnableCrawlTemplates := PrefsINI.ReadBool('General Settings', 'Enable Crawl Templates', FALSE);

    PlaylistAutoSaveEnable := PrefsINI.ReadBool('General Settings', 'Playlist Auto-Save Enable', TRUE);
    PlaylistAutoSaveInterval := PrefsINI.ReadInteger('General Settings', 'Playlist Auto-Save Interval (Minutes)', 2);

    //FormatForSNY := PrefsINI.ReadBool('General Settings', 'Format for SNY', FALSE);

    UseManualRankings_NCAAB := PrefsINI.ReadBool('General Settings', 'Use Manual Rankings (NCCAB)', FALSE);
    UseManualRankings_NCAAF := PrefsINI.ReadBool('General Settings', 'Use Manual Rankings (NCCAF)', FALSE);
    UseManualRankings_NCAAW := PrefsINI.ReadBool('General Settings', 'Use Manual Rankings (NCCAW)', FALSE);
    UseSeedingForNCAABGames := PrefsINI.ReadBool('General Settings', 'Use Seeding for NCAAB Games', FALSE);

    LoadScenesAtStartup := PrefsINI.ReadBool('General Settings', 'Load Scenes at Startup', FALSE);

    AlwaysPlayAllScenesAtStartup := PrefsINI.ReadBool('General Settings', 'Always Play All Scenes at Startup', TRUE);

    DebounceDuration := PrefsINI.ReadInteger('General Settings', 'Remote Control Button De-Bounce Duration (mS)', 1000);

    GameScheduleDaysBack := PrefsINI.ReadInteger('Game Schedule Settings', 'Days Back', 7);
    GameScheduleDaysForward := PrefsINI.ReadInteger('Game Schedule Settings', 'Days Forward', 7);

    DBConnectionString := PrefsINI.ReadString('Database Connections', 'Ticker Database Connection String',
      'Provider=SQLOLEDB.1;Persist Security Info=True;User ID=sa;password=Vds@dmin1;' +
      'Initial Catalog=SNYTicker;Data Source=(local)');
    DBConnectionString2 := PrefsINI.ReadString('Database Connections', 'Sportbase Database Connection String',
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;User ID=sa;' +
      'Initial Catalog=Sportbase;Data Source=(local)');
    DBConnectionString3 := PrefsINI.ReadString('Database Connections', 'Traffic Database Connection String',
      'Provider=SQLOLEDB.1;Persist Security Info=True;User ID=sa;Password=Vds@dmin1;' +
      'Initial Catalog=Metro_Traffic;Data Source=(local)');


    EngineParameters.Enabled := PrefsINI.ReadBool('Ticker Engine Connection', 'Enabled', FALSE);
    EngineParameters.IPAddress := PrefsINI.ReadString('Ticker Engine Connection', 'IP Address',
      '127.0.0.1');
    EngineParameters.Port := PrefsINI.ReadString('Ticker Engine Connection', 'Port', '7795');
    //Game Final Glyph variables
    UseGameFinalGlyph := PrefsINI.ReadBool('Format Settings', 'Enable Game Final Glyph', FALSE);
    GameFinalGlyphCharacterID  := PrefsINI.ReadInteger('Format Settings', 'Game Final Glyph Character ID', 239);

    //For connection to Playout Controller
    PlayoutControllerInfo.Enabled := PrefsINI.ReadBool('Playout Controller Remote Settings', 'Connection Enable', FALSE);
    PlayoutControllerInfo.IPAddress := PrefsINI.ReadString('Playout Controller Remote Settings', 'IP Address', '127.0.0.1');
    PlayoutControllerInfo.Port := PrefsINI.ReadInteger('Playout Controller Remote Settings', 'Port', 3390);

    //For image paths
    League_Logo_Mapped_Drive := PrefsINI.ReadString('Image Path Settings', 'League Logo Mapped Drive', 'E:');
    League_Logo_Host_Drive := PrefsINI.ReadString('Image Path Settings', 'League Logo Host Drive', 'C:\VDS_Projects');
    League_Logo_Base_Path := PrefsINI.ReadString('Image Path Settings', 'League Logo Base Path', 'VDS_Projects\Comcast_2011\CSNMA_LBar\Logos');
    Team_Logo_Mapped_Drive := PrefsINI.ReadString('Image Path Settings', 'Team Logo Mapped Drive', 'E:');
    Team_Logo_Host_Drive := PrefsINI.ReadString('Image Path Settings', 'Team Logo Host Drive', 'C:\VDS_Projects');
    Team_Logo_Base_Path := PrefsINI.ReadString('Image Path Settings', 'Team Logo Base Path', 'VDS_Projects\Comcast_2011\CSNMA_LBar\Logos');
    Player_Headshot_Mapped_Drive := PrefsINI.ReadString('Image Path Settings', 'Player Headshot Mapped Drive', 'E:');
    Player_Headshot_Host_Drive := PrefsINI.ReadString('Image Path Settings', 'Player Headshot Host Drive', 'C:\VDS_Projects');
    Player_Headshot_Base_Path := PrefsINI.ReadString('Image Path Settings', 'Player Headshot Base Path', 'VDS_Projects\Comcast_2011\CSNMA_LBar\Headshots');
    Weather_Icon_Base_Path := PrefsINI.ReadString('Image Path Settings', 'Weather Icon Base Path', 'c:\VDS_Projects\Comcast_2011\CSNMA_LBar\Weather_Icons');
    Sponsor_Logo_Base_Path := PrefsINI.ReadString('Image Path Settings', 'Sponsor Logo Base Path', 'c:\VDS_Projects\Comcast_2011\CSNMA_LBar\Sponsors');
    //Added for Version 2.0
    Full_Promo_Graphic_Mapped_Drive := PrefsINI.ReadString('Image Path Settings', 'Full Promo Graphic Mapped Drive', 'E:');
    Full_Promo_Graphic_Host_Drive := PrefsINI.ReadString('Image Path Settings', 'Full Promo Graphic Host Drive', 'C:\VDS_Projects');
    Full_Promo_Graphic_Base_Path := PrefsINI.ReadString('Image Path Settings', 'Full Promo Graphic Base Path', 'VDS_Projects\Comcast_2011\CSNMA_LBar\Promos');
    Top_Region_Image_Mapped_Drive := PrefsINI.ReadString('Image Path Settings', 'Top Region Image Mapped Drive', 'E:');
    Top_Region_Image_Host_Drive := PrefsINI.ReadString('Image Path Settings', 'Top Region Image Host Drive', 'C:\VDS_Projects');
    Top_Region_Image_Base_Path := PrefsINI.ReadString('Image Path Settings', 'Top Region Image Base Path', 'VDS_Projects\Comcast_2011\CSNMA_LBar\Promos');
    Base_Element_Path := PrefsINI.ReadString('Image Path Settings', 'Base Element Path', 'M:\NBCSN\');
    End_Element_Path := PrefsINI.ReadString('Image Path Settings', 'End Element Path', 'ELEMENTS');
    //Added for Version 2.0 - Image filenames
    Headshot_Backplate_Filename := PrefsINI.ReadString('Image Filename', 'Headshot Backplate Filename', 'ELEMENT 022.png');
    Team_Logo_Headshot_Filename := PrefsINI.ReadString('Image Filename', 'Team Logo Headshot Filename', 'ELEMENT 038.png');
    Team_Chip_A_Filename := PrefsINI.ReadString('Image Filename', 'Team Chip A Filename', 'ELEMENT 053.png');
    Team_Chip_B_Filename := PrefsINI.ReadString('Image Filename', 'Team Chip B Filename', 'ELEMENT 060.png');
    Team_Color_Filename := PrefsINI.ReadString('Image Filename', 'Team Color Filename', 'ELEMENT 085.png');
    Flag_Logo_Headshot_Filename := PrefsINI.ReadString('Image Filename', 'Flag Logo Headshot Filename', 'ELEMENT 013.png');

    //For base scene info
    BaseScene_SceneID := PrefsINI.ReadInteger('Base Scene Info', 'Base Scene ID', 10001001);
    BaseScene_Field_1_In_Command := PrefsINI.ReadString('Base Scene Info', 'Field 1 In Command', 'Field_1_In_Btn');
    BaseScene_Field_1_Out_Command := PrefsINI.ReadString('Base Scene Info', 'Field 1 Out Command', 'Field_1_Out_Btn');
    BaseScene_Field_2_In_Command := PrefsINI.ReadString('Base Scene Info', 'Field 2 In Command', 'Field_2_In_Btn');
    BaseScene_Field_2_Out_Command := PrefsINI.ReadString('Base Scene Info', 'Field 2 Out Command', 'Field_2_Out_Btn');
    BaseScene_Field_3_In_Command := PrefsINI.ReadString('Base Scene Info', 'Field 3 In Command', 'Field_3_In_Btn');
    BaseScene_Field_3_Out_Command := PrefsINI.ReadString('Base Scene Info', 'Field 3 Out Command', 'Field_3_Out_Btn');
    Sponsor_Logo_Scene_ID := PrefsINI.ReadInteger('Sponsor Logo Scene Info', 'Sponsor Logo Scene ID', 10001002);
    Full_Page_Promo_Scene_ID := PrefsINI.ReadInteger('Full Page Promo Scene Info', 'Full Page Promo Scene ID', 10007000);
    Full_Page_Promo_Template := PrefsINI.ReadInteger('Full Page Promo Scene Info', 'Full Page Promo Template', 31);

  finally
    //Free up the object
    PrefsINI.Free;
  end;
end;

//Procedure to store user preferences
procedure TMainForm.StorePrefs;
var
  PrefsINI : TINIFile;
begin
  try
    //Create the INI file
    PrefsINI := TINIFile.Create(ExtractFilePath(Application.EXEName) + 'Comcast_Authoring.ini');
    //Save the settings
    PrefsINI.WriteInteger('General Settings', 'StationID', StationID);
    PrefsINI.WriteBool('General Settings', 'Force Upper Case', ForceUpperCase);
    PrefsINI.WriteBool('General Settings', 'Force Upper Case Game Info', ForceUpperCaseGameInfo);
    PrefsINI.WriteBool('General Settings', 'Auto Trim Edited Text', AutoTrimText);
    PrefsINI.WriteString('General Settings', 'Spellchecker Dictionary Path', SpellCheckerDictionaryDir);
    PrefsINI.WriteInteger('General Settings', 'Game Start Time Offset from ET (hours)', Trunc(GameStartTimeOffset*24));
    PrefsINI.WriteString('General Settings', 'Time Zone Suffix', TimeZoneSuffix);
    PrefsINI.WriteInteger('General Settings', 'Default Ticker Mode', DefaultTickerMode);
    PrefsINI.WriteBool('General Settings', 'Enable Persistent Sponsor Logo', EnablePersistentSponsorLogo);
    PrefsINI.WriteBool('General Settings', 'Enable Template Field Formatting', EnableTemplateFieldFormatting);
    PrefsINI.WriteBool('General Settings', 'Default to One Loop Through', DefaultToOneLoopThrough);
    PrefsINI.WriteBool('General Settings', 'Blank Out Lineup For Sponsors', BlankOutLineupForSponsors);
    PrefsINI.WriteBool('General Settings', 'Enable Local Playlist Controls', EnableLocalPlaylistControls);
    PrefsINI.WriteBool('General Settings', 'Debug Mode', DebugMode);
    PrefsINI.WriteBool('General Settings', 'Allow Alert Backgrounds for News', AllowAlertBackgroundsForNews);
    PrefsINI.WriteBool('General Settings', 'Enable Default Templates', EnableDefaultTemplates);
    PrefsINI.WriteBool('General Settings', 'Show Time and Day for Schedule', ShowTimeAndDay);
    PrefsINI.WriteBool('General Settings', 'Enable Crawl Templates', EnableCrawlTemplates);

    PrefsINI.WriteBool('General Settings', 'Playlist Auto-Save Enable', PlaylistAutoSaveEnable);
    PrefsINI.WriteInteger('General Settings', 'Playlist Auto-Save Interval (Minutes)', PlaylistAutoSaveInterval);

    //PrefsINI.WriteBool('General Settings', 'Format for SNY', FormatForSNY);

    PrefsINI.WriteBool('General Settings', 'Use Manual Rankings (NCCAB)', UseManualRankings_NCAAB);
    PrefsINI.WriteBool('General Settings', 'Use Manual Rankings (NCCAF)', UseManualRankings_NCAAF);
    PrefsINI.WriteBool('General Settings', 'Use Manual Rankings (NCCAW)', UseManualRankings_NCAAW);
    PrefsINI.WriteBool('General Settings', 'Use Seeding for NCAAB Games', UseSeedingForNCAABGames);

    PrefsINI.WriteBool('General Settings', 'Load Scenes at Startup', LoadScenesAtStartup);

    PrefsINI.WriteBool('General Settings', 'Always Play All Scenes at Startup', AlwaysPlayAllScenesAtStartup);

    PrefsINI.WriteInteger('General Settings', 'Remote Control Button De-Bounce Duration (mS)', DebounceDuration);

    PrefsINI.WriteInteger('Game Schedule Settings', 'Days Back', GameScheduleDaysBack);
    PrefsINI.WriteInteger('Game Schedule Settings', 'Days Forward', GameScheduleDaysForward);

    PrefsINI.WriteString('Database Connections', 'Ticker Database Connection String', DBConnectionString);
    PrefsINI.WriteString('Database Connections', 'Sportbase Database Connection String', DBConnectionString2);
    PrefsINI.WriteString('Database Connections', 'Traffic Database Connection String', DBConnectionString3);

    PrefsINI.WriteBool('Ticker Engine Connection', 'Enabled', EngineParameters.Enabled);
    PrefsINI.WriteString('Ticker Engine Connection', 'IP Address', EngineParameters.IPAddress);
    PrefsINI.WriteString('Ticker Engine Connection', 'Port', EngineParameters.Port);
    //Game Final Glyph variables
    PrefsINI.WriteBool('Format Settings', 'Enable Game Final Glyph', UseGameFinalGlyph);
    PrefsINI.WriteInteger('Format Settings', 'Game Final Glyph Character ID', GameFinalGlyphCharacterID);

    //For connection to Playout Controller
    PrefsINI.WriteBool('Playout Controller Remote Settings', 'Connection Enable', PlayoutControllerInfo.Enabled);
    PrefsINI.WriteString('Playout Controller Remote Settings', 'IP Address', PlayoutControllerInfo.IPAddress);
    PrefsINI.WriteInteger('Playout Controller Remote Settings', 'Port', PlayoutControllerInfo.Port);

    //For image paths
    PrefsINI.WriteString('Image Path Settings', 'League Logo Mapped Drive', League_Logo_Mapped_Drive);
    PrefsINI.WriteString('Image Path Settings', 'League Logo Host Drive', League_Logo_Host_Drive);
    PrefsINI.WriteString('Image Path Settings', 'League Logo Base Path', League_Logo_Base_Path);
    PrefsINI.WriteString('Image Path Settings', 'Team Logo Mapped Drive', Team_Logo_Mapped_Drive);
    PrefsINI.WriteString('Image Path Settings', 'Team Logo Host Drive', Team_Logo_Host_Drive);
    PrefsINI.WriteString('Image Path Settings', 'Team Logo Base Path', Team_Logo_Base_Path);
    PrefsINI.WriteString('Image Path Settings', 'Player Headshot Mapped Drive', Player_Headshot_Mapped_Drive);
    PrefsINI.WriteString('Image Path Settings', 'Player Headshot Host Drive', Player_Headshot_Host_Drive);
    PrefsINI.WriteString('Image Path Settings', 'Player Headshot Base Path', Player_Headshot_Base_Path);
    PrefsINI.WriteString('Image Path Settings', 'Weather Icon Base Path', Weather_Icon_Base_Path);
    PrefsINI.WriteString('Image Path Settings', 'Sponsor Logo Base Path', Sponsor_Logo_Base_Path);
    //Added for Version 2.0
    PrefsINI.WriteString('Image Path Settings', 'Full Promo Graphic Mapped Drive', Full_Promo_Graphic_Mapped_Drive);
    PrefsINI.WriteString('Image Path Settings', 'Full Promo Graphic Host Drive', Full_Promo_Graphic_Host_Drive);
    PrefsINI.WriteString('Image Path Settings', 'Full Promo Graphic Base Path', Full_Promo_Graphic_Base_Path);
    PrefsINI.WriteString('Image Path Settings', 'Top Region Image Mapped Drive', Top_Region_Image_Mapped_Drive);
    PrefsINI.WriteString('Image Path Settings', 'Top Region Image Host Drive', Top_Region_Image_Host_Drive);
    PrefsINI.WriteString('Image Path Settings', 'Top Region Image Base Path', Top_Region_Image_Base_Path);
    PrefsINI.WriteString('Image Path Settings', 'Base Element Path', Base_Element_Path);
    PrefsINI.WriteString('Image Path Settings', 'End Element Path', End_Element_Path);
    //Added for Version 2.0 - Image filenames
    PrefsINI.WriteString('Image Filename', 'Headshot Backplate Filename', Headshot_Backplate_Filename);
    PrefsINI.WriteString('Image Filename', 'Team Logo Headshot Filename', Team_Logo_Headshot_Filename);
    PrefsINI.WriteString('Image Filename', 'Team Chip A Filename', Team_Chip_A_Filename);
    PrefsINI.WriteString('Image Filename', 'Team Chip B Filename', Team_Chip_B_Filename);
    PrefsINI.WriteString('Image Filename', 'Team Color Filename', Team_Color_Filename);
    PrefsINI.WriteString('Image Filename', 'Flag Logo Headshot Filename', Flag_Logo_Headshot_Filename);

    //For base scene info
    PrefsINI.WriteInteger('Base Scene Info', 'Base Scene ID', BaseScene_SceneID);
    PrefsINI.WriteString('Base Scene Info', 'Field 1 In Command', BaseScene_Field_1_In_Command);
    PrefsINI.WriteString('Base Scene Info', 'Field 1 Out Command', BaseScene_Field_1_Out_Command);
    PrefsINI.WriteString('Base Scene Info', 'Field 2 In Command', BaseScene_Field_2_In_Command);
    PrefsINI.WriteString('Base Scene Info', 'Field 2 Out Command', BaseScene_Field_2_Out_Command);
    PrefsINI.WriteString('Base Scene Info', 'Field 3 In Command', BaseScene_Field_3_In_Command);
    PrefsINI.WriteString('Base Scene Info', 'Field 3 Out Command', BaseScene_Field_3_Out_Command);
    PrefsINI.WriteInteger('Sponsor Logo Scene Info', 'Sponsor Logo Scene ID', Sponsor_Logo_Scene_ID);
    PrefsINI.WriteInteger('Full Page Promo Scene Info', 'Full Page Promo Scene ID', Full_Page_Promo_Scene_ID);
    PrefsINI.WriteInteger('Full Page Promo Scene Info', 'Full Page Promo Template', Full_Page_Promo_Template);

  finally
    //Free up the object
    PrefsINI.Free;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// PROCEDURES FOR LAUNCHING PROGRAM DIALOGS
////////////////////////////////////////////////////////////////////////////////
//Display about box
procedure TMainForm.About1Click(Sender: TObject);
var
  Modal: TAbout;
begin
  Modal := TAbout.Create(Application);
  try
    Modal.ShowModal;
  finally
    Modal.Free
  end;
end;

//Handler to display dialog for setting user preferences
procedure TMainForm.SetPrefs1Click(Sender: TObject);
var
  Modal: TPrefs;
  Control: Word;
begin
  Modal := TPrefs.Create(Application);
  try
    Modal.SpinEdit1.Value := StationID;
    Modal.Edit1.Text := SpellCheckerDictionaryDir;
    Modal.Edit2.Text := DBConnectionString;
    Modal.Edit3.Text := DBConnectionString2;
    Modal.EngineEnable.Checked := EngineParameters.Enabled;
    Modal.EngineIPAddress.Text := EngineParameters.IPAddress;
    Modal.EnginePort.Text := EngineParameters.Port;
    Modal.ForceUpperCaseCheckBox.Checked := ForceUpperCase;
    Modal.ForceUpperCaseGameInfoCheckBox.Checked := ForceUpperCaseGameInfo;
    Modal.DefaultToSingleLoopCheckBox.Checked := DefaultToOneLoopThrough;
    Modal.ManualRankings_NCAAB.Checked := UseManualRankings_NCAAB;
    Modal.ManualRankings_NCAAW.Checked := UseManualRankings_NCAAW;
    Modal.ManualRankings_NCAAF.Checked := UseManualRankings_NCAAF;

    Modal.RadioGroup1.ItemIndex := DefaultTickerMode-1;
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    //Set new values if user didn't cancel out
    if (Control = mrOK) then
    begin
      ForceUpperCase := Modal.ForceUpperCaseCheckbox.Checked;
      ForceUpperCaseGameInfo := Modal.ForceUpperCaseGameInfoCheckBox.Checked;
      DefaultToOneLoopThrough := Modal.DefaultToSingleLoopCheckBox.Checked;
      StationID := Modal.SpinEdit1.Value;
      DBConnectionString := Modal.Edit2.Text;
      DBConnectionString2 := Modal.Edit3.Text;
      SpellCheckerDictionaryDir := Modal.Edit1.Text;
      EngineParameters.Enabled := Modal.EngineEnable.Checked;
      EngineParameters.IPAddress := Modal.EngineIPAddress.Text;
      EngineParameters.Port := Modal.EnginePort.Text;
      DefaultTickerMode := Modal.RadioGroup1.ItemIndex+1;
      UseManualRankings_NCAAB := Modal.ManualRankings_NCAAB.Checked;
      UseManualRankings_NCAAW := Modal.ManualRankings_NCAAW.Checked;
      UseManualRankings_NCAAF := Modal.ManualRankings_NCAAF.Checked;
      //Activate database & tables
      With dmMain do
      begin
        dbTicker.Connected := FALSE;
        dbTicker.ConnectionString := DBConnectionString;
        dbTicker.Connected := TRUE;
        tblTicker_Elements.Active := TRUE;
        tblTicker_Groups.Active := TRUE;
        tblScheduled_Ticker_Groups.Active := TRUE;
        tblBreakingNews_Elements.Active := TRUE;
        tblBreakingNews_Groups.Active := TRUE;
        tblWeather_Cities.Active := TRUE;
        //tblScheduled_BreakingNews_Groups.Active := TRUE;
        tblSponsor_Logos.Active := TRUE;
        dbSportbase.Connected := FALSE;
        dbSportbase.ConnectionString := DBConnectionString2;
        dbSportbase.Connected := TRUE;
      end;
    end;
    Modal.Free
  end;
  //Store the preferences
  StorePrefs;
  //Set default ticker mode
  if (DefaultTickerMode = 2) then DoubleLineCheck.Checked := TRUE
  else SingleLineCheck.Checked := TRUE;
end;

////////////////////////////////////////////////////////////////////////////////
// DATABASE RELATED FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Handler to reload collections
procedure TMainForm.RepopulateTeamsCollection1Click(Sender: TObject);
begin
  ReloadDataFromDatabase;
end;
//General procedure to cache in data from database into collections
procedure TMainForm.ReloadDataFromDatabase;
var
  TeamPtr: ^TeamRec; //Pointer to team collection object
  SponsorLogoPtr: ^SponsorLogoRec; //Pointer to sponsor logo object
  StatPtr: ^StatRec; //Pointer to stat object
  GamePhasePtr: ^GamePhaseRec;
  TemplateDefRecPtr: ^TemplateDefsRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  ChildTemplateIDRecPtr: ^ChildTemplateIDRec;
  CategoryRecPtr: ^CategoryRec;
  CategoryTemplatesRecPtr: ^CategoryTemplatesRec;
  RecordTypeRecPtr: ^RecordTypeRec;
  StyleChipRecPtr: ^StyleChipRec;
  LeagueCodeRecPtr: ^LeagueCodeRec;
  AutomatedLeagueRecPtr: ^AutomatedLeagueRec;
  WeatherIconRecPtr: ^WeatherIconRec;
  PlayoutStationInfoRecPtr: ^PlayoutStationInfoRec;
  SceneRecPtr: ^SceneRec;
  LBarWeatherIconRecPtr: ^LBarWeatherIconRec;
  SaveCategoryIndex: SmallInt;
begin
  //Load collections for users and block subcategories
  Team_Collection.Clear;
  Team_Collection.Pack;
  //Load in teams information
  dmMain.DBLoadQuery.SQL.Clear;
  dmMain.DBLoadQuery.SQL.Add('SELECT * FROM Teams');
  dmMain.DBLoadQuery.Open;
  if (dmMain.DBLoadQuery.RecordCount > 0) then
  begin
    dmMain.DBLoadQuery.First;
    repeat
      //Add item to teams collection
      GetMem (TeamPtr, SizeOf(TeamRec));
      With TeamPtr^ do
      begin
        League := dmMain.DBLoadQuery.FieldByName('League').AsString;
        //OldSTTeamCode := dmMain.DBLoadQuery.FieldByName('OldSTTeamCode').AsString;
        NewSTTeamCode := dmMain.DBLoadQuery.FieldByName('NewSTTeamCode').AsString;
        StatsIncID := dmMain.DBLoadQuery.FieldByName('StatsIncID').AsFloat;
        LongName := dmMain.DBLoadQuery.FieldByName('LongName').AsString;
        ShortName := dmMain.DBLoadQuery.FieldByName('ShortName').AsString;
        BaseName := dmMain.DBLoadQuery.FieldByName('BaseName').AsString;
        DisplayName1 := dmMain.DBLoadQuery.FieldByName('DisplayName1').AsString;
        DisplayName2 := dmMain.DBLoadQuery.FieldByName('DisplayName2').AsString;
        CamioTeamLogoPath := dmMain.DBLoadQuery.FieldByName('CamioTeamLogoPath').AsString;
        If (Team_Collection.Count <= 5000) then
        begin
          Team_Collection.Insert(TeamPtr);
          Team_Collection.Pack;
        end;
      end;
      //Got to next record
      dmMain.DBLoadQuery.Next;
    until (dmMain.DBLoadQuery.EOF = TRUE); //Repeat until end of dataset
  end
  else begin
    MessageDlg('No data found in Teams database table!', mtError, [mbOk], 0);
  end;
  //Close query
  dmMain.DBLoadQuery.Active := FALSE;

  //Load in the data from the game phase codes table; iterate for all records in table
  Game_Phase_Collection.Clear;
  Game_Phase_Collection.Pack;
  dmMain.DBLoadQuery.SQL.Clear;
  //if (FormatForSNY) then
  //  dmMain.DBLoadQuery.SQL.Add('SELECT * FROM Game_Phase_Codes_SNY')
  //else
    dmMain.DBLoadQuery.SQL.Add('SELECT * FROM Game_Phase_Codes');
  dmMain.DBLoadQuery.Open;
  if (dmMain.DBLoadQuery.RecordCount > 0) then
  begin
    dmMain.DBLoadQuery.First;
    repeat
      //Add item to scripts collection
      GetMem (GamePhasePtr, SizeOf(GamePhaseRec));
      With GamePhasePtr^ do
      begin
        League := dmMain.DBLoadQuery.FieldByName('League').AsString;
        ST_Phase_Code := dmMain.DBLoadQuery.FieldByName('ST_Phase_Code').AsInteger;
        SI_Phase_Code := dmMain.DBLoadQuery.FieldByName('SI_Phase_Code').AsInteger;
        Display_Period := dmMain.DBLoadQuery.FieldByName('Display_Period').AsString;
        End_Is_Half := dmMain.DBLoadQuery.FieldByName('End_Is_Half').AsBoolean;
        Display_Half := dmMain.DBLoadQuery.FieldByName('Display_Half').AsString;
        Display_Final := dmMain.DBLoadQuery.FieldByName('Display_Final').AsString;
        Display_Extended1 := dmMain.DBLoadQuery.FieldByName('Display_Extended1').AsString;
        Display_Extended2 := dmMain.DBLoadQuery.FieldByName('Display_Extended2').AsString;
        If (Game_Phase_Collection.Count <= 500) then
        begin
          //Add to collection
          Game_Phase_Collection.Insert(GamePhasePtr);
          Game_Phase_Collection.Pack;
        end;
      end;
      //Got to next record
      dmMain.DBLoadQuery.Next;
    until (dmMain.DBLoadQuery.EOF = TRUE); //Repeat until end of dataset
  end
  else begin
    MessageDlg('No data found in Game Phase database table!', mtError, [mbOk], 0);
  end;
  //Close query
  dmMain.DBLoadQuery.Active := FALSE;

  //Load in the data from the stats info table; iterate for all records in table
  Stat_Collection.Clear;
  Stat_Collection.Pack;
  dmMain.DBLoadQuery.SQL.Clear;
  dmMain.DBLoadQuery.SQL.Add('SELECT * FROM Stat_Definitions');
  dmMain.DBLoadQuery.Open;
  if (dmMain.DBLoadQuery.RecordCount > 0) then
  begin
    dmMain.DBLoadQuery.First;
    repeat
      //Add item to scripts collection
      GetMem (StatPtr, SizeOf(StatRec));
      With StatPtr^ do
      begin
        StatLeague := dmMain.DBLoadQuery.FieldByName('StatLeague').AsString;
        StatType := dmMain.DBLoadQuery.FieldByName('StatType').AsInteger;
        StatDescription := dmMain.DBLoadQuery.FieldByName('StatDescription').AsString;
        StatHeader := dmMain.DBLoadQuery.FieldByName('StatHeader').AsString;
        StatStoredProcedure := dmMain.DBLoadQuery.FieldByName('StatStoredProcedure').AsString;
        StatDataField := dmMain.DBLoadQuery.FieldByName('StatDataField').AsString;
        StatAbbreviation := dmMain.DBLoadQuery.FieldByName('StatAbbreviation').AsString;
        UseStatQualifier := dmMain.DBLoadQuery.FieldByName('UseStatQualifier').AsBoolean;
        StatQualifier := dmMain.DBLoadQuery.FieldByName('StatQualifier').AsString;
        StatQualifierValue := dmMain.DBLoadQuery.FieldByName('StatQualifierValue').AsInteger;
        If (Stat_Collection.Count <= 100) then
        begin
          //Add to collection
          Stat_Collection.Insert(StatPtr);
          Stat_Collection.Pack;
        end;
      end;
      //Got to next record
      dmMain.DBLoadQuery.Next;
    until (dmMain.DBLoadQuery.EOF = TRUE); //Repeat until end of dataset
  end;
  //Close query
  dmMain.DBLoadQuery.Active := FALSE;

  //Load in the data from the sponsor logos table; iterate for all records in table
  SponsorLogo_Collection.Clear;
  SponsorLogo_Collection.Pack;
  dmMain.DBLoadQuery.SQL.Clear;
  dmMain.DBLoadQuery.SQL.Add('SELECT * FROM Sponsor_Logos');
  dmMain.DBLoadQuery.Open;
  if (dmMain.DBLoadQuery.RecordCount > 0) then
  begin
    dmMain.DBLoadQuery.First;
    repeat
      //Add item to scripts collection
      GetMem (SponsorLogoPtr, SizeOf(SponsorLogoRec));
      With SponsorLogoPtr^ do
      begin
        SponsorLogoIndex := dmMain.DBLoadQuery.FieldByName('LogoIndex').AsInteger;
        SponsorLogoName := dmMain.DBLoadQuery.FieldByName('LogoName').AsString;
        SponsorLogoFilename := dmMain.DBLoadQuery.FieldByName('LogoFilename').AsString;
        IsDefaultLogo := dmMain.DBLoadQuery.FieldByName('IsDefaultLogo').AsBoolean;
        If (SponsorLogo_Collection.Count <= 100) then
        begin
          //Add to collection
          SponsorLogo_Collection.Insert(SponsorLogoPtr);
          SponsorLogo_Collection.Pack;
          //Add to comboboxes
          //ComboBox4.Items.Add(SponsorLogoName);
        end;
      end;
      //Got to next record
      dmMain.DBLoadQuery.Next;
    until (dmMain.DBLoadQuery.EOF = TRUE); //Repeat until end of dataset
  end;
  //Close query
  dmMain.DBLoadQuery.Active := FALSE;

  //Load in the data from the template definitions table; iterate for all records in table
  Template_Defs_Collection.Clear;
  Template_Defs_Collection.Pack;
  dmMain.DBLoadQuery.SQL.Clear;
  //if (FormatForSNY) then
  //  dmMain.DBLoadQuery.SQL.Add('SELECT * FROM Template_Defs_SNY')
  //else
    dmMain.DBLoadQuery.SQL.Add('SELECT * FROM Template_Defs');
  dmMain.DBLoadQuery.Open;
  if (dmMain.DBLoadQuery.RecordCount > 0) then
  begin
    dmMain.DBLoadQuery.First;
    repeat
      //Add item to scripts collection
      GetMem (TemplateDefRecPtr, SizeOf(TemplateDefsRec));
      With TemplateDefRecPtr^ do
      begin
        Template_ID := dmMain.DBLoadQuery.FieldByName('Template_ID').AsInteger;
        Template_Type := dmMain.DBLoadQuery.FieldByName('Template_Type').AsInteger;
        AlternateModeTemplateID := dmMain.DBLoadQuery.FieldByName('AlternateModeTemplateID').AsInteger;
        Template_Description := dmMain.DBLoadQuery.FieldByName('Template_Description').AsString;
        Template_Has_Children := dmMain.DBLoadQuery.FieldByName('Template_Has_Children').AsBoolean;
        Template_Is_Child := dmMain.DBLoadQuery.FieldByName('Template_Is_Child').AsBoolean;
        Record_Type := dmMain.DBLoadQuery.FieldByName('Record_Type').AsInteger;
        TemplateSponsorType := dmMain.DBLoadQuery.FieldByName('TemplateSponsorType').AsInteger;
        Scene_ID_1 := dmMain.DBLoadQuery.FieldByName('Scene_ID_1').AsInteger;
        Scene_ID_2 := dmMain.DBLoadQuery.FieldByName('Scene_ID_2').AsInteger;
        Transition_In := dmMain.DBLoadQuery.FieldByName('Transition_In').AsString;
        Transition_Out := dmMain.DBLoadQuery.FieldByName('Transition_Out').AsString;
        Default_Dwell := dmMain.DBLoadQuery.FieldByName('Default_Dwell').AsInteger;
        ManualLeague := dmMain.DBLoadQuery.FieldByName('ManualLeague').AsBoolean;
        EnableForOddsOnly := dmMain.DBLoadQuery.FieldByName('EnableForOddsOnly').AsBoolean;
        UsesGameData := dmMain.DBLoadQuery.FieldByName('UsesGameData').AsBoolean;
        RequiredGameState := dmMain.DBLoadQuery.FieldByName('RequiredGameState').AsInteger;
        //Special feature for SNY
        if (AllowAlertBackgroundsForNews) then
          Use_Alert_Background := dmMain.DBLoadQuery.FieldByName('Use_Alert_Background').AsBoolean;
        StartEnableDateTime := dmMain.DBLoadQuery.FieldByName('StartEnableTime').AsDateTime;
        EndEnableDateTime := dmMain.DBLoadQuery.FieldByName('EndEnableTime').AsDateTime;
        If (Template_Defs_Collection.Count <= 500) then
        begin
          //Add to collection
          Template_Defs_Collection.Insert(TemplateDefRecPtr);
          Template_Defs_Collection.Pack;
        end;
      end;
      //Got to next record
      dmMain.DBLoadQuery.Next;
    until (dmMain.DBLoadQuery.EOF = TRUE); //Repeat until end of dataset
  end
  else begin
    MessageDlg('No data found in Template Definitions database table!', mtError, [mbOk], 0);
  end;
  //Close query
  dmMain.DBLoadQuery.Active := FALSE;

  //Load in the data from the child template definitions table; iterate for all records in table
  Child_Template_ID_Collection.Clear;
  Child_Template_ID_Collection.Pack;
  dmMain.DBLoadQuery.SQL.Clear;
  dmMain.DBLoadQuery.SQL.Add('SELECT * FROM Child_Template_IDs');
  dmMain.DBLoadQuery.Open;
  if (dmMain.DBLoadQuery.RecordCount > 0) then
  begin
    dmMain.DBLoadQuery.First;
    repeat
      //Add item to scripts collection
      GetMem (ChildTemplateIDRecPtr, SizeOf(ChildTemplateIDRec));
      With ChildTemplateIDRecPtr^ do
      begin
        Template_ID := dmMain.DBLoadQuery.FieldByName('Template_ID').AsInteger;
        Child_Template_ID := dmMain.DBLoadQuery.FieldByName('Child_Template_ID').AsInteger;
        Child_Default_Enable_State := dmMain.DBLoadQuery.FieldByName('Child_Default_Enable_State').AsBoolean;
        If (Child_Template_ID_Collection.Count <= 250) then
        begin
          //Add to collection
          Child_Template_ID_Collection.Insert(ChildTemplateIDRecPtr);
          Child_Template_ID_Collection.Pack;
        end;
      end;
      //Got to next record
      dmMain.DBLoadQuery.Next;
    until (dmMain.DBLoadQuery.EOF = TRUE); //Repeat until end of dataset
  end;
  //Close query
  dmMain.DBLoadQuery.Active := FALSE;

  //Load in the data from the template fieldss table; iterate for all records in table
  Template_Fields_Collection.Clear;
  Template_Fields_Collection.Pack;
  dmMain.DBLoadQuery.SQL.Clear;
  //if (FormatForSNY) then
  //  dmMain.DBLoadQuery.SQL.Add('SELECT * FROM Template_Fields_SNY')
  //else
    dmMain.DBLoadQuery.SQL.Add('SELECT * FROM Template_Fields');
  dmMain.DBLoadQuery.Open;
  if (dmMain.DBLoadQuery.RecordCount > 0) then
  begin
    dmMain.DBLoadQuery.First;
    repeat
      //Add item to scripts collection
      GetMem (TemplateFieldsRecPtr, SizeOf(TemplateFieldsRec));
      With TemplateFieldsRecPtr^ do
      begin
        Template_ID := dmMain.DBLoadQuery.FieldByName('Template_ID').AsInteger;
        Field_ID := dmMain.DBLoadQuery.FieldByName('Field_ID').AsInteger;
        Field_Type := dmMain.DBLoadQuery.FieldByName('Field_Type').AsInteger;
        Field_Is_Manual := dmMain.DBLoadQuery.FieldByName('Field_Is_Manual').AsBoolean;
        Field_Label := dmMain.DBLoadQuery.FieldByName('Field_Label').AsString;
        Field_Contents := dmMain.DBLoadQuery.FieldByName('Field_Contents').AsString;
        Field_Format_Prefix := dmMain.DBLoadQuery.FieldByName('Field_Format_Prefix').AsString;
        Field_Format_Suffix := dmMain.DBLoadQuery.FieldByName('Field_Format_Suffix').AsString;
        Field_Max_Length := dmMain.DBLoadQuery.FieldByName('Field_Max_Length').AsInteger;
        Scene_Field_Name := dmMain.DBLoadQuery.FieldByName('Scene_Field_Name').AsString;
        If (Template_Fields_Collection.Count <= 5000) then
        begin
          //Add to collection
          Template_Fields_Collection.Insert(TemplateFieldsRecPtr);
          Template_Fields_Collection.Pack;
        end;
      end;
      //Got to next record
      dmMain.DBLoadQuery.Next;
    until (dmMain.DBLoadQuery.EOF = TRUE); //Repeat until end of dataset
  end
  else begin
    MessageDlg('No data found in Template Fields database table!', mtError, [mbOk], 0);
  end;
  //Close query
  dmMain.DBLoadQuery.Active := FALSE;

  //Load in the data from the categories table; iterate for all records in table
  //V2.0.2 Store current index to reset to selected category when done; prevents issue after running this function after software has been started
  SaveCategoryIndex := LeagueTab.TabIndex;
  Categories_Collection.Clear;
  Categories_Collection.Pack;
  dmMain.DBLoadQuery.SQL.Clear;
  dmMain.DBLoadQuery.SQL.Add('SELECT * FROM Categories');
  dmMain.DBLoadQuery.Open;
  if (dmMain.DBLoadQuery.RecordCount > 0) then
  begin
    dmMain.DBLoadQuery.First;
    //Clear the categories tab control string list
    LeagueTab.Tabs.Clear;
    repeat
      //Add item to scripts collection
      GetMem (CategoryRecPtr, SizeOf(CategoryRec));
      With CategoryRecPtr^ do
      begin
        Category_Type := dmMain.DBLoadQuery.FieldByName('Category_Type').AsInteger;
        Category_ID := dmMain.DBLoadQuery.FieldByName('Category_ID').AsInteger;
        Category_Name := dmMain.DBLoadQuery.FieldByName('Category_Name').AsString;
        Category_Label := dmMain.DBLoadQuery.FieldByName('Category_Label').AsString;
        Category_Description := dmMain.DBLoadQuery.FieldByName('Category_Description').AsString;
        Category_Is_Sport := dmMain.DBLoadQuery.FieldByName('Category_Is_Sport').AsBoolean;
        Category_Sport := dmMain.DBLoadQuery.FieldByName('Category_Sport').AsString;
        Sport_Games_Table_Name := dmMain.DBLoadQuery.FieldByName('Sport_Games_Table_Name').AsString;
        CamioFolderName := dmMain.DBLoadQuery.FieldByName('CamioFolderName').AsString;
        If (Categories_Collection.Count <= 50) then
        begin
          //Add to collection
          Categories_Collection.Insert(CategoryRecPtr);
          Categories_Collection.Pack;
          //Add the category name to the tab list if it's the first category (init)
          if (Category_Type = 1) then LeagueTab.Tabs.Add (Category_Name);
        end;
      end;
      //Got to next record
      dmMain.DBLoadQuery.Next;
    until (dmMain.DBLoadQuery.EOF = TRUE); //Repeat until end of dataset
  end
  else begin
    MessageDlg('No data found in Ticker Categories database table!', mtError, [mbOk], 0);
  end;
  //Close query
  dmMain.DBLoadQuery.Active := FALSE;
  //V2.0.2 Restore tab index if not -1
  if (SaveCategoryIndex > -1) then LeagueTab.TabIndex := SaveCategoryIndex;

  //Load in the data from the categories table; iterate for all records in table
  Category_Templates_Collection.Clear;
  Category_Templates_Collection.Pack;
  dmMain.DBLoadQuery.SQL.Clear;
  //if (FormatForSNY) then
  //  dmMain.DBLoadQuery.SQL.Add('SELECT * FROM Category_Templates_SNY')
  //else
    dmMain.DBLoadQuery.SQL.Add('SELECT * FROM Category_Templates');
  dmMain.DBLoadQuery.Open;
  if (dmMain.DBLoadQuery.RecordCount > 0) then
  begin
    dmMain.DBLoadQuery.First;
    repeat
      //Add item to scripts collection
      GetMem (CategoryTemplatesRecPtr, SizeOf(CategoryTemplatesRec));
      With CategoryTemplatesRecPtr^ do
      begin
        Category_ID := dmMain.DBLoadQuery.FieldByName('Category_ID').AsInteger;
        Template_ID := dmMain.DBLoadQuery.FieldByName('Template_ID').AsInteger;
        Template_Type := dmMain.DBLoadQuery.FieldByName('Template_Type').AsInteger;
        Template_Description := dmMain.DBLoadQuery.FieldByName('Template_Description').AsString;
        Template_IsOddsOnly := dmMain.DBLoadQuery.FieldByName('Template_IsOddsOnly').AsBoolean;
        If (Category_Templates_Collection.Count <= 500) then
        begin
          //Add to collection
          Category_Templates_Collection.Insert(CategoryTemplatesRecPtr);
          Category_Templates_Collection.Pack;
        end;
      end;
      //Got to next record
      dmMain.DBLoadQuery.Next;
    until (dmMain.DBLoadQuery.EOF = TRUE); //Repeat until end of dataset
  end
  else begin
    MessageDlg('No data found in Category Templates database table!', mtError, [mbOk], 0);
  end;
  //Close query
  dmMain.DBLoadQuery.Active := FALSE;

  //Load in the data from the record types table; iterate for all records in table
  RecordType_Collection.Clear;
  RecordType_Collection.Pack;
  dmMain.DBLoadQuery.SQL.Clear;
  //if (FormatForSNY) then
  //  dmMain.DBLoadQuery.SQL.Add('SELECT * FROM Record_Types_SNY')
  //else
    dmMain.DBLoadQuery.SQL.Add('SELECT * FROM Record_Types');
  dmMain.DBLoadQuery.Open;
  if (dmMain.DBLoadQuery.RecordCount > 0) then
  begin
    dmMain.DBLoadQuery.First;
    repeat
      //Add item to scripts collection
      GetMem (RecordTypeRecPtr, SizeOf(RecordTypeRec));
      With RecordTypeRecPtr^ do
      begin
        Playlist_Type := dmMain.DBLoadQuery.FieldByName('Playlist_Type').AsInteger;
        Record_Type := dmMain.DBLoadQuery.FieldByName('Record_Type').AsInteger;
        Record_Description := dmMain.DBLoadQuery.FieldByName('Record_Description').AsString;
        If (RecordType_Collection.Count <= 250) then
        begin
          //Add to collection
          RecordType_Collection.Insert(RecordTypeRecPtr);
          RecordType_Collection.Pack;
        end;
      end;
      //Got to next record
      dmMain.DBLoadQuery.Next;
    until (dmMain.DBLoadQuery.EOF = TRUE); //Repeat until end of dataset
  end
  else begin
    MessageDlg('No data found in Record Type database table!', mtError, [mbOk], 0);
  end;
  //Close query
  dmMain.DBLoadQuery.Active := FALSE;

  //Load in the data from the style chips table; iterate for all records in table
  StyleChip_Collection.Clear;
  StyleChip_Collection.Pack;
  dmMain.DBLoadQuery.SQL.Clear;
  dmMain.DBLoadQuery.SQL.Add('SELECT * FROM Style_Chips');
  dmMain.DBLoadQuery.Open;
  if (dmMain.DBLoadQuery.RecordCount > 0) then
  begin
    dmMain.DBLoadQuery.First;
    repeat
      //Add item to style chip collection
      GetMem (StyleChipRecPtr, SizeOf(StyleChipRec));
      With StyleChipRecPtr^ do
      begin
        StyleChip_Index := dmMain.DBLoadQuery.FieldByName('StyleChip_Index').AsInteger;
        StyleChip_Description := dmMain.DBLoadQuery.FieldByName('StyleChip_Description').AsString;
        StyleChip_Code := dmMain.DBLoadQuery.FieldByName('StyleChip_Code').AsString;
        StyleChip_Type := dmMain.DBLoadQuery.FieldByName('StyleChip_Type').AsInteger;
        StyleChip_String := dmMain.DBLoadQuery.FieldByName('StyleChip_String').AsString;
        StyleChip_FontCode := dmMain.DBLoadQuery.FieldByName('StyleChip_FontCode').AsInteger;
        StyleChip_CharacterCode := dmMain.DBLoadQuery.FieldByName('StyleChip_CharacterCode').AsInteger;
        If (StyleChip_Collection.Count <= 100) then
        begin
          //Add to collection
          StyleChip_Collection.Insert(StyleChipRecPtr);
          StyleChip_Collection.Pack;
        end;
      end;
      //Got to next record
      dmMain.DBLoadQuery.Next;
    until (dmMain.DBLoadQuery.EOF = TRUE); //Repeat until end of dataset
  end;
  //Close query
  dmMain.DBLoadQuery.Active := FALSE;

  //Load in the data from the custome headers table; iterate for all records in table
  LeagueCode_Collection.Clear;
  LeagueCode_Collection.Pack;
  dmMain.DBLoadQuery.SQL.Clear;
  dmMain.DBLoadQuery.SQL.Add('SELECT * FROM Custom_Headers');
  dmMain.DBLoadQuery.Open;
  if (dmMain.DBLoadQuery.RecordCount > 0) then
  begin
    //Clear the league select combo box entries
    ManualLeagueSelect.Items.Clear;
    dmMain.DBLoadQuery.First;
    repeat
      //Add item to league codes collection
      GetMem (LeagueCodeRecPtr, SizeOf(LeagueCodeRec));
      With LeagueCodeRecPtr^ do
      begin
        Heading := dmMain.DBLoadQuery.FieldByName('Heading').AsString;
        If (LeagueCode_Collection.Count <= 1000) then
        begin
          //Add to collection
          LeagueCode_Collection.Insert(LeagueCodeRecPtr);
          LeagueCode_Collection.Pack;
          //Add to manual league selection combo box
          ManualLeagueSelect.Items.Add(LeagueCodeRecPtr^.Heading);
        end;
      end;
      //Got to next record
      dmMain.DBLoadQuery.Next;
    until (dmMain.DBLoadQuery.EOF = TRUE); //Repeat until end of dataset
  end;
  //Close query
  dmMain.DBLoadQuery.Active := FALSE;
  //Set top of combo box
  ManualLeagueSelect.ItemIndex := 0;

  //Load in the data from the automated leagues table; iterate for all records in table
  Automated_League_Collection.Clear;
  Automated_League_Collection.Pack;
  dmMain.DBLoadQuery.SQL.Clear;
  dmMain.DBLoadQuery.SQL.Add('SELECT * FROM Automated_Leagues');
  dmMain.DBLoadQuery.Open;
  if (dmMain.DBLoadQuery.RecordCount > 0) then
  begin
    dmMain.DBLoadQuery.First;
    repeat
      //Add item to league codes collection
      GetMem (AutomatedLeagueRecPtr, SizeOf(AutomatedLeagueRec));
      With AutomatedLeagueRecPtr^ do
      begin
        SI_LeagueCode := dmMain.DBLoadQuery.FieldByName('SI_LeagueCode').AsString;
        ST_LeagueCode := dmMain.DBLoadQuery.FieldByName('ST_LeagueCode').AsString;
        Display_Mnemonic := dmMain.DBLoadQuery.FieldByName('Display_Mnemonic').AsString;
        If (Automated_League_Collection.Count <= 50) then
        begin
          //Add to collection
          Automated_League_Collection.Insert(AutomatedLeagueRecPtr);
          Automated_League_Collection.Pack;
        end;
      end;
      //Got to next record
      dmMain.DBLoadQuery.Next;
    until (dmMain.DBLoadQuery.EOF = TRUE); //Repeat until end of dataset
  end;
  //Close query
  dmMain.DBLoadQuery.Active := FALSE;

  //Load in the data from the playout location info table; iterate for all records in table
  PlayoutStationInfo_Collection.Clear;
  PlayoutStationInfo_Collection.Pack;
  dmMain.DBLoadQuery.SQL.Clear;
  dmMain.DBLoadQuery.SQL.Add('SELECT * FROM Playout_Station_IDs');
  dmMain.DBLoadQuery.Open;
  if (dmMain.DBLoadQuery.RecordCount > 0) then
  begin
    dmMain.DBLoadQuery.First;
    RegionSelect.Items.Clear;
    repeat
      //Add item to league codes collection
      GetMem (PlayoutStationInfoRecPtr, SizeOf(PlayoutStationInfoRec));
      With PlayoutStationInfoRecPtr^ do
      begin
        Station_ID := dmMain.DBLoadQuery.FieldByName('Station_ID').AsInteger;
        Station_Description := dmMain.DBLoadQuery.FieldByName('Station_Description').AsString;
        if (PlayoutStationInfo_Collection.Count <= 10) then
        begin
          //Add to collection
          PlayoutStationInfo_Collection.Insert(PlayoutStationInfoRecPtr);
          PlayoutStationInfo_Collection.Pack;
          //Add to combo box
          RegionSelect.Items.Add(Station_Description);
        end;
      end;
      //Got to next record
      dmMain.DBLoadQuery.Next;
    until (dmMain.DBLoadQuery.EOF = TRUE); //Repeat until end of dataset
    RegionSelect.ItemIndex := 0;
  end;
  //Close query
  dmMain.DBLoadQuery.Active := FALSE;

  //Load in the scenes collection
  Scene_Collection.Clear;
  Scene_Collection.Pack;
  dmMain.DBLoadQuery.SQL.Clear;
  dmMain.DBLoadQuery.SQL.Add('SELECT * FROM Scene_Defs');
  dmMain.DBLoadQuery.Open;
  if (dmMain.DBLoadQuery.RecordCount > 0) then
  begin
    dmMain.DBLoadQuery.First;
    repeat
      //Add item to league codes collection
      GetMem (SceneRecPtr, SizeOf(SceneRec));
      With SceneRecPtr^ do
      begin
        Scene_ID := dmMain.DBLoadQuery.FieldByName('Scene_ID').AsInteger;
        Scene_Description := dmMain.DBLoadQuery.FieldByName('Scene_Description').AsString;
        Is_Base_Scene := dmMain.DBLoadQuery.FieldByName('Is_Base_Scene').AsBoolean;
        Load_At_Startup := dmMain.DBLoadQuery.FieldByName('Load_At_Startup').AsBoolean;
        Play_At_Startup := dmMain.DBLoadQuery.FieldByName('Play_At_Startup').AsBoolean;
        Run_Startup_Command := dmMain.DBLoadQuery.FieldByName('Run_Startup_Command').AsBoolean;
        Startup_Command := dmMain.DBLoadQuery.FieldByName('Startup_Command').AsString;
        if (Scene_Collection.Count <= 100) then
        begin
          //Add to collection
          Scene_Collection.Insert(SceneRecPtr);
          Scene_Collection.Pack;
        end;
      end;
      //Got to next record
      dmMain.DBLoadQuery.Next;
    until (dmMain.DBLoadQuery.EOF = TRUE); //Repeat until end of dataset
  end;
  //Close query
  dmMain.DBLoadQuery.Active := FALSE;

  //Load in the weather icon collection
  LBar_Weather_Icon_Collection.Clear;
  LBar_Weather_Icon_Collection.Pack;
  dmMain.DBLoadQuery.SQL.Clear;
  dmMain.DBLoadQuery.SQL.Add('SELECT * FROM Weather_Icons_Info');
  dmMain.DBLoadQuery.Open;
  if (dmMain.DBLoadQuery.RecordCount > 0) then
  begin
    dmMain.DBLoadQuery.First;
    repeat
      //Add item to league codes collection
      GetMem (LBarWeatherIconRecPtr, SizeOf(LBarWeatherIconRec));
      With LBarWeatherIconRecPtr^ do
      begin
        IconID := dmMain.DBLoadQuery.FieldByName('IconID').AsInteger;
        IconDescription := dmMain.DBLoadQuery.FieldByName('IconDescription').AsString;
        IconFilename := dmMain.DBLoadQuery.FieldByName('IconFilename').AsString;
        IconCharacterValue := dmMain.DBLoadQuery.FieldByName('IconCharacterValue').AsInteger;
        if (LBar_Weather_Icon_Collection.Count <= 100) then
        begin
          //Add to collection
          LBar_Weather_Icon_Collection.Insert(LBarWeatherIconRecPtr);
          LBar_Weather_Icon_Collection.Pack;
        end;
      end;
      //Go to next record
      dmMain.DBLoadQuery.Next;
    until (dmMain.DBLoadQuery.EOF = TRUE); //Repeat until end of dataset
  end;
  //Close query
  dmMain.DBLoadQuery.Active := FALSE;
end;

//Handler to request complete reload of games from Database
procedure TMainForm.ReloadGamesfromGametrak1Click(Sender: TObject);
begin
  //Refresh games tables
  dmMain.SportbaseQuery.Active := FALSE;
  dmMain.SportbaseQuery.Active := TRUE;
end;

////////////////////////////////////////////////////////////////////////////////
// UTILITY FUNCTIONS
// Various functions used for collection lookup, data processing, etc.
////////////////////////////////////////////////////////////////////////////////
function TMainForm.ScrubText (InText: String) : String;
var
  i: SmallInt;
  OutStr: String;
begin
  OutStr := '';
  for i := 1 to Length(InText) do
    //Filter out control characters, etc.
    if (Ord(InText[i]) >= 32) AND (Ord(InText[i]) <= 126) then OutStr := OutStr + InText[i];
  //Remove leading and trailing blanks from story
  ScrubText := OutStr;
end;

//Function to take a template ID & return its description
function TMainForm.GetTemplateInformation(TemplateID: SmallInt): TemplateDefsRec;
var
  i: SmallInt;
  TemplateDefsRecPtr: ^TemplateDefsRec;
  OutRec: TemplateDefsRec;
begin
  if (Template_Defs_Collection.Count > 0) then
  begin
    for i := 0 to Template_Defs_Collection.Count-1 do
    begin
      TemplateDefsRecPtr := Template_Defs_Collection.At(i);
      if (TemplateDefsRecPtr^.Template_ID = TemplateID) then
      with OutRec do
      begin
        Template_ID := TemplateDefsRecPtr^.Template_ID;
        Template_Type := TemplateDefsRecPtr^.Template_Type;
        Template_Description := TemplateDefsRecPtr^.Template_Description;
        Record_Type := TemplateDefsRecPtr^.Record_Type;
        Scene_ID_1 := TemplateDefsRecPtr^.Scene_ID_1;
        Scene_ID_2 := TemplateDefsRecPtr^.Scene_ID_2;
        Transition_In := TemplateDefsRecPtr^.Transition_In;
        Transition_Out := TemplateDefsRecPtr^.Transition_Out;
        Default_Dwell := TemplateDefsRecPtr^.Default_Dwell;
        ManualLeague := TemplateDefsRecPtr^.ManualLeague;
        EnableForOddsOnly := TemplateDefsRecPtr^.EnableForOddsOnly;
        UsesGameData := TemplateDefsRecPtr^.UsesGameData;
        Template_Has_Children := TemplateDefsRecPtr^.Template_Has_Children;
        Template_Is_Child := TemplateDefsRecPtr^.Template_Is_Child;
        StartEnableDateTime := TemplateDefsRecPtr^.StartEnableDateTime;
        EndEnableDateTime := TemplateDefsRecPtr^.EndEnableDateTime;
      end;
    end
  end;
  GetTemplateInformation := OutRec;
end;

//Function to take a league and a game phase code, and return a game phase
//record
function TMainForm.GetGamePhaseRec(League: String; GPhase: SmallInt): GamePhaseRec;
var
  i: SmallInt;
  GamePhaseRecPtr: ^GamePhaseRec; //Pointer to game phase record type
  OutRec: GamePhaseRec;
  FoundRecord: Boolean;
begin
  FoundRecord := FALSE;
  OutRec.League := ' ';
  OutRec.ST_Phase_Code := 0;
  OutRec.SI_Phase_Code := 0;
  OutRec.Display_Period := ' ';
  OutRec.Display_Half := ' ';
  OutRec.Display_Final := ' ';
  OutRec.Display_Extended1 := ' ';
  OutRec.Display_Extended2 := ' ';
  if (Game_Phase_Collection.Count > 0) then
  begin
    i := 0;
    repeat
      GamePhaseRecPtr := Game_Phase_Collection.At(i);
      if (GamePhaseRecPtr^.League = League) AND (GamePhaseRecPtr^.SI_Phase_Code = GPhase) then
      begin
        OutRec.League := League;
        OutRec.ST_Phase_Code := GPhase;
        OutRec.SI_Phase_Code := GPhase;
        OutRec.Label_Period := GamePhaseRecPtr^.Label_Period;
        OutRec.Display_Period := GamePhaseRecPtr^.Display_Period;
        OutRec.End_Is_Half := GamePhaseRecPtr^.End_Is_Half;
        OutRec.Display_Half := GamePhaseRecPtr^.Display_Half;
        OutRec.Display_Final := GamePhaseRecPtr^.Display_Final;
        OutRec.Display_Extended1 := GamePhaseRecPtr^.Display_Extended1;
        OutRec.Display_Extended2 := GamePhaseRecPtr^.Display_Extended2;
        OutRec.Game_OT := GamePhaseRecPtr^.Game_OT;
      end;
      Inc(i);
    until (i = Game_Phase_Collection.Count) OR (FoundRecord = TRUE);
  end;
  GetGamePhaseRec := OutRec;
end;

//Function to get game time string from GT Server time value
function TMainForm.GetGameTimeString(GTimeStr: String): String;
var
  Min, Sec: String;
begin
  //Blank out case where time = 9999 - for MLB
  if (GTimeStr = '9999') then Result := ' '
  //Actual clock time
  else if (StrToIntDef(GTimeStr, -1) <> -1) AND (Length(GTimeStr) = 4) then
  begin
    Min := GTimeStr[1] + GTimeStr[2];
    Sec := GTimeStr[3] + GTimeStr[4];
    Min := IntToStr(StrToInt(GTimeStr[1]+GTimeStr[2]));
    Result := Min + ':' + Sec;
  end
  else if (GTimeStr = 'END-') then Result := 'End'
  else if (GTimeStr = 'POST') then Result := 'PPD'
  else if (GTimeStr = 'SUSP') then Result := 'PPD'
  else if (GTimeStr = 'DELA') then Result := 'DLY'
  else if (GTimeStr = 'RAIN') then Result := 'RD'
  else if (GTimeStr = 'CANC') then Result := 'PPD'
  else Result := ' ';
end;

//Function to take the stat stored procedure name and return the stat information record
function TMainForm.GetStatInfo (StoredProcedureName: String): StatRec;
var
  i: SmallInt;
  StatRecPtr: ^StatRec; //Pointer to stat record type
  OutRec: StatRec;
begin
  OutRec.StatLeague := ' ';
  OutRec.StatType := 0;
  OutRec.StatDescription := ' ';
  OutRec.StatHeader := ' ';
  OutRec.StatStoredProcedure := ' ';
  OutRec.StatDataField := ' ';
  OutRec.StatAbbreviation := ' ';
  OutRec.UseStatQualifier := FALSE;
  OutRec.StatQualifier := ' ';
  OutRec.StatQualifierValue := 0;
  if (Stat_Collection.Count > 0) then
  begin
    for i := 0 to Stat_Collection.Count-1 do
    begin
      StatRecPtr := Stat_Collection.At(i);
      if (Trim(StatRecPtr^.StatStoredProcedure) = Trim(StoredProcedureName)) then
      begin
        OutRec.StatLeague := StatRecPtr^.StatLeague;
        OutRec.StatType := StatRecPtr^.StatType;
        OutRec.StatDescription := StatRecPtr^.StatDescription;
        OutRec.StatHeader := StatRecPtr^.StatHeader;
        OutRec.StatStoredProcedure := StatRecPtr^.StatStoredProcedure;
        OutRec.StatDataField := StatRecPtr^.StatDataField;
        OutRec.StatAbbreviation := StatRecPtr^.StatAbbreviation;
        OutRec.UseStatQualifier := StatRecPtr^.UseStatQualifier;
        OutRec.StatQualifier := StatRecPtr^.StatQualifier;
        OutRec.StatQualifierValue := StatRecPtr^.StatQualifierValue;
      end;
    end;
  end;
  GetStatInfo := OutRec;
end;

//Function to take the Stats Inc. league code and return the display mnemonic
function TMainForm.GetAutomatedLeagueDisplayMnemonic (League: String): String;
var
  i: SmallInt;
  AutomatedLeagueRecPtr: ^AutomatedLeagueRec; //Pointer to team record type
  OutStr: String;
  FoundRecord: Boolean;
begin
  //Init to passed in variable; will return if no substitution found
  OutStr := League;
  if (Automated_League_Collection.Count > 0) then
  begin
    FoundRecord := FALSE;
    i := 0;
    Repeat
      AutomatedLeagueRecPtr := Automated_League_Collection.At(i);
      if (Trim(AutomatedLeagueRecPtr^.SI_LeagueCode) = League) then
      begin
        OutStr := AutomatedLeagueRecPtr^.Display_Mnemonic;
      end;
      Inc(i);
    Until (FoundRecord = TRUE) OR (i = Automated_League_Collection.Count);
  end;
  GetAutomatedLeagueDisplayMnemonic := OutStr;
end;

//Function to get game state
function TMainForm.GetGameState(GTIME: String; GPHASE: SmallInt): Integer;
begin
  if (GTIME = '9999') OR (GPHASE = 0) then
    //Game not started
    Result := 0
  else if (GTIME = 'FINA') OR (GTIME = 'SUM-') then
    //Game is final
    Result := 3
  else if (GTIME = 'END-') then
    //End of period/quarter
    Result := 2
  else if (GPHASE <> 0) AND (StrToIntDef(GTIME, -1) <> -1) then
    //Game in progress
    Result := 1
  else
    //All other conditions
    Result := -1; // Game is suspended due to rain or cancelled etc. (GTIME = 'RAIN' , GTIME = 'SUSP', GTIME = 'CANC')
end;

//Function to get the number of fields in a template
function TMainForm.GetTemplateFieldsCount(Template_ID: SmallInt): SmallInt;
var
  i: SmallInt;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  OutVal: SmallInt;
begin
  OutVal := 0;
  If (Template_Fields_Collection.Count > 0) then
  begin
    for i := 0 to Template_Fields_Collection.Count-1 do
    begin
      TemplateFieldsRecPtr := Template_Fields_Collection.At(i);
      if (TemplateFieldsRecPtr^.Template_ID = Template_ID) {AND
         (TemplateFieldsRecPtr^.Field_Label <> 'Unused')} then Inc(OutVal);
    end;
  end;
  GetTemplateFieldsCount := OutVal;
end;

//Function to get the record type based on the template ID
function TMainForm.GetRecordTypeFromTemplateID(Template_ID: SmallInt): SmallInt;
var
  i: SmallInt;
  TemplateDefsRecPtr: ^TemplateDefsRec;
  OutVal: SmallInt;
begin
  OutVal := 0;
  if (Template_Defs_Collection.Count > 0) then
  begin
    for i := 0 to Template_Defs_Collection.Count-1 do
    begin
      TemplateDefsRecPtr := Template_Defs_Collection.At(i);
      if (TemplateDefsRecPtr^.Template_ID = Template_ID) then
        OutVal := TemplateDefsRecPtr^.Record_Type;
    end;
  end;
  GetRecordTypeFromTemplateID := OutVal;
end;

//Function to return a record type description base on playlist type and record type code
function TMainForm.GetRecordTypeDescription (Playlist_Type: SmallInt; Record_Type: SmallInt): String;
var
  i: SmallInt;
  RecordTypeRecPtr: ^RecordTypeRec;
  OutStr: String;
begin
  OutStr := '';
  if (RecordType_Collection.Count > 1) then
  begin
    for i := 0 to RecordType_Collection.Count-1 do
    begin
      RecordTypeRecPtr := RecordType_Collection.At(i);
      if (RecordTypeRecPtr^.Playlist_Type = Playlist_Type) AND (RecordTypeRecPtr^.Record_Type = Record_Type) then
        OutStr := RecordTypeRecPtr^.Record_Description;
    end;
  end;
  GetRecordTypeDescription := OutStr;
end;

////////////////////////////////////////////////////////////////////////////////
// HANDLERS FOR GENERAL PROGRAM OPERATIONS
////////////////////////////////////////////////////////////////////////////////
//Event handler for page control change of selection
procedure TMainForm.ProgramModePageControlChange(Sender: TObject);
var
  CategoryRecPtr: ^CategoryRec;
begin
  //Handle page change
  Case ProgramModePageControl.ActivePageIndex of
    //Playlist scheduling mode
    0: Begin
         Case PlaylistSelectTabControl.TabIndex of
             //Ticker
          0: begin
               dmMain.tblTicker_Groups.Active := FALSE;
               dmMain.tblTicker_Groups.Active := TRUE;
               dmMain.tblScheduled_Ticker_Groups.Active := FALSE;
               dmMain.tblScheduled_Ticker_Groups.Active := TRUE;
               AvailablePlaylistsGrid.DataSource := dmMain.dsTicker_Groups;
               ScheduledPlaylistsGrid.DataSource := dmMain.dsScheduled_Ticker_Groups;
             end;
             //Alerts
          1: begin
               dmMain.tblBreakingNews_Groups.Active := FALSE;
               dmMain.tblBreakingNews_Groups.Active := TRUE;
               //dmMain.tblScheduled_BreakingNews_Groups.Active := FALSE;
               //dmMain.tblScheduled_BreakingNews_Groups.Active := TRUE;
               AvailablePlaylistsGrid.DataSource := dmMain.dsBreakingNews_Groups;
               ScheduledPlaylistsGrid.DataSource := dmMain.dsScheduled_BreakingNews_Groups;
             end;
             //Default Templates (by sport)
          2: begin
               dmMain.tblTicker_Groups.Active := FALSE;
               dmMain.tblTicker_Groups.Active := TRUE;
               dmMain.tblScheduled_Ticker_Groups.Active := FALSE;
               dmMain.tblScheduled_Ticker_Groups.Active := TRUE;
               AvailablePlaylistsGrid.DataSource := dmMain.dsTicker_Groups;
               ScheduledPlaylistsGrid.DataSource := dmMain.dsScheduled_Ticker_Groups;
             end;
         end;
       end;
    //Ticker authoring mode
    1: Begin
         //Check to see if category tab is a sport; if so, show and populate games grid
         CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
         //Now, populate the templates grid by querying the database
         dmMain.Query2.Active := FALSE;
         dmMain.Query2.SQL.Clear;
         //Only show games if in ticker mode (not alerts)
         dmMain.Query2.Filtered := TRUE;
         //Filter for templates applicable to current playlist type
         dmMain.Query2.Filter := 'Template_Type = ' + IntToStr(PlaylistSelectTabControl.TabIndex+1);
         dmMain.Query2.SQL.Add ('SELECT * FROM Category_Templates WHERE ' +
           'Category_ID = ' + IntToStr(CategoryRecPtr^.Category_ID));
         dmMain.Query2.Active := TRUE;
       end;
    //DB Maint
    2: Begin
         Case PlaylistSelectTabControl.TabIndex of
             //Ticker
          0: begin
               dmMain.tblTicker_Groups.Active := FALSE;
               dmMain.tblTicker_Groups.Active := TRUE;
               DatabaseMaintPlaylistGrid.DataSource := dmMain.dsTicker_Groups;
               DatabaseMaintBreakingNewsPlaylistGrid.Visible := FALSE;
             end;
             //Alerts
          1: begin
               dmMain.tblBreakingNews_Groups.Active := FALSE;
               dmMain.tblBreakingNews_Groups.Active := TRUE;
               DatabaseMaintBreakingNewsPlaylistGrid.DataSource := dmMain.dsBreakingNews_Groups;
               DatabaseMaintBreakingNewsPlaylistGrid.Visible := TRUE;
             end;
         end;
       end;
  end;
  //Set new page index
  LastPageIndex := ProgramModePageControl.ActivePageIndex;
  //Load current templates
  LeagueTab.OnChange(self);
end;

//Handler for PlaylistSelectTabControl tab change
procedure TMainForm.PlaylistSelectTabControlChange(Sender: TObject);
var
  i: SmallInt;
  CategoryRecPtr: ^CategoryRec;
  TemplateType: SmallInt;
begin
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker
   0: begin
        //Show tab sheet for schedule
        ProgramModePageControl.Pages[0].TabVisible := TRUE;
        //Hide default template grid/list and save panel
        TemplateListSaveControlsPnl.Visible := FALSE;
        DefaultTemplatesGrid.Visible := FALSE;
        //Show playlist grid
        PlaylistGrid.Visible := TRUE;
        DatabaseMaintPlaylistGrid.DataSource := dmMain.dsTicker_Groups;
        AvailablePlaylistsGrid.DataSource := dmMain.dsTicker_Groups;
        ScheduledPlaylistsGrid.DataSource := dmMain.dsScheduled_Ticker_Groups;
        LastSaveTimeLabel.Caption :=
          PlaylistInfo[PlaylistSelectTabControl.TabIndex+1].LastPlaylistSaveTimeString;
        //Show playlist name entry dialog, hide immediate mode checkbox
        Label4.Visible := TRUE;
        Edit1.Visible := TRUE;
        Label9.Visible := FALSE;
        ImmediateModeBreakingNews.Visible := FALSE;
        EnableBreakingNews.Visible := FALSE;
        BreakingNewsIterations.Visible := FALSE;
        SingleLoopEnable.Visible := TRUE;
        DatabaseMaintPlaylistGrid.DataSource := dmMain.dsTicker_Groups;
        DatabaseMaintBreakingNewsPlaylistGrid.Visible := FALSE;
      end;
      //Alerts
   1: begin
        //Hide tab sheet for schedule
        ProgramModePageControl.Pages[0].TabVisible := FALSE;
        //Hide default template grid/list save panel
        TemplateListSaveControlsPnl.Visible := FALSE;
        DefaultTemplatesGrid.Visible := FALSE;
        //Show playlist grid
        PlaylistGrid.Visible := TRUE;
        DatabaseMaintPlaylistGrid.DataSource := dmMain.dsBreakingNews_Groups;
        AvailablePlaylistsGrid.DataSource := dmMain.dsBreakingNews_Groups;
        ScheduledPlaylistsGrid.DataSource := dmMain.dsScheduled_BreakingNews_Groups;
        //Hide playlist name entry dialog, show immediate mode checkbox
        Label4.Visible := FALSE;
        Edit1.Visible := FALSE;
        Label9.Visible := TRUE;
        ImmediateModeBreakingNews.Visible := TRUE;
        EnableBreakingNews.Visible := TRUE;
        BreakingNewsIterations.Visible := TRUE;
        SingleLoopEnable.Visible := FALSE;
        DatabaseMaintBreakingNewsPlaylistGrid.DataSource := dmMain.dsBreakingNews_Groups;
        DatabaseMaintBreakingNewsPlaylistGrid.Visible := TRUE;
      end;
      //Default template specificaton
   2: begin
        //Show tab sheet for schedule
        ProgramModePageControl.Pages[0].TabVisible := TRUE;
        //Hide playlist grid
        PlaylistGrid.Visible := FALSE;
        //Show default template list/grid & save panel
        TemplateListSaveControlsPnl.Visible := TRUE;
        DefaultTemplatesGrid.Visible := TRUE;
        DatabaseMaintPlaylistGrid.DataSource := dmMain.dsTicker_Groups;
        AvailablePlaylistsGrid.DataSource := dmMain.dsTicker_Groups;
        ScheduledPlaylistsGrid.DataSource := dmMain.dsScheduled_Ticker_Groups;
        LastSaveTimeLabel.Caption :=
          PlaylistInfo[PlaylistSelectTabControl.TabIndex+1].LastPlaylistSaveTimeString;
        //Hide playlist name entry dialog, hide immediate mode checkbox
        Label4.Visible := FALSE;
        Edit1.Visible := FALSE;
        Label9.Visible := FALSE;
        ImmediateModeBreakingNews.Visible := FALSE;
        EnableBreakingNews.Visible := FALSE;
        BreakingNewsIterations.Visible := FALSE;
        SingleLoopEnable.Visible := FALSE;
        DatabaseMaintPlaylistGrid.DataSource := dmMain.dsTicker_Groups;
        DatabaseMaintBreakingNewsPlaylistGrid.Visible := FALSE;
      end;
  end;
  //Set playlist name
  Edit1.Text := PlaylistInfo[PlaylistSelectTabControl.TabIndex+1].PlaylistName;
  PlaylistNameLabel.Caption := PlaylistInfo[PlaylistSelectTabControl.TabIndex+1].PlaylistName;
  LastSaveTimeLabel.Caption :=
    PlaylistInfo[PlaylistSelectTabControl.TabIndex+1].LastPlaylistSaveTimeString;
  //Set labels and control attributes
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker
   0: begin
        Label16.Visible := TRUE;
        DoubleLineCheck.Visible := TRUE;
        SingleLineCheck.Visible := TRUE;
        SchedulePanel.Color := clSilver;
        DataEntryPanel.Color := clSilver;
        DBMaintPanel.Color := clSilver;
        PlaylistModeLabel.Color := clSilver;
        PlaylistModeLabel.Caption := 'PLAYLIST MODE';
      end;
      //Alerts
   1: begin
        Label16.Visible := FALSE;
        DoubleLineCheck.Visible := FALSE;
        SingleLineCheck.Visible := FALSE;
        SchedulePanel.Color := clSkyBlue;
        DataEntryPanel.Color := clSkyBlue;
        DBMaintPanel.Color := clSkyBLue;
        PlaylistModeLabel.Color := clSkyBlue;
        PlaylistModeLabel.Caption := 'ALERTS';
        //Check if immediate mode is checked
        if (ImmediateModeBreakingNews.Checked) then
        begin
          ImmediateModeBreakingNews.Color := clRed;
          PlaylistModeLabel.Color := clRed;
          PlaylistModeLabel.Caption := 'IMMEDIATE MODE';
        end
        else begin
          ImmediateModeBreakingNews.Color := clBtnFace;
          PlaylistModeLabel.Color := clSkyBlue;
          PlaylistModeLabel.Caption := 'ALERTS';
        end;
      end;
      //Default templates
   2: begin
        Label16.Visible := FALSE;
        DoubleLineCheck.Visible := TRUE;
        SingleLineCheck.Visible := TRUE;
        SchedulePanel.Color := clGray;
        DataEntryPanel.Color := clGray;
        DBMaintPanel.Color := clGray;
        PlaylistModeLabel.Color := clGray;
        PlaylistModeLabel.Caption := 'DEFAULT TEMPLATES MODE';
      end;
  end;

  //Populate the league tab control based on ticker/alerts selection
  if (Categories_Collection.Count > 0) then
  begin
    LeagueTab.Tabs.Clear;
    for i := 0 to Categories_Collection.Count-1 do
    begin
      CategoryRecPtr := Categories_Collection.At(i);
      With CategoryRecPtr^ do
      begin
        //Modified to handle default templates tab page
        if (Category_Type = PlaylistSelectTabControl.TabIndex+1) OR
           //This causes Default Templates page to be populated
           (Category_Type = PlaylistSelectTabControl.TabIndex-1) then LeagueTab.Tabs.Add (Category_Name);
      end;
   end;
  end;

  //Check to see if category tab is a sport; if so, show and populate games grid
  CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
  //Now, populate the templates grid by querying the database
  dmMain.Query2.Active := FALSE;
  dmMain.Query2.SQL.Clear;
  dmMain.Query2.Filtered := TRUE;
  //Filter for templates applicable to current playlist type
  if (PlaylistSelectTabControl.TabIndex = 1) then
    //Alerts
    TemplateType := 2
  else
    //Ticker or Default Templates
    TemplateType := 1;
  dmMain.Query2.Filter := 'Template_Type = ' + IntToStr(TemplateType);
  dmMain.Query2.SQL.Add ('SELECT * FROM Category_Templates WHERE ' +
    'Category_ID = ' + IntToStr(CategoryRecPtr^.Category_ID));
  dmMain.Query2.Active := TRUE;
  //Refresh the grids
  RefreshPlaylistGrid(FIELD_1, PlaylistGrid);
  RefreshEntryFieldsGrid(TickerDisplayMode, FIELD_1, EntryFieldsGrid);
  //Force refresh of available templates
  LeagueTabChange(Self);
end;

//Handler for change in schedule selection
procedure TMainForm.ScheduleSelectTabChange(Sender: TObject);
begin
end;

//Handler for change in database maintenance selection
procedure TMainForm.DatabaseMaintSelectTabChange(Sender: TObject);
begin
end;

//Handler for check/uncheck of odds only checkbox
procedure TMainForm.ShowOddsOnlyClick(Sender: TObject);
begin
  if (ShowOddsOnly.Checked) then
    ManualOverridePanel.Color := clYellow
  else
    ManualOverridePanel.Color := clBtnFace;
  LeagueTabChange(Self);
end;

//Handler for a change in the league select tab
procedure TMainForm.LeagueTabChange(Sender: TObject);
var
  CategoryRecPtr: ^CategoryRec;
  Day, Month, Year: Word;
  PastStr, FutureStr: String;
  QueryString: String;
  TableName: String;
  OddsOnly: SmallInt;
  LeagueStr: String;
  QueryStr: String;
  TemplateType: SmallInt;
begin
  //Check to see if category tab is a sport; if so, show and populate games grid
  CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
  //First, check to see if sponsors. If so, show sponsor grid for ticker mode (but not for alerts)
  if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') AND (PlaylistSelectTabControl.TabIndex = 0) then
  begin
    SponsorsDBGrid.Visible := TRUE;
    SponsorDwellPanel.Visible := TRUE;
    GamesDBGrid.Visible := FALSE;
    OddsDBGrid.Visible := FALSE;
    ManualOverridePanel.Visible := FALSE;
    ManualLeaguePanel.Visible := FALSE;
    CitiesDBGrid.Visible := FALSE;
  end
  else if (UpperCase(CategoryRecPtr^.Category_Name) = 'WEATHER') AND (PlaylistSelectTabControl.TabIndex = 0) then
  begin
    SponsorsDBGrid.Visible := FALSE;
    SponsorDwellPanel.Visible := FALSE;
    GamesDBGrid.Visible := FALSE;
    OddsDBGrid.Visible := FALSE;
    ManualOverridePanel.Visible := FALSE;
    ManualLeaguePanel.Visible := FALSE;
    CitiesDBGrid.Visible := TRUE;
  end
  //Show games
  else if (CategoryRecPtr^.Category_Is_Sport = TRUE) AND (PlaylistSelectTabControl.TabIndex = 0) then
  begin
    SponsorsDBGrid.Visible := FALSE;
    SponsorDwellPanel.Visible := FALSE;
    CitiesDBGrid.Visible := FALSE;
    if (ShowOddsOnly.Checked) then
    begin
      OddsDBGrid.Visible := TRUE;
      GamesDBGrid.Visible := FALSE;
    end
    else begin
      OddsDBGrid.Visible := FALSE;
      GamesDBGrid.Visible := TRUE;
    end;
    //ManualOverridePanel.Visible := TRUE;
    ManualOverridePanel.Visible := FALSE;
    ManualLeaguePanel.Visible := FALSE;
    //Setup query to run stored procedure
    //Show games grid
    dmMain.SportbaseQuery.Active := FALSE;
    dmMain.SportbaseQuery.SQL.Clear;

    //Setup query
    TableName := CategoryRecPtr^.Sport_Games_Table_Name;
    if (Trim(TableName) <> '') then
    begin
      //Show games grid
      dmMain.SportbaseQuery.Active := FALSE;
      dmMain.SportbaseQuery.SQL.Clear;

      QueryString := 'SELECT * FROM ' + TableName;
      //Now, add so that only today's and yesterday's games are shown
      DecodeDate(Now-GameScheduleDaysBack, Year, Month, Day);
      PastStr := IntToStr(Month) + '/' + IntToStr(Day) + '/' + IntToStr(Year);
      DecodeDate(Now+GameScheduleDaysForward, Year, Month, Day);
      FutureStr := IntToStr(Month) + '/' + IntToStr(Day) + '/' + IntToStr(Year);
      if not (DebugMode) then
      begin
        QueryString := QueryString + ' WHERE ((GDate >= ' + QuotedStr(PastStr) + ') AND (GDate <= ' + QuotedStr(FutureStr) + '))';
      end;
      //Set to order by date
      QueryString := QueryString + ' ORDER BY GDATE ASC';
      //Add additional sort criteria for Subleague if MLB
      if (CategoryRecPtr^.Category_Sport = 'MLB') then
        QueryString := QueryString + ', Subleague ASC';
      //Set to order by date
      QueryString := QueryString + ', CAST(START AS DATETIME) ASC';
      dmMain.SportbaseQuery.SQL.Add(QueryString);
      dmMain.SportbaseQuery.Active := TRUE;
    end;
  end
  //Manual templates
  else begin
    SponsorsDBGrid.Visible := FALSE;
    SponsorDwellPanel.Visible := FALSE;
    GamesDBGrid.Visible := FALSE;
    ManualOverridePanel.Visible := FALSE;
    ManualLeaguePanel.Visible := TRUE;
    CitiesDBGrid.Visible := FALSE;
  end;

  //Now, populate the templates grid by querying the database
  dmMain.Query2.Active := FALSE;
  dmMain.Query2.SQL.Clear;
  dmMain.Query2.Filtered := TRUE;
  //Filter for templates applicable to current playlist type
  //if (PlaylistSelectTabControl.TabIndex = 1) then
  //  //Alerts
  //  TemplateType := 2
  //else
  //  //Ticker or Default Templates
  //  TemplateType := 1;

  //Modified for L-Bar
  //dmMain.Query2.Filter := 'Template_Type = ' + IntToStr(TemplateType);
  dmMain.Query2.Filter := 'Template_Type = 1 OR Template_Type = 2';

  //Add filter to query to ensure that only applicable templates are displayed (odds only)
  if (ShowOddsOnly.Checked) then OddsOnly := 1
  else OddsOnly := 0;
  //if (FormatForSNY) then
  //  QueryStr := 'SELECT * FROM Category_Templates_SNY WHERE ' +
  //    'Category_ID = ' + IntToStr(CategoryRecPtr^.Category_ID) + ' AND Template_IsOddsOnly = ' + IntToStr(OddsOnly)
  //else
    QueryStr := 'SELECT * FROM Category_Templates WHERE ' +
      'Category_ID = ' + IntToStr(CategoryRecPtr^.Category_ID) + ' AND Template_IsOddsOnly = ' + IntToStr(OddsOnly);
  //Add to query to filter templates based on 1 or 2 line display modes
  //if (TickerDisplayMode = 1) OR (TickerDisplayMode = 2) then
  //  //1-line ticker
  //  QueryStr := QueryStr + ' AND ((Template_ID < 1000) OR (Template_ID >= 10000))'
  //else
  //  //2-line ticker
  //  QueryStr := QueryStr + ' AND ((Template_ID >= 1000) AND (Template_ID < 10000))';
  //Check for crawl templates enabled
  if not (EnableCrawlTemplates) then
    QueryStr := QueryStr + ' AND (Template_UsesCrawl = 0)';
  dmMain.Query2.SQL.Add (QueryStr);
  dmMain.Query2.Active := TRUE;

  //Refresh the Default Templates grid
  RefreshDefaultTemplatesGrid;
end;

//Handler for playlist collapse/expand button
procedure TMainForm.ExpandButtonClick(Sender: TObject);
begin
  if (ExpandButton.Caption = 'Expand Playlist') then
  begin
    ExpandButton.Caption := 'Collapse Playlist';
    PlaylistGrid.Top := 43;
    PlaylistGrid.Height := 469;
    PlaylistGrid2.Top := 43;
    PlaylistGrid2.Height := 469;
  end
  else begin
    ExpandButton.Caption := 'Expand Playlist';
    PlaylistGrid.Top := 43;
    PlaylistGrid.Height := 300;
    PlaylistGrid2.Top := 43;
    PlaylistGrid2.Height := 300;
  end;
end;

//Handlers to alert operator if they attempt to change modes with entries in the playlist
procedure TMainForm.DoubleLineCheckMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Control: Word;
begin
  //Alert operator if there are entries in the collection
  if (Ticker_Collection.Count > 0) then
  begin
//    MessageDlg('You have entries in the playlist. To switch modes you must first delete the ' +
//      'current entries in the playlist.', mtWarning, [mbOK], 0);
    Control := MessageDlg('You have entries in the playlist. Switching modes may cause some entries in the ' +
      'playlist to be discarded. Do you wish to proceed?', mtWarning, [mbYes, mbNo], 0);

    //Only proceed if operator confirmed
    if (Control = mrYes) then
    begin
      //Call function to re-assign template IDs
      ReAssignTemplateIDsOnModeChange;

      //Switch to alternate mode
      if (TickerDisplayMode = 1) OR (TickerDisplayMode = 2) then
      begin
        DoubleLineCheck.Checked := TRUE;
        SingleLineCheck.Checked := FALSE;
        Exit;
      end
      else if (TickerDisplayMode = 3) OR (TickerDisplayMode = 4) then
      begin
        DoubleLineCheck.Checked := FALSE;
        SingleLineCheck.Checked := TRUE;
        Exit;
      end;
    end
    else begin
      //Clear the check
      if (TickerDisplayMode = 1) OR (TickerDisplayMode = 2) then
      begin
        DoubleLineCheck.Checked := FALSE;
        SingleLineCheck.Checked := TRUE;
        Exit;
      end
      else if (TickerDisplayMode = 3) OR (TickerDisplayMode = 4) then
      begin
        DoubleLineCheck.Checked := TRUE;
        SingleLineCheck.Checked := FALSE;
        Exit;
      end;
    end;
  end;
end;
procedure TMainForm.SingleLineCheckMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Control: Word;
begin
  //Alert operator if there are entries in the collection
  if (Ticker_Collection.Count > 0) then
  begin
    Control := MessageDlg('You have entries in the playlist. Switching modes may cause some entries in the ' +
      'playlist to be discarded. Do you wish to proceed?', mtWarning, [mbYes, mbNo], 0);

    //Only proceed if operator confirmed
    if (Control = mrYes) then
    begin
      //Call function to re-assign template IDs
      ReAssignTemplateIDsOnModeChange;
      //Switch to alternate mode
      if (TickerDisplayMode = 1) OR (TickerDisplayMode = 2) then
      begin
        DoubleLineCheck.Checked := TRUE;
        SingleLineCheck.Checked := FALSE;
        Exit;
      end
      else if (TickerDisplayMode = 3) OR (TickerDisplayMode = 4) then
      begin
        DoubleLineCheck.Checked := FALSE;
        SingleLineCheck.Checked := TRUE;
        Exit;
      end;
    end
    else begin
      //Clear the check
      if (TickerDisplayMode = 1) OR (TickerDisplayMode = 2) then
      begin
        DoubleLineCheck.Checked := FALSE;
        SingleLineCheck.Checked := TRUE;
        Exit;
      end
      else if (TickerDisplayMode = 3) OR (TickerDisplayMode = 4) then
      begin
        DoubleLineCheck.Checked := TRUE;
        SingleLineCheck.Checked := FALSE;
        Exit;
      end;
    end;
  end;
end;

//Handler for clicking double line ticker checkbox
procedure TMainForm.DoubleLineCheckClick(Sender: TObject);
begin
  if (DoubleLineCheck.Checked = TRUE) then
  begin
    DoubleLineCheck.Color := clWhite;
    SingleLineCheck.Color := clSilver;
    SingleLineCheck.Checked := FALSE;
    if (SingleLoopEnable.Checked) then
      //2-line, single loop
      TickerDisplayMode := 3
    else
      //2-line, continuous loop
      TickerDisplayMode := 4;
  end
  else begin
    DoubleLineCheck.Color := clSilver;
    SingleLineCheck.Color := clWhite;
    SingleLineCheck.Checked := TRUE;
    if (SingleLoopEnable.Checked) then
      //1-line, single loop
      TickerDisplayMode := 1
    else
      //1-line, continuous loop
      TickerDisplayMode := 2;
  end;
  //Refresh the entries field grid
  RefreshEntryFieldsGrid(TickerDisplayMode, FIELD_1, EntryFieldsGrid);
  //Preview the record
  PreviewCurrentPlaylistRecord(PLAYLIST_1);
  PreviewCurrentPlaylistRecord(PLAYLIST_2);
  //Reload templates
  LeagueTab.OnChange(self);
end;

//Handler for clicking single line ticker checkbox
procedure TMainForm.SingleLineCheckClick(Sender: TObject);
begin
  if (SingleLineCheck.Checked = TRUE) then
  begin
    DoubleLineCheck.Color := clSilver;
    SingleLineCheck.Color := clWhite;
    DoubleLineCheck.Checked := FALSE;
    if (SingleLoopEnable.Checked) then
      //1-line, single loop
      TickerDisplayMode := 1
    else
      //1-line, continuous loop
      TickerDisplayMode := 2;
  end
  else begin
    DoubleLineCheck.Color := clWhite;
    SingleLineCheck.Color := clSilver;
    DoubleLineCheck.Checked := TRUE;
    if (SingleLoopEnable.Checked) then
      //2-line, single loop
      TickerDisplayMode := 3
    else
      //2-line, continuous loop
      TickerDisplayMode := 4;
  end;
  //Reload templates
  LeagueTab.OnChange(self);
end;

//Procedure to set to single line ticker mode
procedure TMainForm.SetToSingleLineMode;
begin
  DoubleLineCheck.Color := clSilver;
  SingleLineCheck.Color := clWhite;
  SingleLineCheck.Checked := TRUE;
  DoubleLineCheck.Checked := FALSE;
  if (SingleLoopEnable.Checked) then
    //1-line, single loop
    TickerDisplayMode := 1
  else
    //1-line, continuous loop
    TickerDisplayMode := 2;
end;

//Procedure to set to single line ticker mode
procedure TMainForm.SetToDoubleLineMode;
begin
  DoubleLineCheck.Color := clWhite;
  SingleLineCheck.Color := clSilver;
  SingleLineCheck.Checked := FALSE;
  DoubleLineCheck.Checked := TRUE;
  if (SingleLoopEnable.Checked) then
    //2-line, single loop
    TickerDisplayMode := 3
  else
    //2-line, continuous loop
    TickerDisplayMode := 4;
end;

//Handler for 1x loop enable checkbox
procedure TMainForm.SingleLoopEnableClick(Sender: TObject);
begin
  //Loop 1X
  if (SingleLoopEnable.Checked = TRUE) then
  begin
    if (SingleLineCheck.Checked) then
      //2-line, single loop
      TickerDisplayMode := 1
    else
      //2-line, continuous loop
      TickerDisplayMode := 3;
    //Set color
    SingleLoopEnable.Color := clYellow;
  end
  //Loop continuously
  else begin
    if (SingleLineCheck.Checked) then
      //2-line, single loop
      TickerDisplayMode := 2
    else
      //2-line, continuous loop
      TickerDisplayMode := 4;
    //Set color
    SingleLoopEnable.Color := clGray;
  end;
end;

//Procedure to switch to alternate template IDs on mode change
//Initial version supports 1-Line -> 2-Line conversion
procedure TMainForm.ReAssignTemplateIDsOnModeChange;
var
  i,j,k: SmallInt;
  TickerCollectionCount: SmallInt;
  TickerRecPtr, TickerRecPtr2, NewTickerRecPtr: ^TickerRec;
  TemplateDefsRecPtr: ^TemplateDefsRec;
  Temp_Template_ID: SmallInt;
  MaxCount: SmallInt;
  SaveString1, SaveString2: String;
  EventGUID: TGUID;
begin
  //Check for collection entries and substitute alternate template IDs
  if (Ticker_Collection.Count > 0) then
  begin
    //Check to see if successive 1-line entries need to be combined into a single 2-line entry
    //Only act if saving out in 2-line mode FROM 1-line mode
    if (TickerDisplayMode = 1) OR (TickerDisplayMode = 2) then
    begin
      //Iterate through collection; if template ID is for 1-line and saving in 2-line mode,
      //combine successive 1-line notes into single 2-line note
      MaxCount := Ticker_Collection.Count-1;
      i := 0;
      While i <= MaxCount do
      begin
        //Point to collection object
        TickerRecPtr := Ticker_Collection.At(i);

        //Check for record type > 1000 indicating successive notes should be combined; > 10000 is a special case
        //for stats related pages
        if (GetRecordTypeForTemplate(TickerRecPtr^.Template_ID) > 1000) AND
           (GetRecordTypeForTemplate(TickerRecPtr^.Template_ID) < 10000) AND
           (i < Ticker_Collection.Count-1) then
        begin
          //Save template ID of first note
          Temp_Template_ID :=  TickerRecPtr^.Template_ID;
          //Save first user defined string value; will be combined
          SaveString1 := TickerRecPtr^.UserData[1];
          //Increment to next record
          Inc(i);
          //Point to collection object
          TickerRecPtr := Ticker_Collection.At(i);
          //Check template ID and record type for next record; if also of same type and record type > 1000,
          //combine notes into single template and proceed
          if (GetRecordTypeForTemplate(TickerRecPtr^.Template_ID) > 1000) AND
             (GetRecordTypeForTemplate(TickerRecPtr^.Template_ID) < 10000) AND
             (TickerRecPtr^.Template_ID = Temp_Template_ID) then
          begin
            SaveString2 := TickerRecPtr^.UserData[1];
            TickerRecPtr^.UserData[1] := SaveString1;
            TickerRecPtr^.UserData[2] := SaveString2;
            //Now, set the prior record for deletion - will be deleted below
            TickerRecPtr2 := Ticker_Collection.At(i-1);
            TickerRecPtr2^.UserData[1] := 'DELETE';
          end
          //Not of same type, so don't combine; decrement ticker collection counter to make sure separate
          //record is inserted
          else Dec(i);
        end;
        inc(i);
      end;
    end

    //Check to see if single 2-line entry should be expanded into 2 separate 1-line entries
    //Only act if saving out in 1-line mode FROM 2-line mode
    else if (TickerDisplayMode = 3) OR (TickerDisplayMode = 4) then
    begin
      //Iterate through collection; if template ID is for 1-line and saving in 2-line mode,
      //combine successive 1-line notes into single 2-line note
      MaxCount := Ticker_Collection.Count-1;
      i := 0;
      While i <= MaxCount do
      begin
        //Point to collection object
        TickerRecPtr := Ticker_Collection.At(i);

        //Check for record type > 1000 indicating notes should be expanded
        //Note: Record types > 10000 reserved for stats related functions
        if (GetRecordTypeForTemplate(TickerRecPtr^.Template_ID) > 1000) AND
           (GetRecordTypeForTemplate(TickerRecPtr^.Template_ID) < 10000) AND (i <= Ticker_Collection.Count-1) then
        begin
          //Save second user defined string values; will be expanded into two templates if there is text
          //in the second field
          SaveString1 := TickerRecPtr^.UserData[2];
          if (Trim(SaveString1) <> '') then
          begin
            //Insert new record
            GetMem (NewTickerRecPtr, SizeOf(TickerRec));
            With NewTickerRecPtr^ do
            begin
              Event_Index := TickerRecPtr^.Event_Index+1;
              CreateGUID(EventGUID);
              Event_GUID := EventGUID;
              Enabled := TickerRecPtr^.Enabled;
              Is_Child := TickerRecPtr^.Is_Child;
              League := TickerRecPtr^.League;
              Subleague_Mnemonic_Standard := TickerRecPtr^.Subleague_Mnemonic_Standard;
              Mnemonic_LiveEvent := TickerRecPtr^.Mnemonic_LiveEvent;
              Template_ID := TickerRecPtr^.Template_ID;
              Record_Type := TickerRecPtr^.Record_Type;
              GameID := TickerRecPtr^.GameID;
              SponsorLogo_Name := TickerRecPtr^.SponsorLogo_Name;
              SponsorLogo_Dwell := TickerRecPtr^.SponsorLogo_Dwell;
              StatStoredProcedure := TickerRecPtr^.StatStoredProcedure;
              StatType := TickerRecPtr^.StatType;
              StatTeam := TickerRecPtr^.StatTeam;
              StatLeague := TickerRecPtr^.StatLeague;
              StatRecordNumber := TickerRecPtr^.StatRecordNumber;
              UserData[1] := SaveString1;
              for k := 2 to 50 do UserData[k] := '';
              StartEnableDateTime := TickerRecPtr^.StartEnableDateTime;
              EndEnableDateTime := TickerRecPtr^.EndEnableDateTime;
              Selected := TickerRecPtr^.Selected;
              DwellTime := TickerRecPtr^.DwellTime;
              Description := TickerRecPtr^.Description;
              Comments := TickerRecPtr^.Comments;
              If (Ticker_Collection.Count <= 1500) then
              begin
                //If last entry, append to end of collection; otherwise insert
                if (i = MaxCount) then
                  Ticker_Collection.Insert(NewTickerRecPtr)
                else
                  Ticker_Collection.AtInsert(i+1, NewTickerRecPtr);
                Ticker_Collection.Pack;
                //Increment the Max Count variable
                Inc(MaxCount);
              end;
            end;
          end;
        end;
        //Go to next record
        inc(i);
      end;
    end;

    //Re-assign the template IDs 1-Line <-> 2-Line
    //Set ticker collection count - may be decremented if entry needs to be discarded
    TickerCollectionCount := Ticker_Collection.Count;
    i := 0;
    //Walk through ticker collection and switch template IDs
    while i < TickerCollectionCount do
    begin
      //Get ticker element and current template ID
      TickerRecPtr := Ticker_Collection.At(i);
      Temp_Template_ID := TickerRecPtr^.Template_ID;
      //Only act if there are template definitions
      if (Template_Defs_Collection.Count > 0) then
      begin
        //Walk through templates to find alternate mode ID
        for j := 0 to Template_Defs_Collection.Count-1 do
        begin
          TemplateDefsRecPtr := Template_Defs_Collection.At(j);
          //Check for match between ticker element template ID and entry in template defs
          if (TemplateDefsRecPtr^.Template_ID = Temp_Template_ID) then
          begin
            //Alternate template ID found, so re-assign template ID
            if (TemplateDefsRecPtr^.AlternateModeTemplateID > 0) then
            begin
              TickerRecPtr^.Template_ID := TemplateDefsRecPtr^.AlternateModeTemplateID;
              TickerRecPtr^.Record_Type := GetRecordTypeForTemplate(TemplateDefsRecPtr^.AlternateModeTemplateID);
            end
            //No alternate template ID, so delete record (invalid alternate mode template IDs set to -1)
            else begin
              //Delete record
              Ticker_Collection.AtDelete(i);
              //Decrement collection count if less than max
              if (i < TickerCollectionCount-1) then
              begin
                i := i-1;
                Dec(TickerCollectionCount);
              end;
            end;
          end;
        end;
      end;
      Inc(i);
    end;

    //Delete any records marked for deletion after combining notes from consecutive 1-line entries
    //into a single 2-line entry
    MaxCount := Ticker_Collection.Count-1;
    i := 0;
    while i < MaxCount do
    begin
      TickerRecPtr := Ticker_Collection.At(i);
      if (TickerRecPtr^.UserData[1] = 'DELETE') then
      begin
        Ticker_Collection.AtDelete(i);
        Ticker_Collection.Pack;
        Dec(MaxCount);
      end;
      inc(i);
    end;

    //Added for Phase 2 Stats Inc. mods
    //Check to see if single stat entry should be expanded into 2 separate 1-line entries
    //The re-assigned template will be the first line; 2nd template added below
    //e.g. First template = Visitor; Second template = Home
    if (TickerDisplayMode = 3) OR (TickerDisplayMode = 4) then
    begin
      MaxCount := Ticker_Collection.Count-1;
      i := 0;
      While i <= MaxCount do
      begin
        //Point to collection object
        TickerRecPtr := Ticker_Collection.At(i);
        //Added for Phase 2 Stats Inc. Mod; expands stats related 2-line templates to two single-line templates
        //Check for record types > 20000 reserved for 1st template of 2 template expansion
        if (GetRecordTypeForTemplate(TickerRecPtr^.Template_ID) > 20000) AND (i <= Ticker_Collection.Count-1) then
        begin
          //Bump template ID and record type for single-line mode
          //Insert new record
          GetMem (NewTickerRecPtr, SizeOf(TickerRec));
          With NewTickerRecPtr^ do
          begin
            Event_Index := TickerRecPtr^.Event_Index+1;
            CreateGUID(EventGUID);
            Event_GUID := EventGUID;
            Enabled := TickerRecPtr^.Enabled;
            Is_Child := TickerRecPtr^.Is_Child;
            League := TickerRecPtr^.League;
            Subleague_Mnemonic_Standard := TickerRecPtr^.Subleague_Mnemonic_Standard;
            Mnemonic_LiveEvent := TickerRecPtr^.Mnemonic_LiveEvent;
            Template_ID := TickerRecPtr^.Template_ID + 10000; //Add 10000 to get home team template
            Record_Type := TickerRecPtr^.Record_Type + 10000; //Add 10000 to get home team template record type
            GameID := TickerRecPtr^.GameID;
            SponsorLogo_Name := TickerRecPtr^.SponsorLogo_Name;
            SponsorLogo_Dwell := TickerRecPtr^.SponsorLogo_Dwell;
            StatStoredProcedure := TickerRecPtr^.StatStoredProcedure;
            StatType := TickerRecPtr^.StatType;
            StatTeam := TickerRecPtr^.StatTeam;
            StatLeague := TickerRecPtr^.StatLeague;
            StatRecordNumber := TickerRecPtr^.StatRecordNumber;
            UserData[1] := SaveString1;
            for k := 2 to 50 do UserData[k] := '';
            StartEnableDateTime := TickerRecPtr^.StartEnableDateTime;
            EndEnableDateTime := TickerRecPtr^.EndEnableDateTime;
            Selected := TickerRecPtr^.Selected;
            DwellTime := TickerRecPtr^.DwellTime;
            Description := TickerRecPtr^.Description;
            Comments := TickerRecPtr^.Comments;
            If (Ticker_Collection.Count <= 1500) then
            begin
              //If last entry, append to end of collection; otherwise insert
              if (i = MaxCount) then
                Ticker_Collection.Insert(NewTickerRecPtr)
              else
                Ticker_Collection.AtInsert(i+1, NewTickerRecPtr);
              Ticker_Collection.Pack;
              //Increment the Max Count variable
              Inc(MaxCount);
            end;
          end;
          inc(i); //Jump past inserted record
        end;
        Inc(i);
      end;
    end;

    //Re-order event indices in collection; may have been affected by conversions
    if (Ticker_Collection.Count > 0) then
    begin
      for i := 0 to Ticker_Collection.Count-1 do
      begin
        TickerRecPtr := Ticker_Collection.At(i);
        TickerRecPtr^.Event_Index := i+1;
      end;
    end;

    //Refresh the grid
    RefreshPlaylistGrid(FIELD_1, PlaylistGrid);
  end;
end;

//Function to return the record type for a specific template ID
function TMainForm.GetRecordTypeForTemplate(TemplateID: SmallInt): SmallInt;
var
  i: SmallInt;
  TemplateDefsRecPtr: ^TemplateDefsRec;
  OutVal: SmallInt;
begin
  if (Template_Defs_Collection.Count > 0) then
  begin
    for i := 0 to Template_Defs_Collection.Count-1 do
    begin
      TemplateDefsRecPtr := Template_Defs_Collection.At(i);
      if (TemplateDefsRecPtr^.Template_ID = TemplateID) then
        OutVal := TemplateDefsRecPtr^.Record_Type;
    end;
  end;
  GetRecordTypeForTemplate := OutVal;
end;

////////////////////////////////////////////////////////////////////////////////
// PROCEDURES AND HANDLERS FOR ZIPPER PLAYLIST CREATION AND EDITING
////////////////////////////////////////////////////////////////////////////////
//General procedure to clear and refresh the trial playlist grid
procedure TMainForm.RefreshPlaylistGrid(GridID: SmallInt; var DataGrid: TtsGrid);
var
  i: SmallInt;
  TickerPtr: ^TickerRec;
  BreakingNewsPtr: ^BreakingNewsRec;
  CollectionCount: SmallInt;
  AutoLeagueMnemonic: String;
  CurrentRow, CurrentTopRow: SmallInt;
begin
  //Get current row
  CurrentRow := DataGrid.CurrentDataRow;
  CurrentTopRow := DataGrid.TopRow;

  //Clear grid values
  if (DataGrid.Rows > 0) then
    DataGrid.DeleteRows (1, DataGrid.Rows);

  //Init values
  Case PlaylistSelectTabControl.TabIndex of
   0: begin
        case GridID of
          FIELD_1: CollectionCount := Ticker_Collection.Count;
          FIELD_2: CollectionCount := Ticker_Collection2.Count;
         end;
      end;
   1: CollectionCount := BreakingNews_Collection.Count;
  end;

  //Setup grid
  if (CollectionCount > 0) then
  begin
    DataGrid.StoreData := TRUE;
    DataGrid.Cols := 7;
    DataGrid.Rows := CollectionCount;
    //Populate the grid
    for i := 0 to CollectionCount-1 do
    begin
      Case PlaylistSelectTabControl.TabIndex of
          //Ticker
       0: begin
            case GridID of
              FIELD_1: TickerPtr := Ticker_Collection.At(i);
              FIELD_2: TickerPtr := Ticker_Collection2.At(i);
            end;
            DataGrid.Cell[1,i+1] := IntToStr(i+1); //Index
            //Check if automated league; if so, lookup display mnemonic
            //DataGrid.Cell[2,i+1] := TickerPtr^.Subleague_Mnemonic_Standard; //Subleague name
            DataGrid.Cell[2,i+1] := TickerPtr^.Enabled; //Record enable
            DataGrid.Cell[3,i+1] := IntToStr(TickerPtr^.DwellTime DIV 1000); //Dwell time
            DataGrid.Cell[4,i+1] := TickerPtr^.Template_ID; //Record enable
            DataGrid.Cell[5,i+1] :=
              GetRecordTypeDescription(PlaylistSelectTabControl.TabIndex+1, TickerPtr^.Record_Type);
            if (TickerPtr^.Record_Type = 2) OR (TickerPtr^.Record_Type = 37) OR (TickerPtr^.Record_Type = 45) then
              DataGrid.Cell[6,i+1] := TickerPtr^.SponsorLogo_Name
            else if (GetTemplateInformation(TickerPtr^.Template_ID).UsesGameData = TRUE) then
              DataGrid.Cell[6,i+1] := TickerPtr^.Description
            else if (TickerPtr^.Record_Type = 3) or (TickerPtr^.Record_Type = 4) or (TickerPtr^.Record_Type = 5) then
              DataGrid.Cell[6,i+1] := TickerPtr^.UserData[21]
            else
              DataGrid.Cell[6,i+1] := TickerPtr^.UserData[1];
            DataGrid.Cell[7,i+1] := TickerPtr^.League; //League name
            DataGrid.Cell[8,i+1] := TickerPtr^.Comments; //Record enable
            //By default, show item over white background except if child template and added as group
            //Modified to always show child templates in gray or silver
            if (GetTemplateInformation(TickerPtr^.Template_ID).Template_Is_Child = TRUE) AND
               (TickerPtr^.Is_Child = TRUE) then
              DataGrid.RowColor[i+1] := clGray
            else if (GetTemplateInformation(TickerPtr^.Template_ID).Template_Is_Child = TRUE) then
              DataGrid.RowColor[i+1] := clMedGray
            else
              DataGrid.RowColor[i+1] := clWindow;
            //Update number fo entries label
            NumEntries.Caption := IntToStr(Ticker_Collection.Count);
            NumEntries2.Caption := IntToStr(Ticker_Collection2.Count);
          end;
          //Alerts
       1: begin
            BreakingNewsPtr := BreakingNews_Collection.At(i);
            DataGrid.Cell[1,i+1] := IntToStr(i+1); //Index
            DataGrid.Cell[2,i+1] := BreakingNewsPtr^.League; //League name
            DataGrid.Cell[3,i+1] := IntToStr(BreakingNewsPtr^.Event_GUID.D2); //League name
            DataGrid.Cell[4,i+1] := BreakingNewsPtr^.Enabled; //Record enable
            DataGrid.Cell[5,i+1] := BreakingNewsPtr^.Template_ID; //Record enable
            DataGrid.Cell[6,i+1] :=
              GetRecordTypeDescription(PlaylistSelectTabControl.TabIndex+1, BreakingNewsPtr^.Record_Type);
            DataGrid.Cell[7,i+1] := BreakingNewsPtr^.UserData[1];
            DataGrid.Cell[8,i+1] := BreakingNewsPtr^.Comments; //Record enable
            DataGrid.RowColor[i+1] := clWindow;
            //Update number fo entries label
            NumEntries.Caption := IntToStr(BreakingNews_Collection.Count);
          end;
      end;
    end;
  end
  else begin
    case GridID of
      FIELD_1: NumEntries.Caption := IntToStr(CollectionCount);
      FIELD_2: NumEntries2.Caption := IntToStr(CollectionCount);
    end;
  end;
  //Refresh grid
  if (CurrentTopRow > 0) AND (CollectionCount > 0) then
  begin
    if (CurrentTopRow > CollectionCount) then CurrentTopRow := CollectionCount;
    DataGrid.TopRow := CurrentTopRow;
  end;
  if (CurrentRow > 0) AND (CollectionCount > 0) then
  begin
    if (CurrentRow > CollectionCount) then CurrentRow := CollectionCount;
    DataGrid.CurrentDataRow := CurrentRow;
    DataGrid.RowSelected[CurrentRow] := TRUE;
  end;
end;

//Handler to refresh the entries grids when a new playlist record is selected
procedure TMainForm.PlaylistGridRowChanged(Sender: TObject; OldRow,
  NewRow: Integer);
begin
  if (PlaylistGrid.Rows > 0) then RefreshEntryFieldsGrid(TickerDisplayMode, FIELD_1, EntryFieldsGrid);
end;
procedure TMainForm.PlaylistGrid2RowChanged(Sender: TObject; OldRow,
  NewRow: Integer);
begin
  if (PlaylistGrid2.Rows > 0) then RefreshEntryFieldsGrid(TickerDisplayMode, FIELD_2, EntryFieldsGrid2);
end;

//Procedure to update the entry fields grid with the data for the currently selected
//record in the playlist; also updates other entry specific data entry controls
procedure TMainForm.RefreshEntryFieldsGrid (TickerMode: SmallInt; GridID: SmallInt; var DataGrid: TtsGrid);
var
  i, j, k: SmallInt;
  TickerRecPtr: ^TickerRec;
  BreakingNewsRecPtr: ^BreakingNewsRec;
  CurrentTemplateID: SmallInt;
  NumTemplateFields: SmallInt;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  CollectionCount: SmallInt;
  UserDataBias: SmallInt;
  TemplateInfo: TemplateDefsRec;
  CurrentGameData: GameRec;
begin
  Case PlaylistSelectTabControl.TabIndex of
   0: begin
        case GridID of
          FIELD_1: CollectionCount := Ticker_Collection.Count;
          FIELD_2: CollectionCount := Ticker_Collection2.Count;
         end;
      end;
   1: CollectionCount := BreakingNews_Collection.Count;
  end;
  //Get data for current record in grid
  if (CollectionCount > 0) then
  begin
    Case PlaylistSelectTabControl.TabIndex of
        //Ticker
     0: begin
          case GridID of
            FIELD_1: TickerRecPtr := Ticker_Collection.At(PlaylistGrid.CurrentDataRow-1);
            FIELD_2: TickerRecPtr := Ticker_Collection2.At(PlaylistGrid2.CurrentDataRow-1);
          end;
          CurrentTemplateID := TickerRecPtr^.Template_ID;
        end;
        //Alerts
     1: begin
          BreakingNewsRecPtr := BreakingNews_Collection.At(PlaylistGrid.CurrentDataRow-1);
          CurrentTemplateID := BreakingNewsRecPtr^.Template_ID;
        end;
    end;
    TemplateDescription.Caption := GetTemplateInformation(CurrentTemplateID).Template_Description;
    NumTemplateFields := GetTemplateFieldsCount(CurrentTemplateID);
    if (NumTemplateFields > 0) then
    begin
      //Clear grid values - grid name passed into procedure
      if (DataGrid.Rows > 0) then
        DataGrid.DeleteRows (1, DataGrid.Rows);
      DataGrid.StoreData := TRUE;
      DataGrid.Cols := 3;
      DataGrid.Rows := NumTemplateFields;
      //Populate the grid
      i := 0;
      k := 0;
      //Get game data if template requires it
      Case PlaylistSelectTabControl.TabIndex of
          //Ticker
       0: begin
            TemplateInfo := EngineInterface.LoadTempTemplateFields(TickerRecPtr^.Template_ID);
            if (TemplateInfo.UsesGameData) then
            begin
              //Use full game data
              CurrentGameData := GameDataFunctions.GetGameData(TRIM(TickerRecPtr^.League), TRIM(TickerRecPtr^.GameID));
            end;
          end;
          //Alerts
       1: begin
            TemplateInfo := EngineInterface.LoadTempTemplateFields(BreakingNewsRecPtr^.Template_ID);
          end;
      end;
      //Get the manually entered fields
      for j := 0 to Template_Fields_Collection.Count-1 do
      begin
        TemplateFieldsRecPtr := Template_Fields_Collection.At(j);
        if (TemplateFieldsRecPtr^.Template_ID = CurrentTemplateID) then
        begin
          DataGrid.Cell[1,i+1] := TemplateFieldsRecPtr^.Field_ID; //Index
          DataGrid.Cell[2,i+1] := TemplateFieldsRecPtr^.Field_Label; //Description
          //Check for manual fields
          if (TemplateFieldsRecPtr^.Field_Is_Manual = TRUE) then
          begin
            //Set bias
            //Standard mode
            UserDataBias := 0;
            DataGrid.RowColor[i+1] := clWindow;
            Case PlaylistSelectTabControl.TabIndex of
                //Ticker
             0: begin
                  //Insert league mnemonic and apply offset to account for league value being
                  //stored explicitly in ticker record
                  if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_LEAGUE_HEADER) then
                  begin
                    DataGrid.Cell[3,i+1] := TickerRecPtr^.League;
                    Dec(k);
                  end
                  //Check for image files
                  else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_LEAGUE_LOGO_1A) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_LEAGUE_LOGO_1B) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_1A) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_1B) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_PLAYER_HEADSHOT_1A) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_PLAYER_HEADSHOT_1B) or
                    //Added for Version 2.0
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_FULL_PROMO_GRAPHIC) then
                    DataGrid.Cell[3,i+1] := TickerRecPtr^.UserData[21]
                  else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_LEAGUE_LOGO_2A) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_LEAGUE_LOGO_2B) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_2A) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_2B) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_PLAYER_HEADSHOT_2A) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_PLAYER_HEADSHOT_2B) then
                    DataGrid.Cell[3,i+1] := TickerRecPtr^.UserData[22]
                  else
                    DataGrid.Cell[3,i+1] := TickerRecPtr^.UserData[UserDataBias + k+1];
                end;
                //Alerts
             1: begin
                  //Insert league mnemonic and apply offset to account for league value being
                  //stored explicitly in ticker record
                  if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_LEAGUE_HEADER) then
                  begin
                    DataGrid.Cell[3,i+1] := BreakingNewsRecPtr^.League;
                    Dec(k);
                  end
                  else
                    DataGrid.Cell[3,i+1] := BreakingNewsRecPtr^.UserData[k+1];
                end;
            end;
            Inc(k);
          end
          else begin
            //Set cell value
            Case PlaylistSelectTabControl.TabIndex of
                //Ticker
             0: begin
                  //Get game data if template requires it
                  case GridID of
                    FIELD_1: DataGrid.Cell[3,i+1] :=
                      GetValueOfSymbol(FIELD_1, TICKER, TemplateFieldsRecPtr^.Field_Contents,
                        PlaylistGrid.CurrentDataRow-1, CurrentGameData, TickerDisplayMode, TickerRecPtr^.UserData[21]).SymbolValue;
                    FIELD_2: DataGrid.Cell[3,i+1] :=
                      GetValueOfSymbol(FIELD_2, TICKER, TemplateFieldsRecPtr^.Field_Contents,
                        PlaylistGrid2.CurrentDataRow-1, CurrentGameData, TickerDisplayMode, TickerRecPtr^.UserData[21]).SymbolValue;
                  end;
                end;
                //Alerts
             1: begin
                  DataGrid.Cell[3,i+1] :=
                    GetValueOfSymbol(FIELD_1, BREAKINGNEWS, TemplateFieldsRecPtr^.Field_Contents,
                      PlaylistGrid.CurrentDataRow-1, CurrentGameData, TickerDisplayMode, '').SymbolValue;
                end;
            end;
            DataGrid.RowColor[i+1] := clAqua;
          end;
          //Increment the manual fields counter
          Inc (i);
        end;
      end;
    end;
    //Update other fields
    Case PlaylistSelectTabControl.TabIndex of
        //Ticker
     0: begin
          EntryNote.Text := TickerRecPtr^.Comments;
          EntryEnable.Checked := TickerRecPtr^.Enabled;
          EntryStartEnableDate.Date := TickerRecPtr^.StartEnableDateTime;
          EntryStartEnableTime.Time := TickerRecPtr^.StartEnableDateTime;
          EntryEndEnableDate.Date := TickerRecPtr^.EndEnableDateTime;
          EntryEndEnableTime.Time := TickerRecPtr^.EndEnableDateTime;
          EntryDwellTime.Value := Trunc(TickerRecPtr^.DwellTime/1000);
        end;
        //Alerts
     1: begin
          EntryNote.Text := BreakingNewsRecPtr^.Comments;
          EntryEnable.Checked := BreakingNewsRecPtr^.Enabled;
          EntryStartEnableDate.Date := BreakingNewsRecPtr^.StartEnableDateTime;
          EntryStartEnableTime.Time := BreakingNewsRecPtr^.StartEnableDateTime;
          EntryEndEnableDate.Date := BreakingNewsRecPtr^.EndEnableDateTime;
          EntryEndEnableTime.Time := BreakingNewsRecPtr^.EndEnableDateTime;
          EntryDwellTime.Value := Trunc(BreakingNewsRecPtr^.DwellTime/1000);
        end;
    end;
  end
  else begin
    if (DataGrid.Rows > 0) then
      DataGrid.DeleteRows (1, DataGrid.Rows);
  end;
end;

//Handler for append entry to playlist #1 button
procedure TMainForm.AddToPlaylistBtnClick(Sender: TObject);
begin
  Case PlaylistSelectTabControl.TabIndex of
    0..1: AddPlaylistEntry(dmMain.Query2.FieldByName('Template_ID').AsInteger, SINGLETEMPLATEONLY, FIELD_1, PlaylistGrid);
    2: AddDefaultTemplateEntry(dmMain.Query2.FieldByName('Template_ID').AsInteger);
  end;
end;
//Handler for append entry to playlist #2 button
procedure TMainForm.AddToPlaylist2BtnClick(Sender: TObject);
begin
  Case PlaylistSelectTabControl.TabIndex of
    0..1: begin
      if (dmMain.Query2.FieldByName('Template_Type').AsInteger = FIELD_3) then
        MessageDlg('Field 3 templates can only be added to Playlist 1.', mtInformation, [mbOK], 0)
      else if (dmMain.Query2.FieldByName('Category_ID').AsInteger = SPONSOR_CATEGORY) then
        MessageDlg('Sponsors can only be added to Playlist 1.', mtInformation, [mbOK], 0)
      else
        AddPlaylistEntry(dmMain.Query2.FieldByName('Template_ID').AsInteger, SINGLETEMPLATEONLY, FIELD_2, PlaylistGrid2);
    end;
    2: AddDefaultTemplateEntry(dmMain.Query2.FieldByName('Template_ID').AsInteger);
  end;
end;

//Handler for double-click on template select grid  - defaults to Field 1 to add selected template
procedure TMainForm.AvailableTemplatesDBGridDblClick(Sender: TObject);
begin
  Case PlaylistSelectTabControl.TabIndex of
    0..1: begin
      AddPlaylistEntry(dmMain.Query2.FieldByName('Template_ID').AsInteger, SINGLETEMPLATEONLY, FIELD_1, PlaylistGrid);
    end;
    2: AddDefaultTemplateEntry(dmMain.Query2.FieldByName('Template_ID').AsInteger);
  end;
end;

//General procedure for appending an entry to the main ticker playlist
procedure TMainForm.AddPlaylistEntry(CurrentTemplateID: SmallInt; AddAllDefaultTemplates: Boolean; GridID: SmallInt; var DataGrid: TtsGrid);
var
  i,j,k,m,n: SmallInt;
  TickerRecPtr: ^TickerRec;
  BreakingNewsRecPtr: ^BreakingNewsRec;
  SaveRow, SaveRow2: SmallInt;
  Control: Word;
  Modal: TZipperEntryDlg;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  CurrentTemplateDefs: TemplateDefsRec;
  ChildTemplateIDRecPtr: ^ChildTemplateIDRec;
  NumTemplateFields: SmallInt;
  //CurrentTemplateID: SmallInt;
  User_Data: Array[1..50] of String[255];
  CollectionCount: SmallInt;
  CategoryRecPtr: ^CategoryRec;
  EventGUID: TGUID;
  OKToGo: Boolean;
  CurrentGameData: GameRec;
  NumGamesSelected: SmallInt;
  StyleChipRecPtr: ^StyleChipRec;
  LeagueCodeRecPtr: ^LeagueCodeRec;
  WeatherIconRecPtr: ^WeatherIconRec;
  RowSelected: Boolean;
  RecordCount: SmallInt;
  SelectedTemplatesRecPtr: ^DefaultTemplatesRec;
begin
  //Init
  OKToGo := TRUE;

  //////////////////////////////////////////////////////////////////////////////
  //Execute this block of code if selecting individual templates; code for
  //Default Template list below
  //////////////////////////////////////////////////////////////////////////////
  if (AddAllDefaultTemplates = SINGLETEMPLATEONLY) then
  begin
    //Check to see if category tab is a sport; if so, show and populate games grid
    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
    //First, check to see if sponsors. If so, show sponsor grid
    if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
    begin
      //Get number of games selected
      if (dmMain.SportbaseQuery.RecordCount > 0) then
      begin
        NumGamesSelected := GamesDBGrid.SelectedRows.Count;
        if (NumGamesSelected = 0) then
        begin
          GamesDBGrid.SelectRows(GamesDBGrid.CurrentDataRow, GamesDBGrid.CurrentDataRow, TRUE);
          NumGamesSelected := 1;
        end;
      end
      else NumGamesSelected := 0;
    end
    else NumGamesSelected := 0;

    //Init fields
    for i := 1 to 50 do User_Data[i] := ' ';

    //Get collection count
    Case PlaylistSelectTabControl.TabIndex of
      //Ticker
      TICKER:
        Case GridID of
          FIELD_1: CollectionCount := Ticker_Collection.Count;
          FIELD_2: CollectionCount := Ticker_Collection2.Count;
        end;
         //Alerts
      BREAKINGNEWS: CollectionCount := BreakingNews_Collection.Count;
    end;
    if (CollectionCount < 1500) then
    begin
      CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
      NumTemplateFields := GetTemplateFieldsCount(CurrentTemplateID);
      //First, check to make sure that there are games listed if the template requires one
      CurrentTemplateDefs := GetTemplateInformation(CurrentTemplateID);
      if (CurrentTemplateDefs.Record_Type = 1) AND
        (CategoryRecPtr^.Category_Is_Sport = TRUE) AND (dmMain.SportbaseQuery.RecordCount = 0) then
      begin
        MessageDlg('There are no games available or a game has not been selected. The record ' +
                   'cannot be added to the playlist.', mtWarning, [mbOK], 0);
      end
      //Check to see if odds only selected and user is selecting a template that requires game data
      else if (CurrentTemplateDefs.EnableForOddsOnly = FALSE) AND (ShowOddsOnly.Checked) then
      begin
        MessageDlg('The selected template cannot be used when game data is not available. Please select ' +
          'an "Odds Only" template.', mtError, [mbOK], 0);
      end
      //OK to proceed
      else begin
        //Only proceed if fields defined, prompt for data enabled and NOT a parent template; if more than one
        //game selected, don't prompt operator
        if (NumTemplateFields > 0) AND (PromptForData.Checked) AND
           (CurrentTemplateDefs.Template_Has_Children = FALSE) AND
           (((NumGamesSelected = 1) AND (GetTemplateInformation(CurrentTemplateID).UsesGameData=TRUE) AND
             (dmMain.SportbaseQuery.RecordCount > 0)) OR (GetTemplateInformation(CurrentTemplateID).UsesGameData=FALSE)) then
        begin
          try
            Modal := TZipperEntryDlg.Create(Application);

            //Add style chips
            if (StyleChip_Collection.Count > 0) then
            begin
              Modal.StyleChipsGrid.StoreData := TRUE;
              Modal.StyleChipsGrid.Cols := 2;
              Modal.StyleChipsGrid.Rows := StyleChip_Collection.Count;
              for i := 0 to StyleChip_Collection.Count-1 do
              begin
                StyleChipRecPtr := StyleChip_Collection.At(i);
                Modal.StyleChipsGrid.Cell[1,i+1] := StyleChipRecPtr^.StyleChip_Code;
                Modal.StyleChipsGrid.Cell[2,i+1] := StyleChipRecPtr^.StyleChip_Description;
              end;
            end;

            //Set initial values for grid
            //Clear grid values
            if (Modal.RecordGrid.Rows > 0) then
              Modal.RecordGrid.DeleteRows (1, Modal.RecordGrid.Rows);
            Modal.RecordGrid.StoreData := TRUE;
            Modal.RecordGrid.Cols := 5;
            Modal.RecordGrid.Rows := NumTemplateFields;
            //Populate the grid
            i := 0;
            //Get the game data
            if (GetTemplateInformation(CurrentTemplateID).UsesGameData) then
            begin
              CurrentGameData := GameDataFunctions.GetGameData(CategoryRecPtr^.Category_Sport,
                Trim(dmMain.SportbaseQuery.FieldByname('GCode').AsString));
            end;
            //Iterate through the fields
            for j := 0 to Template_Fields_Collection.Count-1 do
            begin
              TemplateFieldsRecPtr := Template_Fields_Collection.At(j);
              if (TemplateFieldsRecPtr^.Template_ID = CurrentTemplateID) then
              begin
                Modal.RecordGrid.Cell[1,i+1] := TemplateFieldsRecPtr^.Field_ID; //Index
                Modal.RecordGrid.Cell[2,i+1] := TemplateFieldsRecPtr^.Field_Label; //Description
                //Set color & read only property
                Modal.RecordGrid.CellReadOnly[1,i+1] := roOn;
                Modal.RecordGrid.CellReadOnly[2,i+1] := roOn;
                Modal.RecordGrid.Cell[4,i+1] := TemplateFieldsRecPtr^.Field_Type;
                Modal.RecordGrid.CellReadOnly[4,i+1] := roOn;
                //Add max character count
                Modal.RecordGrid.Cell[5,i+1] := TemplateFieldsRecPtr^.Field_Max_Length;
                if (TemplateFieldsRecPtr^.Field_Is_Manual = TRUE) then
                begin
                  //First check to see if it's a manual game winner indicator or animation
                  if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_MANUAL_GAME_WINNER) OR (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_GAME_WINNER_ANIMATION) then
                  begin
                    Modal.RecordGrid.CellControlType[3,i+1] := ctCheck;
                    Modal.RecordGrid.CellCheckBoxState[3,i+1] := cbUnchecked;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                  end
                  //Next, check to see if it's a manual next page icon animation
                  else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_NEXT_PAGE_ANIMATION) then
                  begin
                    Modal.RecordGrid.CellControlType[3,i+1] := ctCheck;
                    Modal.RecordGrid.CellCheckBoxState[3,i+1] := cbUnchecked;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                  end
                  //Next, check to see if it's a weather icon; if so, display flipbooks in dropdown
                  else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_WEATHER_ICON) then
                  begin
                    Modal.RecordGrid.AssignCellCombo(3,i+1);
                    Modal.RecordGrid.CellButtonType[3,i+1] := btCombo;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                    //Init first combo box entry
                    WeatherIconRecPtr := Weather_Icon_Collection.At(0);
                    Modal.RecordGrid.Cell[3, i+1] := WeatherIconRecPtr^.Icon_Name;
                  end
                  //Next, check to see if it's the manual league entry; if so, display choices in dropdown
                  else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_LEAGUE_HEADER) then
                  begin
                    //Init combo box entry; fixed for breaking news
                    LeagueCodeRecPtr := LeagueCode_Collection.At(ManualLeagueSelect.ItemIndex);
                    Modal.RecordGrid.Cell[3,i+1] :=  LeagueCodeRecPtr^.Heading;
                  end
                  //Next, check for league logo
                  else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_LEAGUE_LOGO_1A) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_LEAGUE_LOGO_1B) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_LEAGUE_LOGO_2A) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_LEAGUE_LOGO_2B) then
                  begin
                    Modal.RecordGrid.CellButtonType[3,i+1] := btNormal;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                  end
                  //Next, check for team logo
                  else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_1A) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_1B) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_2A) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_2B) then
                  begin
                    Modal.RecordGrid.CellButtonType[3,i+1] := btNormal;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                  end
                  //Next, check for player headshot
                  else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_PLAYER_HEADSHOT_1A) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_PLAYER_HEADSHOT_1B) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_PLAYER_HEADSHOT_2A) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_PLAYER_HEADSHOT_2B) then
                  begin
                    Modal.RecordGrid.CellButtonType[3,i+1] := btNormal;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                  end
                  //Added for Version 2.0
                  //Next, check for full page promo graphic
                  else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_FULL_PROMO_GRAPHIC) then
                  begin
                    Modal.RecordGrid.CellButtonType[3,i+1] := btNormal;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                  end
                  else begin
                    Modal.RecordGrid.RowColor[i+1] := clWindow;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                  end;
                end
                //Not a manual field, so populate automatically
                else begin
                  //Set cell value
                  if (Pos('$', TemplateFieldsRecPtr^.Field_Contents) <> 0) then
                  begin
                    //Check if manual template; if so put league into first field
                    if (TemplateFieldsRecPtr^.Field_Contents = '$League') then
                    begin
                      LeagueCodeRecPtr := LeagueCode_Collection.At(ManualLeagueSelect.ItemIndex);
                      Modal.RecordGrid.Cell[3,i+1] := LeagueCodeRecPtr^.Heading;
                    end
                    else
                      Modal.RecordGrid.Cell[3,i+1] := GetValueOfSymbol(GridID, PlaylistSelectTabControl.TabIndex+1,
                        TemplateFieldsRecPtr^.Field_Contents, NOENTRYINDEX, CurrentGameData, 0,
                          dmMain.tblWeather_Cities.FieldByName('LocationCode').AsString).SymbolValue;
                  end
                  else
                    Modal.RecordGrid.Cell[3,i+1] := TemplateFieldsRecPtr^.Field_Contents;
                  Modal.RecordGrid.RowColor[i+1] := clAqua;
                  Modal.RecordGrid.CellReadOnly[3,i+1] := roOn;
                end;
                //Increment the template fields match counter
                Inc (i);
              end;
            end;
            Modal.TemplateName.Caption := CurrentTemplateDefs.Template_Description;

            //Show the dialog
            Control := Modal.ShowModal;
          finally
            //Set new values if user didn't cancel out
            if (Control = mrOK) then OKToGo := TRUE
            else OKToGo := FALSE;
            //Set values based on data in grid
            if (Modal.RecordGrid.Rows > 0) then
            begin
              i := 1;
              for j := 1 to Modal.RecordGrid.Rows do
              begin
                if (Modal.RecordGrid.CellReadOnly[3,j] = roOff) then
                begin
                  //Check for visitor manual winner indication record type
                  if (Modal.RecordGrid.Cell[4,j] = 5) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbChecked) then
                  begin
                    User_Data[24] := 'TRUE';
                    User_Data[49] := 'TRUE';
                    Dec(i);
                  end
                  else if (Modal.RecordGrid.Cell[4,j] = 5) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbUnChecked) then
                  begin
                    User_Data[24] := 'FALSE';
                    User_Data[49] := 'FALSE';
                    Dec(i);
                  end
                  else if (Modal.RecordGrid.Cell[4,j] = 6) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbChecked) then
                  begin
                    User_Data[25] := 'TRUE';
                    User_Data[50] := 'TRUE';
                    Dec(i);
                  end
                  else if (Modal.RecordGrid.Cell[4,j] = 6) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbUnChecked) then
                  begin
                    User_Data[25] := 'FALSE';
                    User_Data[50] := 'FALSE';
                    Dec(i);
                  end
                  //Check for next page animated icon
                  else if (Modal.RecordGrid.Cell[4,j] = 9) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbChecked) then
                  begin
                    User_Data[23] := 'TRUE';
                    User_Data[48] := 'TRUE';
                    Dec(i);
                  end
                  else if (Modal.RecordGrid.Cell[4,j] = 9) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbUnChecked) then
                  begin
                    User_Data[23] := 'FALSE';
                    User_Data[48] := 'FALSE';
                    Dec(i);
                  end
                  //Check for image file types
                  else if (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_LEAGUE_LOGO_1A) or
                          (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_LEAGUE_LOGO_1B) or
                          (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_TEAM_LOGO_1A) or
                          (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_TEAM_LOGO_1B) or
                          (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_PLAYER_HEADSHOT_1A) or
                          (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_PLAYER_HEADSHOT_1B) or
                          //Added for Version 2.0
                          //Next, check for full page promo graphic
                          (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_FULL_PROMO_GRAPHIC) then
                  begin
                    User_Data[21] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                    User_Data[46] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                    Dec(i);
                  end
                  else if (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_LEAGUE_LOGO_2A) or
                          (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_LEAGUE_LOGO_2B) or
                          (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_TEAM_LOGO_2A) or
                          (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_TEAM_LOGO_2B) or
                          (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_PLAYER_HEADSHOT_2A) or
                          (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_PLAYER_HEADSHOT_2B) then
                  begin
                    User_Data[22] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                    User_Data[47] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                    Dec(i);
                  end
                  //All else
                  else begin
                    //Set user value text and increment
                    User_Data[i] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                    //Set default live mode values - can be edited later
                    //User_Data[25+i] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                  end;
                  Inc(i);
                end;
                //For weather location code
                if (Modal.RecordGrid.Cell[4,j] = WEATHER_LOCATION_CODE) then
                begin
                  User_Data[21] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                  User_Data[46] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                  Dec(i);
                end;
              end;
            end;
            //Save current grid rows
            SaveRow := PlaylistGrid.CurrentDataRow;
            SaveRow2 := PlaylistGrid2.CurrentDataRow;
            Modal.Free;
          end;
        end
        else begin
          //Init fields
          for i := 1 to 50 do User_Data[i] := ' ';
        end;

        //Append record; falls through to here whether or not user dialog is displayed
        if (OKToGo) then
        begin
          ////////////////////////////////////////////////////////////////////////
          // THIS BLOCK EXECUTED FOR CATEGORIES THAT ARE AUTOMATED SPORTS AND FOR
          // TEMPLATES THAT REQUIRE GAME DATA
          ////////////////////////////////////////////////////////////////////////
          //Check to see if at least one game in sportbase database
          if (CategoryRecPtr^.Category_Is_Sport = TRUE) AND
             (dmMain.SportbaseQuery.RecordCount > 0) AND
             (CurrentTemplateDefs.UsesGameData = TRUE) then
          begin
            //Loop through for all selected games
            if (ShowOddsOnly.Checked) then
            begin
              //Go to first record in dataset
              dmMain.SportbaseQuery3.First;
              RecordCount := dmMain.SportbaseQuery3.RecordCount
            end
            else begin
              //Go to first record in dataset
              dmMain.SportbaseQuery.First;
              RecordCount := dmMain.SportbaseQuery.RecordCount;
            end;
            for m := 1 to RecordCount do
            begin
              //Check to see if game is selected
              if (ShowOddsOnly.Checked) then
                RowSelected := OddsDBGrid.RowSelected[OddsDBGrid.CurrentDataRow]
              else
                RowSelected := GamesDBGrid.RowSelected[GamesDBGrid.CurrentDataRow];
              if (RowSelected = TRUE) then
              begin
                ////////////////////////////////////////////////////////////////////////
                // THIS BLOCK EXECUTED FOR PARENT TEMPLATES - ADDS CHILDREN
                ////////////////////////////////////////////////////////////////////////
                //Check to see if template is a "parent" template; if so, add all child templates
                if (CurrentTemplateDefs.Template_Has_Children) then
                begin
                  if (Child_Template_ID_Collection.Count > 0) then
                  begin
                    for i := 0 to Child_Template_ID_Collection.Count-1 do
                    begin
                      ChildTemplateIDRecPtr := Child_Template_ID_Collection.At(i);
                      //Check for child, and append if match
                      if (ChildTemplateIDRecPtr^.Template_ID = CurrentTemplateID) then
                      begin
                        //Allocate memory for the object
                        Case PlaylistSelectTabControl.TabIndex of
                             //Ticker
                         0: begin
                              GetMem (TickerRecPtr, SizeOf(TickerRec));
                              With TickerRecPtr^ do
                              begin
                                //Set values for collection record
                                Event_Index := Ticker_Collection.Count+1;
                                Field_ID := GridID; //Indicates Field 1 or Field 2
                                CreateGUID(EventGUID);
                                Event_GUID := EventGUID;
                                Enabled := ChildTemplateIDRecPtr^.Child_Default_Enable_State;
                                Template_ID := ChildTemplateIDRecPtr^.Child_Template_ID;
                                Subleague_Mnemonic_Standard := '';
                                Mnemonic_LiveEvent := '';
                                Record_Type := GetRecordTypeFromTemplateID(Template_ID);
                                //Set league ID
                                if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                                begin
                                  League := CategoryRecPtr^.Category_Sport;
                                  //League := Trim(dmMain.SportbaseQuery.FieldByname('League').AsString);
                                  GameID := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                                  Description := GameDataFunctions.GetGameData(TRIM(League), TRIM(GameID)).Description;
                                end
                                else begin
                                  League := 'NONE';
                                  GameID := '-1';
                                  Description := '';
                                end;
                                Selected := FALSE;
                                //Check to see if sponsor logo
                                CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                                if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                                begin
                                  SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                                  SponsorLogo_Dwell := SponsorLogoDwell.Value;
                                end
                                else begin
                                  SponsorLogo_Name := ' ';
                                  SponsorLogo_Dwell := 0;
                                end;
                                //Set Is Child bit
                                Is_Child := TRUE;
                                //For stats
                                StatStoredProcedure := ' ';
                                StatType := 0;;
                                StatTeam := ' ';
                                StatLeague := ' ';
                                StatRecordNumber := 0;
                                for k := 1 to 50 do UserData[k] := User_Data[k];
                                //Set the start/end enable times to the template defaults
                                StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                                EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                                DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                                Comments := ' ';
                                //Insert object to collection
                                Case GridID of
                                  FIELD_1: begin
                                    Ticker_Collection.Insert(TickerRecPtr);
                                    Ticker_Collection.Pack;
                                  end;
                                  FIELD_2: begin
                                    Ticker_Collection2.Insert(TickerRecPtr);
                                    Ticker_Collection2.Pack;
                                  end;
                                end;
                              end;
                            end;
                             //Alerts
                         1: begin
                              GetMem (BreakingNewsRecPtr, SizeOf(BreakingNewsRec));
                              With BreakingNewsRecPtr^ do
                              begin
                                //Set values for collection record
                                Event_Index := BreakingNews_Collection.Count+1;
                                CreateGUID(EventGUID);
                                Event_GUID := EventGUID;
                                Enabled := ChildTemplateIDRecPtr^.Child_Default_Enable_State;
                                LeagueCodeRecPtr := LeagueCode_Collection.At(ManualLeagueSelect.ItemIndex);
                                League := LeagueCodeRecPtr^.Heading;
                                Template_ID := ChildTemplateIDRecPtr^.Child_Template_ID;
                                Record_Type := GetRecordTypeFromTemplateID(Template_ID);
                                Description := '';
                                Selected := FALSE;
                                //Set Is Child bit
                                for k := 1 to 2 do UserData[k] := User_Data[k];
                                //Set the start/end enable times to the template defaults
                                StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                                EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                                DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                                Comments := ' ';
                                //Insert object to collection
                                BreakingNews_Collection.Insert(BreakingNewsRecPtr);
                                BreakingNews_Collection.Pack;
                              end;
                            end;
                        end;
                      end;
                    end;
                  end;
                end
                ////////////////////////////////////////////////////////////////////////
                // THIS BLOCK EXECUTED FOR NON-PARENT TEMPLATES IN SPORTS CATEGORIES
                ////////////////////////////////////////////////////////////////////////
                //Not a parent template, so just append record for selected record type
                else begin
                  //Allocate memory for the object
                  Case PlaylistSelectTabControl.TabIndex of
                       //Ticker
                   0: begin
                        GetMem (TickerRecPtr, SizeOf(TickerRec));
                        With TickerRecPtr^ do
                        begin
                          //Set values for collection record
                          Event_Index := Ticker_Collection.Count+1;
                          Field_ID := GridID; //Indicates Field 1 or Field 2
                          CreateGUID(EventGUID);
                          Event_GUID := EventGUID;
                          Enabled := EntryEnable.Checked;
                          Template_ID := CurrentTemplateID;
                          Subleague_Mnemonic_Standard := '';
                          Mnemonic_LiveEvent := '';
                          Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                          //Set league ID
                          if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                          begin
                            League := CategoryRecPtr^.Category_Sport;
                            //League := Trim(dmMain.SportbaseQuery.FieldByname('League').AsString);
                            GameID := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                            Description := GameDataFunctions.GetGameData(TRIM(League), TRIM(GameID)).Description;
                          end
                          else begin
                            League := 'NONE';
                            GameID := '-1';
                            Description := '';
                          end;
                          //Check to see if sponsor logo
                          CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                          if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                          begin
                            SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                            SponsorLogo_Dwell := SponsorLogoDwell.Value;
                          end
                          else begin
                            SponsorLogo_Name := ' ';
                            SponsorLogo_Dwell := 0;
                          end;
                          //Clear Is Child bit
                          Is_Child := FALSE;
                          Selected := FALSE;
                          //For stats
                          StatStoredProcedure := '';
                          StatType := 0;;
                          StatTeam := ' ';
                          StatLeague := ' ';
                          StatRecordNumber := 0;
                          for k := 1 to 50 do UserData[k] := User_Data[k];
                          //Set the start/end enable times to the template defaults
                          StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                          EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                          DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                          Comments := ' ';
                          //Insert object to collection
                          Case GridID of
                            FIELD_1: begin
                              Ticker_Collection.Insert(TickerRecPtr);
                              Ticker_Collection.Pack;
                            end;
                            FIELD_2: begin
                              Ticker_Collection2.Insert(TickerRecPtr);
                              Ticker_Collection2.Pack;
                            end;
                          end;
                        end;
                      end;
                      //Alerts
                   1: begin
                        GetMem (BreakingNewsRecPtr, SizeOf(BreakingNewsRec));
                        With BreakingNewsRecPtr^ do
                        begin
                          //Set values for collection record
                          Event_Index := BreakingNews_Collection.Count+1;
                          CreateGUID(EventGUID);
                          Event_GUID := EventGUID;
                          Enabled := EntryEnable.Checked;
                          LeagueCodeRecPtr := LeagueCode_Collection.At(ManualLeagueSelect.ItemIndex);
                          League := LeagueCodeRecPtr^.Heading;
                          Template_ID := CurrentTemplateID;
                          Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                          Description := '';
                          for k := 1 to 2 do UserData[k] := User_Data[k];
                          //Set the start/end enable times to the template defaults
                          StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                          EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                          DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                          Selected := FALSE;
                          Comments := ' ';
                          //Insert object to collection
                          BreakingNews_Collection.Insert(BreakingNewsRecPtr);
                          BreakingNews_Collection.Pack;
                        end;
                      end;
                  end;
                end; //Single template type (NOT a parent record)
              end;
              //Go to next record
              if (ShowOddsOnly.Checked) then
                dmMain.SportbaseQuery3.Next
              else
                dmMain.SportbaseQuery.Next;
            end; //Loop for selected games
          end
          ////////////////////////////////////////////////////////////////////////
          // THIS BLOCK EXECUTED FOR TEMPLATES THAT DO NOT REQUIRE GAME DATA
          ////////////////////////////////////////////////////////////////////////
          //No games in Sportbase query - if not a game specific record type, just add one record
          else if (CurrentTemplateDefs.UsesGameData = FALSE) then
          begin
            //Allocate memory for the object
            Case PlaylistSelectTabControl.TabIndex of
                 //Ticker
             0: begin
                  GetMem (TickerRecPtr, SizeOf(TickerRec));
                  With TickerRecPtr^ do
                  begin
                    //Set values for collection record
                    Event_Index := Ticker_Collection.Count+1;
                    Field_ID := GridID; //Indicates Field 1 or Field 2
                    CreateGUID(EventGUID);
                    Event_GUID := EventGUID;
                    Enabled := EntryEnable.Checked;
                    Template_ID := CurrentTemplateID;
                    Subleague_Mnemonic_Standard := '';
                    Mnemonic_LiveEvent := '';
                    Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                    //Get category & league information
                    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                    if (Trim(CategoryRecPtr^.Category_Sport) <> '') then
                    begin
                      League := CategoryRecPtr^.Category_Sport;
                    end
                    else begin
                      //Check if manual league; if so, first user text field will be league
                      if (CurrentTemplateDefs.ManualLeague) then
                      begin
                        LeagueCodeRecPtr := LeagueCode_Collection.At(ManualLeagueSelect.ItemIndex);
                        League := LeagueCodeRecPtr^.Heading;
                      end
                      else begin
                        League := 'NONE';
                      end;
                    end;
                    GameID := '0';
                    Description := '';
                    //Check to see if sponsor logo
                    if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                    begin
                      SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                      SponsorLogo_Dwell := SponsorLogoDwell.Value;
                    end
                    else begin
                      SponsorLogo_Name := '';
                      SponsorLogo_Dwell := 0;
                    end;
                    //Clear Is Child bit
                    Is_Child := FALSE;
                    //For stats
                    StatStoredProcedure := '';
                    StatType := 0;;
                    StatTeam := '';
                    StatLeague := '';
                    StatRecordNumber := 0;
                    for k := 1 to 50 do UserData[k] := User_Data[k];
                    //Set the start/end enable times to the template defaults
                    StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                    EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                    DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                    Selected := FALSE;
                    Comments := ' ';
                    //Insert object to collection
                    Case GridID of
                      FIELD_1: begin
                        Ticker_Collection.Insert(TickerRecPtr);
                        Ticker_Collection.Pack;
                      end;
                      FIELD_2: begin
                        Ticker_Collection2.Insert(TickerRecPtr);
                        Ticker_Collection2.Pack;
                      end;
                    end;
                  end;
                end;
                 //Alerts
             1: begin
                  GetMem (BreakingNewsRecPtr, SizeOf(BreakingNewsRec));
                  With BreakingNewsRecPtr^ do
                  begin
                    //Set values for collection record
                    Event_Index := BreakingNews_Collection.Count+1;
                    CreateGUID(EventGUID);
                    Event_GUID := EventGUID;
                    Enabled := EntryEnable.Checked;
                    LeagueCodeRecPtr := LeagueCode_Collection.At(ManualLeagueSelect.ItemIndex);
                    League := LeagueCodeRecPtr^.Heading;
                    Template_ID := CurrentTemplateID;
                    Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                    //Get category & league information
                    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                    Description := '';
                    for k := 1 to 22 do UserData[k] := User_Data[k];
                    //Set the start/end enable times to the template defaults
                    StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                    EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                    DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                    Selected := FALSE;
                    Comments := ' ';
                    //Insert object to collection
                    BreakingNews_Collection.Insert(BreakingNewsRecPtr);
                    BreakingNews_Collection.Pack;
                  end;
                end;
            end; //Single template type (NOT a parent record)
          end;
        end;
        //Refresh grids
        RefreshPlaylistGrid(FIELD_1, PlaylistGrid);
        RefreshPlaylistGrid(FIELD_2, PlaylistGrid2);
        //Refresh the fields grids
        RefreshEntryFieldsGrid(TickerDisplayMode, FIELD_1, EntryFieldsGrid);
        RefreshEntryFieldsGrid(TickerDisplayMode, FIELD_2, EntryFieldsGrid2);
        //Restore current grid rows
        if (SaveRow < 1) then SaveRow := 1;
        PlaylistGrid.CurrentDataRow := SaveRow;
        if (SaveRow2 < 1) then SaveRow2 := 1;
        PlaylistGrid2.CurrentDataRow := SaveRow2;
      end;
    end;
  end
  //////////////////////////////////////////////////////////////////////////////
  //Execute this block of code if using Default Template list
  //////////////////////////////////////////////////////////////////////////////
  else begin
    //Save current grid row
    SaveRow := PlaylistGrid.CurrentDataRow;
    OKToGo := TRUE;
    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
    //First, check to see if sponsors. If so, show sponsor grid
    if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
    begin
      //Get number of games selected
      if (dmMain.SportbaseQuery.RecordCount > 0) then
      begin
        NumGamesSelected := GamesDBGrid.SelectedRows.Count;
        if (NumGamesSelected = 0) then
        begin
          GamesDBGrid.SelectRows(GamesDBGrid.CurrentDataRow, GamesDBGrid.CurrentDataRow, TRUE);
          NumGamesSelected := 1;
        end;
      end
      else begin
        MessageDlg('There are no games available or a game has not been selected. The records ' +
                   'cannot be added to the playlist.', mtWarning, [mbOK], 0);
        NumGamesSelected := 0;
        OKToGo := FALSE;
      end;
    end
    else begin
      NumGamesSelected := 0;
      OKToGo := FALSE;
    end;

    //Proceed if OK
    if (OKToGo) AND (CollectionCount < 1500) AND (Selected_Templates_Collection.Count > 0) then
    begin
      //Go to first record in dataset
      dmMain.SportbaseQuery.First;
      RecordCount := dmMain.SportbaseQuery.RecordCount;
      for m := 1 to RecordCount do
      begin
        RowSelected := GamesDBGrid.RowSelected[GamesDBGrid.CurrentDataRow];
        if (RowSelected = TRUE) then
        begin
          //Iterate through all default templates
          for n := 0 to Selected_Templates_Collection.Count-1 do
          begin
            SelectedTemplatesRecPtr := Selected_Templates_Collection.At(n);
            //Only proceed if template enabled
            if (SelectedTemplatesRecPtr^.Enabled) then
            begin
              CurrentTemplateID := SelectedTemplatesRecPtr^.Template_ID;
              CurrentTemplateDefs := GetTemplateInformation(CurrentTemplateID);
              ////////////////////////////////////////////////////////////////////////
              // THIS BLOCK EXECUTED FOR CATEGORIES THAT ARE AUTOMATED SPORTS AND FOR
              // TEMPLATES THAT REQUIRE GAME DATA
              ////////////////////////////////////////////////////////////////////////
              if (CurrentTemplateDefs.UsesGameData = TRUE) then
              begin
                //Init fields
                for i := 1 to 50 do User_Data[i] := ' ';
                //CurrentTemplateID := dmMain.Query2.FieldByName('Template_ID').AsInteger;
                NumTemplateFields := GetTemplateFieldsCount(CurrentTemplateID);

                ////////////////////////////////////////////////////////////////////////
                // THIS BLOCK EXECUTED FOR PARENT TEMPLATES - ADDS CHILDREN
                ////////////////////////////////////////////////////////////////////////
                //Check to see if template is a "parent" template; if so, add all child templates
                if (CurrentTemplateDefs.Template_Has_Children) then
                begin
                  if (Child_Template_ID_Collection.Count > 0) then
                  begin
                    for i := 0 to Child_Template_ID_Collection.Count-1 do
                    begin
                      ChildTemplateIDRecPtr := Child_Template_ID_Collection.At(i);
                      //Check for child, and append if match
                      if (ChildTemplateIDRecPtr^.Template_ID = CurrentTemplateID) then
                      begin
                        //Allocate memory for the object
                        GetMem (TickerRecPtr, SizeOf(TickerRec));
                        With TickerRecPtr^ do
                        begin
                          //Set values for collection record
                          Event_Index := Ticker_Collection.Count+1;
                          CreateGUID(EventGUID);
                          Event_GUID := EventGUID;
                          Enabled := ChildTemplateIDRecPtr^.Child_Default_Enable_State;
                          Template_ID := ChildTemplateIDRecPtr^.Child_Template_ID;
                          Subleague_Mnemonic_Standard := '';
                          Mnemonic_LiveEvent := '';
                          Record_Type := GetRecordTypeFromTemplateID(Template_ID);
                          //Set league ID
                          if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                          begin
                            League := CategoryRecPtr^.Category_Sport;
                            //League := Trim(dmMain.SportbaseQuery.FieldByname('League').AsString);
                            GameID := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                            Description := GameDataFunctions.GetGameData(TRIM(League), TRIM(GameID)).Description;
                          end
                          else begin
                            League := 'NONE';
                            GameID := '-1';
                            Description := '';
                          end;
                          Selected := FALSE;
                          //Check to see if sponsor logo
                          CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                          if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                          begin
                            SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                            SponsorLogo_Dwell := SponsorLogoDwell.Value;
                          end
                          else begin
                            SponsorLogo_Name := ' ';
                            SponsorLogo_Dwell := 0;
                          end;
                          //Set Is Child bit
                          Is_Child := TRUE;
                          //For stats
                          StatStoredProcedure := ' ';
                          StatType := 0;;
                          StatTeam := ' ';
                          StatLeague := ' ';
                          StatRecordNumber := 0;
                          for k := 1 to 50 do UserData[k] := User_Data[k];
                          //Set the start/end enable times to the template defaults
                          StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                          EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                          DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                          Comments := ' ';
                          //Insert object to collection
                          Ticker_Collection.Insert(TickerRecPtr);
                          Ticker_Collection.Pack;
                        end;
                      end;
                    end;
                  end;
                end
                ////////////////////////////////////////////////////////////////////////
                // THIS BLOCK EXECUTED FOR NON-PARENT TEMPLATES IN SPORTS CATEGORIES
                ////////////////////////////////////////////////////////////////////////
                //Not a parent template, so just append record for selected record type
                else begin
                  //Allocate memory for the object
                  GetMem (TickerRecPtr, SizeOf(TickerRec));
                  With TickerRecPtr^ do
                  begin
                    //Set values for collection record
                    Event_Index := Ticker_Collection.Count+1;
                    CreateGUID(EventGUID);
                    Event_GUID := EventGUID;
                    Enabled := EntryEnable.Checked;
                    Template_ID := CurrentTemplateID;
                    Subleague_Mnemonic_Standard := '';
                    Mnemonic_LiveEvent := '';
                    Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                    //Set league ID
                    if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                    begin
                      League := CategoryRecPtr^.Category_Sport;
                      //League := Trim(dmMain.SportbaseQuery.FieldByname('League').AsString);
                      GameID := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                      Description := GameDataFunctions.GetGameData(TRIM(League), TRIM(GameID)).Description;
                    end
                    else begin
                      League := 'NONE';
                      GameID := '-1';
                      Description := '';
                    end;
                    //Check to see if sponsor logo
                    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                    if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                    begin
                      SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                      SponsorLogo_Dwell := SponsorLogoDwell.Value;
                    end
                    else begin
                      SponsorLogo_Name := ' ';
                      SponsorLogo_Dwell := 0;
                    end;
                    //Clear Is Child bit
                    Is_Child := FALSE;
                    Selected := FALSE;
                    //For stats
                    StatStoredProcedure := '';
                    StatType := 0;;
                    StatTeam := ' ';
                    StatLeague := ' ';
                    StatRecordNumber := 0;
                    for k := 1 to 50 do UserData[k] := User_Data[k];
                    //Set the start/end enable times to the template defaults
                    StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                    EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                    DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                    Comments := ' ';
                    //Insert object to collection
                    Ticker_Collection.Insert(TickerRecPtr);
                    Ticker_Collection.Pack;
                  end;
                end; //Single template type (NOT a parent record)
              end //End of block for templates that use game data

              //////////////////////////////////////////////////////////////////
              //THIS BLOCK OF CODE FOR TEMPLATES THAT DO NOT REQUIRE GAME DATA
              //////////////////////////////////////////////////////////////////
              else begin
                //Allocate memory for the object
                GetMem (TickerRecPtr, SizeOf(TickerRec));
                With TickerRecPtr^ do
                begin
                  //Set values for collection record
                  Event_Index := Ticker_Collection.Count+1;
                  CreateGUID(EventGUID);
                  Event_GUID := EventGUID;
                  Enabled := EntryEnable.Checked;
                  Template_ID := CurrentTemplateID;
                  Subleague_Mnemonic_Standard := '';
                  Mnemonic_LiveEvent := '';
                  Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                  //Get category & league information
                  CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                  if (Trim(CategoryRecPtr^.Category_Sport) <> '') then
                  begin
                    League := CategoryRecPtr^.Category_Sport;
                  end
                  else begin
                    //Check if manual league; if so, first user text field will be league
                    if (CurrentTemplateDefs.ManualLeague) then
                    begin
                      LeagueCodeRecPtr := LeagueCode_Collection.At(ManualLeagueSelect.ItemIndex);
                      League := LeagueCodeRecPtr^.Heading;
                    end
                    else begin
                      League := 'NONE';
                    end;
                  end;
                  GameID := '0';
                  Description := '';
                  //Check to see if sponsor logo
                  if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                  begin
                    SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                    SponsorLogo_Dwell := SponsorLogoDwell.Value;
                  end
                  else begin
                    SponsorLogo_Name := '';
                    SponsorLogo_Dwell := 0;
                  end;
                  //Clear Is Child bit
                  Is_Child := FALSE;
                  //For stats
                  StatStoredProcedure := '';
                  StatType := 0;;
                  StatTeam := '';
                  StatLeague := '';
                  StatRecordNumber := 0;
                  for k := 1 to 50 do UserData[k] := User_Data[k];
                  //Set the start/end enable times to the template defaults
                  StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                  EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                  DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                  Selected := FALSE;
                  Comments := ' ';
                  //Insert object to collection
                  Ticker_Collection.Insert(TickerRecPtr);
                  Ticker_Collection.Pack;
                end; //Single template type (NOT a parent record)
              end;
            end;
          end;
        end;
        //Next game
        dmMain.SportbaseQuery.Next;
      end;
    end;
  end;

  Case GridID of
    FIELD_1: begin
      //Refresh grids
      RefreshPlaylistGrid(FIELD_1, PlaylistGrid);
      //Refresh the fields grid
      RefreshEntryFieldsGrid(TickerDisplayMode, FIELD_1, EntryFieldsGrid);
      //Restore current grid row
      if (SaveRow < 1) then SaveRow := 1;
      PlaylistGrid.CurrentDataRow := SaveRow;
    end;
    FIELD_2: begin
      //Refresh grids
      RefreshPlaylistGrid(FIELD_2, PlaylistGrid2);
      //Refresh the fields grid
      RefreshEntryFieldsGrid(TickerDisplayMode, FIELD_2, EntryFieldsGrid2);
      //Restore current grid row
      if (SaveRow2 < 1) then SaveRow2 := 1;
      PlaylistGrid2.CurrentDataRow := SaveRow2;
    end;
  end;

  //Save the playlist out and signal the Playout Controller to re-load if in automated playout mode
  if (InRemotePlayoutMode) then
  begin
    //Save the playlist
    SavePlaylistWithoutClear;
    //Signal playout controller to refresh the playlist
    PlayoutInterface.RefreshCurrentPlaylist(PlaylistInfo[TICKER].PlaylistID);
  end;
end;

//Handler for insert entry into playlist #1 button
procedure TMainForm.InsertIntoPlaylistBtnClick(Sender: TObject);
begin
  Case PlaylistSelectTabControl.TabIndex of
    0..1: InsertPlaylistEntry(dmMain.Query2.FieldByName('Template_ID').AsInteger, SINGLETEMPLATEONLY, FIELD_1, PlaylistGrid);
    2: InsertDefaultTemplateEntry(dmMain.Query2.FieldByName('Template_ID').AsInteger);
  end;
end;
//Handler for insert entry into playlist #2 button
procedure TMainForm.InsertIntoPlaylist2BtnClick(Sender: TObject);
begin
  Case PlaylistSelectTabControl.TabIndex of
    0..1: begin
      if (dmMain.Query2.FieldByName('Template_Type').AsInteger = FIELD_2) then
        MessageDlg('Field 3 templates can only be added to Playlist 1.', mtInformation, [mbOK], 0)
      else if (dmMain.Query2.FieldByName('Category_ID').AsInteger = SPONSOR_CATEGORY) then
        MessageDlg('Sponsors can only be added to Playlist 1.', mtInformation, [mbOK], 0)
      else
        InsertPlaylistEntry(dmMain.Query2.FieldByName('Template_ID').AsInteger, SINGLETEMPLATEONLY, FIELD_2, PlaylistGrid2);
    end;
    2: InsertDefaultTemplateEntry(dmMain.Query2.FieldByName('Template_ID').AsInteger);
  end;
end;

//General procedure to insert an entry into the main ticker playlist
procedure TMainForm.InsertPlaylistEntry(CurrentTemplateID: SmallInt; AddAllDefaultTemplates: Boolean; GridID: SmallInt; var DataGrid: TtsGrid);
var
  OKToGo: Boolean;
  SaveRow, SaveRow2, SaveTopRow, SaveTopRow2: SmallInt;
  SaveGamesRow, SaveGamesTopRow: Variant;
  i,j,k,m,n: SmallInt;
  TickerRecPtr: ^TickerRec;
  BreakingNewsRecPtr: ^BreakingNewsRec;
  Control: Word;
  Modal: TZipperEntryDlg;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  ChildTemplateIDRecPtr: ^ChildTemplateIDRec;
  NumTemplateFields: SmallInt;
  //CurrentTemplateID: SmallInt;
  CurrentTemplateDefs: TemplateDefsRec;
  User_Data: Array[1..50] of String[255];
  CollectionCount: SmallInt;
  CategoryRecPtr: ^CategoryRec;
  CurrentGameData: GameRec;
  EventGUID: TGUID;
  NumGamesSelected: SmallInt;
  StyleChipRecPtr: ^StyleChipRec;
  LeagueCodeRecPtr: ^LeagueCodeRec;
  WeatherIconRecPtr: ^WeatherIconRec;
  RowSelected: Boolean;
  RecordCount: SmallInt;
  SelectedTemplatesRecPtr: ^DefaultTemplatesRec;
begin
  //Init
  OKToGo := TRUE;
  for i := 1 to 50 do User_Data[i] := '';

  //////////////////////////////////////////////////////////////////////////////
  //Execute this block of code if selecting individual templates; code for
  //Default Template list below
  //////////////////////////////////////////////////////////////////////////////
  if (AddAllDefaultTemplates = SINGLETEMPLATEONLY) then
  begin
    //Check to see if category tab is a sport; if so, show and populate games grid
    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
    //First, check to see if sponsors. If so, show sponsor grid
    if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
    begin
      //Get number of games selected
      if (dmMain.SportbaseQuery.RecordCount > 0) then
      begin
        NumGamesSelected := GamesDBGrid.SelectedRows.Count;
        if (NumGamesSelected = 0) then
        begin
          GamesDBGrid.SelectRows(GamesDBGrid.CurrentDataRow, GamesDBGrid.CurrentDataRow, TRUE);
          NumGamesSelected := 1;
        end;
      end
      else NumGamesSelected := 0;
    end
    else NumGamesSelected := 0;

    //Init fields
    for i := 1 to 50 do User_Data[i] := ' ';

    //Get collection count
    Case PlaylistSelectTabControl.TabIndex of
         //Ticker
      0: Case GridID of
           FIELD_1: CollectionCount := Ticker_Collection.Count;
           FIELD_2: CollectionCount := Ticker_Collection2.Count;
         end;
        //Alerts
      1: CollectionCount := BreakingNews_Collection.Count;
    end;
    if (CollectionCount < 1500) then
    begin
      CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
      //Save current grid rows
      SaveRow := PlaylistGrid.CurrentDataRow;
      SaveTopRow := PlaylistGrid.TopRow;
      SaveRow2 := PlaylistGrid2.CurrentDataRow;
      SaveTopRow2 := PlaylistGrid2.TopRow;
      SaveGamesRow := GamesDBGrid.CurrentDataRow;
      SaveGamesTopRow := GamesDBGrid.TopRow;

      //Get template information
      //CurrentTemplateID := dmMain.Query2.FieldByName('Template_ID').AsInteger;
      NumTemplateFields := GetTemplateFieldsCount(CurrentTemplateID);
      CurrentTemplateDefs := GetTemplateInformation(CurrentTemplateID);
      //First, check to make sure that there are games listed if the template requires one
      if (CurrentTemplateDefs.Record_Type = 1) AND
        (CategoryRecPtr^.Category_Is_Sport = TRUE) AND (dmMain.SportbaseQuery.RecordCount = 0) then
      begin
        MessageDlg('There are no games available or a game has not been selected. The record ' +
                   'cannot be added to the playlist.', mtWarning, [mbOK], 0);
      end
      //Check to see if odds only selected and user is selecting a template that requires game data
      else if (CurrentTemplateDefs.EnableForOddsOnly = FALSE) AND (ShowOddsOnly.Checked) then
      begin
        MessageDlg('The selected template cannot be used when game data is not available (odds only).', mtError, [mbOK], 0);
      end
      //OK to proceed
      else begin
        //Only proceed if fields defined, prompt for data enabled and NOT a parent template
        if (NumTemplateFields > 0) AND (PromptForData.Checked) AND
           (CurrentTemplateDefs.Template_Has_Children = FALSE) AND
           (((NumGamesSelected = 1) AND (GetTemplateInformation(CurrentTemplateID).UsesGameData=TRUE) AND
             (dmMain.SportbaseQuery.RecordCount > 0)) OR (GetTemplateInformation(CurrentTemplateID).UsesGameData=FALSE)) then
        begin
          try
            Modal := TZipperEntryDlg.Create(Application);
            //Set initial values for grid
            //Add style chips
            if (StyleChip_Collection.Count > 0) then
            begin
              Modal.StyleChipsGrid.StoreData := TRUE;
              Modal.StyleChipsGrid.Cols := 2;
              Modal.StyleChipsGrid.Rows := StyleChip_Collection.Count;
              for i := 0 to StyleChip_Collection.Count-1 do
              begin
                StyleChipRecPtr := StyleChip_Collection.At(i);
                Modal.StyleChipsGrid.Cell[1,i+1] := StyleChipRecPtr^.StyleChip_Code;
                Modal.StyleChipsGrid.Cell[2,i+1] := StyleChipRecPtr^.StyleChip_Description;
              end;
            end;
            //Clear grid values
            if (Modal.RecordGrid.Rows > 0) then
              Modal.RecordGrid.DeleteRows (1, Modal.RecordGrid.Rows);
            Modal.RecordGrid.StoreData := TRUE;
            Modal.RecordGrid.Cols := 5;
            Modal.RecordGrid.Rows := NumTemplateFields;
            //Populate the grid
            i := 0;
            //Get the game data
            if (GetTemplateInformation(CurrentTemplateID).UsesGameData) then
            begin
              //Query for Game data
              CurrentGameData := GameDataFunctions.GetGameData(CategoryRecPtr^.Category_Sport,
                TRIM(dmMain.SportbaseQuery.FieldByname('GCode').AsString));
            end;
            //Iterate through the fields
            for j := 0 to Template_Fields_Collection.Count-1 do
            begin
              TemplateFieldsRecPtr := Template_Fields_Collection.At(j);
              if (TemplateFieldsRecPtr^.Template_ID = CurrentTemplateID) then
              begin
                Modal.RecordGrid.Cell[1,i+1] := TemplateFieldsRecPtr^.Field_ID; //Index
                Modal.RecordGrid.Cell[2,i+1] := TemplateFieldsRecPtr^.Field_Label; //Description
                Modal.RecordGrid.Cell[4,i+1] := TemplateFieldsRecPtr^.Field_Type;
                //Set color & read only property
                Modal.RecordGrid.CellReadOnly[1,i+1] := roOn;
                Modal.RecordGrid.CellReadOnly[2,i+1] := roOn;
                Modal.RecordGrid.CellReadOnly[4,i+1] := roOn;
                //Add max character count
                Modal.RecordGrid.Cell[5,i+1] := TemplateFieldsRecPtr^.Field_Max_Length;
                if (TemplateFieldsRecPtr^.Field_Is_Manual = TRUE) then
                begin
                  //Firt check to see if it's a manual game winner indicator or animation
                  if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_MANUAL_GAME_WINNER) OR (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_GAME_WINNER_ANIMATION) then
                  begin
                    Modal.RecordGrid.CellControlType[3,i+1] := ctCheck;
                    Modal.RecordGrid.CellCheckBoxState[3,i+1] := cbUnchecked;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                  end
                  //Next, check to see if it's a manual next page icon animation
                  else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_NEXT_PAGE_ANIMATION) then
                  begin
                    Modal.RecordGrid.CellControlType[3,i+1] := ctCheck;
                    Modal.RecordGrid.CellCheckBoxState[3,i+1] := cbUnchecked;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                  end
                  //Next, check to see if it's a weather icon; if so, display flipbooks in dropdown
                  else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_WEATHER_ICON) then
                  begin
                    Modal.RecordGrid.AssignCellCombo(3,i+1);
                    Modal.RecordGrid.CellButtonType[3,i+1] := btCombo;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                    //Init first combo box entry
                    WeatherIconRecPtr := Weather_Icon_Collection.At(0);
                    Modal.RecordGrid.Cell[3, i+1] := WeatherIconRecPtr^.Icon_Name;
                  end
                  //Next, check to see if it's the manual league entry; if so, display choices in dropdown
                  else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_LEAGUE_HEADER) then
                  begin
                    //Init combo box entry; fixed for breaking news
                    LeagueCodeRecPtr := LeagueCode_Collection.At(ManualLeagueSelect.ItemIndex);
                    Modal.RecordGrid.Cell[3,i+1] :=  LeagueCodeRecPtr^.Heading;
                  end
                  //Next, check for league logo
                  else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_LEAGUE_LOGO_1A) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_LEAGUE_LOGO_1B) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_LEAGUE_LOGO_2A) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_LEAGUE_LOGO_2B) then
                  begin
                    Modal.RecordGrid.CellButtonType[3,i+1] := btNormal;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                  end
                  //Next, check for team logo
                  else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_1A) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_1B) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_2A) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_2B) then
                  begin
                    Modal.RecordGrid.CellButtonType[3,i+1] := btNormal;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                  end
                  //Next, check for player headshot
                  else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_PLAYER_HEADSHOT_1A) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_PLAYER_HEADSHOT_1B) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_PLAYER_HEADSHOT_2A) or
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_PLAYER_HEADSHOT_2B) or
                    //Added for Version 2.0
                    //Next, check for full page promo graphic
                    (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_FULL_PROMO_GRAPHIC) then
                   begin
                    Modal.RecordGrid.CellButtonType[3,i+1] := btNormal;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                  end
                  else begin
                    Modal.RecordGrid.RowColor[i+1] := clWindow;
                    Modal.RecordGrid.CellReadOnly[3,i+1] := roOff;
                  end;
                end
                else begin
                  //Set cell value
                  Modal.RecordGrid.Cell[3,i+1] := GetValueOfSymbol(GridID, PlaylistSelectTabControl.TabIndex+1,
                    TemplateFieldsRecPtr^.Field_Contents, NOENTRYINDEX, CurrentGameData, 0,
                      dmMain.tblWeather_Cities.FieldByName('LocationCode').AsString).SymbolValue;
                  Modal.RecordGrid.RowColor[i+1] := clAqua;
                  Modal.RecordGrid.CellReadOnly[3,i+1] := roOn;
                end;
                //Increment the template fields match counter
                Inc (i);
              end;
            end;
            Modal.TemplateName.Caption := CurrentTemplateDefs.Template_Description;
            //Show the dialog
            Control := Modal.ShowModal;
          finally
            //Set new values if user didn't cancel out
            if (Control = mrOK) then OKToGo := TRUE
            else OKToGo := FALSE;
            //Set values based on data in grid
            if (Modal.RecordGrid.Rows > 0) then
            begin
              i := 1;
              for j := 1 to Modal.RecordGrid.Rows do
              begin
                if (Modal.RecordGrid.CellReadOnly[3,j] = roOff) then
                begin
                  //Check for visitor manual winner indication record type
                  if (Modal.RecordGrid.Cell[4,j] = 5) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbChecked) then
                  begin
                    User_Data[24] := 'TRUE';
                    User_Data[49] := 'TRUE';
                    Dec(i);
                  end
                  else if (Modal.RecordGrid.Cell[4,j] = 5) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbUnChecked) then
                  begin
                    User_Data[24] := 'FALSE';
                    User_Data[49] := 'FALSE';
                    Dec(i);
                  end
                  else if (Modal.RecordGrid.Cell[4,j] = 6) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbChecked) then
                  begin
                    User_Data[25] := 'TRUE';
                    User_Data[50] := 'TRUE';
                    Dec(i);
                  end
                  else if (Modal.RecordGrid.Cell[4,j] = 6) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbUnChecked) then
                  begin
                    User_Data[25] := 'FALSE';
                    User_Data[50] := 'FALSE';
                    Dec(i);
                  end
                  //Check for next page animated icon
                  else if (Modal.RecordGrid.Cell[4,j] = 9) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbChecked) then
                  begin
                    User_Data[23] := 'TRUE';
                    User_Data[48] := 'TRUE';
                    Dec(i);
                  end
                  else if (Modal.RecordGrid.Cell[4,j] = 9) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbUnChecked) then
                  begin
                    User_Data[23] := 'FALSE';
                    User_Data[48] := 'FALSE';
                    Dec(i);
                  end
                  //Check for image file types
                  else if (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_LEAGUE_LOGO_1A) or
                          (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_LEAGUE_LOGO_1B) or
                          (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_TEAM_LOGO_1A) or
                          (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_TEAM_LOGO_1B) or
                          (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_PLAYER_HEADSHOT_1A) or
                          (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_PLAYER_HEADSHOT_1B) or
                          //Added for Version 2.0
                          //Next, check for full page promo graphic
                          (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_FULL_PROMO_GRAPHIC) then
                  begin
                    User_Data[21] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                    User_Data[46] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                    Dec(i);
                  end
                  else if (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_LEAGUE_LOGO_2A) or
                          (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_LEAGUE_LOGO_2B) or
                          (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_TEAM_LOGO_2A) or
                          (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_TEAM_LOGO_2B) or
                          (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_PLAYER_HEADSHOT_2A) or
                          (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_PLAYER_HEADSHOT_2B) then
                  begin
                    User_Data[22] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                    User_Data[47] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                    Dec(i);
                  end
                  //All else
                  else begin
                    //Set user value text and increment
                    User_Data[i] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                    //Set default live mode values - can be edited later
                    //User_Data[25+i] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                  end;
                  Inc(i);
                end;
                //For weather location code
                if (Modal.RecordGrid.Cell[4,j] = WEATHER_LOCATION_CODE) then
                begin
                  User_Data[21] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                  User_Data[46] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                  Dec(i);
                end;
              end;
            end;
            //Save current grid rows
            SaveRow := PlaylistGrid.CurrentDataRow;
            SaveRow2 := PlaylistGrid2.CurrentDataRow;
            Modal.Free;
          end;
        end
        else begin
          //Init fields
          for i := 1 to 50 do User_Data[i] := ' ';
        end;
        if (OKToGo) then
        begin
          ////////////////////////////////////////////////////////////////////////
          // THIS BLOCK EXECUTED FOR CATEGORIES THAT ARE AUTOMATED SPORTS AND FOR
          // TEMPLATES THAT REQUIRE GAME DATA
          ////////////////////////////////////////////////////////////////////////
          //Check to see if at least one game in sportbase database
          if (CategoryRecPtr^.Category_Is_Sport = TRUE) AND (dmMain.SportbaseQuery.RecordCount > 0) AND
             (CurrentTemplateDefs.UsesGameData = TRUE) then
          begin
            //Loop through for all selected games
            if (ShowOddsOnly.Checked) then
            begin
              //Go to first record in dataset
              //dmMain.SportbaseQuery3.First;
              dmMain.SportbaseQuery3.Last;
              RecordCount := dmMain.SportbaseQuery3.RecordCount
            end
            else begin
              //Go to first record in dataset
              //dmMain.SportbaseQuery.First;
              dmMain.SportbaseQuery.Last;
              RecordCount := dmMain.SportbaseQuery.RecordCount;
            end;
            for m := 1 to RecordCount do
            begin
              //Check to see if game is selected
              if (ShowOddsOnly.Checked) then
                RowSelected := OddsDBGrid.RowSelected[OddsDBGrid.CurrentDataRow]
              else
                RowSelected := GamesDBGrid.RowSelected[GamesDBGrid.CurrentDataRow];
              if (RowSelected = TRUE) then
              begin
                ////////////////////////////////////////////////////////////////////////
                // THIS BLOCK EXECUTED FOR PARENT TEMPLATES - ADDS CHILDREN
                ////////////////////////////////////////////////////////////////////////
                //Check to see if template is a "parent" template; if so, add all child templates
                if (CurrentTemplateDefs.Template_Has_Children) then
                begin
                  if (Child_Template_ID_Collection.Count > 0) then
                  begin
                    for i := Child_Template_ID_Collection.Count-1 downto 0 do
                    begin
                      //Need to go backwards for insert to make it work for record ordering
                      ChildTemplateIDRecPtr := Child_Template_ID_Collection.At(i);
                      //Check for child, and append if match
                      if (ChildTemplateIDRecPtr^.Template_ID = CurrentTemplateID) then
                      begin
                        //Allocate memory for the object
                        Case PlaylistSelectTabControl.TabIndex of
                            //Ticker
                         0: begin
                              //Allocate memory for the object
                              GetMem (TickerRecPtr, SizeOf(TickerRec));
                              With TickerRecPtr^ do
                              begin
                                //Set values for collection record
                                Event_Index := Ticker_Collection.Count+1;
                                Field_ID := GridID; //Indicates Field 1 or Field 2
                                CreateGUID(EventGUID);
                                Event_GUID := EventGUID;
                                Enabled := ChildTemplateIDRecPtr^.Child_Default_Enable_State;
                                Subleague_Mnemonic_Standard := '';
                                Mnemonic_LiveEvent := '';
                                Template_ID := ChildTemplateIDRecPtr^.Child_Template_ID;
                                Record_Type := GetRecordTypeFromTemplateID(Template_ID);
                                //Set league ID
                                if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                                begin
                                  League := CategoryRecPtr^.Category_Sport;
                                  //League := Trim(dmMain.SportbaseQuery.FieldByname('League').AsString);
                                  GameID := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                                  Description := GameDataFunctions.GetGameData(TRIM(League), TRIM(GameID)).Description;
                                end
                                else begin
                                  League := 'NONE';
                                  GameID := '-1';
                                  Description := '';
                                end;
                                if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                                begin
                                  SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                                  SponsorLogo_Dwell := SponsorLogoDwell.Value;
                                end
                                else begin
                                  SponsorLogo_Name := '';
                                  SponsorLogo_Dwell := 0;
                                end;
                                //Set Is Child bit
                                Is_Child := TRUE;
                                //For stats
                                StatStoredProcedure := '';
                                StatType := 0;;
                                StatTeam := '';
                                StatLeague := '';
                                StatRecordNumber := 0;
                                for k := 1 to 50 do UserData[k] := User_Data[k];
                                //Set the start/end enable times to the template defaults
                                StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                                EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                                //DwellTime := EntryDwellTime.Value*1000;
                                DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                                Selected := FALSE;
                                Comments := ' ';
                                //Insert object to collection
                                Case GridID of
                                  FIELD_1: begin
                                    if (PlaylistGrid.CurrentDataRow > 0) then
                                    begin
                                      Ticker_Collection.AtInsert(PlaylistGrid.CurrentDataRow-1, TickerRecPtr);
                                      Ticker_Collection.Pack;
                                    end
                                    else begin
                                      Ticker_Collection.Insert(TickerRecPtr);
                                      Ticker_Collection.Pack;
                                    end;
                                  end;
                                  FIELD_2: begin
                                    if (PlaylistGrid2.CurrentDataRow > 0) then
                                    begin
                                      Ticker_Collection2.AtInsert(PlaylistGrid2.CurrentDataRow-1, TickerRecPtr);
                                      Ticker_Collection2.Pack;
                                    end
                                    else begin
                                      Ticker_Collection2.Insert(TickerRecPtr);
                                      Ticker_Collection2.Pack;
                                    end;
                                  end;
                                end;
                              end;
                            end;
                            //Alerts
                         1: begin
                              //Allocate memory for the object
                              GetMem (BreakingNewsRecPtr, SizeOf(BreakingNewsRec));
                              With BreakingNewsRecPtr^ do
                              begin
                                //Set values for collection record
                                Event_Index := BreakingNews_Collection.Count+1;
                                CreateGUID(EventGUID);
                                Event_GUID := EventGUID;
                                Enabled := ChildTemplateIDRecPtr^.Child_Default_Enable_State;
                                LeagueCodeRecPtr := LeagueCode_Collection.At(ManualLeagueSelect.ItemIndex);
                                League := LeagueCodeRecPtr^.Heading;
                                Template_ID := ChildTemplateIDRecPtr^.Child_Template_ID;
                                Record_Type := GetRecordTypeFromTemplateID(Template_ID);
                                Description := 'BreakingNews';
                                for k := 1 to 2 do UserData[k] := User_Data[k];
                                //Set the start/end enable times to the template defaults
                                StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                                EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                                //DwellTime := EntryDwellTime.Value*1000;
                                DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                                Selected := FALSE;
                                Comments := ' ';
                                //Insert object to collection
                                if (PlaylistGrid.CurrentDataRow > 0) then
                                  BreakingNews_Collection.AtInsert(PlaylistGrid.CurrentDataRow-1, BreakingNewsRecPtr)
                                else
                                  BreakingNews_Collection.Insert(BreakingNewsRecPtr);
                                BreakingNews_Collection.Pack;
                              end;
                            end;
                        end;
                      end;
                    end;
                  end;
                end
                ////////////////////////////////////////////////////////////////////////
                // THIS BLOCK EXECUTED FOR NON-PARENT TEMPLATES IN SPORTS CATEGORIES
                ////////////////////////////////////////////////////////////////////////
                //Not a parent template, so just append record for selected record type
                else begin
                  Case PlaylistSelectTabControl.TabIndex of
                      //Ticker
                   0: begin
                        //Allocate memory for the object
                        GetMem (TickerRecPtr, SizeOf(TickerRec));
                        With TickerRecPtr^ do
                        begin
                          //Set values for collection record
                          Event_Index := Ticker_Collection.Count+1;
                          Field_ID := GridID; //Indicates Field 1 or Field 2
                          CreateGUID(EventGUID);
                          Event_GUID := EventGUID;
                          Enabled := EntryEnable.Checked;
                          Template_ID := CurrentTemplateID;
                          Subleague_Mnemonic_Standard := '';
                          Mnemonic_LiveEvent := '';
                          Record_Type := GetRecordTypeFromTemplateID(dmMain.Query2.FieldByName('Template_ID').AsInteger);
                          //Set league ID
                          if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                          begin
                            League := CategoryRecPtr^.Category_Sport;
                            //League := Trim(dmMain.SportbaseQuery.FieldByname('League').AsString);
                            GameID := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                            Description := GameDataFunctions.GetGameData(TRIM(League), TRIM(GameID)).Description;
                          end
                          else begin
                            League := 'NONE';
                            GameID := '-1';
                            Description := '';
                          end;
                          if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                          begin
                            SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                            SponsorLogo_Dwell := SponsorLogoDwell.Value;
                          end
                          else begin
                            SponsorLogo_Name := '';
                            SponsorLogo_Dwell := 0;
                          end;
                          //Clear Is Child bit
                          Is_Child := FALSE;
                          //For stats
                          StatStoredProcedure := '';
                          StatType := 0;;
                          StatTeam := '';
                          StatLeague := '';
                          StatRecordNumber := 0;
                          for k := 1 to 50 do UserData[k] := User_Data[k];
                          //Set the start/end enable times to the template defaults
                          StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                          EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                          //DwellTime := EntryDwellTime.Value*1000;
                          DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                          Selected := FALSE;
                          Comments := ' ';
                          //Insert object to collection
                          Case GridID of
                            FIELD_1: begin
                              if (PlaylistGrid.CurrentDataRow > 0) then
                              begin
                                Ticker_Collection.AtInsert(PlaylistGrid.CurrentDataRow-1, TickerRecPtr);
                                Ticker_Collection.Pack;
                              end
                              else begin
                                Ticker_Collection.Insert(TickerRecPtr);
                                Ticker_Collection.Pack;
                              end;
                            end;
                            FIELD_2: begin
                              if (PlaylistGrid2.CurrentDataRow > 0) then
                              begin
                                Ticker_Collection2.AtInsert(PlaylistGrid2.CurrentDataRow-1, TickerRecPtr);
                                Ticker_Collection2.Pack;
                              end
                              else begin
                                Ticker_Collection2.Insert(TickerRecPtr);
                                Ticker_Collection2.Pack;
                              end;
                            end;
                          end;
                        end;
                      end;
                      //Alerts
                   1: begin
                        //Allocate memory for the object
                        GetMem (BreakingNewsRecPtr, SizeOf(BreakingNewsRec));
                        With BreakingNewsRecPtr^ do
                        begin
                          //Set values for collection record
                          Event_Index := BreakingNews_Collection.Count+1;
                          CreateGUID(EventGUID);
                          Event_GUID := EventGUID;
                          Enabled := EntryEnable.Checked;
                          LeagueCodeRecPtr := LeagueCode_Collection.At(ManualLeagueSelect.ItemIndex);
                          League := LeagueCodeRecPtr^.Heading;
                          Template_ID := CurrentTemplateID;
                          Record_Type := GetRecordTypeFromTemplateID(dmMain.Query2.FieldByName('Template_ID').AsInteger);
                          Description := 'BreakingNews';
                          for k := 1 to 2 do UserData[k] := User_Data[k];
                          //Set the start/end enable times to the template defaults
                          StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                          EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                          //DwellTime := EntryDwellTime.Value*1000;
                          DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                          Selected := FALSE;
                          Comments := ' ';
                          //Insert object to collection
                          if (PlaylistGrid.CurrentDataRow > 0) then
                            BreakingNews_Collection.AtInsert(PlaylistGrid.CurrentDataRow-1, BreakingNewsRecPtr)
                          else
                            BreakingNews_Collection.Insert(BreakingNewsRecPtr);
                          BreakingNews_Collection.Pack;
                        end;
                      end;
                  end;
                end;
              end;
              //Go to next record
             if (ShowOddsOnly.Checked) then
               //dmMain.SportbaseQuery3.Next
               dmMain.SportbaseQuery3.Prior
             else
               //dmMain.SportbaseQuery.Next;
               dmMain.SportbaseQuery.Prior;
            end;
          end
          ////////////////////////////////////////////////////////////////////////
          // THIS BLOCK EXECUTED FOR TRMPLATES THAT DO NOT REQUIRE GAME DATA
          ////////////////////////////////////////////////////////////////////////
          //No games in Sportbase query - if not a game specific record type, just add one record
          else if (CurrentTemplateDefs.UsesGameData = FALSE) then
          begin
            //Allocate memory for the object
            Case PlaylistSelectTabControl.TabIndex of
                 //Ticker
             0: begin
                  GetMem (TickerRecPtr, SizeOf(TickerRec));
                  With TickerRecPtr^ do
                  begin
                    //Set values for collection record
                    Event_Index := Ticker_Collection.Count+1;
                    Field_ID := GridID; //Indicates Field 1 or Field 2
                    CreateGUID(EventGUID);
                    Event_GUID := EventGUID;
                    Enabled := EntryEnable.Checked;
                    Subleague_Mnemonic_Standard := '';
                    Mnemonic_LiveEvent := '';
                    Template_ID := CurrentTemplateID;
                    Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                    //Get category & league information
                    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                    if (Trim(CategoryRecPtr^.Category_Sport) <> '') then
                    begin
                      League := CategoryRecPtr^.Category_Sport;
                    end
                    else begin
                      //Check if manual league; if so, first user text field will be league
                      if (CurrentTemplateDefs.ManualLeague) then
                      begin
                        LeagueCodeRecPtr := LeagueCode_Collection.At(ManualLeagueSelect.ItemIndex);
                        League := LeagueCodeRecPtr^.Heading;
                      end
                      else begin
                        League := 'NONE';
                      end;
                    end;
                    GameID := '0';
                    Description := '';
                    //Check to see if sponsor logo
                    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                    if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                    begin
                      SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                      SponsorLogo_Dwell := SponsorLogoDwell.Value;
                    end
                    else begin
                      SponsorLogo_Name := '';
                      SponsorLogo_Dwell := 0;
                    end;
                    //Clear Is Child bit
                    Is_Child := FALSE;
                    //For stats
                    StatStoredProcedure := '';
                    StatType := 0;;
                    StatTeam := '';
                    StatLeague := '';
                    StatRecordNumber := 0;
                    for k := 1 to 50 do UserData[k] := User_Data[k];
                    //Set the start/end enable times to the template defaults
                    StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                    EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                    DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                    Selected := FALSE;
                    Comments := ' ';

                    //Insert object to collection
                    Case GridID of
                      FIELD_1: begin
                        if (PlaylistGrid.CurrentDataRow > 0) then
                        begin
                          Ticker_Collection.AtInsert(PlaylistGrid.CurrentDataRow-1, TickerRecPtr);
                          Ticker_Collection.Pack;
                        end
                        else begin
                          Ticker_Collection.Insert(TickerRecPtr);
                          Ticker_Collection.Pack;
                        end;
                      end;
                      FIELD_2: begin
                        if (PlaylistGrid2.CurrentDataRow > 0) then
                        begin
                          Ticker_Collection2.AtInsert(PlaylistGrid2.CurrentDataRow-1, TickerRecPtr);
                          Ticker_Collection2.Pack;
                        end
                        else begin
                          Ticker_Collection2.Insert(TickerRecPtr);
                          Ticker_Collection2.Pack;
                        end;
                      end;
                    end;
                  end;
                end;
                 //Alerts
             1: begin
                  GetMem (BreakingNewsRecPtr, SizeOf(BreakingNewsRec));
                  With BreakingNewsRecPtr^ do
                  begin
                    //Set values for collection record
                    Event_Index := BreakingNews_Collection.Count+1;
                    CreateGUID(EventGUID);
                    Event_GUID := EventGUID;
                    Enabled := EntryEnable.Checked;
                    LeagueCodeRecPtr := LeagueCode_Collection.At(ManualLeagueSelect.ItemIndex);
                    League := LeagueCodeRecPtr^.Heading;
                    Template_ID := CurrentTemplateID;
                    Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                    //Get category & league information
                    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                    Description := '';
                    for k := 1 to 2 do UserData[k] := User_Data[k];
                    //Set the start/end enable times to the template defaults
                    StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                    EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                    DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                    Selected := FALSE;
                    Comments := ' ';
                    //Insert object to collection
                    if (PlaylistGrid.CurrentDataRow > 0) then
                      BreakingNews_Collection.AtInsert(PlaylistGrid.CurrentDataRow-1, BreakingNewsRecPtr)
                    else
                      BreakingNews_Collection.Insert(BreakingNewsRecPtr);
                    BreakingNews_Collection.Pack;
                  end;
                end;
            end; //Single template type (NOT a parent record)
          end;
        end;
      end;
    end;
  end
  //////////////////////////////////////////////////////////////////////////////
  //Execute this block of code if using Default Template list
  //////////////////////////////////////////////////////////////////////////////
  else begin
    OKToGo := TRUE;
    //Save current grid row
    SaveRow := PlaylistGrid.CurrentDataRow;
    SaveRow2 := PlaylistGrid2.CurrentDataRow;
    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
    //First, check to see if sponsors. If so, show sponsor grid
    if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
    begin
      //Get number of games selected
      if (dmMain.SportbaseQuery.RecordCount > 0) then
      begin
        NumGamesSelected := GamesDBGrid.SelectedRows.Count;
        if (NumGamesSelected = 0) then
        begin
          GamesDBGrid.SelectRows(GamesDBGrid.CurrentDataRow, GamesDBGrid.CurrentDataRow, TRUE);
          NumGamesSelected := 1;
        end;
      end
      else begin
        MessageDlg('There are no games available or a game has not been selected. The records ' +
                   'cannot be added to the playlist.', mtWarning, [mbOK], 0);
        NumGamesSelected := 0;
        OKToGo := FALSE;
      end;
    end
    else begin
      NumGamesSelected := 0;
      OKToGo := FALSE;
    end;

    //Proceed if OK
    if (OKToGo) AND (CollectionCount < 1500) AND (Selected_Templates_Collection.Count > 0) then
    begin
      //Go to LAST record in dataset - required to get inserts in correct order
      dmMain.SportbaseQuery.Last;
      RecordCount := dmMain.SportbaseQuery.RecordCount;
      for m := 1 to RecordCount do
      begin
        RowSelected := GamesDBGrid.RowSelected[GamesDBGrid.CurrentDataRow];
        if (RowSelected = TRUE) then
        begin
          //Iterate through all default templates; iterate from bottom up to keep templates in correct
          //order upon insert
          for n := Selected_Templates_Collection.Count-1 downto 0 do
          begin
            SelectedTemplatesRecPtr := Selected_Templates_Collection.At(n);
            //Only proceed if template enabled
            if (SelectedTemplatesRecPtr^.Enabled) then
            begin
              CurrentTemplateID := SelectedTemplatesRecPtr^.Template_ID;
              CurrentTemplateDefs := GetTemplateInformation(CurrentTemplateID);
              ////////////////////////////////////////////////////////////////////////
              // THIS BLOCK EXECUTED FOR CATEGORIES THAT ARE AUTOMATED SPORTS AND FOR
              // TEMPLATES THAT REQUIRE GAME DATA
              ////////////////////////////////////////////////////////////////////////
              if (CurrentTemplateDefs.UsesGameData = TRUE) then
              begin
                //Init fields
                for i := 1 to 50 do User_Data[i] := ' ';
                //CurrentTemplateID := dmMain.Query2.FieldByName('Template_ID').AsInteger;
                NumTemplateFields := GetTemplateFieldsCount(CurrentTemplateID);

                ////////////////////////////////////////////////////////////////////////
                // THIS BLOCK EXECUTED FOR PARENT TEMPLATES - ADDS CHILDREN
                ////////////////////////////////////////////////////////////////////////
                //Check to see if template is a "parent" template; if so, add all child templates
                if (CurrentTemplateDefs.Template_Has_Children) then
                begin
                  if (Child_Template_ID_Collection.Count > 0) then
                  begin
                    //Count DOWN template list - required to get inserts in correct order
                    for i := Child_Template_ID_Collection.Count-1 downto 0 do
                    begin
                      ChildTemplateIDRecPtr := Child_Template_ID_Collection.At(i);
                      //Check for child, and append if match
                      if (ChildTemplateIDRecPtr^.Template_ID = CurrentTemplateID) then
                      begin
                        //Allocate memory for the object
                        GetMem (TickerRecPtr, SizeOf(TickerRec));
                        With TickerRecPtr^ do
                        begin
                          //Set values for collection record
                          Event_Index := Ticker_Collection.Count+1;
                          Field_ID := GridID; //Indicates Field 1 or Field 2
                          CreateGUID(EventGUID);
                          Event_GUID := EventGUID;
                          Enabled := ChildTemplateIDRecPtr^.Child_Default_Enable_State;
                          Template_ID := ChildTemplateIDRecPtr^.Child_Template_ID;
                          Subleague_Mnemonic_Standard := '';
                          Mnemonic_LiveEvent := '';
                          Record_Type := GetRecordTypeFromTemplateID(Template_ID);
                          //Set league ID
                          if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                          begin
                            League := CategoryRecPtr^.Category_Sport;
                            //League := Trim(dmMain.SportbaseQuery.FieldByname('League').AsString);
                            GameID := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                            Description := GameDataFunctions.GetGameData(TRIM(League), TRIM(GameID)).Description;
                          end
                          else begin
                            League := 'NONE';
                            GameID := '-1';
                            Description := '';
                          end;
                          Selected := FALSE;
                          //Check to see if sponsor logo
                          CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                          if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                          begin
                            SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                            SponsorLogo_Dwell := SponsorLogoDwell.Value;
                          end
                          else begin
                            SponsorLogo_Name := ' ';
                            SponsorLogo_Dwell := 0;
                          end;
                          //Set Is Child bit
                          Is_Child := TRUE;
                          //For stats
                          StatStoredProcedure := ' ';
                          StatType := 0;;
                          StatTeam := ' ';
                          StatLeague := ' ';
                          StatRecordNumber := 0;
                          for k := 1 to 50 do UserData[k] := User_Data[k];
                          //Set the start/end enable times to the template defaults
                          StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                          EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                          DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                          Comments := ' ';

                          //Insert object to collection
                          Case GridID of
                            FIELD_1: begin
                              if (PlaylistGrid.CurrentDataRow > 0) then
                              begin
                                Ticker_Collection.AtInsert(PlaylistGrid.CurrentDataRow-1, TickerRecPtr);
                                Ticker_Collection.Pack;
                              end
                              else begin
                                Ticker_Collection.Insert(TickerRecPtr);
                                Ticker_Collection.Pack;
                              end;
                            end;
                            FIELD_2: begin
                              if (PlaylistGrid2.CurrentDataRow > 0) then
                              begin
                                Ticker_Collection2.AtInsert(PlaylistGrid2.CurrentDataRow-1, TickerRecPtr);
                                Ticker_Collection2.Pack;
                              end
                              else begin
                                Ticker_Collection2.Insert(TickerRecPtr);
                                Ticker_Collection2.Pack;
                              end;
                            end;
                          end;
                        end;
                      end;
                    end;
                  end;
                end
                ////////////////////////////////////////////////////////////////////////
                // THIS BLOCK EXECUTED FOR NON-PARENT TEMPLATES IN SPORTS CATEGORIES
                ////////////////////////////////////////////////////////////////////////
                //Not a parent template, so just append record for selected record type
                else begin
                  //Allocate memory for the object
                  GetMem (TickerRecPtr, SizeOf(TickerRec));
                  With TickerRecPtr^ do
                  begin
                    //Set values for collection record
                    Event_Index := Ticker_Collection.Count+1;
                    Field_ID := GridID; //Indicates Field 1 or Field 2
                    CreateGUID(EventGUID);
                    Event_GUID := EventGUID;
                    Enabled := EntryEnable.Checked;
                    Template_ID := CurrentTemplateID;
                    Subleague_Mnemonic_Standard := '';
                    Mnemonic_LiveEvent := '';
                    Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                    //Set league ID
                    if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
                    begin
                      League := CategoryRecPtr^.Category_Sport;
                      //League := Trim(dmMain.SportbaseQuery.FieldByname('League').AsString);
                      GameID := dmMain.SportbaseQuery.FieldByname('GCode').AsString;
                      Description := GameDataFunctions.GetGameData(TRIM(League), TRIM(GameID)).Description;
                    end
                    else begin
                      League := 'NONE';
                      GameID := '-1';
                      Description := '';
                    end;
                    //Check to see if sponsor logo
                    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                    if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                    begin
                      SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                      SponsorLogo_Dwell := SponsorLogoDwell.Value;
                    end
                    else begin
                      SponsorLogo_Name := ' ';
                      SponsorLogo_Dwell := 0;
                    end;
                    //Clear Is Child bit
                    Is_Child := FALSE;
                    Selected := FALSE;
                    //For stats
                    StatStoredProcedure := '';
                    StatType := 0;;
                    StatTeam := ' ';
                    StatLeague := ' ';
                    StatRecordNumber := 0;
                    for k := 1 to 50 do UserData[k] := User_Data[k];
                    //Set the start/end enable times to the template defaults
                    StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                    EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                    DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                    Comments := ' ';

                    //Insert object to collection
                    Case GridID of
                      FIELD_1: begin
                        if (PlaylistGrid.CurrentDataRow > 0) then
                        begin
                          Ticker_Collection.AtInsert(PlaylistGrid.CurrentDataRow-1, TickerRecPtr);
                          Ticker_Collection.Pack;
                        end
                        else begin
                          Ticker_Collection.Insert(TickerRecPtr);
                          Ticker_Collection.Pack;
                        end;
                      end;
                      FIELD_2: begin
                        if (PlaylistGrid2.CurrentDataRow > 0) then
                        begin
                          Ticker_Collection2.AtInsert(PlaylistGrid2.CurrentDataRow-1, TickerRecPtr);
                          Ticker_Collection2.Pack;
                        end
                        else begin
                          Ticker_Collection2.Insert(TickerRecPtr);
                          Ticker_Collection2.Pack;
                        end;
                      end;
                    end;
                  end;
                end; //Single template type (NOT a parent record)
              end //End of block for templates that use game data

              //////////////////////////////////////////////////////////////////
              //THIS BLOCK OF CODE FOR TEMPLATES THAT DO NOT REQUIRE GAME DATA
              //////////////////////////////////////////////////////////////////
              else begin
                //Allocate memory for the object
                GetMem (TickerRecPtr, SizeOf(TickerRec));
                With TickerRecPtr^ do
                begin
                  //Set values for collection record
                  Event_Index := Ticker_Collection.Count+1;
                  Field_ID := GridID; //Indicates Field 1 or Field 2
                  CreateGUID(EventGUID);
                  Event_GUID := EventGUID;
                  Enabled := EntryEnable.Checked;
                  Template_ID := CurrentTemplateID;
                  Subleague_Mnemonic_Standard := '';
                  Mnemonic_LiveEvent := '';
                  Record_Type := GetRecordTypeFromTemplateID(CurrentTemplateID);
                  //Get category & league information
                  CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
                  if (Trim(CategoryRecPtr^.Category_Sport) <> '') then
                  begin
                    League := CategoryRecPtr^.Category_Sport;
                  end
                  else begin
                    //Check if manual league; if so, first user text field will be league
                    if (CurrentTemplateDefs.ManualLeague) then
                    begin
                      LeagueCodeRecPtr := LeagueCode_Collection.At(ManualLeagueSelect.ItemIndex);
                      League := LeagueCodeRecPtr^.Heading;
                    end
                    else begin
                      League := 'NONE';
                    end;
                  end;
                  GameID := '0';
                  Description := '';
                  //Check to see if sponsor logo
                  if (UpperCase(CategoryRecPtr^.Category_Name) = 'SPONSORS') then
                  begin
                    SponsorLogo_Name := dmMain.tblSponsor_Logos.FieldByName('LogoFilename').AsString;
                    SponsorLogo_Dwell := SponsorLogoDwell.Value;
                  end
                  else begin
                    SponsorLogo_Name := '';
                    SponsorLogo_Dwell := 0;
                  end;
                  //Clear Is Child bit
                  Is_Child := FALSE;
                  //For stats
                  StatStoredProcedure := '';
                  StatType := 0;;
                  StatTeam := '';
                  StatLeague := '';
                  StatRecordNumber := 0;
                  for k := 1 to 50 do UserData[k] := User_Data[k];
                  //Set the start/end enable times to the template defaults
                  StartEnableDateTime := CurrentTemplateDefs.StartEnableDateTime;
                  EndEnableDateTime := CurrentTemplateDefs.EndEnableDateTime;
                  DwellTime := GetTemplateInformation(Template_ID).Default_Dwell;
                  Selected := FALSE;
                  Comments := ' ';

                  //Insert object to collection
                  Case GridID of
                    FIELD_1: begin
                      if (PlaylistGrid.CurrentDataRow > 0) then
                      begin
                        Ticker_Collection.AtInsert(PlaylistGrid.CurrentDataRow-1, TickerRecPtr);
                        Ticker_Collection.Pack;
                      end
                      else begin
                        Ticker_Collection.Insert(TickerRecPtr);
                        Ticker_Collection.Pack;
                      end;
                    end;
                    FIELD_2: begin
                      if (PlaylistGrid2.CurrentDataRow > 0) then
                      begin
                        Ticker_Collection2.AtInsert(PlaylistGrid2.CurrentDataRow-1, TickerRecPtr);
                        Ticker_Collection2.Pack;
                      end
                      else begin
                        Ticker_Collection2.Insert(TickerRecPtr);
                        Ticker_Collection2.Pack;
                      end;
                    end;
                  end;  
                end; //Single template type (NOT a parent record)
              end;
            end;
          end;
        end;
        //Go to PRIOR game - required to get inserts in correct order
        dmMain.SportbaseQuery.Prior;
      end;
    end;
  end;
  //Refresh grids
  Case GridID of
    FIELD_1: begin
      RefreshPlaylistGrid(FIELD_1, PlaylistGrid);
      //Refresh the fields grid
      RefreshEntryFieldsGrid(TickerDisplayMode, FIELD_1, EntryFieldsGrid);
      //Restore current grid rows
      if (SaveRow < 1) then SaveRow := 1;
      PlaylistGrid.CurrentDataRow := SaveRow;
    end;
    FIELD_2: begin
      RefreshPlaylistGrid(FIELD_2, PlaylistGrid2);
      //Refresh the fields grid
      RefreshEntryFieldsGrid(TickerDisplayMode, FIELD_2, EntryFieldsGrid2);
      //Restore current grid rows
      if (SaveRow2 < 1) then SaveRow2 := 1;
      PlaylistGrid2.CurrentDataRow := SaveRow2;
    end;
  end;

  //Save the playlist out and signal the Playout Controller to re-load if in automated playout mode
  if (InRemotePlayoutMode) then
  begin
    //Save the playlist
    SavePlaylistWithoutClear;
    //Signal playout controller to refresh the playlist
    PlayoutInterface.RefreshCurrentPlaylist(PlaylistInfo[TICKER].PlaylistID);
  end;
end;

//Handler for Enable selected playlist records
//Field 1
procedure TMainForm.Enable1Click(Sender: TObject);
begin
  EnableDisablePlaylistEntries(TRUE, FIELD_1, PlaylistGrid);
end;
//Field 2
procedure TMainForm.Enable_2Click(Sender: TObject);
begin
  EnableDisablePlaylistEntries(TRUE, FIELD_2, PlaylistGrid2);
end;
//Handlers for Disable selected playlist records
//Field 1
procedure TMainForm.Disable1Click(Sender: TObject);
begin
  EnableDisablePlaylistEntries(FALSE, FIELD_1, PlaylistGrid);
end;
//Field 2
procedure TMainForm.Disable_2Click(Sender: TObject);
begin
  EnableDisablePlaylistEntries(FALSE, FIELD_2, PlaylistGrid2);
end;
procedure TMainForm.EnableDisablePlaylistEntries(EnableState: Boolean; GridID: SmallInt; var DataGrid: TtsGrid);
var
  TickerRecPtr, TickerRecPtr2: ^TickerRec;
  BreakingNewsRecPtr, BreakingNewsRecPtr2: ^BreakingNewsRec;
  i,j: SmallInt;
  CollectionCount: SmallInt;
begin
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker
   0: begin
        Case GridID of
          FIELD_1: CollectionCount := Ticker_Collection.Count;
          FIELD_2: CollectionCount := Ticker_Collection2.Count;
        end;
        if (CollectionCount > 0) then
          for i := 1 to CollectionCount do
          begin
            Case GridID of
              FIELD_1: TickerRecPtr := Ticker_Collection.At(i-1);
              FIELD_2: TickerRecPtr := Ticker_Collection2.At(i-1);
            end;
            if (DataGrid.RowSelected[i] = TRUE) then
              TickerRecPtr^.Enabled := EnableState;
            //Now, perform same operation on all copies of pages
            for j := 1 to CollectionCount do
            begin
              Case GridID of
                FIELD_1: TickerRecPtr2 := Ticker_Collection.At(j-1);
                FIELD_2: TickerRecPtr2 := Ticker_Collection2.At(j-1);
              end;
              if (IsEqualGUID(TickerRecPtr^.Event_GUID, TickerRecPtr2^.Event_GUID)) then
              begin
                if (DataGrid.RowSelected[i] = TRUE) then
                  TickerRecPtr2^.Enabled := EnableState;
              end;
            end;
          end;
      end;
      //Alerts
   1: begin
        if (BreakingNews_Collection.Count > 0) then
          for i := 1 to BreakingNews_Collection.Count do
          begin
            BreakingNewsRecPtr := BreakingNews_Collection.At(i-1);
            if (PlaylistGrid.RowSelected[i] = TRUE) then
              BreakingNewsRecPtr^.Enabled := EnableState;
            //Now, perform same operation on all copies of pages
            for j := 1 to BreakingNews_Collection.Count do
            begin
              BreakingNewsRecPtr2 := BreakingNews_Collection.At(j-1);
              if (IsEqualGUID(BreakingNewsRecPtr^.Event_GUID, BreakingNewsRecPtr2^.Event_GUID)) then
              begin
                if (PlaylistGrid.RowSelected[i] = TRUE) then
                  BreakingNewsRecPtr2^.Enabled := EnableState;
              end;
            end;
          end;
      end;
  end;
  //Refresh the grid
  Case GridID of
    FIELD_1: RefreshPlaylistGrid(FIELD_1, PlaylistGrid);
    FIELD_2: RefreshPlaylistGrid(FIELD_2, PlaylistGrid2);
  end;
end;

//Handler for popup menu entry to allow editing of start/end enable times
//Field 1
procedure TMainForm.EditStartEndEnableTimes1Click(Sender: TObject);
begin
  EditStartEndEnableTimes(FIELD_1, PlaylistGrid);
end;
//Field 2
procedure TMainForm.EditStartEndEnableTimes2Click(Sender: TObject);
begin
  EditStartEndEnableTimes(FIELD_2, PlaylistGrid2);
end;
procedure TMainForm.EditStartEndEnableTimes(GridID: SmallInt; var DataGrid: TtsGrid);
var
  i,j,k,m: SmallInt;
  CurrentRow, CurrentTopRow: SmallInt;
  Modal: TEnableDateTimeEditorDlg;
  Control: Word;
  TickerRecPtr, TickerRecPtr2: ^TickerRec;
  BreakingNewsRecPtr, BreakingNewsRecPtr2: ^BreakingNewsRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  User_Data: Array[1..50] of String[255];
  CurrentTemplateID: SmallInt;
  NumTemplateFields: SmallInt;
  CollectionCount: SmallInt;
  EventGUID: TGUID;
begin
  Case PlaylistSelectTabControl.TabIndex of
       //Ticker
    0: Case GridID of
         FIELD_1: CollectionCount := Ticker_Collection.Count;
         FIELD_2: CollectionCount := Ticker_Collection2.Count;
       end;
       //Alerts
    1: CollectionCount := BreakingNews_Collection.Count;
  end;
  If ((CollectionCount > 0) AND (DataGrid.Rows >= 0)) then
  begin
    //Get current row
    Case GridID of
      FIELD_1: begin
        CurrentRow := PlaylistGrid.CurrentDataRow;
        CurrentTopRow := PlaylistGrid.TopRow;
      end;
      FIELD_2: begin
        CurrentRow := PlaylistGrid2.CurrentDataRow;
        CurrentTopRow := PlaylistGrid2.TopRow;
      end;
    end;
    //Only proceed if a row was selected
    if (CurrentRow <> -1) then
    begin
      Case PlaylistSelectTabControl.TabIndex of
          //Ticker
       0: begin
            Case GridID of
              FIELD_1: TickerRecPtr := Ticker_Collection.At(CurrentRow-1);
              FIELD_2: TickerRecPtr := Ticker_Collection2.At(CurrentRow-1);
            end;
            //Setup and launch editing dialog if applicable
            try
              Modal := TEnableDateTimeEditorDlg.Create(Application);
              Modal.EntryNote.Text := TickerRecPtr^.Comments;
              Modal.EntryStartEnableDate.Date := TickerRecPtr^.StartEnableDateTime;
              Modal.EntryStartEnableTime.Time := TickerRecPtr^.StartEnableDateTime;
              Modal.EntryEndEnableDate.Date := TickerRecPtr^.EndEnableDateTime;
              Modal.EntryEndEnableTime.Time := TickerRecPtr^.EndEnableDateTime;
              //If a sponsor dwell was specified, it's a sponsor logo
              if (TickerRecPtr^.SponsorLogo_Dwell > 0) then
                Modal.EntryDwellTime.Value := Trunc(TickerRecPtr^.SponsorLogo_Dwell)
              else
                Modal.EntryDwellTime.Value := Trunc(TickerRecPtr^.DwellTime/1000);

              //Show the dialog
              Control := Modal.ShowModal;
            finally
              //Set new values if user didn't cancel out
              if (Control = mrOK) then
              begin
                //Edit all matching objects using comparsion of GUIDs
                Case GridID of
                  FIELD_1: CollectionCount := Ticker_Collection.Count;
                  FIELD_2: CollectionCount := Ticker_Collection2.Count;
                end;
                for j := 0 to CollectionCount-1 do
                begin
                  if (DataGrid.RowSelected[j+1] = TRUE) then
                  begin
                    Case GridID of
                      FIELD_1: TickerRecPtr := Ticker_Collection.At(j);
                      FIELD_2: TickerRecPtr := Ticker_Collection2.At(j);
                    end;
                    EventGUID := TickerRecPtr^.Event_GUID;
                    for m := 0 to CollectionCount-1 do
                    begin
                      Case GridID of
                        FIELD_1: TickerRecPtr2 := Ticker_Collection.At(m);
                        FIELD_2: TickerRecPtr2 := Ticker_Collection2.At(m);
                      end;
                      if (IsEqualGUID(TickerRecPtr2^.Event_GUID, EventGUID)) then
                      With TickerRecPtr2^ do
                      begin
                        StartEnableDateTime := Trunc(Modal.EntryStartEnableDate.Date) +
                            (Modal.EntryStartEnableTime.Time - Trunc(Modal.EntryStartEnableTime.Time));
                        EndEnableDateTime := Trunc(Modal.EntryEndEnableDate.Date) +
                            (Modal.EntryEndEnableTime.Time - Trunc(Modal.EntryEndEnableTime.Time));
                        if (Modal.NoteChangeEnable.Checked) then
                          Comments := Modal.EntryNote.Text;
                        //If a sponsor dwell was specified, it's a sponsor logo
                        if (TickerRecPtr^.SponsorLogo_Dwell > 0) then
                          SponsorLogo_Dwell := Modal.EntryDwellTime.Value
                        else
                          DwellTime := Modal.EntryDwellTime.Value*1000;
                      end;
                    end;
                  end;
                end;
              end;
              Modal.Free;
            end;
          end;
          //Alerts
       1: begin
            BreakingNewsRecPtr := BreakingNews_Collection.At(CurrentRow-1);
            //Setup and launch editing dialog if applicable
            try
              Modal := TEnableDateTimeEditorDlg.Create(Application);
              Modal.EntryNote.Text := BreakingNewsRecPtr^.Comments;
              Modal.EntryStartEnableDate.Date := BreakingNewsRecPtr^.StartEnableDateTime;
              Modal.EntryStartEnableTime.Time := BreakingNewsRecPtr^.StartEnableDateTime;
              Modal.EntryEndEnableDate.Date := BreakingNewsRecPtr^.EndEnableDateTime;
              Modal.EntryEndEnableTime.Time := BreakingNewsRecPtr^.EndEnableDateTime;
              Modal.EntryDwellTime.Value := Trunc(BreakingNewsRecPtr^.DwellTime/1000);
              //Show the dialog
              Control := Modal.ShowModal;
            finally
              //Set new values if user didn't cancel out
              if (Control = mrOK) then
              begin
                //Edit all matching objects using comparsion of GUIDs
                for j := 0 to BreakingNews_Collection.Count-1 do
                begin
                  if (PlaylistGrid.RowSelected[j+1] = TRUE) then
                  begin
                    BreakingNewsRecPtr := BreakingNews_Collection.At(j);
                    EventGUID := BreakingNewsRecPtr^.Event_GUID;
                    for m := 0 to BreakingNews_Collection.Count-1 do
                    begin
                      BreakingNewsRecPtr2 := BreakingNews_Collection.At(m);
                      if (IsEqualGUID(BreakingNewsRecPtr2^.Event_GUID, EventGUID)) then
                      With BreakingNewsRecPtr2^ do
                      begin
                        StartEnableDateTime := Trunc(Modal.EntryStartEnableDate.Date) +
                            (Modal.EntryStartEnableTime.Time - Trunc(Modal.EntryStartEnableTime.Time));
                        EndEnableDateTime := Trunc(Modal.EntryEndEnableDate.Date) +
                            (Modal.EntryEndEnableTime.Time - Trunc(Modal.EntryEndEnableTime.Time));
                        if (Modal.NoteChangeEnable.Checked) then
                          Comments := Modal.EntryNote.Text;
                        //if (Modal.DwellTimeEnable.Checked) then
                        DwellTime := Modal.EntryDwellTime.Value*1000;
                      end;
                    end;
                  end;
                end;
              end;
              Modal.Free;
            end;
          end;
      end;
    end;
    //Refresh grids
    Case GridID of
      FIELD_1: begin
        RefreshPlaylistGrid(FIELD_1, PlaylistGrid);
        PlaylistGrid.CurrentDataRow := CurrentRow;
        PlaylistGrid.TopRow := CurrentTopRow;
        PlaylistGrid.RowSelected[CurrentRow] := TRUE;
      end;
      FIELD_2: begin
        RefreshPlaylistGrid(FIELD_2, PlaylistGrid2);
        PlaylistGrid2.CurrentDataRow := CurrentRow;
        PlaylistGrid2.TopRow := CurrentTopRow;
        PlaylistGrid2.RowSelected[CurrentRow] := TRUE;
      end;
    end;
  end;
end;

//Handler for cut playlist entry
//Field 1
procedure TMainForm.Cut1Click(Sender: TObject);
begin
  Cut(FIELD_1, PlaylistGrid);
end;
//Field 2
procedure TMainForm.Cut2Click(Sender: TObject);
begin
  Cut(FIELD_2, PlaylistGrid2);
end;
procedure TMainForm.Cut(GridID: SmallInt; var DataGrid: TtsGrid);
begin
  //Copy records to temp collection
  SelectForCopyOrCut(GridID, DataGrid);
  //Set flag
  SelectForCut := TRUE;
  //Delete records from main collection
  Case GridID of
    FIELD_1: DeleteGraphicFromZipperPlaylist(FALSE, FIELD_1, PlaylistGrid);
    FIELD_2: DeleteGraphicFromZipperPlaylist(FALSE, FIELD_2, PlaylistGrid2);
  end;
end;

//Handler for copy playlist entry
//Field 1
procedure TMainForm.Copy1Click(Sender: TObject);
begin
  Copy(FIELD_1, PlaylistGrid);
end;
//Field 2
procedure TMainForm.Copy2Click(Sender: TObject);
begin
  Copy(FIELD_2, PlaylistGrid2);
end;
procedure TMainForm.Copy(GridID: SmallInt; var DataGrid: TtsGrid);
begin
  SelectForCopyOrCut(GridID, DataGrid);
  SelectForCut := FALSE;
end;

//General procedure to set the record to be copied or cut
procedure TMainForm.SelectForCopyOrCut(GridID: SmallInt; var DataGrid: TtsGrid);
var
  i,j: SmallInt;
  CutCopyRecordCount: SmallInt;
  SelectedRecordFound: Boolean;
  TickerRecPtr, TempTickerRecPtr: ^TickerRec;
  BreakingNewsRecPtr, TempBreakingNewsRecPtr: ^BreakingNewsRec;
  Save_Cursor: TCursor;
  CollectionCount: SmallInt;
begin
  //Change cursor
  Save_Cursor := Screen.Cursor;
  //Show hourglass cursor  Screen.Cursor := crHourGlass;
  SelectedRecordFound := FALSE;
  CutCopyRecordCount := 0;
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker
   0: begin
        Case GridID of
          FIELD_1: CollectionCount := Ticker_Collection.Count;
          FIELD_2: CollectionCount := Ticker_Collection2.Count;
        end;
        if (CollectionCount > 0) then
        begin
          //Clear the temp collection
          Temp_Ticker_Collection.Clear;
          Temp_Ticker_Collection.Pack;
          for i := 0 to CollectionCount-1 do
          begin
            Case GridID of
              FIELD_1: TickerRecPtr := Ticker_Collection.At(i);
              FIELD_2: TickerRecPtr := Ticker_Collection2.At(i);
            end;
            if (DataGrid.RowSelected[i+1] = TRUE) then
            begin
              //Set selected flag to true
              TickerRecPtr^.Selected := TRUE;
              //Inset record into temporary collection - will be used for copy/paste operation
//DISABLED DUE TO MEMORY ALLOCATION ISSUES - USING STATIC ARRAY
              //GetMem (TempTickerRecPtr, SizeOf(TickerRec));
//DISABLED DUE TO MEMORY ALLOCATION ISSUES - USING STATIC ARRAY
              //With TempTickerRecPtr^ do
              With TemporaryTickerDataArray[CutCopyRecordCount+1] do
              begin
                Event_Index := TickerRecPtr^.Event_Index;
                Field_ID := TickerRecPtr^.Field_ID;
                Event_GUID := TickerRecPtr^.Event_GUID;
                Is_Child := TickerRecPtr^.Is_Child;
                Enabled := TickerRecPtr^.Enabled;
                League := TickerRecPtr^.League;
                Subleague_Mnemonic_Standard := TickerRecPtr^.Subleague_Mnemonic_Standard;
                Mnemonic_LiveEvent := TickerRecPtr^.Mnemonic_LiveEvent;
                Template_ID := TickerRecPtr^.Template_ID;
                Record_Type := TickerRecPtr^.Record_Type;
                GameID := TickerRecPtr^.GameID;
                SponsorLogo_Name := TickerRecPtr^.SponsorLogo_Name;
                SponsorLogo_Dwell := TickerRecPtr^.SponsorLogo_Dwell;
                StatStoredProcedure := TickerRecPtr^.StatStoredProcedure;
                StatType := TickerRecPtr^.StatType;
                StatTeam := TickerRecPtr^.StatTeam;
                StatLeague := TickerRecPtr^.StatLeague;
                StatRecordNumber := TickerRecPtr^.StatRecordNumber;
                for j := 1 to 50 do UserData[j] := TickerRecPtr^.UserData[j];
                StartEnableDateTime := TickerRecPtr^.StartEnableDateTime;
                EndEnableDateTime := TickerRecPtr^.EndEnableDateTime;
                Description := TickerRecPtr^.Description;
                Comments := TickerRecPtr^.Comments;
                Selected := TickerRecPtr^.Selected;
                DwellTime := TickerRecPtr^.DwellTime;
              end;
//DISABLED DUE TO MEMORY ALLOCATION ISSUES - USING STATIC ARRAY
              //Insert the new record into the collection
              //Temp_Ticker_Collection.Insert(TempTickerRecPtr);
              //Temp_Ticker_Collection.Pack;
              Inc(CutCopyRecordCount);
            end
            else TickerRecPtr^.Selected := FALSE;
          end;
          TemporaryTickerDataCount := CutCopyRecordCount;
        end;
      end;
      //Alerts
   1: begin
        if (BreakingNews_Collection.Count > 0) then
        begin
          //Clear the temp collection
          Temp_BreakingNews_Collection.Clear;
          Temp_BreakingNews_Collection.Pack;
          for i := 0 to BreakingNews_Collection.Count-1 do
          begin
            BreakingNewsRecPtr := BreakingNews_Collection.At(i);
            if (PlaylistGrid.RowSelected[i+1] = TRUE) then
            begin
              BreakingNewsRecPtr^.Selected := TRUE;
              //Insert record into temporary collection - will be used for copy/paste operation
//DISABLED DUE TO MEMORY ALLOCATION ISSUES - USING STATIC ARRAY
              //GetMem (TempBugRecPtr, SizeOf(BugRec));
//DISABLED DUE TO MEMORY ALLOCATION ISSUES - USING STATIC ARRAY
              //With TempBugRecPtr^ do
              With TemporaryBreakingNewsDataArray[CutCopyRecordCount+1] do
              begin
                Event_Index := BreakingNewsRecPtr^.Event_Index;
                Event_GUID := BreakingNewsRecPtr^.Event_GUID;
                Enabled := BreakingNewsRecPtr^.Enabled;
                Template_ID := BreakingNewsRecPtr^.Template_ID;
                Record_Type := BreakingNewsRecPtr^.Record_Type;
                for j := 1 to 2 do UserData[j] := BreakingNewsRecPtr^.UserData[j];
                StartEnableDateTime := BreakingNewsRecPtr^.StartEnableDateTime;
                EndEnableDateTime := BreakingNewsRecPtr^.EndEnableDateTime;
                Description := BreakingNewsRecPtr^.Description;
                Comments := BreakingNewsRecPtr^.Comments;
                Selected := BreakingNewsRecPtr^.Selected;
                DwellTime := BreakingNewsRecPtr^.DwellTime;
              end;
              //Insert the new record into the collection
//DISABLED DUE TO MEMORY ALLOCATION ISSUES - USING STATIC ARRAY
              //Insert the new record into the collection
              //Temp_BreakingNews_Collection.Insert(TempBreakingNewsRecPtr);
              //Temp_BreakingNews_Collection.Pack;
              Inc(CutCopyRecordCount);
            end
            else BreakingNewsRecPtr^.Selected := FALSE;
          end;
          TemporaryBreakingNewsDataCount := CutCopyRecordCount;
        end;
      end;
  end;
  //Restore cursor
  Screen.Cursor := Save_Cursor;
end;

//Handler for paste selected record(s) at specified record location (not last)
//Field 1
procedure TMainForm.Paste1Click(Sender: TObject);
begin
  Paste(FIELD_1, PlaylistGrid);
end;
//Field 2
procedure TMainForm.Paste2Click(Sender: TObject);
begin
  Paste(FIELD_2, PlaylistGrid2);
end;
procedure TMainForm.Paste(GridID: SmallInt; var DataGrid: TtsGrid);
begin
  //Call paste entries procedure WITHOUT append and one copy
  PasteEntries(FALSE, GridID, DataGrid);
end;
//Handler for paste selected record(s) at end of playlist
//Field 1
procedure TMainForm.AppendtoEnd1Click(Sender: TObject);
begin
  PasteEnd(FIELD_1, PlaylistGrid);
end;
//Field 2
procedure TMainForm.AppendToEnd2Click(Sender: TObject);
begin
  PasteEnd(FIELD_2, PlaylistGrid2);
end;
procedure TMainForm.PasteEnd(GridID: SmallInt; var DataGrid: TtsGrid);
begin
  //Call paste entries procedure WITH append and one copy
  PasteEntries(TRUE, GridID, DataGrid);
end;
//Handler for duplicate selected record at specified record location (not last)
procedure TMainForm.Duplicate1Click(Sender: TObject);
begin
  Duplicate(FIELD_1, PlaylistGrid);
end;
procedure TMainForm.Duplicate2Click(Sender: TObject);
begin
  Duplicate(FIELD_2, PlaylistGrid2);
end;
procedure TMainForm.Duplicate(GridID: SmallInt; var DataGrid: TtsGrid);
var
  DuplicationCount: SmallInt;
  Modal: TDuplicationCountSelectDlg;
  Control: Word;
begin
  try
    Modal := TDuplicationCountSelectDlg.Create(Application);
    Modal.DuplicationCount.Value := LastDuplicationCount;
    Control := Modal.ShowModal;
  finally
    if (Control = mrOK) then
    begin
      DuplicationCount := Modal.DuplicationCount.Value;
      LastDuplicationCount := Modal.DuplicationCount.Value;
      //Call paste entries procedure WITHOUT append and required number of copies
      DuplicateEntries(FALSE, DuplicationCount, GridID, DataGrid);
    end;
    Modal.Free;
  end;
end;
//Handler for duplicate selected record at end of playlist
//Field 1
procedure TMainForm.DuplicateAndAppendToEnd1Click(Sender: TObject);
begin
  DuplicateAndAppendToEnd(FIELD_1, PlaylistGrid);
end;
//Field 2
procedure TMainForm.DuplicateAndAppendToEnd2Click(Sender: TObject);
begin
  DuplicateAndAppendToEnd(FIELD_2, PlaylistGrid2);
end;
procedure TMainForm.DuplicateAndAppendToEnd(GridID: SmallInt; var DataGrid: TtsGrid);
var
  DuplicationCount: SmallInt;
  Modal: TDuplicationCountSelectDlg;
  Control: Word;
begin
  try
    Modal := TDuplicationCountSelectDlg.Create(Application);
    Modal.DuplicationCount.Value := LastDuplicationCount;
    Control := Modal.ShowModal;
  finally
    if (Control = mrOK) then
    begin
      DuplicationCount := Modal.DuplicationCount.Value;
      LastDuplicationCount := Modal.DuplicationCount.Value;
      //Call paste entries procedure WITH append and required number of copies
      DuplicateEntries(TRUE, DuplicationCount, GridID, DataGrid);
    end;
    Modal.Free;
  end;
end;

//General procedure for pasting selected records
procedure TMainForm.DuplicateEntries(AppendToEnd: Boolean; DuplicationCount: SmallInt; GridID: SmallInt; var DataGrid: TtsGrid);
var
  i,j,k, m: SmallInt;
  InsertPoint: SmallInt;
  ExistingTickerRecPtr, TickerRecPtr, NewTickerRecPtr: ^TickerRec;
  ExistingBreakingNewsRecPtr, BreakingNewsRecPtr, NewBreakingNewsRecPtr: ^BreakingNewsRec;
  EventGUID: TGUID;
  UpdateOnlyMode: Boolean;
  Control: Word;
  CurrentEventGUID: TGUID;
  Save_Cursor: TCursor;
  ArrayIndex: SmallInt;
  SaveCurrentDataRow, SaveTopRow: SmallInt;
begin
  //Change cursor
  Save_Cursor := Screen.Cursor;
  //Show hourglass cursor
  Screen.Cursor := crHourGlass;
  //Init
  UpdateOnlyMode := FALSE;
  //Save grid position
  Case GridID of
    FIELD_1: begin
      SaveCurrentDataRow := PlaylistGrid.CurrentDataRow;
      SaveTopRow := PlaylistGrid.TopRow;
    end;
    FIELD_2: begin
      SaveCurrentDataRow := PlaylistGrid2.CurrentDataRow;
      SaveTopRow := PlaylistGrid2.TopRow;
    end;
  end;

  Case PlaylistSelectTabControl.TabIndex of
      //Ticker
   0: begin
        //Set the paste/insertion point
        Case GridID of
          FIELD_1: InsertPoint := PlaylistGrid.CurrentDataRow-1;
          FIELD_2: InsertPoint := PlaylistGrid2.CurrentDataRow-1;
        end;
        if (InsertPoint < 0) then InsertPoint := 0;
        //Iterate through temporary collection starting at last element and working to
        //top to keep in correct order
        Case GridID of
          FIELD_1: TickerRecPtr := Ticker_Collection.At(InsertPoint);
          FIELD_2: TickerRecPtr := Ticker_Collection2.At(InsertPoint);
        end;
        for k := 1 to DuplicationCount do
        begin
          GetMem (NewTickerRecPtr, SizeOf(TickerRec));
          With NewTickerRecPtr^ do
          begin
            Event_Index := TickerRecPtr.Event_Index;
            Field_ID := GridID; //For Field 1 or Field 2
            CreateGUID(EventGUID);
            Event_GUID := EventGUID;
            Is_Child := TickerRecPtr.Is_Child;
            Enabled := TickerRecPtr.Enabled;
            League := TickerRecPtr.League;
            Subleague_Mnemonic_Standard := TickerRecPtr^.Subleague_Mnemonic_Standard;
            Mnemonic_LiveEvent := TickerRecPtr^.Mnemonic_LiveEvent;
            Template_ID := TickerRecPtr.Template_ID;
            Record_Type := TickerRecPtr.Record_Type;
            GameID := TickerRecPtr^.GameID;
            SponsorLogo_Name := TickerRecPtr.SponsorLogo_Name;
            SponsorLogo_Dwell := TickerRecPtr.SponsorLogo_Dwell;
            StatStoredProcedure := TickerRecPtr.StatStoredProcedure;
            StatType := TickerRecPtr.StatType;
            StatTeam := TickerRecPtr.StatTeam;
            StatLeague := TickerRecPtr.StatLeague;
            StatRecordNumber := TickerRecPtr.StatRecordNumber;
            for j := 1 to 50 do UserData[j] := TickerRecPtr.UserData[j];
            StartEnableDateTime := TickerRecPtr.StartEnableDateTime;
            EndEnableDateTime := TickerRecPtr.EndEnableDateTime;
            Description := TickerRecPtr^.Description;
            Comments := TickerRecPtr.Comments;
            Selected := TickerRecPtr.Selected;
            DwellTime := TickerRecPtr.DwellTime;
          end;
          //Insert the new record into the collection
          Case GridID of
            FIELD_1: begin
              if (AppendToEnd) then
                Ticker_Collection.Insert(NewTickerRecPtr)
              else
                Ticker_Collection.AtInsert(InsertPoint, NewTickerRecPtr);
            end;
            FIELD_2: begin
              if (AppendToEnd) then
                Ticker_Collection2.Insert(NewTickerRecPtr)
              else
                Ticker_Collection2.AtInsert(InsertPoint, NewTickerRecPtr);
            end;
          end;
        end;
        Case GridID of
          FIELD_1: RefreshPlaylistGrid(FIELD_1, PlaylistGrid);
          FIELD_2: RefreshPlaylistGrid(FIELD_2, PlaylistGrid2);
        end;
      end;
      //Alerts
   1: begin
        //Set the paste/insertion point
        InsertPoint := PlaylistGrid.CurrentDataRow-1;
        if (InsertPoint < 0) then InsertPoint := 0;
        //Iterate through temporary collection starting at last element and working to
        //top to keep in correct order
        BreakingNewsRecPtr := BreakingNews_Collection.At(InsertPoint);
        for k := 1 to DuplicationCount do
        begin
          GetMem (NewBreakingNewsRecPtr, SizeOf(BreakingNewsRec));
          With NewBreakingNewsRecPtr^ do
          begin
            Event_Index := BreakingNewsRecPtr.Event_Index;
            CreateGUID(EventGUID);
            Event_GUID := EventGUID;
            Enabled := BreakingNewsRecPtr.Enabled;
            Template_ID := BreakingNewsRecPtr.Template_ID;
            Record_Type := BreakingNewsRecPtr.Record_Type;
            for j := 1 to 2 do UserData[j] := BreakingNewsRecPtr.UserData[j];
            StartEnableDateTime := BreakingNewsRecPtr.StartEnableDateTime;
            EndEnableDateTime := BreakingNewsRecPtr.EndEnableDateTime;
            Description := BreakingNewsRecPtr.Description;
            Comments := BreakingNewsRecPtr.Comments;
            Selected := BreakingNewsRecPtr.Selected;
            DwellTime := BreakingNewsRecPtr.DwellTime;
          end;
          //Insert the new record into the collection
          if (AppendToEnd) then
            BreakingNews_Collection.Insert(NewBreakingNewsRecPtr)
          else
            BreakingNews_Collection.AtInsert(InsertPoint, NewBreakingNewsRecPtr);
        end;
        RefreshPlaylistGrid(FIELD_1, PlaylistGrid);
      end;
  end;
  //Set current row to bottom duplication row
  Case GridID of
    FIELD_1: begin
      PlaylistGrid.CurrentDataRow := SaveCurrentDataRow + DuplicationCount;
      PlaylistGrid.TopRow := SaveTopRow + DuplicationCount;
      PlaylistGrid.RowSelected[SaveCurrentDataRow] := FALSE;
      PlaylistGrid.RowSelected[SaveCurrentDataRow+DuplicationCount] := TRUE;
    end;
    FIELD_2: begin
      PlaylistGrid2.CurrentDataRow := SaveCurrentDataRow + DuplicationCount;
      PlaylistGrid2.TopRow := SaveTopRow + DuplicationCount;
      PlaylistGrid2.RowSelected[SaveCurrentDataRow] := FALSE;
      PlaylistGrid2.RowSelected[SaveCurrentDataRow+DuplicationCount] := TRUE;
    end;
  end;
  //Restore cursor
  Screen.Cursor := Save_Cursor;

  //Save the playlist out and signal the Playout Controller to re-load if in automated playout mode
  if (InRemotePlayoutMode) then
  begin
    //Save the playlist
    SavePlaylistWithoutClear;
    //Signal playout controller to refresh the playlist
    PlayoutInterface.RefreshCurrentPlaylist(PlaylistInfo[TICKER].PlaylistID);
  end;
end;

//General procedure for pasting selected records
procedure TMainForm.PasteEntries(AppendToEnd: Boolean; GridID: SmallInt; var DataGrid: TtsGrid);
var
  i,j,k, m: SmallInt;
  InsertPoint: SmallInt;
  ExistingTickerRecPtr, TickerRecPtr, NewTickerRecPtr: ^TickerRec;
  ExistingBreakingNewsRecPtr, BreakingNewsRecPtr, NewBreakingNewsRecPtr: ^BreakingNewsRec;
  EventGUID: TGUID;
  UpdateOnlyMode: Boolean;
  Control: Word;
  CurrentEventGUID: TGUID;
  Save_Cursor: TCursor;
  ArrayIndex: SmallInt;
  CollectionCount: SmallInt;
begin
  //Change cursor
  Save_Cursor := Screen.Cursor;
  //Show hourglass cursor
  Screen.Cursor := crHourGlass;
  //Init
  UpdateOnlyMode := FALSE;

  //Prompt for data update only mode
  Control := MessageDlg('Do you wish to OVERWRITE the data for matching records (' +
      'Selecting NO will paste new records)?', mtWarning, [mbYes, mbNo], 0);
  if (Control = mrYes) then UpDateOnlyMode := TRUE;

  Case PlaylistSelectTabControl.TabIndex of
      //Ticker
   0: begin
        //Set the paste/insertion point
        Case GridID of
          FIELD_1: InsertPoint := PlaylistGrid.CurrentDataRow-1;
          FIELD_2: InsertPoint := PlaylistGrid2.CurrentDataRow-1;
        end;
        if (InsertPoint < 0) then InsertPoint := 0;
        //Iterate through temporary collection starting at last element and working to
        //top to keep in correct order
        if (TemporaryTickerDataCount > 0) then
        begin
          for i := 0 to TemporaryTickerDataCount-1 do
          begin
            if (AppendToEnd) then
              ArrayIndex := i+1
            else
              ArrayIndex := TemporaryTickerDataCount-i;
            CurrentEventGUID := TemporaryTickerDataArray[ArrayIndex].Event_GUID;
            //Edit all matching objects using comparsion of GUIDs
            Case GridID of
              FIELD_1: CollectionCount := Ticker_Collection.Count;
              FIELD_2: CollectionCount := Ticker_Collection2.Count;
            end;
            for j := 0 to CollectionCount-1 do
            begin
              Case GridID of
                FIELD_1: ExistingTickerRecPtr := Ticker_Collection.At(j);
                FIELD_2: ExistingTickerRecPtr := Ticker_Collection2.At(j);
              end;
              if (IsEqualGUID(ExistingTickerRecPtr^.Event_GUID, CurrentEventGUID)) then
              begin
                With ExistingTickerRecPtr^ do
                begin
                  //Set values for collection record
                  for m := 1 to 50 do ExistingTickerRecPtr^.UserData[m] := TemporaryTickerDataArray[ArrayIndex].UserData[m];
                  ExistingTickerRecPtr^.StartEnableDateTime := TemporaryTickerDataArray[ArrayIndex].StartEnableDateTime;
                  ExistingTickerRecPtr^.EndEnableDateTime := TemporaryTickerDataArray[ArrayIndex].EndEnableDateTime;
                  ExistingTickerRecPtr^.Enabled := TemporaryTickerDataArray[ArrayIndex].Enabled;
                  ExistingTickerRecPtr^.Description := TemporaryTickerDataArray[ArrayIndex].Description;
                  ExistingTickerRecPtr^.Comments := TemporaryTickerDataArray[ArrayIndex].Comments;
                  ExistingTickerRecPtr^.DwellTime := TemporaryTickerDataArray[ArrayIndex].DwellTime;
                end;
              end;
            end;
            //Append the new record if not data update only mode
            if (UpdateOnlyMode = FALSE) then
            begin
              GetMem (NewTickerRecPtr, SizeOf(TickerRec));
              With NewTickerRecPtr^ do
              begin
                Event_Index := TemporaryTickerDataArray[ArrayIndex].Event_Index;
                Field_ID := TemporaryTickerDataArray[ArrayIndex].Field_ID;
                Event_GUID := TemporaryTickerDataArray[ArrayIndex].Event_GUID;
                Is_Child := TemporaryTickerDataArray[ArrayIndex].Is_Child;
                Enabled := TemporaryTickerDataArray[ArrayIndex].Enabled;
                League := TemporaryTickerDataArray[ArrayIndex].League;
                Subleague_Mnemonic_Standard := TemporaryTickerDataArray[ArrayIndex].Subleague_Mnemonic_Standard;
                Mnemonic_LiveEvent := TemporaryTickerDataArray[ArrayIndex].Mnemonic_LiveEvent;
                Template_ID := TemporaryTickerDataArray[ArrayIndex].Template_ID;
                Record_Type := TemporaryTickerDataArray[ArrayIndex].Record_Type;
                GameID := TemporaryTickerDataArray[ArrayIndex].GameID;
                SponsorLogo_Name := TemporaryTickerDataArray[ArrayIndex].SponsorLogo_Name;
                SponsorLogo_Dwell := TemporaryTickerDataArray[ArrayIndex].SponsorLogo_Dwell;
                StatStoredProcedure := TemporaryTickerDataArray[ArrayIndex].StatStoredProcedure;
                StatType := TemporaryTickerDataArray[ArrayIndex].StatType;
                StatTeam := TemporaryTickerDataArray[ArrayIndex].StatTeam;
                StatLeague := TemporaryTickerDataArray[ArrayIndex].StatLeague;
                StatRecordNumber := TemporaryTickerDataArray[ArrayIndex].StatRecordNumber;
                for j := 1 to 50 do UserData[j] := TemporaryTickerDataArray[ArrayIndex].UserData[j];
                StartEnableDateTime := TemporaryTickerDataArray[ArrayIndex].StartEnableDateTime;
                EndEnableDateTime := TemporaryTickerDataArray[ArrayIndex].EndEnableDateTime;
                Description := TemporaryTickerDataArray[ArrayIndex].Description;
                Comments := TemporaryTickerDataArray[ArrayIndex].Comments;
                Selected := TemporaryTickerDataArray[ArrayIndex].Selected;
                DwellTime := TemporaryTickerDataArray[ArrayIndex].DwellTime;
              end;
              //Insert the new record into the collection
              Case GridID of
                FIELD_1: begin
                  if (AppendToEnd) then
                    Ticker_Collection.Insert(NewTickerRecPtr)
                  else
                    Ticker_Collection.AtInsert(InsertPoint, NewTickerRecPtr);
                end;
                FIELD_2: begin
                  if (AppendToEnd) then
                    Ticker_Collection2.Insert(NewTickerRecPtr)
                  else
                    Ticker_Collection2.AtInsert(InsertPoint, NewTickerRecPtr);
                end;
              end;
            end;
          end;
        end;
        Case GridID of
          FIELD_1: RefreshPlaylistGrid(FIELD_1, PlaylistGrid);
          FIELD_2: RefreshPlaylistGrid(FIELD_2, PlaylistGrid2);
        end;
      end;
      //Alerts
   1: begin
        //Set the paste/insertion point
        InsertPoint := PlaylistGrid.CurrentDataRow-1;
        if (InsertPoint < 0) then InsertPoint := 0;
        //Iterate through temporary collection starting at last element and working to
        //top to keep in correct order
        if (TemporaryBreakingNewsDataCount > 0) then
        begin
          for i := 0 to TemporaryBreakingNewsDataCount-1 do
          begin
            if (AppendToEnd) then
              ArrayIndex := i+1
            else
              ArrayIndex := TemporaryBreakingNewsDataCount-i;
            //Append the new record
            GetMem (NewBreakingNewsRecPtr, SizeOf(BreakingNewsRec));
            With NewBreakingNewsRecPtr^ do
            begin
              Event_Index := TemporaryBreakingNewsDataArray[ArrayIndex].Event_Index;
              Event_GUID := TemporaryBreakingNewsDataArray[ArrayIndex].Event_GUID;
              Enabled := TemporaryBreakingNewsDataArray[ArrayIndex].Enabled;
              Template_ID := TemporaryBreakingNewsDataArray[ArrayIndex].Template_ID;
              Record_Type := TemporaryBreakingNewsDataArray[ArrayIndex].Record_Type;
              for j := 1 to 2 do UserData[j] := TemporaryBreakingNewsDataArray[ArrayIndex].UserData[j];
              StartEnableDateTime := TemporaryBreakingNewsDataArray[ArrayIndex].StartEnableDateTime;
              EndEnableDateTime := TemporaryBreakingNewsDataArray[ArrayIndex].EndEnableDateTime;
              Description := TemporaryBreakingNewsDataArray[ArrayIndex].Description;
              Comments := TemporaryBreakingNewsDataArray[ArrayIndex].Comments;
              Selected := TemporaryBreakingNewsDataArray[ArrayIndex].Selected;
              DwellTime := TemporaryBreakingNewsDataArray[ArrayIndex].DwellTime;
            end;
            //Insert the new record into the collection
            if (AppendToEnd) then
              BreakingNews_Collection.Insert(NewBreakingNewsRecPtr)
            else
              BreakingNews_Collection.AtInsert(InsertPoint, NewBreakingNewsRecPtr)
          end;
        end;
        RefreshPlaylistGrid(FIELD_1, PlaylistGrid);
      end;
  end;
  //Restore cursor
  Screen.Cursor := Save_Cursor;

  //Save the playlist out and signal the Playout Controller to re-load if in automated playout mode
  if (InRemotePlayoutMode) then
  begin
    //Save the playlist
    SavePlaylistWithoutClear;
    //Signal playout controller to refresh the playlist
    PlayoutInterface.RefreshCurrentPlaylist(PlaylistInfo[TICKER].PlaylistID);
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// General procedure for moving a record up or down in the playlist
////////////////////////////////////////////////////////////////////////////////
procedure TMainForm.MoveRecord(Direction: SmallInt; GridID: SmallInt; var DataGrid: TtsGrid);
var
  i,j: SmallInt;
  CutCopyRecordCount: SmallInt;
  SelectedRecordFound: Boolean;
  TickerRecPtr, TempTickerRecPtr: ^TickerRec;
  BreakingNewsRecPtr, TempBreakingNewsRecPtr: ^BreakingNewsRec;
  Save_Cursor: TCursor;
  FoundRecord: Boolean;
  SaveGridPosition: SmallInt;
  SaveDataRow: SmallInt;
  SaveTopRow: SmallInt;
  CollectionCount: SmallInt;
begin
  //Change cursor
  Save_Cursor := Screen.Cursor;
  //Show hourglass cursor
  Screen.Cursor := crHourGlass;
  SelectedRecordFound := FALSE;
  CutCopyRecordCount := 0;
  Case GridID of
    FIELD_1: begin
      SaveDataRow := PlaylistGrid.CurrentDataRow;
      SaveTopRow := PlaylistGrid.TopRow;
    end;
    FIELD_2: begin
      SaveDataRow := PlaylistGrid2.CurrentDataRow;
      SaveTopRow := PlaylistGrid2.TopRow;
    end;
  end;
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker
   0: begin
        Case GridID of
          FIELD_1: CollectionCount := Ticker_Collection.Count;
          FIELD_2: CollectionCount := Ticker_Collection2.Count;
        end;
        if (CollectionCount > 0) then
        begin
          //Clear the temp collection
          for i := 0 to CollectionCount-1 do
          begin
            Case GridID of
              FIELD_1: TickerRecPtr := Ticker_Collection.At(i);
              FIELD_2: TickerRecPtr := Ticker_Collection2.At(i);
            end;
            if (DataGrid.CurrentDataRow-1 = i) AND (SelectedRecordFound = FALSE) then
            begin
              //Set selected flag to true
              SelectedRecordFound := TRUE;
              //Allocate memory for the record
              GetMem (TempTickerRecPtr, SizeOf(TickerRec));
              //Copy the data to the temporary array
              With TempTickerRecPtr^ do
              begin
                Event_Index := TickerRecPtr^.Event_Index;
                Field_ID := TickerRecPtr^.Field_ID;
                Event_GUID := TickerRecPtr^.Event_GUID;
                Is_Child := TickerRecPtr^.Is_Child;
                Enabled := TickerRecPtr^.Enabled;
                League := TickerRecPtr^.League;
                Subleague_Mnemonic_Standard := TickerRecPtr^.Subleague_Mnemonic_Standard;
                Mnemonic_LiveEvent := TickerRecPtr^.Mnemonic_LiveEvent;
                Template_ID := TickerRecPtr^.Template_ID;
                Record_Type := TickerRecPtr^.Record_Type;
                GameID := TickerRecPtr^.GameID;
                SponsorLogo_Name := TickerRecPtr^.SponsorLogo_Name;
                SponsorLogo_Dwell := TickerRecPtr^.SponsorLogo_Dwell;
                StatStoredProcedure := TickerRecPtr^.StatStoredProcedure;
                StatType := TickerRecPtr^.StatType;
                StatTeam := TickerRecPtr^.StatTeam;
                StatLeague := TickerRecPtr^.StatLeague;
                StatRecordNumber := TickerRecPtr^.StatRecordNumber;
                for j := 1 to 50 do UserData[j] := TickerRecPtr^.UserData[j];
                StartEnableDateTime := TickerRecPtr^.StartEnableDateTime;
                EndEnableDateTime := TickerRecPtr^.EndEnableDateTime;
                Description := TickerRecPtr^.Description;
                Comments := TickerRecPtr^.Comments;
                Selected := TickerRecPtr^.Selected;
                DwellTime := TickerRecPtr^.DwellTime;
              end;
              //Delete the current record
              Case GridID of
                FIELD_1: begin
                  Ticker_Collection.AtDelete(i);
                  Ticker_Collection.Pack;
                end;
                FIELD_2: begin
                  Ticker_Collection2.AtDelete(i);
                  Ticker_Collection2.Pack;
                end;
              end;
              //Insert the record at the next position up in the grid
              if (i > 0) AND (Direction = UP) then
              begin
                Case GridID of
                  FIELD_1: Ticker_Collection.AtInsert(i-1, TempTickerRecPtr);
                  FIELD_2: Ticker_Collection2.AtInsert(i-1, TempTickerRecPtr);
                end;
                Dec(SaveDataRow);
                if (SaveTopRow > 1) then Dec(SaveTopRow);
              end
              else if (i = 0) AND (Direction = UP) then
              begin
                Case GridID of
                  FIELD_1: Ticker_Collection.AtInsert(0, TempTickerRecPtr);
                  FIELD_2: Ticker_Collection2.AtInsert(0, TempTickerRecPtr);
                end;
                SaveDataRow := 1;
                SaveTopRow := 1;
              end
              else if (i < DataGrid.Rows-2) AND (Direction = DOWN) then
              begin
                Case GridID of
                  FIELD_1: Ticker_Collection.AtInsert(i+1, TempTickerRecPtr);
                  FIELD_2: Ticker_Collection2.AtInsert(i+1, TempTickerRecPtr);
                end;
                Inc(SaveDataRow);
                if (SaveTopRow < DataGrid.Rows) then Inc(SaveTopRow);
              end
              else if ((i = DataGrid.Rows-2) OR (i = DataGrid.Rows-1)) AND (Direction = DOWN) then
              begin
                Case GridID of
                  FIELD_1: Ticker_Collection.Insert(TempTickerRecPtr);
                  FIELD_2: Ticker_Collection2.Insert(TempTickerRecPtr);
                end;
                SaveDataRow := DataGrid.Rows;
              end;
            end;
          end;
        end;
      end;
      //Alerts
   1: begin
        if (BreakingNews_Collection.Count > 0) then
        begin
          //Clear the temp collection
          for i := 0 to BreakingNews_Collection.Count-1 do
          begin
            BreakingNewsRecPtr := BreakingNews_Collection.At(i);
            if (PlaylistGrid.CurrentDataRow-1 = i) AND (SelectedRecordFound = FALSE) then
            begin
              //Set selected flag to true
              SelectedRecordFound := TRUE;
              //Allocate memory for the record
              GetMem (TempBreakingNewsRecPtr, SizeOf(BreakingNewsRec));
              //Copy the data to the temporary array
              With TempBreakingNewsRecPtr^ do
              begin
                Event_Index := BreakingNewsRecPtr^.Event_Index;
                Event_GUID := BreakingNewsRecPtr^.Event_GUID;
                Enabled := BreakingNewsRecPtr^.Enabled;
                Template_ID := BreakingNewsRecPtr^.Template_ID;
                Record_Type := BreakingNewsRecPtr^.Record_Type;
                for j := 1 to 2 do UserData[j] := BreakingNewsRecPtr^.UserData[j];
                StartEnableDateTime := BreakingNewsRecPtr^.StartEnableDateTime;
                EndEnableDateTime := BreakingNewsRecPtr^.EndEnableDateTime;
                Description := BreakingNewsRecPtr^.Description;
                Comments := BreakingNewsRecPtr^.Comments;
                Selected := BreakingNewsRecPtr^.Selected;
                DwellTime := BreakingNewsRecPtr^.DwellTime;
              end;
              //Delete the current record
              BreakingNews_Collection.AtDelete(i);
              BreakingNews_Collection.Pack;
              //Insert the record at the next position up in the grid
              if (i > 0) AND (Direction = UP) then
              begin
                BreakingNews_Collection.AtInsert(i-1, TempBreakingNewsRecPtr);
                Dec(SaveDataRow);
              end
              else if (i = 0) AND (Direction = UP) then
              begin
                BreakingNews_Collection.AtInsert(0, TempBreakingNewsRecPtr);
                SaveDataRow := 1;
              end
              else if (i < PlaylistGrid.Rows-2) AND (Direction = DOWN) then
              begin
                BreakingNews_Collection.AtInsert(i+1, TempBreakingNewsRecPtr);
                Inc(SaveDataRow);
              end
              else if ((i = PlaylistGrid.Rows-2) OR (i = PlaylistGrid.Rows-1)) AND (Direction = DOWN) then
              begin
                BreakingNews_Collection.Insert(TempBreakingNewsRecPtr);
                SaveDataRow := PlaylistGrid.Rows;
              end;
            end;
          end;
        end;
      end;
  end;
  //Refresh the grid
  RefreshPlaylistGrid(GridID, DataGrid);

  //Restore cursor & selected record
  Screen.Cursor := Save_Cursor;
  DataGrid.CurrentDataRow := SaveDataRow;
  DataGrid.SelectRows(1, DataGrid.Rows, FALSE);
  DataGrid.SelectRows(DataGrid.CurrentDataRow, DataGrid.CurrentDataRow, TRUE);
  DataGrid.TopRow := SaveTopRow;

  //Save the playlist out and signal the Playout Controller to re-load if in automated playout mode
  if (InRemotePlayoutMode) then
  begin
    //Save the playlist
    SavePlaylistWithoutClear;
    //Signal playout controller to refresh the playlist
    PlayoutInterface.RefreshCurrentPlaylist(PlaylistInfo[TICKER].PlaylistID);
  end;
end;

//Handler for delete element from zipper playlist button
procedure TMainForm.BitBtn5Click(Sender: TObject);
begin
  DeleteGraphicFromZipperPlaylist(TRUE, FIELD_1, PlaylistGrid);
end;
//Handler for delete from pop-up menu
//Field 1
procedure TMainForm.Delete1Click(Sender: TObject);
begin
  DeleteGraphicFromZipperPlaylist(TRUE, FIELD_1, PlaylistGrid);
end;
//Field 2
procedure TMainForm.Delete2Click(Sender: TObject);
begin
  DeleteGraphicFromZipperPlaylist(TRUE, FIELD_2, PlaylistGrid2);
end;
//General procedure to delete an element from the zipper playlist
procedure TMainForm.DeleteGraphicFromZipperPlaylist(Confirm: Boolean; GridID: SmallInt; var DataGrid: TtsGrid);
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  BreakingNewsRecPtr: ^BreakingNewsRec;
  CollectionCount: SmallInt;
  Control: Word;
begin
  //Alert operator
  if (Confirm) then
    Control := MessageDlg('Are you sure you want to delete the selected records?', mtConfirmation, [mbYes, mbNo], 0)
  else
    Control := mrYes;
  if (Control = mrYes) then
  begin
    Case PlaylistSelectTabControl.TabIndex of
         //Ticker
      0: Case GridID of
           FIELD_1: CollectionCount := Ticker_Collection.Count;
           FIELD_2: CollectionCount := Ticker_Collection2.Count;
         end;
         //Alerts
      1: CollectionCount := BreakingNews_Collection.Count;
    end;
    If ((CollectionCount > 0) AND (DataGrid.Rows >= 0)) then
    begin
      //Point to current record & get game id
      //Delete the item from the stack
      Case PlaylistSelectTabControl.TabIndex of
          //Ticker
       0: begin
            for i := 0 to CollectionCount-1 do
            begin
              Case GridID of
                FIELD_1: TickerRecPtr := Ticker_Collection.At(i);
                FIELD_2: TickerRecPtr := Ticker_Collection2.At(i);
              end;
              if (DataGrid.RowSelected[i+1] = TRUE) then TickerRecPtr^.Selected := TRUE
              else TickerRecPtr^.Selected := FALSE;
            end;
            i := 0;
            Repeat
              Case GridID of
                FIELD_1: begin
                  TickerRecPtr := Ticker_Collection.At(i);
                  if (TickerRecPtr^.Selected = TRUE) then
                  begin
                    Ticker_Collection.AtDelete(i);
                    Ticker_Collection.Pack;
                  end
                  else Inc(i);
                  CollectionCount := Ticker_Collection.Count;
                end;
                FIELD_2: begin
                  TickerRecPtr := Ticker_Collection2.At(i);
                  if (TickerRecPtr^.Selected = TRUE) then
                  begin
                    Ticker_Collection2.AtDelete(i);
                    Ticker_Collection2.Pack;
                  end
                  else Inc(i);
                  CollectionCount := Ticker_Collection2.Count;
                end;
              end;
            Until (i = CollectionCount);
          end;
          //Alerts
       1: begin
            for i := 0 to BreakingNews_Collection.Count-1 do
            begin
              BreakingNewsRecPtr := BreakingNews_Collection.At(i);
              if (PlaylistGrid.RowSelected[i+1] = TRUE) then BreakingNewsRecPtr^.Selected := TRUE
              else BreakingNewsRecPtr^.Selected := FALSE;
            end;
            i := 0;
            Repeat
              BreakingNewsRecPtr := BreakingNews_Collection.At(i);
              if (BreakingNewsRecPtr^.Selected = TRUE) then
              begin
                BreakingNews_Collection.AtDelete(i);
                BreakingNews_Collection.Pack;
              end
              else Inc(i);
            Until (i = BreakingNews_Collection.Count);
          end;
      end;
      //Refresh the grid
      Case GridID of
        FIELD_1: RefreshPlaylistGrid(FIELD_1, PlaylistGrid);
        FIELD_2: RefreshPlaylistGrid(FIELD_2, PlaylistGrid2);
      end;
    end;
  end;

  //Save the playlist out and signal the Playout Controller to re-load if in automated playout mode
  if (InRemotePlayoutMode) then
  begin
    //Save the playlist
    SavePlaylistWithoutClear;
    //Signal playout controller to refresh the playlist
    PlayoutInterface.RefreshCurrentPlaylist(PlaylistInfo[TICKER].PlaylistID);
  end;
end;

//Handler for edit current zipper entry button
procedure TMainForm.BitBtn3Click(Sender: TObject);
begin
  EditZipperPlaylistEntry(FIELD_1, PlaylistGrid);
  //Preview the edited record
  PreviewCurrentPlaylistRecord(FIELD_1);
end;
//Handler for double-click on zipper entry in grid - edit entry
//Field 1
procedure TMainForm.PlaylistGridDblClick(Sender: TObject);
begin
  EditZipperPlaylistEntry(FIELD_1, PlaylistGrid);
  //Preview the edited record
  PreviewCurrentPlaylistRecord(FIELD_1);
end;
//Field 2
procedure TMainForm.PlaylistGrid2DblClick(Sender: TObject);
begin
  EditZipperPlaylistEntry(FIELD_2, PlaylistGrid2);
  //Preview the edited record
  PreviewCurrentPlaylistRecord(FIELD_2);
end;

//Procedure to launch dialog for editing currently selected zipper entry
procedure TMainForm.EditZipperPlaylistEntry(GridID: SmallInt; var DataGrid: TtsGrid);
var
  i,j,k: SmallInt;
  CurrentRow: SmallInt;
  Modal: TZipperEntryEditorDlg;
  Control: Word;
  TickerRecPtr: ^TickerRec;
  BreakingNewsRecPtr: ^BreakingNewsRec;
  TemplateFieldsRecPtr: ^TemplateFieldsRec;
  User_Data: Array[1..50] of String[255];
  CurrentTemplateID: SmallInt;
  NumTemplateFields: SmallInt;
  CollectionCount: SmallInt;
  CurrentEventGUID: TGUID;
  CurrentGameData: GameRec;
  StyleChipRecPtr: ^StyleChipRec;
  CurrentTemplateDefs: TemplateDefsRec;
  SaveTopRow: SmallInt;
begin
  Case PlaylistSelectTabControl.TabIndex of
       //Ticker
    0: Case GridID of
         FIELD_1: CollectionCount := Ticker_Collection.Count;
         FIELD_2: CollectionCount := Ticker_Collection2.Count;
       end;
       //Alerts
    1: CollectionCount := BreakingNews_Collection.Count;
  end;

  //Init fields
  for i := 1 to 50 do User_Data[i] := '';

  If ((CollectionCount > 0) AND (PlaylistGrid.Rows >= 0)) then
  begin
    //Get current row
    Case GridID of
      FIELD_1: begin
        CurrentRow := PlaylistGrid.CurrentDataRow;
        SaveTopRow := PlaylistGrid.TopRow;
      end;
      FIELD_2: begin
        CurrentRow := PlaylistGrid2.CurrentDataRow;
        SaveTopRow := PlaylistGrid2.TopRow;
      end;
    end;
    //Only proceed if a row was selected
    if (CurrentRow <> -1) then
    begin
      Case PlaylistSelectTabControl.TabIndex of
          //Ticker
       0: begin
            Case GridID of
              FIELD_1: TickerRecPtr := Ticker_Collection.At(CurrentRow-1);
              FIELD_2: TickerRecPtr := Ticker_Collection2.At(CurrentRow-1);
            end;
            CurrentEventGUID := TickerRecPtr^.Event_GUID;
            //Setup and launch editing dialog if applicable
            try
              Modal := TZipperEntryEditorDlg.Create(Application);
              //Add style chips
              if (StyleChip_Collection.Count > 0) then
              begin
                Modal.StyleChipsGrid.StoreData := TRUE;
                Modal.StyleChipsGrid.Cols := 2;
                Modal.StyleChipsGrid.Rows := StyleChip_Collection.Count;
                for i := 0 to StyleChip_Collection.Count-1 do
                begin
                  StyleChipRecPtr := StyleChip_Collection.At(i);
                  Modal.StyleChipsGrid.Cell[1,i+1] := StyleChipRecPtr^.StyleChip_Code;
                  Modal.StyleChipsGrid.Cell[2,i+1] := StyleChipRecPtr^.StyleChip_Description;
                end;
              end;
              Modal.EntryEnable.Checked := TickerRecPtr^.Enabled;
              Modal.EntryNote.Text := TickerRecPtr^.Comments;
              Modal.EntryStartEnableDate.Date := TickerRecPtr^.StartEnableDateTime;
              Modal.EntryStartEnableTime.Time := TickerRecPtr^.StartEnableDateTime;
              Modal.EntryEndEnableDate.Date := TickerRecPtr^.EndEnableDateTime;
              Modal.EntryEndEnableTime.Time := TickerRecPtr^.EndEnableDateTime;
              //If a sponsor dwell was specified, it's a sponsor logo
              if (TickerRecPtr^.SponsorLogo_Dwell > 0) then
                Modal.EntryDwellTime.Value := Trunc(TickerRecPtr^.SponsorLogo_Dwell)
              else
                Modal.EntryDwellTime.Value := Trunc(TickerRecPtr^.DwellTime/1000);
              Modal.BitBtn4.Caption := 'Replace Existing Entry';
              //Set data mode label
              //Check for 2-line mode
              if (TickerDisplayMode = 3) OR (TickerDisplayMode = 4) then
                Modal.DataModeLabel.Caption := '2-LINE MODE'
              //1-line mode
              else if (TickerDisplayMode = 1) OR (TickerDisplayMode = 2) then
                Modal.DataModeLabel.Caption := '1-LINE MODE';
              //Set initial values for grid
              CurrentTemplateID := TickerRecPtr^.Template_ID;
              NumTemplateFields := GetTemplateFieldsCount(CurrentTemplateID);
              CurrentTemplateDefs := GetTemplateInformation(CurrentTemplateID);
              Modal.TemplateName.Caption := CurrentTemplateDefs.Template_Description;
              if (NumTemplateFields > 0) then
              begin
                //Clear grid values
                if (Modal.RecordGrid.Rows > 0) then
                  Modal.RecordGrid.DeleteRows (1, Modal.RecordGrid.Rows);
                Modal.RecordGrid.StoreData := TRUE;
                Modal.RecordGrid.Cols := 5;
                Modal.RecordGrid.Rows := NumTemplateFields;
                Modal.HasWeather := FALSE;
                Modal.HasManualLeague := FALSE;
                //Populate the grid
                i := 1; k := 1;
                //Check if manual league; if not, get the game data
                if (CurrentTemplateDefs.ManualLeague = FALSE) then
                begin
                  CurrentGameData := GameDataFunctions.GetGameData(TRIM(TickerRecPtr^.League), TRIM(TickerRecPtr^.GameID));
                end;
                for j := 0 to Template_Fields_Collection.Count-1 do
                begin
                  TemplateFieldsRecPtr := Template_Fields_Collection.At(j);
                  if (TemplateFieldsRecPtr^.Template_ID = CurrentTemplateID) then
                  begin
                    Modal.RecordGrid.Cell[1,i] := TemplateFieldsRecPtr^.Field_ID; //Index
                    Modal.RecordGrid.Cell[2,i] := TemplateFieldsRecPtr^.Field_Label; //Description
                    Modal.RecordGrid.Cell[4,i] := TemplateFieldsRecPtr^.Field_Type;
                    //Add max character count
                    Modal.RecordGrid.Cell[5,i] := TemplateFieldsRecPtr^.Field_Max_Length;
                    //Set color & read only property
                    Modal.RecordGrid.CellReadOnly[1,i] := roOn;
                    Modal.RecordGrid.CellReadOnly[2,i] := roOn;
                    Modal.RecordGrid.CellReadOnly[4,i] := roOn;
                    if (TemplateFieldsRecPtr^.Field_Is_Manual = TRUE) then
                    begin
                      //Firt check to see if it's a manual game winner indicator or animation
                      if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_MANUAL_GAME_WINNER) then
                      begin
                        Modal.RecordGrid.CellControlType[3,i] := ctCheck;
                        if (TickerRecPtr^.UserData[24] = 'TRUE') then
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbChecked
                        else
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbUnchecked;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_GAME_WINNER_ANIMATION) then
                      begin
                        Modal.RecordGrid.CellControlType[3,i] := ctCheck;
                        if (TickerRecPtr^.UserData[25] = 'TRUE') then
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbChecked
                        else
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbUnchecked;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      //Next, check to see if it's a manual next page icon animation
                      else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_NEXT_PAGE_ANIMATION) then
                      begin
                        Modal.RecordGrid.CellControlType[3,i] := ctCheck;
                        if (TickerRecPtr^.UserData[23] = 'TRUE') then
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbChecked
                        else
                          Modal.RecordGrid.CellCheckBoxState[3,i] := cbUnchecked;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      //Next, check to see if it's a weather icon; if so, display flipbooks in dropdown
                      else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_WEATHER_ICON) then
                      begin
                        Modal.RecordGrid.AssignCellCombo(3,i);
                        Modal.RecordGrid.CellButtonType[3,i] := btCombo;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        //Init first combo box entry
                        Modal.RecordGrid.Cell[3,i] := TickerRecPtr^.UserData[k];
                        //Set flag
                        Modal.HasWeather := TRUE;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      //Next, check to see if it's the manual league entry; if so, display choices in dropdown
                      else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_LEAGUE_HEADER) then
                      begin
                        Modal.RecordGrid.AssignCellCombo(3,i);
                        Modal.RecordGrid.CellButtonType[3,i] := btCombo;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        //Init combo box entry
                        //Modal.RecordGrid.Cell[3,i] := TickerRecPtr^.League + '/' +
                        //  TickerRecPtr^.Subleague_Mnemonic_Standard;
                        Modal.RecordGrid.Cell[3,i] := TickerRecPtr^.League;
                        //Set flag
                        Modal.HasManualLeague := TRUE;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      //Next, check to see if it's a league logo image file; if so, show image selector dialog
                      else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_LEAGUE_LOGO_1A) or
                        (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_LEAGUE_LOGO_1B)or
                        (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_1A) or
                        (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_1B) or
                        (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_PLAYER_HEADSHOT_1A) or
                        (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_PLAYER_HEADSHOT_1B) or
                        //Added for Version 2.0
                        //Next, check for full page promo graphic
                        (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_FULL_PROMO_GRAPHIC) then
                      begin
                        Modal.RecordGrid.CellButtonType[3,i] := btNormal;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        Modal.RecordGrid.Cell[3,i] := Trim(TickerRecPtr^.UserData[21]);
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      //Next, check to see if it's a team logo image file; if so, show image selector dialog
                      else if (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_LEAGUE_LOGO_2A) or
                        (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_LEAGUE_LOGO_2B) or
                        (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_2A) or
                        (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_TEAM_LOGO_2B) or
                        (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_PLAYER_HEADSHOT_2A) or
                        (TemplateFieldsRecPtr^.Field_Type = FIELD_TYPE_PLAYER_HEADSHOT_2B) then
                      begin
                        Modal.RecordGrid.CellButtonType[3,i] := btNormal;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        Modal.RecordGrid.Cell[3,i] := Trim(TickerRecPtr^.UserData[22]);
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      else begin
                        Modal.RecordGrid.RowColor[i] := clWindow;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        if (AutoTrimText) then
                          Modal.RecordGrid.Cell[3,i] := Trim(TickerRecPtr^.UserData[k])
                        else
                          Modal.RecordGrid.Cell[3,i] := TickerRecPtr^.UserData[k];
                      end;
                      Inc(k);
                    end
                    else begin
                      //Set cell value
                      Modal.RecordGrid.Cell[3,i] := GetValueOfSymbol(GridID, PlaylistSelectTabControl.TabIndex+1,
                        TemplateFieldsRecPtr^.Field_Contents, CurrentRow-1, CurrentGameData, TickerDisplayMode,
                          TickerRecPtr^.UserData[21]).SymbolValue;
                      Modal.RecordGrid.RowColor[i] := clAqua;
                      Modal.RecordGrid.CellReadOnly[3,i] := roOn;
                    end;
                    //Increment the template fields match counter
                    Inc (i);
                  end;
                end;
              end;
              //Show the dialog
              Control := Modal.ShowModal;
            finally
              //Set new values if user didn't cancel out
              if (Control = mrOK) then
              begin
                //Set values based on data in grid
                if (Modal.RecordGrid.Rows > 0) then
                begin
                  i := 1;
                  for j := 1 to Modal.RecordGrid.Rows do
                  begin
                    if (Modal.RecordGrid.CellReadOnly[3,j] = roOff) then
                    begin
                      //Check for visitor manual winner indication record type
                      if (Modal.RecordGrid.Cell[4,j] = 5) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbChecked) then
                      begin
                        User_Data[24] := 'TRUE';
                        User_Data[49] := 'TRUE';
                        Dec(i);
                      end
                      else if (Modal.RecordGrid.Cell[4,j] = 5) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbUnChecked) then
                      begin
                        User_Data[24] := 'FALSE';
                        User_Data[49] := 'FALSE';
                        Dec(i);
                      end
                      else if (Modal.RecordGrid.Cell[4,j] = 6) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbChecked) then
                      begin
                        User_Data[25] := 'TRUE';
                        User_Data[50] := 'TRUE';
                        Dec(i);
                      end
                      else if (Modal.RecordGrid.Cell[4,j] = 6) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbUnChecked) then
                      begin
                        User_Data[25] := 'FALSE';
                        User_Data[50] := 'FALSE';
                        Dec(i);
                      end
                      //Check for next page icon
                      else if (Modal.RecordGrid.Cell[4,j] = 9) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbChecked) then
                      begin
                        User_Data[23] := 'TRUE';
                        User_Data[48] := 'TRUE';
                        Dec(i);
                      end
                      else if (Modal.RecordGrid.Cell[4,j] = 9) AND (Modal.RecordGrid.CellCheckboxState[3,j] = cbUnChecked) then
                      begin
                        User_Data[23] := 'FALSE';
                        User_Data[48] := 'FALSE';
                        Dec(i);
                      end
                      //Check for manual league
                      else if (Modal.RecordGrid.Cell[4,j] = 8) then
                      begin
                        //If User changed the entry via dropdown, get the new values
                        if (Modal.League <> '') then
                        begin
                          TickerRecPtr^.League := Modal.League;
                        end;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(i);
                      end
                      //Check for image file types
                      else if (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_LEAGUE_LOGO_1A) or
                              (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_LEAGUE_LOGO_1B) or
                              (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_TEAM_LOGO_1A) or
                              (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_TEAM_LOGO_1B) or
                              (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_PLAYER_HEADSHOT_1A) or
                              (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_PLAYER_HEADSHOT_1B) or
                              //Added for Version 2.0
                              //Next, check for full page promo graphic
                              (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_FULL_PROMO_GRAPHIC) then
                      begin
                        User_Data[21] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                        User_Data[46] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                        Dec(i);
                      end
                      else if (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_LEAGUE_LOGO_2A) or
                              (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_LEAGUE_LOGO_2B) or
                              (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_TEAM_LOGO_2A) or
                              (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_TEAM_LOGO_2B) or
                              (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_PLAYER_HEADSHOT_2A) or
                              (Modal.RecordGrid.Cell[4,j] = FIELD_TYPE_PLAYER_HEADSHOT_2B) then
                      begin
                        User_Data[22] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                        User_Data[47] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                        Dec(i);
                      end
                      else begin
                        //Standard mode, so change all text values
                        //Set user value text and increment
                        User_Data[i] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                        //User_Data[25+i] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                      end;
                      Inc(i);
                    end;
                  end;
                end;
                //Set other values
                TickerRecPtr^.StartEnableDateTime := Trunc(Modal.EntryStartEnableDate.Date) +
                    (Modal.EntryStartEnableTime.Time - Trunc(Modal.EntryStartEnableTime.Time));
                TickerRecPtr^.EndEnableDateTime := Trunc(Modal.EntryEndEnableDate.Date) +
                    (Modal.EntryEndEnableTime.Time - Trunc(Modal.EntryEndEnableTime.Time));
                TickerRecPtr^.Enabled := Modal.EntryEnable.Checked;
                //If a sponsor logo dwell was specified, it's a sponsor logo
                if (TickerRecPtr^.SponsorLogo_Dwell > 0) then
                  TickerRecPtr^.SponsorLogo_Dwell := Modal.EntryDwellTime.Value
                else
                  TickerRecPtr^.DwellTime := Modal.EntryDwellTime.Value*1000;
                TickerRecPtr^.Comments := Modal.EntryNote.Text;

                //Edit all matching objects using comparsion of GUIDs
                Case GridID of
                  FIELD_1: CollectionCount := Ticker_Collection.Count;
                  FIELD_2: CollectionCount := Ticker_Collection2.Count;
                end;
                for j := 0 to CollectionCount-1 do
                begin
                  Case GridID of
                    FIELD_1: TickerRecPtr := Ticker_Collection.At(j);
                    FIELD_2: TickerRecPtr := Ticker_Collection2.At(j);
                  end;
                  if (IsEqualGUID(TickerRecPtr^.Event_GUID, CurrentEventGUID)) then
                  begin
                    With TickerRecPtr^ do
                    begin
                      //Set values for collection record
                      for i := 1 to 50 do UserData[i] := User_Data[i];
                      TickerRecPtr^.StartEnableDateTime := Trunc(Modal.EntryStartEnableDate.Date) +
                          (Modal.EntryStartEnableTime.Time - Trunc(Modal.EntryStartEnableTime.Time));
                      TickerRecPtr^.EndEnableDateTime := Trunc(Modal.EntryEndEnableDate.Date) +
                          (Modal.EntryEndEnableTime.Time - Trunc(Modal.EntryEndEnableTime.Time));
                      TickerRecPtr^.Enabled := Modal.EntryEnable.Checked;
                      TickerRecPtr^.DwellTime := Modal.EntryDwellTime.Value*1000;
                      TickerRecPtr^.Comments := Modal.EntryNote.Text;
                      //Re-eanble to change league for all matching GUIDs
                      if (Modal.League <> '') then
                      begin
                        TickerRecPtr^.League := Modal.League;
                      end;
                    end;
                  end;
                end;
              end;
              Modal.Free;
              //Refresh grid
              Case GridID of
                FIELD_1: begin
                  RefreshPlaylistGrid(FIELD_1, PlaylistGrid);
                  //Refresh the fields grid
                  RefreshEntryFieldsGrid(TickerDisplayMode, FIELD_1, EntryFieldsGrid);
                end;
                FIELD_2: begin
                  RefreshPlaylistGrid(FIELD_2, PlaylistGrid2);
                  //Refresh the fields grid
                  RefreshEntryFieldsGrid(TickerDisplayMode, FIELD_2, EntryFieldsGrid2);
                end;
              end;
            end;
          end;
          //Alerts
       1: begin
            BreakingNewsRecPtr := BreakingNews_Collection.At(CurrentRow-1);
            CurrentEventGUID := BreakingNewsRecPtr^.Event_GUID;
            //Setup and launch editing dialog if applicable
            try
              Modal := TZipperEntryEditorDlg.Create(Application);
              //Add style chips
              if (StyleChip_Collection.Count > 0) then
              begin
                Modal.StyleChipsGrid.StoreData := TRUE;
                Modal.StyleChipsGrid.Cols := 2;
                Modal.StyleChipsGrid.Rows := StyleChip_Collection.Count;
                for i := 0 to StyleChip_Collection.Count-1 do
                begin
                  StyleChipRecPtr := StyleChip_Collection.At(i);
                  Modal.StyleChipsGrid.Cell[1,i+1] := StyleChipRecPtr^.StyleChip_Code;
                  Modal.StyleChipsGrid.Cell[2,i+1] := StyleChipRecPtr^.StyleChip_Description;
                end;
              end;
              Modal.EntryEnable.Checked := BreakingNewsRecPtr^.Enabled;
              Modal.EntryNote.Text := BreakingNewsRecPtr^.Comments;
              Modal.EntryStartEnableDate.Date := BreakingNewsRecPtr^.StartEnableDateTime;
              Modal.EntryStartEnableTime.Time := BreakingNewsRecPtr^.StartEnableDateTime;
              Modal.EntryEndEnableDate.Date := BreakingNewsRecPtr^.EndEnableDateTime;
              Modal.EntryEndEnableTime.Time := BreakingNewsRecPtr^.EndEnableDateTime;
              Modal.EntryDwellTime.Value := Trunc(BreakingNewsRecPtr^.DwellTime/1000);
              Modal.BitBtn4.Caption := 'Replace Existing Entry';
              //Set initial values for grid
              CurrentTemplateID := BreakingNewsRecPtr^.Template_ID;
              NumTemplateFields := GetTemplateFieldsCount(CurrentTemplateID);
              CurrentTemplateDefs := GetTemplateInformation(CurrentTemplateID);
              Modal.TemplateName.Caption := CurrentTemplateDefs.Template_Description;
              if (NumTemplateFields > 0) then
              begin
                //Clear grid values
                if (Modal.RecordGrid.Rows > 0) then
                  Modal.RecordGrid.DeleteRows (1, Modal.RecordGrid.Rows);
                Modal.RecordGrid.StoreData := TRUE;
                Modal.RecordGrid.Cols := 4;
                Modal.RecordGrid.Rows := NumTemplateFields;
                //Populate the grid
                i := 1; k := 1;
                for j := 0 to Template_Fields_Collection.Count-1 do
                begin
                  TemplateFieldsRecPtr := Template_Fields_Collection.At(j);
                  if (TemplateFieldsRecPtr^.Template_ID = CurrentTemplateID) then
                  begin
                    Modal.RecordGrid.Cell[1,i] := TemplateFieldsRecPtr^.Field_ID; //Index
                    Modal.RecordGrid.Cell[2,i] := TemplateFieldsRecPtr^.Field_Label; //Description
                    Modal.RecordGrid.Cell[4,i] := TemplateFieldsRecPtr^.Field_Type;
                    //Set color & read only property
                    Modal.RecordGrid.CellReadOnly[1,i] := roOn;
                    Modal.RecordGrid.CellReadOnly[2,i] := roOn;
                    Modal.RecordGrid.CellReadOnly[4,i] := roOn;
                    if (TemplateFieldsRecPtr^.Field_Is_Manual = TRUE) then
                    begin
                      //Check to see if it's the manual league entry; if so, display choices in dropdown
                      if (TemplateFieldsRecPtr^.Field_Type = 8) then
                      begin
                        Modal.RecordGrid.AssignCellCombo(3,i);
                        Modal.RecordGrid.CellButtonType[3,i] := btCombo;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        //Init combo box entry
                        //Modal.RecordGrid.Cell[3,i] := TickerRecPtr^.League + '/' +
                        //  TickerRecPtr^.Subleague_Mnemonic_Standard;
                        Modal.RecordGrid.Cell[3,i] := BreakingNewsRecPtr^.League;
                        //Set flag
                        Modal.HasManualLeague := TRUE;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(k);
                      end
                      else begin
                        Modal.RecordGrid.RowColor[i] := clWindow;
                        Modal.RecordGrid.CellReadOnly[3,i] := roOff;
                        if (AutoTrimText) then
                          Modal.RecordGrid.Cell[3,i] := Trim(BreakingNewsRecPtr^.UserData[k])
                        else
                          Modal.RecordGrid.Cell[3,i] := BreakingNewsRecPtr^.UserData[k];
                      end;
                      Inc(k);
                    end
                    else begin
                      //Set cell value
                      Modal.RecordGrid.Cell[3,i] := GetValueOfSymbol(GridID, PlaylistSelectTabControl.TabIndex+1,
                        TemplateFieldsRecPtr^.Field_Contents, CurrentRow-1, CurrentGameData, 0,
                          TickerRecPtr^.UserData[21]).SymbolValue;
                      Modal.RecordGrid.RowColor[i] := clAqua;
                      Modal.RecordGrid.CellReadOnly[3,i] := roOn;
                    end;
                    //Increment the template fields match counter
                    Inc (i);
                  end;
                end;
              end;
              //Show the dialog
              Control := Modal.ShowModal;
            finally
              //Set new values if user didn't cancel out
              if (Control = mrOK) then
              begin
                //Set values based on data in grid
                if (Modal.RecordGrid.Rows > 0) then
                begin
                  i := 1;
                  for j := 1 to Modal.RecordGrid.Rows do
                  begin
                    if (Modal.RecordGrid.CellReadOnly[3,j] = roOff) then
                    begin
                      //Check for manual league
                      if (Modal.RecordGrid.Cell[4,j] = 8) then
                      begin
                        //If User changed the entry via dropdown, get the new values
                        if (Modal.League <> '') then
                        begin
                          BreakingNewsRecPtr^.League := Modal.League;
                        end;
                        //Decrement the user data counter - league is stored in ticker record explicitly
                        Dec(i);
                      end
                      else begin
                        //Standard mode, so change all text values
                        //Set user value text and increment
                        User_Data[i] := ScrubText(Modal.RecordGrid.Cell[3,j]);
                      end;
                      Inc(i);
                    end;
                  end;
                end;
                //Set other values
                BreakingNewsRecPtr^.StartEnableDateTime := Trunc(Modal.EntryStartEnableDate.Date) +
                    (Modal.EntryStartEnableTime.Time - Trunc(Modal.EntryStartEnableTime.Time));
                BreakingNewsRecPtr^.EndEnableDateTime := Trunc(Modal.EntryEndEnableDate.Date) +
                    (Modal.EntryEndEnableTime.Time - Trunc(Modal.EntryEndEnableTime.Time));
                BreakingNewsRecPtr^.Enabled := Modal.EntryEnable.Checked;
                BreakingNewsRecPtr^.DwellTime := Modal.EntryDwellTime.Value*1000;
                BreakingNewsRecPtr^.Comments := Modal.EntryNote.Text;
                //Edit all matching objects using comparsion of GUIDs
                for j := 0 to BreakingNews_Collection.Count-1 do
                begin
                  BreakingNewsRecPtr := BreakingNews_Collection.At(j);
                  if (IsEqualGUID(BreakingNewsRecPtr^.Event_GUID, CurrentEventGUID)) then
                  begin
                    With BreakingNewsRecPtr^ do
                    begin
                      //Set values for collection record
                      for i := 1 to 50 do UserData[i] := User_Data[i];
                      BreakingNewsRecPtr^.StartEnableDateTime := Trunc(Modal.EntryStartEnableDate.Date) +
                          (Modal.EntryStartEnableTime.Time - Trunc(Modal.EntryStartEnableTime.Time));
                      BreakingNewsRecPtr^.EndEnableDateTime := Trunc(Modal.EntryEndEnableDate.Date) +
                          (Modal.EntryEndEnableTime.Time - Trunc(Modal.EntryEndEnableTime.Time));
                      BreakingNewsRecPtr^.Enabled := Modal.EntryEnable.Checked;
                      BreakingNewsRecPtr^.DwellTime := Modal.EntryDwellTime.Value*1000;
                      BreakingNewsRecPtr^.Comments := Modal.EntryNote.Text;
                      //BreakingNewsRecPtr^.League := Modal.League;
                    end;
                  end;
                end;
              end;
              Modal.Free;
              //Refresh grid
              RefreshPlaylistGrid(FIELD_1, PlaylistGrid);
              //Refresh the fields grid
              RefreshEntryFieldsGrid(TickerDisplayMode, FIELD_1, EntryFieldsGrid);
            end;
          end;
      end;
    end;
    //Reset to entry
    Case GridID of
      FIELD_1: begin
        PlaylistGrid.CurrentDataRow := CurrentRow;
        PlaylistGrid.TopRow := SaveTopRow;
      end;
      FIELD_2: begin
        PlaylistGrid2.CurrentDataRow := CurrentRow;
        PlaylistGrid2.TopRow := SaveTopRow;
      end;
    end;
  end;

  //Save the playlist out and signal the Playout Controller to re-load if in automated playout mode
  if (InRemotePlayoutMode) then
  begin
    //Save the playlist
    SavePlaylistWithoutClear;
    //Signal playout controller to refresh the playlist
    PlayoutInterface.RefreshCurrentPlaylist(PlaylistInfo[TICKER].PlaylistID);
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// PROCEDURES AND HANDLERS FOR ZIPPER PLAYLIST STORAGE,, LOADING & DELETION
////////////////////////////////////////////////////////////////////////////////
//Procedure to delete a specified zipper playlist from the database
procedure TMainForm.DeleteZipperPlaylist (PlaylistType: SmallInt; Playlist_ID: Double; Playlist_Description: String);
begin
  Case PlaylistType of
      //Ticker
   1: begin
        try
          if (dmMain.tblTicker_Groups.RecordCount > 0) then
          begin
            dmMain.tblTicker_Groups.First;
            While(dmMain.tblTicker_Groups.EOF = FALSE) do
            begin
              if (dmMain.tblTicker_Groups.FieldByName('Playlist_ID').AsString = FloatToStr(Trunc(Playlist_ID))) then
              begin
                dmMain.tblTicker_Groups.Delete;
              end
              else dmMain.tblTicker_Groups.Next;
            end;
          end;

          //Delete any associated elements
          if (dmMain.tblTicker_Elements.RecordCount > 0) then
          begin
            dmMain.tblTicker_Elements.First;
            While(dmMain.tblTicker_Elements.EOF = FALSE) do
            begin
              if (dmMain.tblTicker_Elements.FieldByName('Playlist_ID').AsString = FloatToStr(Trunc(Playlist_ID))) then
              begin
                dmMain.tblTicker_Elements.Delete;
              end
              else dmMain.tblTicker_Elements.Next;
            end;
          end;
        except
          //Alert operator
          MessageDlg('An error occurred while trying to delete the existing copy of the ' +
                     'L-Bar playlist from the database.', mtError, [mbOK], 0)
        end;
      end;
      //Alerts
   2: begin
        try
          if (dmMain.tblBreakingNews_Groups.RecordCount > 0) then
          begin
            dmMain.tblBreakingNews_Groups.First;
            While(dmMain.tblBreakingNews_Groups.EOF = FALSE) do
            begin
              if (dmMain.tblBreakingNews_Groups.FieldByName('Playlist_ID').AsString = FloatToStr(Trunc(Playlist_ID))) then
              begin
                dmMain.tblBreakingNews_Groups.Delete;
              end
              else dmMain.tblBreakingNews_Groups.Next;
            end;
          end;

          //Delete any associated elements
          if (dmMain.tblBreakingNews_Elements.RecordCount > 0) then
          begin
            dmMain.tblBreakingNews_Elements.First;
            While(dmMain.tblBreakingNews_Elements.EOF = FALSE) do
            begin
              if (dmMain.tblBreakingNews_Elements.FieldByName('Playlist_ID').AsString = FloatToStr(Trunc(Playlist_ID))) then
              begin
                dmMain.tblBreakingNews_Elements.Delete;
              end
              else dmMain.tblBreakingNews_Elements.Next;
            end;
          end;
        except
          //Alert operator
          MessageDlg('An error occurred while trying to delete the existing copy of the ' +
                     'Alerts playlist from the database.', mtError, [mbOK], 0)
        end;
      end;
  end;
end;

//Handler for playlist auto-save timer
procedure TMainForm.PlaylistAutoSaveTimerTimer(Sender: TObject);
begin
  //Check for a playlist being loaded and a name specified
  if (Ticker_Collection.Count > 0) AND (Trim (Edit1.Text) <> '') then
    SavePlaylistWithoutClear;
end;
//Handler for save playlist button WITHOUT clear
procedure TMainForm.BitBtn6Click(Sender: TObject);
begin
  SavePlaylistWithoutClear;
end;
procedure TMainForm.SavePlaylistWithoutClear;
begin
  //Check for invalid template entries
  if (CheckForInvalidTemplates(TickerDisplayMode) = TRUE) then
  begin
    //Alert operator
    MessageDlg('You have specified invalid templates in the playlist for the currently selected ' +
               'display mode. Playlist cannot be saved to the database.', mtError, [mbOK], 0);
  end
  else begin
    //Save playlist, but don't clear
    SaveTickerPlaylist(PlaylistSelectTabControl.TabIndex+1, FALSE);
  end;  
end;
//Handler for save playlist button WITH clear
procedure TMainForm.BitBtn1Click(Sender: TObject);
begin
  //Check for invalid template entries
  if (CheckForInvalidTemplates(TickerDisplayMode) = TRUE) then
    //Alert operator
    MessageDlg('You have specified invalid templates in the playlist for the currently selected ' +
               'display mode. Playlist cannot be saved to the database.', mtError, [mbOK], 0)
  else begin
    //Save playlist and clear
     SaveTickerPlaylist(PlaylistSelectTabControl.TabIndex+1, TRUE);
    //Disable single loop mode to prevent operator error with next playlist
    SingleLoopEnable.Checked := FALSE;
  end;
end;
//General procedure to save zipper collection out to database
function TMainForm.SaveTickerPlaylist(PlaylistType: SmallInt; ClearCollection: Boolean): Boolean;
var
  i: SmallInt;
  Playlist_Description: String;
  Playlist_ID: Double;
  TickerRecPtr: ^TickerRec;
  BreakingNewsRecPtr: ^BreakingNewsRec;
  Control: Word;
  FoundMatch: Boolean;
  OKToGo: Boolean;
  OriginalCreationDate: TDateTime;
  Save_Cursor: TCursor;
  Success: Boolean;
  FoundBreakingNews, FoundNonBreakingNews: Boolean;
  BreakingNewsMode: SmallInt;
  PlaylistStationID: SmallInt;
begin
  //Init
  OKToGo := TRUE;
  Success := TRUE;

  //Change cursor
  Save_Cursor := Screen.Cursor;
  //Show hourglass cursor
  Screen.Cursor := crHourGlass;

  //Set playlist name
  Playlist_Description := Trim (Edit1.Text);
  //Set the region ID
  if (RegionSelectMode.ItemIndex = 0) then PlaylistStationID := 0
  else PlaylistStationID := RegionSelect.ItemIndex+1;

  //Check to see if a playlist with a matching ID exists. If so, prompt operator for overwrite confirmation
  Case PlaylistType of
    TICKER: FoundMatch := dmMain.tblTicker_Groups.Locate('Playlist_Description; Station_ID', VarArrayOf([Playlist_Description, PlaylistStationID]), []);
    BREAKINGNEWS: FoundMatch := dmMain.tblBreakingNews_Groups.Locate('Playlist_ID', PlaylistStationID, []);
  end;

  //Don't prompt for overwrite if in remote playout mode
  if (Foundmatch = TRUE) then
  begin
    if not (InRemotePlayoutMode) then
    begin
      Control := MessageDlg('A playlist with a matching description and Playout Station identifier was found in the ' +
                            'database. Do you wish to overwrite it?',
                            mtConfirmation, [mbYes, mbNo], 0);
      if (Control = mrYes) then
        OKToGo := TRUE
      else
        OKToGo := FALSE;
    end;
    if (OKToGo) then
    begin
      Case PlaylistType of
          //Ticker
       TICKER: begin
            Playlist_ID := dmMain.tblTicker_Groups.FieldByName('Playlist_ID').AsFloat;
            OriginalCreationDate := dmMain.tblTicker_Groups.FieldByName('Entry_Date').AsDateTime;
            DeleteZipperPlaylist (TICKER, Playlist_ID, Playlist_Description);
          end;
          //Alerts
       BREAKINGNEWS: begin
            //Set the playlist ID to the station ID (0 = global) for breaking news
            Playlist_ID := PlaylistStationID;
            OriginalCreationDate := dmMain.tblBreakingNews_Groups.FieldByName('Entry_Date').AsDateTime;
            DeleteZipperPlaylist (BREAKINGNEWS, Playlist_ID, Playlist_Description);
          end;
      end;
    end;
  end;

  //Proceed if confirmed
  if (OKToGo = TRUE) then
  begin
    begin
      begin
        Case PlaylistType of
         //Ticker
         TICKER: begin
              //Only proceed if at least one element in playlist
              if (Length(Playlist_Description) >= 3) then
              begin
                if (Ticker_Collection.Count > 0) or (Ticker_Collection2.Count > 0) then
                begin
                  //If not overwriting the list, generate a new playlist ID
                  if (FoundMatch = FALSE) then
                  begin
                    Playlist_ID := Trunc(Now*100000);
                    OriginalCreationDate := Now;
                  end;
                  //Set Playlist_ID to 0 if it's the CURRENT playlist
                  if (Playlist_Description = 'CURRENT') then Playlist_ID := 0;
                  try
                    //Append new record for playlist
                    dmMain.tblTicker_Groups.AppendRecord ([
                      Playlist_ID,
                      PlaylistStationID,
                      Playlist_Description,
                      TickerDisplayMode,
                      OriginalCreationDate, //Creation date/timestamp
                      'N/A', //Operator name
                      Now //Edit date/time
                    ]);
                    //Now, insert all elements into ticker elements table
                    try
                      //Iterate for all records in collection #1
                      if (Ticker_Collection.Count > 0) then
                      begin
                        for i := 0 to Ticker_Collection.Count-1 do
                        begin
                          //Point to collection object
                          TickerRecPtr := Ticker_Collection.At(i);
                          //Append graphic record to graphics pages table in database}
                          dmMain.tblTicker_Elements.AppendRecord ([
                            Playlist_ID, //Block ID
                            FIELD_1, //Field 1 collection
                            i+1, //Index
                            //Set GUID as string
                            GUIDToString(TickerRecPtr^.Event_GUID),
                            //Default to NOT a mini playlist with a zero playlist ID
                            TickerRecPtr^.Enabled,
                            TickerRecPtr^.Is_Child,
                            TickerRecPtr^.League,
                            TickerRecPtr^.Subleague_Mnemonic_Standard,
                            TickerRecPtr^.Mnemonic_LiveEvent,
                            TickerRecPtr^.Template_ID,
                            TickerRecPtr^.Record_Type,
                            TickerRecPtr^.GameID,
                            TickerRecPtr^.SponsorLogo_Name, //Sponsor logo name
                            TickerRecPtr^.SponsorLogo_Dwell, //Dwell for sponsor logo
                            //For stats
                            TickerRecPtr^.StatStoredProcedure,
                            TickerRecPtr^.StatType,
                            TickerRecPtr^.StatTeam,
                            TickerRecPtr^.StatLeague,
                            TickerRecPtr^.StatRecordNumber,
                            TickerRecPtr^.UserData[1],
                            TickerRecPtr^.UserData[2],
                            TickerRecPtr^.UserData[3],
                            TickerRecPtr^.UserData[4],
                            TickerRecPtr^.UserData[5],
                            TickerRecPtr^.UserData[6],
                            TickerRecPtr^.UserData[7],
                            TickerRecPtr^.UserData[8],
                            TickerRecPtr^.UserData[9],
                            TickerRecPtr^.UserData[10],
                            TickerRecPtr^.UserData[11],
                            TickerRecPtr^.UserData[12],
                            TickerRecPtr^.UserData[13],
                            TickerRecPtr^.UserData[14],
                            TickerRecPtr^.UserData[15],
                            TickerRecPtr^.UserData[16],
                            TickerRecPtr^.UserData[17],
                            TickerRecPtr^.UserData[18],
                            TickerRecPtr^.UserData[19],
                            TickerRecPtr^.UserData[20],
                            TickerRecPtr^.UserData[21],
                            TickerRecPtr^.UserData[22],
                            TickerRecPtr^.UserData[23],
                            TickerRecPtr^.UserData[24],
                            TickerRecPtr^.UserData[25],
                            TickerRecPtr^.UserData[26],
                            TickerRecPtr^.UserData[27],
                            TickerRecPtr^.UserData[28],
                            TickerRecPtr^.UserData[29],
                            TickerRecPtr^.UserData[30],
                            TickerRecPtr^.UserData[31],
                            TickerRecPtr^.UserData[32],
                            TickerRecPtr^.UserData[33],
                            TickerRecPtr^.UserData[34],
                            TickerRecPtr^.UserData[35],
                            TickerRecPtr^.UserData[36],
                            TickerRecPtr^.UserData[37],
                            TickerRecPtr^.UserData[38],
                            TickerRecPtr^.UserData[39],
                            TickerRecPtr^.UserData[40],
                            TickerRecPtr^.UserData[41],
                            TickerRecPtr^.UserData[42],
                            TickerRecPtr^.UserData[43],
                            TickerRecPtr^.UserData[44],
                            TickerRecPtr^.UserData[45],
                            TickerRecPtr^.UserData[46],
                            TickerRecPtr^.UserData[47],
                            TickerRecPtr^.UserData[48],
                            TickerRecPtr^.UserData[49],
                            TickerRecPtr^.UserData[50],
                            TickerRecPtr^.StartEnableDateTime,
                            TickerRecPtr^.EndEnableDateTime,
                            TickerRecPtr^.DwellTime,
                            TickerRecPtr^.Description,
                            TickerRecPtr^.Comments
                          ]);
                        end;
                      end;
                      //Iterate for all records in collection #2
                      if (Ticker_Collection2.Count > 0) then
                      begin
                        for i := 0 to Ticker_Collection2.Count-1 do
                        begin
                          //Point to collection object
                          TickerRecPtr := Ticker_Collection2.At(i);
                          //Append graphic record to graphics pages table in database}
                          dmMain.tblTicker_Elements.AppendRecord ([
                            Playlist_ID, //Block ID
                            FIELD_2, //Field 2 collection
                            i+1, //Index
                            //Set GUID as string
                            GUIDToString(TickerRecPtr^.Event_GUID),
                            //Default to NOT a mini playlist with a zero playlist ID
                            TickerRecPtr^.Enabled,
                            TickerRecPtr^.Is_Child,
                            TickerRecPtr^.League,
                            TickerRecPtr^.Subleague_Mnemonic_Standard,
                            TickerRecPtr^.Mnemonic_LiveEvent,
                            TickerRecPtr^.Template_ID,
                            TickerRecPtr^.Record_Type,
                            TickerRecPtr^.GameID,
                            TickerRecPtr^.SponsorLogo_Name, //Sponsor logo name
                            TickerRecPtr^.SponsorLogo_Dwell, //Dwell for sponsor logo
                            //For stats
                            TickerRecPtr^.StatStoredProcedure,
                            TickerRecPtr^.StatType,
                            TickerRecPtr^.StatTeam,
                            TickerRecPtr^.StatLeague,
                            TickerRecPtr^.StatRecordNumber,
                            TickerRecPtr^.UserData[1],
                            TickerRecPtr^.UserData[2],
                            TickerRecPtr^.UserData[3],
                            TickerRecPtr^.UserData[4],
                            TickerRecPtr^.UserData[5],
                            TickerRecPtr^.UserData[6],
                            TickerRecPtr^.UserData[7],
                            TickerRecPtr^.UserData[8],
                            TickerRecPtr^.UserData[9],
                            TickerRecPtr^.UserData[10],
                            TickerRecPtr^.UserData[11],
                            TickerRecPtr^.UserData[12],
                            TickerRecPtr^.UserData[13],
                            TickerRecPtr^.UserData[14],
                            TickerRecPtr^.UserData[15],
                            TickerRecPtr^.UserData[16],
                            TickerRecPtr^.UserData[17],
                            TickerRecPtr^.UserData[18],
                            TickerRecPtr^.UserData[19],
                            TickerRecPtr^.UserData[20],
                            TickerRecPtr^.UserData[21],
                            TickerRecPtr^.UserData[22],
                            TickerRecPtr^.UserData[23],
                            TickerRecPtr^.UserData[24],
                            TickerRecPtr^.UserData[25],
                            TickerRecPtr^.UserData[26],
                            TickerRecPtr^.UserData[27],
                            TickerRecPtr^.UserData[28],
                            TickerRecPtr^.UserData[29],
                            TickerRecPtr^.UserData[30],
                            TickerRecPtr^.UserData[31],
                            TickerRecPtr^.UserData[32],
                            TickerRecPtr^.UserData[33],
                            TickerRecPtr^.UserData[34],
                            TickerRecPtr^.UserData[35],
                            TickerRecPtr^.UserData[36],
                            TickerRecPtr^.UserData[37],
                            TickerRecPtr^.UserData[38],
                            TickerRecPtr^.UserData[39],
                            TickerRecPtr^.UserData[40],
                            TickerRecPtr^.UserData[41],
                            TickerRecPtr^.UserData[42],
                            TickerRecPtr^.UserData[43],
                            TickerRecPtr^.UserData[44],
                            TickerRecPtr^.UserData[45],
                            TickerRecPtr^.UserData[46],
                            TickerRecPtr^.UserData[47],
                            TickerRecPtr^.UserData[48],
                            TickerRecPtr^.UserData[49],
                            TickerRecPtr^.UserData[50],
                            TickerRecPtr^.StartEnableDateTime,
                            TickerRecPtr^.EndEnableDateTime,
                            TickerRecPtr^.DwellTime,
                            TickerRecPtr^.Description,
                            TickerRecPtr^.Comments
                          ]);
                        end;
                      end;
                    except
                      MessageDlg ('Error occurred while trying to insert ticker element into database.',
                                   mtError, [mbOk], 0);
                    end;
                    //Update label & array
                    PlaylistInfo[TICKER].PlaylistName := Edit1.Text;
                    PlaylistInfo[TICKER].PlaylistID := Playlist_ID;
                    PlaylistNameLabel.Caption := Playlist_Description;
                  except
                    MessageDlg ('Error occurred while trying to insert ticker playlist into database.',
                                 mtError, [mbOk], 0);
                  end;
                  //Clear out & pack the collections if enabled
                  if (ClearCollection) then
                  begin
                    Ticker_Collection.Clear;
                    Ticker_Collection.Pack;
                    Ticker_Collection2.Clear;
                    Ticker_Collection2.Pack;
                  end;
                end
                else begin
                  MessageDlg('You must specify one or more elements to save out a playlist.',
                             mtInformation, [mbOk], 0);
                end;
              end
              else begin
                //Clear flag
                Success := FALSE;
                MessageDlg('You must specify a Playlist Name/Description of at least four (4) characters ' +
                           'before committing to the database.',
                            mtInformation, [mbOk], 0);
              end;
            end;
         //Alerts
         BREAKINGNEWS: begin
              //Only proceed if at least one element in playlist
              //if (BreakingNews_Collection.Count > 0) then
              begin
                //If not overwriting the list, generate a new playlist ID
                if (FoundMatch = FALSE) then
                begin
                  //Playlist ID is the Playout Station ID for Alerts (0 = Global)
                  Playlist_ID := PlaylistStationID;
                  OriginalCreationDate := Now;
                end;
                if (ImmediateModeBreakingNews.Checked) then BreakingNewsMode := 0
                else BreakingNewsMode := 1;
                try
                  //Append new record for playlist
                  dmMain.tblBreakingNews_Groups.AppendRecord ([
                    Playlist_ID,
                    PlaylistStationID,
                    'BREAKING NEWS',
                    EnableBreakingNews.Checked,
                    BreakingNewsMode, //Breaking news mode
                    BreakingNewsIterations.Value,
                    OriginalCreationDate, //Creation date/timestamp
                    'N/A', //Operator name
                    Now //Edit date/time
                  ]);
                  //Now, insert all elements into BreakingNews elements table
                  try
                    //Iterate for all records in collection
                    if (BreakingNews_Collection.Count > 0) then
                    begin
                      for i := 0 to BreakingNews_Collection.Count-1 do
                      begin
                        //Point to collection object
                        BreakingNewsRecPtr := BreakingNews_Collection.At(i);
                        //Append graphic record to graphics pages table in database}
                        dmMain.tblBreakingNews_Elements.AppendRecord ([
                          Playlist_ID, //Block ID
                          i+1, //Index
                          //Set GUID as string
                          GUIDToString(BreakingNewsRecPtr^.Event_GUID),
                          //Default to NOT a mini playlist with a zero playlist ID
                          BreakingNewsRecPtr^.Enabled,
                          BreakingNewsRecPtr^.League,
                          BreakingNewsRecPtr^.Template_ID,
                          BreakingNewsRecPtr^.Record_Type,
                          BreakingNewsRecPtr^.UserData[1],
                          BreakingNewsRecPtr^.UserData[2],
                          BreakingNewsRecPtr^.StartEnableDateTime,
                          BreakingNewsRecPtr^.EndEnableDateTime,
                          BreakingNewsRecPtr^.DwellTime,
                          BreakingNewsRecPtr^.Description,
                          BreakingNewsRecPtr^.Comments
                        ]);
                      end;
                    end;
                  except
                    MessageDlg ('Error occurred while trying to insert Alerts element into database.',
                                 mtError, [mbOk], 0);
                  end;
                  //Update label & array
                  PlaylistInfo[BREAKINGNEWS].PlaylistName := 'BREAKING NEWS';
                  PlaylistInfo[BREAKINGNEWS].PlaylistID := Playlist_ID;
                  PlaylistNameLabel.Caption := 'BREAKING NEWS';
                except
                  MessageDlg ('Error occurred while trying to insert Alerts playlist into database.',
                               mtError, [mbOk], 0);
                end;
                //Clear out & pack the collection if enabled
                if (ClearCollection) then
                begin
                  BreakingNews_Collection.Clear;
                  BreakingNews_Collection.Pack;
                  //Clear immediate mode alerts & alerts enable
                  ImmediateModeBreakingNews.Checked := FALSE;
                  EnableBreakingNews.Checked := FALSE;
                end;
              end;
              //else begin
              //  MessageDlg('You must specify one or more elements to save out a playlist.',
              //             mtInformation, [mbOk], 0);
              //end;
            end;
        end; //Case

        //Now, walk through scheduled playlists, and update time/datestamp and ticker mode for any matching entries
        //to insure refresh is picked up
        Case PlaylistType of
         //Ticker
         1: begin
              dmMain.tblScheduled_Ticker_Groups.Active := FALSE;
              dmMain.tblScheduled_Ticker_Groups.Active := TRUE;
              if (dmMain.tblScheduled_Ticker_Groups.RecordCount > 0) then
              begin
                for i := 1 to dmMain.tblScheduled_Ticker_Groups.RecordCount do
                begin
                  if (Playlist_ID = dmMain.tblScheduled_Ticker_Groups.FieldByName('Playlist_ID').AsFloat) then
                  begin
                    //Match was found, so update the timestamp
                    dmMain.tblScheduled_Ticker_Groups.Edit;
                    dmMain.tblScheduled_Ticker_Groups.FieldByName('Entry_Date').AsFloat := Now;
                    dmMain.tblScheduled_Ticker_Groups.FieldByName('Ticker_Mode').AsInteger := TickerDisplayMode;
                    dmMain.tblScheduled_Ticker_Groups.Post;
                  end;
                dmMain.tblScheduled_Ticker_Groups.Next;
                end;
              end;
            end;
         //Alerts
         2: begin
            end;
        end;
        if (ClearCollection) then
        begin
          //Clear the edit boxes for playlist name
          Edit1.Text := '';
          PlaylistInfo[PlaylistType].PlaylistName := '';
          PlaylistNameLabel.Caption := '';
          LastSaveTimeLabel.Caption := '';
          PlaylistInfo[PlaylistType].LastPlaylistSaveTimeString :=
            LastSaveTimeLabel.Caption;
        end
        else begin
          LastSaveTimeLabel.Caption := DateTimeToStr(Now);
          PlaylistInfo[PlaylistType].LastPlaylistSaveTimeString :=
            LastSaveTimeLabel.Caption;
        end;
      end;
    end;
    //Refresh the grids
    RefreshPlaylistGrid(FIELD_1, PlaylistGrid);
    RefreshEntryFieldsGrid(TickerDisplayMode, FIELD_1, EntryFieldsGrid);
    RefreshPlaylistGrid(FIELD_2, PlaylistGrid2);
    RefreshEntryFieldsGrid(TickerDisplayMode, FIELD_2, EntryFieldsGrid2);
  end;
  //Restore cursor
  Screen.Cursor := Save_Cursor;
  //Set success value
  SaveTickerPlaylist := Success;
end;

//Handler to load zipper collection from database
procedure TMainForm.BitBtn24Click(Sender: TObject);
begin
  LoadZipperCollection;
end;
//General procedure to load a playlist collection; modifed to handle different playlist types
procedure TMainForm.LoadZipperCollection;
var
  Control, Control2: Word;
  OKToGo: Boolean;
  Modal: TPlaylistSelectDlg;
  Modal2: TBreakingNewsPlaylistSelectDlg;
  SelectedPlaylistID: Double;
  CurrentPlaylistType: SmallInt;
  CollectionCount: SmallInt;
  Confirmed: Boolean;
begin
  //Reset the auto logout timer & init
  OKToGo := TRUE;
  if (OKToGo = TRUE) then
  begin
    //Set intial values
    CurrentPlaylistType := PlaylistSelectTabControl.TabIndex+1;

    //Display the dialog
    Case CurrentPlaylistType of
     TICKER: Modal := TPlaylistSelectDlg.Create(Application);
     BREAKINGNEWS: Modal2 := TBreakingNewsPlaylistSelectDlg.Create(Application);
    end;

    try
      Case CurrentPlaylistType of
       TICKER: begin
         Modal.PlaylistType := CurrentPlaylistType;
         Control := Modal.ShowModal;
       end;
       BREAKINGNEWS: begin
         Modal2.PlaylistType := CurrentPlaylistType;
         Control := Modal2.ShowModal;
       end;
      end;
    finally
      //Init
      Confirmed := TRUE;
      //CLEAR AND LOAD NEW PLAYLIST
      if (Control = mrOK) then
      begin
        Case CurrentPlaylistType of
         TICKER: CollectionCount := Ticker_Collection.Count;
         BREAKINGNEWS: CollectionCount := BreakingNews_Collection.Count;
        end;
        Case CurrentPlaylistType of
         TICKER: SelectedPlaylistID := Modal.SelectedPlaylistID;
         BREAKINGNEWS: SelectedPlaylistID := Modal2.SelectedPlaylistID;
        end;
        if (CollectionCount > 0) then
        begin
          Control2 := MessageDlg('There are currently entries in the playlist. ' +
            'Do you wish to save them before loading the new playlist?',
            mtWarning, [mbYes, mbNo], 0);
          //If operator confirmed, save the playlist out
          if (Control2 = mrYes) then Confirmed := SaveTickerPlaylist(CurrentPlaylistType, TRUE);
        end;
        //Load the new playlist as long as no existing entries or the old one save out OK
        if (Confirmed) then LoadPlaylistCollection (CurrentPlaylistType, 0, SelectedPlaylistID);
      end
      //APPEND PLAYLIST
      else if (Control= mrYes) then
      begin
        Case CurrentPlaylistType of
         TICKER: SelectedPlaylistID := Modal.SelectedPlaylistID;
         BREAKINGNEWS: SelectedPlaylistID := Modal2.SelectedPlaylistID;
        end;
        if (Ticker_Collection.Count > 0) then
        begin
          Control2 := MessageDlg('There are currently entries in the playlist. ' +
            'Do you wish to append the selected playlist to the existing playlist?',
            mtWarning, [mbYes, mbNo], 0);
          if (Control2 = mrYes) then
            LoadPlaylistCollection (CurrentPlaylistType, 1, SelectedPlaylistID);
        end
        else LoadPlaylistCollection (CurrentPlaylistType, 1, SelectedPlaylistID);
      end
      //INSERT PLAYLIST
      else if (Control= mrYesToAll) then
      begin
        Case CurrentPlaylistType of
         TICKER: SelectedPlaylistID := Modal.SelectedPlaylistID;
         BREAKINGNEWS: SelectedPlaylistID := Modal2.SelectedPlaylistID;
        end;
        if (Ticker_Collection.Count > 0) then
        begin
          Control2 := MessageDlg('There are currently entries in the playlist. ' +
            'Do you wish to insert the selected playlist into the existing playlist?',
            mtWarning, [mbYes, mbNo], 0);
          if (Control2 = mrYes) then
            LoadPlaylistCollection (CurrentPlaylistType, 2, SelectedPlaylistID);
        end
        else
          LoadPlaylistCollection (CurrentPlaylistType, 2, SelectedPlaylistID);
      end;
      //Free up the dialog
      Case CurrentPlaylistType of
        TICKER: Modal.Free;
        BREAKINGNEWS: Modal2.Free;
      end;
    end;
  end;
  //Load the playlist remotely if in remote playout mode
  if (InRemotePlayoutMode) and (CurrentPlaylistType = TICKER) then
  begin
    PlayoutInterface.LoadPlaylist(SelectedPlaylistID);
  end;
end;

//Procedure to load default zipper collection from database
procedure TMainForm.LoadPlaylistCollection (PlaylistType: SmallInt; AppendMode: SmallInt; PlaylistID: Double);
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  BreakingNewsRecPtr: ^BreakingNewsRec;
  SelectedPlaylistID: Double;
  InsertPoint: SmallInt;
  Save_Cursor: TCursor;
begin
  //Change cursor
  Save_Cursor := Screen.Cursor;
  //Show hourglass cursor
  Screen.Cursor := crHourGlass;
  Case AppendMode of
      //Clear and load new collection
   0: begin
        Case PlaylistType of
            //Ticker
         TICKER: begin
              //Clear the collections
              Ticker_Collection.Clear;
              Ticker_Collection.Pack;
              Ticker_Collection2.Clear;
              Ticker_Collection2.Pack;
              //Set controls to values from selected block
              SelectedPlaylistID := PlaylistID;
              dmMain.tblTicker_Groups.Locate('Playlist_ID', SelectedPlaylistID, []);
              Edit1.Text := dmMain.tblTicker_Groups.FieldByName('Playlist_Description').AsString;
              //Set playlist ID controls
              if (dmMain.tblTicker_Groups.FieldByName('Station_ID').AsInteger = 0) then
              begin
                RegionSelectMode.ItemIndex := 0;
                RegionSelect.ItemIndex := 0;
                RegionSelectMode.Color := clBtnFace;
                RegionSelect.Color := clBtnFace;
                RegionIDNum.Text := IntToStr(0);
                RegionIDNum.Color := clBtnFace;
              end
              else begin
                RegionSelectMode.ItemIndex := 1;
                RegionSelect.ItemIndex := dmMain.tblTicker_Groups.FieldByName('Station_ID').AsInteger-1;
                RegionSelectMode.Color := clYellow;
                RegionSelect.Color := clYellow;
                RegionIDNum.Text := IntToStr(RegionSelect.ItemIndex+1);
                RegionIDNum.Color := clYellow;
              end;
              //Set looping mode and 1-line, 2-line mode; clear entry in preview mode
              Case dmMain.tblTicker_Groups.FieldByName('Ticker_Mode').AsInteger of
                1: begin
                     SingleLoopEnable.Checked := TRUE;
                     SingleLineCheck.Checked := TRUE;
                   end;
                2: begin
                     SingleLoopEnable.Checked := FALSE;
                     SingleLineCheck.Checked := TRUE;
                   end;
                3: begin
                     SingleLoopEnable.Checked := TRUE;
                     DoubleLineCheck.Checked := TRUE;
                   end;
                4: begin
                     SingleLoopEnable.Checked := FALSE;
                     DoubleLineCheck.Checked := TRUE;
                   end;
              end;

              //Clear the graphics output
              //EngineInterface.TriggerGraphic(2);

              //Set UI control values
              PlaylistInfo[PlaylistType].PlaylistName := Edit1.Text;
              PlaylistInfo[PlaylistType].PlaylistID := PlaylistID;
              PlaylistNameLabel.Caption := Edit1.Text;

              //Query for the elements table for those that match the playlist ID
              dmMain.Query1.Close;
              dmMain.Query1.SQL.Clear;
              dmMain.Query1.SQL.Add('SELECT * FROM Ticker_Elements ' +
                                       'WHERE Playlist_ID = ' + FloatToStr(SelectedPlaylistID));
              dmMain.Query1.Open;
              if (dmMain.Query1.RecordCount > 0) then

              //Iterate through all records in the table
              repeat
                GetMem (TickerRecPtr, SizeOf(TickerRec));
                With TickerRecPtr^ do
                begin
                  //Set values for collection record
                  Event_Index := dmMain.Query1.FieldByName('Event_Index').AsInteger;
                  Field_ID := dmMain.Query1.FieldByName('Field_ID').AsInteger;
                  Event_GUID := StringToGUID(dmMain.Query1.FieldByName('Event_GUID').AsString);
                  Is_Child := dmMain.Query1.FieldByName('Is_Child').AsBoolean;
                  Enabled := dmMain.Query1.FieldByName('Enabled').AsBoolean;
                  League := dmMain.Query1.FieldByName('League').AsString;
                  Subleague_Mnemonic_Standard := dmMain.Query1.FieldByName('Subleague_Mnemonic_Standard').AsString;
                  Mnemonic_LiveEvent := dmMain.Query1.FieldByName('Mnemonic_LiveEvent').AsString;
                  Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
                  Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
                  GameID := dmMain.Query1.FieldByName('GameID').AsString;
                  SponsorLogo_Name := dmMain.Query1.FieldByName('SponsorLogoName').AsString;
                  SponsorLogo_Dwell := dmMain.Query1.FieldByName('SponsorLogoDwell').AsInteger;
                  StatStoredProcedure := dmMain.Query1.FieldByName('StatStoredProcedure').AsString;
                  StatType := dmMain.Query1.FieldByName('StatType').AsInteger;
                  StatTeam := dmMain.Query1.FieldByName('StatTeam').AsString;
                  StatLeague := dmMain.Query1.FieldByName('StatLeague').AsString;
                  StatRecordNumber := dmMain.Query1.FieldByName('StatRecordNumber').AsInteger;
                  //Load the user-defined text fields
                  for i := 1 to 50 do
                    UserData[i] := dmMain.Query1.FieldByName('UserData_' + IntToStr(i)).AsString;
                  StartEnableDateTime := dmMain.Query1.FieldByName('StartEnableTime').AsDateTime;
                  EndEnableDateTime := dmMain.Query1.FieldByName('EndEnableTime').AsDateTime;
                  DwellTime := dmMain.Query1.FieldByName('DwellTime').AsInteger;
                  Description := dmMain.Query1.FieldByName('Description').AsString;
                  Selected := FALSE;
                  Comments := dmMain.Query1.FieldByName('Comments').AsString;
                  //Insert the object based on the field ID
                  Case dmMain.Query1.FieldByName('Field_ID').AsInteger of
                    FIELD_1: begin
                      Ticker_Collection.Insert(TickerRecPtr);
                      Ticker_Collection.Pack;
                    end;
                    FIELD_2: begin
                      Ticker_Collection2.Insert(TickerRecPtr);
                      Ticker_Collection2.Pack;
                    end;
                  end;
                  //Go to next record in query
                  dmMain.Query1.Next;
                end;
              until (dmMain.Query1.EOF = TRUE);
            end;
            //Alerts
         BREAKINGNEWS: begin
              //Clear the collection
              BreakingNews_Collection.Clear;
              BreakingNews_Collection.Pack;
              //Set controls to values from selected block
              SelectedPlaylistID := PlaylistID;
              dmMain.tblBreakingNews_Groups.Locate('Playlist_ID', SelectedPlaylistID, []);
              Edit1.Text := dmMain.tblBreakingNews_Groups.FieldByName('Playlist_Description').AsString;
              //Set playlist ID controls
              if (dmMain.tblBreakingNews_Groups.FieldByName('Playlist_ID').AsInteger = 0) then
              begin
                RegionSelectMode.ItemIndex := 0;
                RegionSelect.ItemIndex := 0;
                RegionSelectMode.Color := clBtnFace;
                RegionSelect.Color := clBtnFace;
                RegionIDNum.Text := IntToStr(0);
                RegionIDNum.Color := clBtnFace;
              end
              else begin
                RegionSelectMode.ItemIndex := 1;
                RegionSelect.ItemIndex := dmMain.tblBreakingNews_Groups.FieldByName('Playlist_ID').AsInteger-1;
                RegionSelectMode.Color := clYellow;
                RegionSelect.Color := clYellow;
                RegionIDNum.Text := IntToStr(RegionSelect.ItemIndex+1);
                RegionIDNum.Color := clYellow;
              end;
              BreakingNewsIterations.Value := dmMain.tblBreakingNews_Groups.FieldByName('Iterations').AsInteger;
              EnableBreakingNews.Checked := dmMain.tblBreakingNews_Groups.FieldByName('BreakingNews_Enable').AsBoolean;
              Case dmMain.tblBreakingNews_Groups.FieldByName('BreakingNews_Mode').AsInteger of
                0: ImmediateModeBreakingNews.Checked := TRUE;
                1: ImmediateModeBreakingNews.Checked := FALSE;
              end;
              PlaylistInfo[PlaylistType].PlaylistName := Edit1.Text;
              PlaylistInfo[PlaylistType].PlaylistID := PlaylistID;
              PlaylistNameLabel.Caption := Edit1.Text;
              //Store playlist info
              //Load the collection from the database
              //Query for the elements table for those that match the playlist ID
              dmMain.Query1.Close;
              dmMain.Query1.SQL.Clear;
              dmMain.Query1.SQL.Add('SELECT * FROM BreakingNews_Elements ' +
                                       'WHERE Playlist_ID = ' + FloatToStr(SelectedPlaylistID));
              dmMain.Query1.Open;
              if (dmMain.Query1.RecordCount > 0) then

              //Iterate through all records in the table
              repeat
                GetMem (BreakingNewsRecPtr, SizeOf(BreakingNewsRec));
                With BreakingNewsRecPtr^ do
                begin
                  //Set values for collection record
                  Event_Index := dmMain.Query1.FieldByName('Event_Index').AsInteger;
                  Event_GUID := StringToGUID(dmMain.Query1.FieldByName('Event_GUID').AsString);
                  Enabled := dmMain.Query1.FieldByName('Enabled').AsBoolean;
                  League := dmMain.Query1.FieldByName('League').AsString;
                  Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
                  Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
                  //Load the user-defined text fields
                  for i := 1 to 2 do
                    UserData[i] := dmMain.Query1.FieldByName('UserData_' + IntToStr(i)).AsString;
                  StartEnableDateTime := dmMain.Query1.FieldByName('StartEnableTime').AsDateTime;
                  EndEnableDateTime := dmMain.Query1.FieldByName('EndEnableTime').AsDateTime;
                  DwellTime := dmMain.Query1.FieldByName('DwellTime').AsInteger;
                  Description := dmMain.Query1.FieldByName('Description').AsString;
                  Selected := FALSE;
                  Comments := dmMain.Query1.FieldByName('Comments').AsString;
                  //Insert the object
                  BreakingNews_Collection.Insert(BreakingNewsRecPtr);
                  BreakingNews_Collection.Pack;
                  //Go to next record in query
                  dmMain.Query1.Next;
                end;
              until (dmMain.Query1.EOF = TRUE);
            end;
        end;
        //Refresh the playlist grids
        RefreshPlaylistGrid(FIELD_1, PlaylistGrid);
        RefreshPlaylistGrid(FIELD_2, PlaylistGrid2);
        //Refresh the field entries grids
        RefreshEntryFieldsGrid(TickerDisplayMode, FIELD_1, EntryFieldsGrid);
        RefreshEntryFieldsGrid(TickerDisplayMode, FIELD_2, EntryFieldsGrid2);
        //Reset indices
        CurrentTickerEntryIndex1 := 0;
        MainForm.PlaylistGrid.CurrentDataRow := CurrentTickerEntryIndex1+1;
        MainForm.PlaylistGrid.SetTopLeft(1, MainForm.PlaylistGrid.CurrentDataRow);
        CurrentTickerEntryIndex2 := 0;
        MainForm.PlaylistGrid2.CurrentDataRow := CurrentTickerEntryIndex2+1;
        MainForm.PlaylistGrid2.SetTopLeft(1, MainForm.PlaylistGrid2.CurrentDataRow);
      end;
      //Append or insert to existing collection
   1: begin
        Case PlaylistType of
            //Ticker
         1: begin
              if (dmMain.tblTicker_Groups.RecordCount > 0) then
              begin
                //Set controls to values from selected block
                SelectedPlaylistID := PlaylistID;
                dmMain.tblTicker_Groups.Locate('Playlist_ID', SelectedPlaylistID, []);
                Edit1.Text := dmMain.tblTicker_Groups.FieldByName('Playlist_Description').AsString;
                //Set looping mode
                Case dmMain.tblTicker_Groups.FieldByName('Ticker_Mode').AsInteger of
                  1, 3: SingleLoopEnable.Checked := TRUE;
                  2, 4: SingleLoopEnable.Checked := FALSE;
                end;
                PlaylistInfo[PlaylistType].PlaylistName := Edit1.Text;
                PlaylistInfo[PlaylistType].PlaylistID := PlaylistID;
                PlaylistNameLabel.Caption := Edit1.Text;
                //Load the collection from the database
                //Query for the elements table for those that match the playlist ID
                dmMain.Query1.Close;
                dmMain.Query1.SQL.Clear;
                dmMain.Query1.SQL.Add('SELECT * FROM Ticker_Elements ' +
                                         'WHERE Playlist_ID = ' + FloatToStr(SelectedPlaylistID));
                dmMain.Query1.Open;
                if (dmMain.Query1.RecordCount > 0) then

                //Iterate through all records in the table
                repeat
                  GetMem (TickerRecPtr, SizeOf(TickerRec));
                  With TickerRecPtr^ do
                  begin
                    //Set values for collection record
                    Event_Index := dmMain.Query1.FieldByName('Event_Index').AsInteger;
                    Field_ID := dmMain.Query1.FieldByName('Field_ID').AsInteger;
                    Event_GUID := StringToGUID(dmMain.Query1.FieldByName('Event_GUID').AsString);
                    Is_Child := dmMain.Query1.FieldByName('Is_Child').AsBoolean;
                    Enabled := dmMain.Query1.FieldByName('Enabled').AsBoolean;
                    League := dmMain.Query1.FieldByName('League').AsString;
                    Subleague_Mnemonic_Standard := dmMain.Query1.FieldByName('Subleague_Mnemonic_Standard').AsString;
                    Mnemonic_LiveEvent := dmMain.Query1.FieldByName('Mnemonic_LiveEvent').AsString;
                    Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
                    Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
                    GameID := dmMain.Query1.FieldByName('GameID').AsString;
                    SponsorLogo_Name := dmMain.Query1.FieldByName('SponsorLogoName').AsString;
                    SponsorLogo_Dwell := dmMain.Query1.FieldByName('SponsorLogoDwell').AsInteger;
                    StatStoredProcedure := dmMain.Query1.FieldByName('StatStoredProcedure').AsString;
                    StatType := dmMain.Query1.FieldByName('StatType').AsInteger;
                    StatTeam := dmMain.Query1.FieldByName('StatTeam').AsString;
                    StatLeague := dmMain.Query1.FieldByName('StatLeague').AsString;
                    StatRecordNumber := dmMain.Query1.FieldByName('StatRecordNumber').AsInteger;
                    //Load the user-defined text fields
                    for i := 1 to 50 do
                      UserData[i] := dmMain.Query1.FieldByName('UserData_' + IntToStr(i)).AsString;
                    StartEnableDateTime := dmMain.Query1.FieldByName('StartEnableTime').AsDateTime;
                    EndEnableDateTime := dmMain.Query1.FieldByName('EndEnableTime').AsDateTime;
                    DwellTime := dmMain.Query1.FieldByName('DwellTime').AsInteger;
                    Description := dmMain.Query1.FieldByName('Description').AsString;
                    Selected := FALSE;
                    Comments := dmMain.Query1.FieldByName('Comments').AsString;
                    //Insert the object based on the field ID
                    Case dmMain.Query1.FieldByName('Field_ID').AsInteger of
                      FIELD_1: begin
                        if (PlaylistGrid.CurrentDataRow < 1) then PlaylistGrid.CurrentDataRow := 1;
                        InsertPoint := PlaylistGrid.CurrentDataRow-1;
                        if (AppendMode = 1) then
                          Ticker_Collection.Insert(TickerRecPtr)
                        else if (AppendMode = 2) then
                          Ticker_Collection.AtInsert(InsertPoint, TickerRecPtr);
                        Ticker_Collection.Pack;
                      end;
                      FIELD_2: begin
                        if (PlaylistGrid2.CurrentDataRow < 1) then PlaylistGrid2.CurrentDataRow := 1;
                        InsertPoint := PlaylistGrid2.CurrentDataRow-1;
                        if (AppendMode = 1) then
                          Ticker_Collection2.Insert(TickerRecPtr)
                        else if (AppendMode = 2) then
                          Ticker_Collection2.AtInsert(InsertPoint, TickerRecPtr);
                        Ticker_Collection2.Pack;
                      end;
                    end;
                    //Go to next record in query
                    dmMain.Query1.Next;
                  end;
                until (dmMain.Query1.EOF = TRUE);
                //Refresh the grids
                RefreshPlaylistGrid(FIELD_1, PlaylistGrid);
                RefreshPlaylistGrid(FIELD_2, PlaylistGrid2);
              end;
            end;
            //Alerts
         2: begin
              if (dmMain.tblBreakingNews_Groups.RecordCount > 0) then
              begin
                //Set controls to values from selected block
                SelectedPlaylistID := PlaylistID;
                dmMain.tblBreakingNews_Groups.Locate('Playlist_ID', SelectedPlaylistID, []);
                Edit1.Text := dmMain.tblBreakingNews_Groups.FieldByName('Playlist_Description').AsString;
                BreakingNewsIterations.Value := dmMain.tblBreakingNews_Groups.FieldByName('Iterations').AsInteger;
                EnableBreakingNews.Checked := dmMain.tblBreakingNews_Groups.FieldByName('BreakingNews_Enable').AsBoolean;
                Case dmMain.tblBreakingNews_Groups.FieldByName('BreakingNews_Mode').AsInteger of
                  0: ImmediateModeBreakingNews.Checked := TRUE;
                  1: ImmediateModeBreakingNews.Checked := FALSE;
                end;
                PlaylistInfo[PlaylistType].PlaylistName := Edit1.Text;
                PlaylistInfo[PlaylistType].PlaylistID := PlaylistID;
                PlaylistNameLabel.Caption := Edit1.Text;
                //Load the collection from the database
                //Query for the elements table for those that match the playlist ID
                dmMain.Query1.Close;
                dmMain.Query1.SQL.Clear;
                dmMain.Query1.SQL.Add('SELECT * FROM BreakingNews_Elements ' +
                                         'WHERE Playlist_ID = ' + FloatToStr(SelectedPlaylistID));
                dmMain.Query1.Open;
                if (dmMain.Query1.RecordCount > 0) then

                //Iterate through all records in the table
                repeat
                  GetMem (BreakingNewsRecPtr, SizeOf(BreakingNewsRec));
                  With BreakingNewsRecPtr^ do
                  begin
                    //Set values for collection record
                    Event_Index := dmMain.Query1.FieldByName('Event_Index').AsInteger;
                    Event_GUID := StringToGUID(dmMain.Query1.FieldByName('Event_GUID').AsString);
                    Enabled := dmMain.Query1.FieldByName('Enabled').AsBoolean;
                    League := dmMain.Query1.FieldByName('League').AsString;
                    Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
                    Record_Type := dmMain.Query1.FieldByName('Record_Type').AsInteger;
                    //Load the user-defined text fields
                    for i := 1 to 2 do
                      UserData[i] := dmMain.Query1.FieldByName('UserData_' + IntToStr(i)).AsString;
                    StartEnableDateTime := dmMain.Query1.FieldByName('StartEnableTime').AsDateTime;
                    EndEnableDateTime := dmMain.Query1.FieldByName('EndEnableTime').AsDateTime;
                    DwellTime := dmMain.Query1.FieldByName('DwellTime').AsInteger;
                    Description := dmMain.Query1.FieldByName('Description').AsString;
                    Selected := FALSE;
                    Comments := dmMain.Query1.FieldByName('Comments').AsString;
                    //Insert the object
                    if (PlaylistGrid.CurrentDataRow < 1) then PlaylistGrid.CurrentDataRow := 1;
                    InsertPoint := PlaylistGrid.CurrentDataRow-1;
                    if (AppendMode = 1) then
                      BreakingNews_Collection.Insert(BreakingNewsRecPtr)
                    else if (AppendMode = 2) then
                      BreakingNews_Collection.AtInsert(InsertPoint, BreakingNewsRecPtr);
                    BreakingNews_Collection.Pack;
                    //Go to next record in query
                    dmMain.Query1.Next;
                  end;
                until (dmMain.Query1.EOF = TRUE);          //Refresh the grid
                RefreshPlaylistGrid(FIELD_1, PlaylistGrid);
              end;
            end;
        end;
      end;
  end;
  //Restore default cursor
  Screen.Cursor := Save_Cursor;
end;

////////////////////////////////////////////////////////////////////////////////
// PLAYLIST SCHEDULE FUNCTIONS - TICKER, BUGS, EXTRA LINE
////////////////////////////////////////////////////////////////////////////////
//Handler for adding a ticker playlist to the ticker schedule
procedure TMainForm.BitBtn42Click(Sender: TObject);
begin
  AddScheduleEntry;
end;
procedure TMainForm.AvailablePlaylistsGridDblClick(Sender: TObject);
begin
  AddScheduleEntry;
end;
procedure TMainForm.AddScheduleEntry;
var
  EncodedStartEnableTime: TDateTime;
  EncodedEndEnableTime: TDateTime;
  FoundMatchingPlaylist: Boolean;
  Control: Word;
  OKToGo: Boolean;
  OverlapCount: SmallInt;
  EncodedStartEnableTimeStr: String;
  EncodedEndEnableTimeStr: String;
  CurrentRecordCount: SmallInt;
  QueryStr: String;
  StationID: SmallInt;
begin
  begin
    //Check to see if a playlist with a matching ID exists. If so, prompt operator
    //for overwrite confirmation
    OKToGo := TRUE;

    //Set the record count based on the playlist type selected
    Case PlaylistSelectTabControl.TabIndex of
         //Ticker
      0: CurrentRecordCount := dmMain.tblTicker_Groups.RecordCount;
         //Alerts
      1: CurrentRecordCount := dmMain.tblBreakingNews_Groups.RecordCount;
    end;

    //Only proceed if at least playlist in database
    if (CurrentRecordCount > 0) then
    begin
      try
        //Encode start and end enable times
        EncodedStartEnableTime := Trunc(ScheduleEntryStartDate.Date) +
          (ScheduleEntryStartTime.Time - Trunc(ScheduleEntryStartTime.Time));
        EncodedEndEnableTime := Trunc(ScheduleEntryEndDate.Date) +
          (ScheduleEntryEndTime.Time - Trunc(ScheduleEntryEndTime.Time));

        //Make sure end time is greater than start time
        if (EncodedStartEnableTime >= EncodedEndEnableTime) then
        begin
          OKToGo := FALSE;
          MessageDlg('You must enter an End Enable Time that occurs AFTER the Start Enable Time',
                     mtError, [mbOK], 0);
        end;

        //Set the station ID
        StationID := dmMain.tblTicker_Groups.FieldByName('Station_ID').AsInteger;

        //NEED TO CHECK FOR BOTH GLOBAL AND REGION-SPECIFIC PLAYLISTS
        //Check for matching start time and Global Station ID in schedule (global)
        Case PlaylistSelectTabControl.TabIndex of
            //Ticker
         0: FoundMatchingPlaylist :=
              dmMain.tblScheduled_Ticker_Groups.Locate('Start_Enable_Time; Station_ID', VarArrayOf([EncodedStartEnableTime, 0]), []);
            //Alerts
         1: FoundMatchingPlaylist :=
              dmMain.tblScheduled_BreakingNews_Groups.Locate('Start_Enable_Time; Playlist_ID', VarArrayOf([EncodedStartEnableTime, 0]), []);
        end;
        if (FoundMatchingPlaylist = TRUE) then
        begin
          Control := MessageDlg('A playlist with the same start enable time and a Global Playout Station ID was found in the ' +
                                'database. Do you wish to overwrite it?', mtConfirmation,
                                [mbYes, mbNo], 0);
          if (Control = mrYes) then
          begin
            Case PlaylistSelectTabControl.TabIndex of
                //Ticker
             0: DeletePlaylistFromSchedule(TICKER, dmMain.tblScheduled_Ticker_Groups.FieldByName('Start_Enable_Time').AsFloat, 0);
                //Alerts
             1: DeletePlaylistFromSchedule(BREAKINGNEWS, dmMain.tblScheduled_BreakingNews_Groups.FieldByName('Start_Enable_Time').AsFloat, 0);
            end;
          end
          else OKToGo := FALSE;
        end;

        //Check for matching start time and Playout Station ID in schedule (regin specific)
        Case PlaylistSelectTabControl.TabIndex of
            //Ticker
         0: FoundMatchingPlaylist :=
              dmMain.tblScheduled_Ticker_Groups.Locate('Start_Enable_Time; Station_ID', VarArrayOf([EncodedStartEnableTime, StationID]), []);
            //Alerts
         1: FoundMatchingPlaylist :=
              dmMain.tblScheduled_BreakingNews_Groups.Locate('Start_Enable_Time; Playlist_ID', VarArrayOf([EncodedStartEnableTime, StationID]), []);
        end;
        if (FoundMatchingPlaylist = TRUE) then
        begin
          Control := MessageDlg('A playlist with the same start enable time and Playout Station ID was found in the ' +
                                'database. Do you wish to overwrite it?', mtConfirmation,
                                [mbYes, mbNo], 0);
          if (Control = mrYes) then
          begin
            Case PlaylistSelectTabControl.TabIndex of
                //Ticker
             0: DeletePlaylistFromSchedule(TICKER, dmMain.tblScheduled_Ticker_Groups.FieldByName('Start_Enable_Time').AsFloat, StationID);
                //Alerts
             1: DeletePlaylistFromSchedule(BREAKINGNEWS, dmMain.tblScheduled_BreakingNews_Groups.FieldByName('Start_Enable_Time').AsFloat, StationID);
            end;
          end
          else OKToGo := FALSE;
        end;

        //Check to make sure there are no overlapping tickers; if so, alert operator and promot for continue
        if (OKToGo = TRUE) then
        begin
          EncodedStartEnableTimeStr := DateTimeToStr(EncodedStartEnableTime);
          EncodedEndEnableTimeStr := DateTimeToStr(EncodedEndEnableTime);
          dmMain.Query1.Close;
          dmMain.Query1.SQL.Clear;
          //Set the query string
          Case PlaylistSelectTabControl.TabIndex of
              //Ticker
           0: QueryStr := 'SELECT * FROM Scheduled_Ticker_Groups WHERE ' +
                'Station_ID = ' + IntToStr(StationID) + ' AND ' +
                'NOT (((' + QuotedStr(EncodedStartEnableTimeStr) + '> Start_Enable_Time) AND (' +
                            QuotedStr(EncodedStartEnableTimeStr) + '> End_Enable_Time)) OR ' +
                     '((' + QuotedStr(EncodedEndEnableTimeStr) + '< Start_Enable_Time) AND (' +
                            QuotedStr(EncodedEndEnableTimeStr) + '< End_Enable_Time)))';
          end;
          dmMain.Query1.SQL.Add(QueryStr);
          dmMain.Query1.Open;
          //Get the count
          OverlapCount := dmMain.Query1.RecordCount;
          //Close the query and return
          dmMain.Query1.Close;
          //Alert if any overlapping playlists were found
          if (OverlapCount > 0) then
          begin
            Control := MessageDlg('One or more playlists were found in the database that overlap all or part ' +
                                  'of the time window specified for this playlist and for the specified Playout Stations. ' +
                                  'Do you wish to schedule this new playlist anyway?', mtWarning, [mbYes, mbNo], 0);
            if (Control = mrNo) then OKToGo := FALSE;
          end;
        end;

        //If no problems, append the record
        if (OKToGo) then
        begin
          Case PlaylistSelectTabControl.TabIndex of
              //Ticker
           0: begin
                //Append new record for playlist
                dmMain.tblScheduled_Ticker_Groups.AppendRecord ([
                  dmMain.tblTicker_Groups.FieldByName('Playlist_ID').AsFloat, //Playlist ID
                  dmMain.tblTicker_Groups.FieldByName('Station_ID').AsInteger, //Playlist ID
                  dmMain.tblTicker_Groups.FieldByName('Playlist_Description').AsString, //Playlist ID
                  EncodedStartEnableTime, //Start date/time
                  EncodedEndEnableTime, //End date/time
                  dmMain.tblTicker_Groups.FieldByName('Ticker_Mode').AsInteger, //1-Line/2-Line ticker mode
                  Now, //Creation date/timestamp
                  'N/A' //Operator name
                ]);
                //Refresh the table
                RefreshScheduleGrid;
              end;
              //Alerts
           1: begin
                //Append new record for playlist
                //dmMain.tblScheduled_BreakingNews_Groups.AppendRecord ([
                //  dmMain.tblBreakingNews_Groups.FieldByName('Playlist_ID').AsFloat, //Playlist ID
                //  dmMain.tblBreakingNews_Groups.FieldByName('Playlist_Description').AsString, //Playlist ID
                //  EncodedStartEnableTime, //Start date/time
                //  EncodedEndEnableTime, //End date/time
                //  dmMain.tblBreakingNews_Groups.FieldByName('BreakingNews_Mode').AsInteger,
                //  Now, //Creation date/timestamp
                //  'N/A' //Operator name
                //]);
                //Refresh the table
                RefreshScheduleGrid;
              end;
          end;
        end;
      except
        MessageDlg ('Error occurred while trying to insert playlist into schedule database.',
                     mtError, [mbOk], 0);
      end;
    end
    else begin
      MessageDlg('There must be at least one playlist in the database to save out to the schedule.',
                 mtInformation, [mbOk], 0);
    end;
  end;
end;

//Procedure to delete a specified stack from the database
procedure TMainForm.DeletePlaylistFromSchedule (PlaylistType: SmallInt; Start_Enable_Time: Double; Station_ID: SmallInt);
begin
  Case PlaylistType of
      //Ticker
   TICKER: begin
        try
          //Refresh table
          dmMain.tblScheduled_Ticker_Groups.Active := FALSE;
          dmMain.tblScheduled_Ticker_Groups.Active := TRUE;
          //Do the delete
          if (dmMain.tblScheduled_Ticker_Groups.RecordCount > 0) then
          begin
            dmMain.tblScheduled_Ticker_Groups.First;
            While(dmMain.tblScheduled_Ticker_Groups.EOF = FALSE) do
            begin
              if (dmMain.tblScheduled_Ticker_Groups.FieldByName('Start_Enable_Time').AsFloat = Start_Enable_Time) AND
                 (dmMain.tblScheduled_Ticker_Groups.FieldByName('Station_ID').AsInteger = Station_ID) then
              begin
                dmMain.tblScheduled_Ticker_Groups.Delete;
              end
              else dmMain.tblScheduled_Ticker_Groups.Next;
            end;
          end;
        except
          MessageDlg ('Error occurred while trying to delete the ticker playlist from the schedule database.',
                       mtError, [mbOk], 0);
        end;
      end;
      //Alerts
   BREAKINGNEWS: begin
        try
          //Refresh table
          //dmMain.tblScheduled_BreakingNews_Groups.Active := FALSE;
          //dmMain.tblScheduled_BreakingNews_Groups.Active := TRUE;
          //Do the delete
          //if (dmMain.tblScheduled_BreakingNews_Groups.RecordCount > 0) then
          //begin
          //  dmMain.tblScheduled_BreakingNews_Groups.First;
          //  While(dmMain.tblScheduled_BreakingNews_Groups.EOF = FALSE) do
          //  begin
          //    if (dmMain.tblScheduled_BreakingNews_Groups.FieldByName('Start_Enable_Time').AsFloat = Start_Enable_Time) then
          //    begin
          //      dmMain.tblScheduled_BreakingNews_Groups.Delete;
          //    end
          //    else dmMain.tblScheduled_BreakingNews_Groups.Next;
          //  end;
          //end;
        except
          MessageDlg ('Error occurred while trying to delete the Alerts playlist from the schedule database.',
                       mtError, [mbOk], 0);
        end;
      end;
  end;
end;

//Handler for Delete Ticker Playlist from Schedule button
procedure TMainForm.BitBtn44Click(Sender: TObject);
begin
  DeleteSelectedScheduleEntry;
end;
procedure TMainForm.DeleteSelectedScheduleEntry1Click(Sender: TObject);
begin
  DeleteSelectedScheduleEntry;
end;
procedure TMainForm.DeleteSelectedScheduleEntry;
var
  Control: Word;
begin
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker
   0: begin
        if (dmMain.tblScheduled_Ticker_Groups.RecordCount = 1) then
        begin
          Control := MessageDlg ('There must always be at least one ticker playlist in the schedule.',
                                  mtInformation, [mbOk], 0);
        end
        else if (dmMain.tblScheduled_Ticker_Groups.RecordCount > 1) then
        begin
          //Confirm
          Control := MessageDlg ('Are you sure you want to delete the selected ticker playlist schedule entry?',
                                  mtConfirmation, [mbYes, mbNo], 0);
          if (Control = mryes) then
            DeletePlaylistFromSchedule (1, dmMain.tblScheduled_Ticker_Groups.FieldByName('Start_Enable_Time').AsFloat,
              dmMain.tblScheduled_Ticker_Groups.FieldByName('Station_ID').AsInteger);
        end;
      end;
      //Alerts
   1: begin
        //if (dmMain.tblScheduled_BreakingNews_Groups.RecordCount = 1) then
        //begin
        //  Control := MessageDlg ('There must always be at least one BreakingNews playlist in the schedule.',
        //                          mtInformation, [mbOk], 0);
        //end
        //else if (dmMain.tblScheduled_BreakingNews_Groups.RecordCount > 1) then
        //begin
        //  //Confirm
        //  Control := MessageDlg ('Are you sure you want to delete the selected Alerts playlist schedule entry?',
        //                          mtConfirmation, [mbYes, mbNo], 0);
        //  if (Control = mryes) then
        //    DeletePlaylistFromSchedule (2, dmMain.tblScheduled_BreakingNews_Groups.FieldByName('Start_Enable_Time').AsFloat);
        //end;
      end;
  end;
end;

//Procedure to refresh the schedule grid
procedure TMainForm.RefreshScheduleGrid;
var
  SaveID: Double;
begin
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker
   0: begin
        //Save the current record being pointed to in the grid
        SaveID := dmMain.tblScheduled_Ticker_Groups.FieldByName('Playlist_ID').AsFloat;
        //Refresh the table
        dmMain.tblScheduled_Ticker_Groups.Active := FALSE;
        dmMain.tblScheduled_Ticker_Groups.Active := TRUE;
        //Search to the saved record
        dmMain.tblScheduled_Ticker_Groups.Locate('Playlist_ID', SaveID, []);
      end;
      //Alerts
   1: begin
        //Save the current record being pointed to in the grid
        //SaveID := dmMain.tblScheduled_BreakingNews_Groups.FieldByName('Playlist_ID').AsFloat;
        //Refresh the table
        //dmMain.tblScheduled_BreakingNews_Groups.Active := FALSE;
        //dmMain.tblScheduled_BreakingNews_Groups.Active := TRUE;
        //Search to the saved record
        //dmMain.tblScheduled_BreakingNews_Groups.Locate('Playlist_ID', SaveID, []);
      end;
  end;
end;

//Handler for menu selection to purge expired scheduled playlists
procedure TMainForm.PurgeScheduledEvents1DayOld1Click(Sender: TObject);
var
  Control: Word;
begin
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker
   0: begin
        //Refresh
        dmMain.tblScheduled_Ticker_Groups.Active := FALSE;
        dmMain.tblScheduled_Ticker_Groups.Active := TRUE;
        //Confirm
        if (dmMain.tblScheduled_Ticker_Groups.RecordCount > 0) then
        begin
          Control := MessageDlg ('Are you sure you want to delete the expired schedule events from the ticker schedule?',
                                  mtConfirmation, [mbYes, mbNo], 0);
          if (Control = mrYes) then
          try
            //Delete the expired events
            if (dmMain.tblScheduled_Ticker_Groups.RecordCount > 0) then
            begin
              Repeat
                if (dmMain.tblScheduled_Ticker_Groups.FieldByName('End_Enable_Time').AsFloat < Now) then
                dmMain.tblScheduled_Ticker_Groups.Delete
              else dmMain.tblScheduled_Ticker_Groups.Next;
              Until (dmMain.tblScheduled_Ticker_Groups.EOF = TRUE);
              //Refresh the table
              RefreshScheduleGrid;
            end;
          finally
          end;
        end;
      end;
      //Alerts
   1: begin
        //Refresh
        //dmMain.tblScheduled_BreakingNews_Groups.Active := FALSE;
        //dmMain.tblScheduled_BreakingNews_Groups.Active := TRUE;
        //Confirm
        //if (dmMain.tblScheduled_BreakingNews_Groups.RecordCount > 0) then
        //begin
        //  Control := MessageDlg ('Are you sure you want to delete the expired schedule events from the BreakingNews schedule?',
        //                          mtConfirmation, [mbYes, mbNo], 0);
        //  if (Control = mrYes) then
        //  try
        //    //Delete the expired events
        //    if (dmMain.tblScheduled_BreakingNews_Groups.RecordCount > 0) then
        //    begin
        //      Repeat
        //        if (dmMain.tblScheduled_BreakingNews_Groups.FieldByName('End_Enable_Time').AsFloat < Now) then
        //        dmMain.tblScheduled_BreakingNews_Groups.Delete
        //      else dmMain.tblScheduled_BreakingNews_Groups.Next;
        //      Until (dmMain.tblScheduled_BreakingNews_Groups.EOF = TRUE);
        //      //Refresh the table
        //      RefreshScheduleGrid;
        //    end;
        //  finally
        //  end;
        //end;
      end;
  end;
end;

//Handler for edit selected schedule entry
procedure TMainForm.BitBtn43Click(Sender: TObject);
begin
  EditCurrentScheduleEntry;
end;
procedure TMainForm.ScheduledPlaylistsGridDblClick(Sender: TObject);
begin
  EditCurrentScheduleEntry;
end;
//Procedure to edit the current schedule entry
procedure TMainForm.EditCurrentScheduleEntry;
var
  OKToGo: Boolean;
  Modal: TScheduleEntryEditDlg;
  Start_Enable_Time, End_Enable_Time: TDateTime;
  StartHour, StartMinute, EndHour, EndMinute, Sec, mSec: Word;
  Control: Word;
begin
  //Init
  OKToGo := TRUE;
  //Setup and launch editing dialog
  Modal := TScheduleEntryEditDlg.Create(Application);
  try
    //Set control values on dialog
    Case PlaylistSelectTabControl.TabIndex of
        //Ticker
     0: begin
          Start_Enable_Time := dmMain.tblScheduled_Ticker_Groups.FieldByName('Start_Enable_Time').AsDateTime;
          End_Enable_Time := dmMain.tblScheduled_Ticker_Groups.FieldByName('End_Enable_Time').AsDateTime;
        end;
        //Alerts
     1: begin
          //Start_Enable_Time := dmMain.tblScheduled_BreakingNews_Groups.FieldByName('Start_Enable_Time').AsDateTime;
          //End_Enable_Time := dmMain.tblScheduled_BreakingNews_Groups.FieldByName('End_Enable_Time').AsDateTime;
        end;
    end;

    //Set initial value of controls
    Modal.ScheduleEntryStartDate.Date := Trunc(Start_Enable_Time);
    Modal.ScheduleEntryEndDate.Date := Trunc(End_Enable_Time);
    Modal.ScheduleEntryStartTime.Time :=
      (Start_Enable_Time - Trunc(Start_Enable_Time));
    Modal.ScheduleEntryEndTime.Time :=
      (End_Enable_Time - Trunc(End_Enable_Time));

    Control := Modal.ShowModal;
    //Set initial values
  finally
    //Set new values if user didn't cancel out
    if (Control = mrOK) then
    begin
      //Encode start and end enable times
      Start_Enable_Time := Trunc(Modal.ScheduleEntryStartDate.Date) +
        (Modal.ScheduleEntryStartTime.Time - Trunc(Modal.ScheduleEntryStartTime.Time));
      End_Enable_Time := Trunc(Modal.ScheduleEntryEndDate.Date) +
        (Modal.ScheduleEntryEndTime.Time - Trunc(Modal.ScheduleEntryEndTime.Time));

      //Make sure end time is greater than start time
      if (Start_Enable_Time >= End_Enable_Time) then
      begin
        OKToGo := FALSE;
        MessageDlg('You must enter an End Enable Time that occurs AFTER the Start Enable Time',
                   mtError, [mbOK], 0);
      end;

      //If OK, edit the values
      if (OKToGo = TRUE) then
      begin
        try
          //Edit the values
          Case PlaylistSelectTabControl.TabIndex of
              //Ticker
           0: begin
                dmMain.tblScheduled_Ticker_Groups.Edit;
                //Encode start and end enable times
                dmMain.tblScheduled_Ticker_Groups.FieldByName('Start_Enable_Time').AsDateTime := Start_Enable_Time;
                dmMain.tblScheduled_Ticker_Groups.FieldByName('End_Enable_Time').AsDateTime := End_Enable_Time;
                dmMain.tblScheduled_Ticker_Groups.Post;
              end;
              //Alerts
           1: begin
                //dmMain.tblScheduled_BreakingNews_Groups.Edit;
                //dmMain.tblScheduled_BreakingNews_Groups.FieldByName('Start_Enable_Time').AsDateTime := Start_Enable_Time;
                //dmMain.tblScheduled_BreakingNews_Groups.FieldByName('End_Enable_Time').AsDateTime := End_Enable_Time;
                //dmMain.tblScheduled_BreakingNews_Groups.Post;
              end;
          end;
          //Refresh the grid
          RefreshSchedulegrid;
        except
          MessageDlg('Error occurred while trying to edit the schedule entry in the database.',
                      mtError, [mbOK], 0);
        end;
      end;
    end;
    Modal.Free
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// PROCEDURES AND HANDLERS FOR DATABASE MAINTENANCE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Refresh database maintenance playlist list grid
procedure TMainForm.BitBtn32Click(Sender: TObject);
begin
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker playlists
   0: begin
        dmMain.tblTicker_Groups.Active := FALSE;
        dmMain.tblTicker_Groups.Active := TRUE;
      end;
      //Alerts playlists
   1: begin
        dmMain.tblBreakingNews_Groups.Active := FALSE;
        dmMain.tblBreakingNews_Groups.Active := TRUE;
      end;
  end;
end;

//Handler for deleting selected zipper group entry from database
procedure TMainForm.BitBtn34Click(Sender: TObject);
begin
  DeleteSelectedPlaylist;
end;
procedure TMainForm.DeletePlaylist1Click(Sender: TObject);
begin
  DeleteSelectedPlaylist;
end;
procedure TMainForm.DeleteSelectedPlaylist;
var
  MyPlaylistID: Double;
  PlaylistIsScheduled: Boolean;
  Control: Word;
  OKToGo: Boolean;
begin
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker playlists
   0: begin
        try
          OKToGo := TRUE;
          if (dmMain.tblTicker_Groups.RecordCount > 0) then
          begin
            MyPlaylistID := dmMain.tblTicker_Groups.FieldByName('Playlist_ID').AsFloat;
            //First, check to see if this playlist is scheduled and alert operator if so
            PlaylistIsScheduled := dmMain.tblScheduled_Ticker_Groups.Locate('Playlist_ID', MyPlaylistID, []);
            if (PlaylistIsScheduled) then
            begin
              Control := MessageDlg('At least one schedule entry has been found that includes this ticker playlist. ' +
                                    'You must delete any associated ticker schedule entries before deleting the ' +
                                    'playlist.', mtError, [mbOk], 0);
              OKToGo := FALSE;
            end;
            if (OkToGo = TRUE) then
            begin
              //Alert operator
              Control := MessageDlg('Are you sure you want to delete the selected ticker playlist?',
                         mtConfirmation, [mbYes, mbNo], 0);
              if (Control = mrYes) then
              begin
                //Delete the entry in the stacks table
                dmMain.tblTicker_Groups.Delete;
                dmMain.tblTicker_Groups.Active := FALSE;
                dmMain.tblTicker_Groups.Active := TRUE;
                //Delete any associated events
                if (dmMain.tblTicker_Elements.RecordCount > 0) then
                begin
                  dmMain.tblTicker_Elements.First;
                  While(dmMain.tblTicker_Elements.EOF = FALSE) do
                  begin
                    if (dmMain.tblTicker_Elements.FieldByName('Playlist_ID').AsString = FloatToStr(MyPlaylistID)) then
                    begin
                      dmMain.tblTicker_Elements.Delete;
                    end
                    else dmMain.tblTicker_Elements.Next;
                  end;
                end;
              end;
            end;
          end;
        except
          //Alert operator
          MessageDlg('An error occurred while trying to delete the ticker playlist.',
                      mtError, [mbOK], 0);
        end;
      end;
      //Alerts playlists
   1: begin
        try
          OKToGo := TRUE;
          if (dmMain.tblBreakingNews_Groups.RecordCount > 0) then
          begin
            MyPlaylistID := dmMain.tblBreakingNews_Groups.FieldByName('Playlist_ID').AsFloat;
            //First, check to see if this playlist is scheduled and alert operator if so
            //PlaylistIsScheduled := dmMain.tblScheduled_BreakingNews_Groups.Locate('Playlist_ID', MyPlaylistID, []);
            //if (PlaylistIsScheduled) then
            //begin
            //  Control := MessageDlg('At least one schedule entry has been found that includes this BreakingNews playlist. ' +
            //                        'You must delete any associated schedule entries before deleting the ' +
            //                        'BreakingNews playlist.', mtError, [mbOk], 0);
            //  OKToGo := FALSE;
            //end;
            if (OkToGo = TRUE) then
            begin
              //Alert operator
              Control := MessageDlg('Are you sure you want to delete the selected BreakingNews playlist?',
                         mtConfirmation, [mbYes, mbNo], 0);
              if (Control = mrYes) then
              begin
                //Delete the entry in the stacks table
                dmMain.tblBreakingNews_Groups.Delete;
                dmMain.tblBreakingNews_Groups.Active := FALSE;
                dmMain.tblBreakingNews_Groups.Active := TRUE;
                //Delete any associated events
                if (dmMain.tblBreakingNews_Elements.RecordCount > 0) then
                begin
                  dmMain.tblBreakingNews_Elements.First;
                  While(dmMain.tblBreakingNews_Elements.EOF = FALSE) do
                  begin
                    if (dmMain.tblBreakingNews_Elements.FieldByName('Playlist_ID').AsString = FloatToStr(MyPlaylistID)) then
                    begin
                      dmMain.tblBreakingNews_Elements.Delete;
                    end
                    else dmMain.tblBreakingNews_Elements.Next;
                  end;
                end;
              end;
            end;
          end;
        except
          //Alert operator
          MessageDlg('An error occurred while trying to delete the Alerts playlist.',
                      mtError, [mbOK], 0);
        end;
      end;
  end; //Case
end;

procedure TMainForm.TimeOfDayTimerTimer(Sender: TObject);
begin
  DateTimeLabel.Caption := DateTimeToStr(Now);
end;

//Handler for hotkeys for playlist operations (cut, copy, paste, duplicate) - Playlist Grid #1
procedure TMainForm.PlaylistGridKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (ssCtrl in Shift) and (Key = Ord('X')) then Cut(FIELD_1, PlaylistGrid)
  else if (ssCtrl in Shift) and (Key = Ord('C')) then Copy(FIELD_1, PlaylistGrid)
  else if (ssCtrl in Shift) and (Key = Ord('V')) then Paste(FIELD_1, PlaylistGrid)
  else if (ssCtrl in Shift) and (Key = Ord('E')) then PasteEnd(FIELD_1, PlaylistGrid)
  else if (ssCtrl in Shift) and (Key = Ord('D')) then Duplicate(FIELD_1, PlaylistGrid)
  else if (ssCtrl in Shift) and (Key = Ord('A')) then SelectDeselectAll(SELECT_ALL, FIELD_1, PlaylistGrid)
  else if (ssCtrl in Shift) and (Key = Ord('U')) then SelectDeSelectAll(DESELECT_ALL, FIELD_1, PlaylistGrid)
  else if (ssShift in Shift) and (Key = VK_UP) then MoveRecord(UP, FIELD_1, PlaylistGrid)
  else if (ssShift in Shift) and (Key = VK_DOWN) then MoveRecord(DOWN,FIELD_1, PlaylistGrid)
  else if (Key = VK_DELETE) then DeleteGraphicFromZipperPlaylist(TRUE, FIELD_1, PlaylistGrid);
end;

//Handler for hotkeys for playlist operations (cut, copy, paste, duplicate) - Playlist Grid #2
procedure TMainForm.PlaylistGrid2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (ssCtrl in Shift) and (Key = Ord('X')) then Cut(FIELD_2, PlaylistGrid2)
  else if (ssCtrl in Shift) and (Key = Ord('C')) then Copy(FIELD_2, PlaylistGrid2)
  else if (ssCtrl in Shift) and (Key = Ord('V')) then Paste(FIELD_2, PlaylistGrid2)
  else if (ssCtrl in Shift) and (Key = Ord('E')) then PasteEnd(FIELD_2, PlaylistGrid2)
  else if (ssCtrl in Shift) and (Key = Ord('D')) then Duplicate(FIELD_2, PlaylistGrid2)
  else if (ssCtrl in Shift) and (Key = Ord('A')) then SelectDeSelectAll(SELECT_ALL, FIELD_2, PlaylistGrid2)
  else if (ssCtrl in Shift) and (Key = Ord('U')) then SelectDeSelectAll(DESELECT_ALL, FIELD_2, PlaylistGrid2)
  else if (ssShift in Shift) and (Key = VK_UP) then MoveRecord(UP, FIELD_2, PlaylistGrid2)
  else if (ssShift in Shift) and (Key = VK_DOWN) then MoveRecord(DOWN, FIELD_2, PlaylistGrid2)
  else if (Key = VK_DELETE) then DeleteGraphicFromZipperPlaylist(TRUE, FIELD_2, PlaylistGrid2);
end;

//Handler for Select ALL
//Field 1
procedure TMainForm.SelectAll1Click(Sender: TObject);
begin
  SelectDeSelectAll(SELECT_ALL, FIELD_1, PlaylistGrid);
end;
//Field 2
procedure TMainForm.SelectAll2Click(Sender: TObject);
begin
  SelectDeSelectAll(SELECT_ALL, FIELD_2, PlaylistGrid2);
end;
//Handler for De-Select ALL
//Field 1
procedure TMainForm.DeSelectAll1Click(Sender: TObject);
begin
  SelectDeSelectAll(DESELECT_ALL, FIELD_1, PlaylistGrid);
end;
//Field 2
procedure TMainForm.DeSelectAll2Click(Sender: TObject);
begin
  SelectDeSelectAll(DESELECT_ALL, FIELD_2, PlaylistGrid2);
end;
procedure TMainForm.SelectDeselectAll(SelectMode: Boolean; GridID: SmallInt; var DataGrid: TtsGrid);
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  BreakingNewsRecPtr: ^BreakingNewsRec;
  CollectionCount: SmallInt;
begin
  Case PlaylistSelectTabControl.TabIndex of
      //Ticker playlists
   0: begin
        Case GridID of
          FIELD_1: CollectionCount := Ticker_Collection.Count;
          FIELD_2: CollectionCount := Ticker_Collection2.Count;
        end;
        if (CollectionCount > 0) then
        begin
          for i := 0 to CollectionCount-1 do
          begin
            Case GridID of
              FIELD_1: TickerRecPtr := Ticker_Collection.At(i);
              FIELD_2: TickerRecPtr := Ticker_Collection2.At(i);
            end;
            DataGrid.RowSelected[i+1] := SelectMode;
            TickerRecPtr^.Selected := SelectMode;
          end;
        end;
      end;
      //Alerts playlists
   1: begin
        if (BreakingNews_Collection.Count > 0) then
        begin
          for i := 0 to BreakingNews_Collection.Count-1 do
          begin
            BreakingNewsRecPtr := BreakingNews_Collection.At(i);
            PlaylistGrid.RowSelected[i+1] := SelectMode;
            BreakingNewsRecPtr^.Selected := SelectMode;
          end;
        end;
      end;
  end;
end;

//Handler for click on cell - draws current page
procedure TMainForm.PlaylistGridClick(Sender: TObject);
begin
  PreviewCurrentPlaylistRecord(FIELD_1);
end;
procedure TMainForm.PlaylistGrid2Click(Sender: TObject);
begin
  PreviewCurrentPlaylistRecord(FIELD_2);
end;

//Procedure for previewing the currently selected record in the playlist
procedure TMainForm.PreviewCurrentPlaylistRecord(GridID: SmallInt);
var
  SaveTop: LongInt;
begin
  //Save the top row
  SaveTop := PlaylistGrid.TopRow;
  if (PlaylistGrid.CurrentDataRow <= 0) then PlaylistGrid.CurrentDataRow := 1;
  Case PlaylistSelectTabControl.TabIndex of
   0: begin
        Case GridID of
          FIELD_1: begin
            PlaylistGrid.SetFocus;
            EngineInterface.SendTickerRecord(PLAYLIST_1, FALSE, PlaylistGrid.CurrentDataRow-1);
          end;
          FIELD_2: begin
            PlaylistGrid2.SetFocus;
            EngineInterface.SendTickerRecord(PLAYLIST_2, FALSE, PlaylistGrid2.CurrentDataRow-1);
          end;
        end;
      end;
  end;
  //Restore the top row
  PlaylistGrid.TopRow := SaveTop;
end;

//Handler for arrow up/down when playlist grid has the focus
procedure TMainForm.FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  //Disable these keys in remote mode
  if not (InRemotePlayoutMode) then
  begin
    if ((Key = VK_UP) OR (Key = VK_DOWN)) AND (PlaylistGrid.IsFocused) then
    begin
      if (PlaylistGrid.CurrentDataRow <= 0) then PlaylistGrid.CurrentDataRow := 1;
      Case PlaylistSelectTabControl.TabIndex of
       0: begin
            EngineInterface.SendTickerRecord(PLAYLIST_1, FALSE, PlaylistGrid.CurrentDataRow-1);
          end;
       1: begin
  //          EngineInterface.SendBugRecord(FALSE, PlaylistGrid.CurrentDataRow-1);
          end;
      end;
    end
    else if ((Key = VK_UP) OR (Key = VK_DOWN)) AND (PlaylistGrid2.IsFocused) then
    begin
      if (PlaylistGrid2.CurrentDataRow <= 0) then PlaylistGrid2.CurrentDataRow := 1;
      Case PlaylistSelectTabControl.TabIndex of
       0: begin
            EngineInterface.SendTickerRecord(PLAYLIST_2, FALSE, PlaylistGrid2.CurrentDataRow-1);
          end;
       1: begin
  //          EngineInterface.SendBugRecord(FALSE, PlaylistGrid.CurrentDataRow-1);
          end;
      end;
    end;
  end;  
end;

//Handler to reconnect to graphics engine
procedure TMainForm.ReconnecttoGraphicsEngine1Click(Sender: TObject);
begin
  //Connect to the graphics engine if it's enabled
  if (EngineParameters.Enabled) then
  begin
    try
      EngineParameters.Enabled := TRUE;
      EngineInterface.EnginePort.Address := EngineParameters.IPAddress;
      EngineInterface.EnginePort.Port := StrToIntDef(EngineParameters.Port, 7795);
      EngineInterface.EnginePort.Active := TRUE;
    except
      if (ErrorLoggingEnabled = True) then
      begin
        Error_Condition := True;
        Label16.Caption := 'ERROR';
        //WriteToErrorLog
        EngineInterface.WriteToErrorLog('Error occurred while trying connect to database engine');
      end;
    end;
  end;
end;

//Handler for template change - sets dwell time
procedure TMainForm.AvailableTemplatesDBGridRowChanged(Sender: TObject; OldRow, NewRow: Variant);
begin
  EntryDwellTime.Value :=
    Trunc(GetTemplateInformation(dmMain.Query2.FieldByName('Template_ID').AsInteger).Default_Dwell/1000);
end;

//Hanlder for alerts checkbox
procedure TMainForm.ImmediateModeBreakingNewsClick(Sender: TObject);
begin
  if (ImmediateModeBreakingNews.Checked) then
  begin
    ImmediateModeBreakingNews.Color := clRed;
    PlaylistModeLabel.Color := clRed;
    PlaylistModeLabel.Caption := 'IMMEDIATE MODE';
  end
  else begin
    ImmediateModeBreakingNews.Color := clBtnFace;
    PlaylistModeLabel.Color := clSkyBlue;
    PlaylistModeLabel.Caption := 'ALERTS';
  end;
end;

//Handlers to clear graphics from CAL output
procedure TMainForm.ClearGraphicsOutput1Click(Sender: TObject);
begin
  ClearAllTemplates;
end;
procedure TMainForm.ClearGraphicsOutput2Click(Sender: TObject);
begin
  ClearAllTemplates;
end;
//Procedure to clear all graphics; sends transition out to all templates; removes backplates and
//resets last template variables for all regions
procedure TMainForm.ClearAllTemplates;
var
  i: SmallInt;
  TemplateDefsRecPtr: ^TemplateDefsRec;
begin
  if (Template_Defs_Collection.Count > 0) then
  begin
    for i := 0 to Template_Defs_Collection.Count-1 do
    begin
      TemplateDefsRecPtr := Template_Defs_Collection.At(i);
      if (TemplateDefsRecPtr^.Template_Type > 0) then
      begin
        EngineInterface.UpdateScene(TemplateDefsRecPtr^.Scene_ID_1, TemplateDefsRecPtr^.Transition_Out + '�' + 'T');
        EngineInterface.UpdateScene(TemplateDefsRecPtr^.Scene_ID_2, TemplateDefsRecPtr^.Transition_Out + '�' + 'T');
      end;
    end;
  end;
  //Clear backplates
  EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_1_Out_Command + '�' + 'T');
  EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_2_Out_Command + '�' + 'T');
  EngineInterface.UpdateScene(BaseScene_SceneID, BaseScene_Field_3_Out_Command + '�' + 'T');
  //Clear out the last template info
  LastTemplateInfo[PLAYLIST_1].LastFieldTypeID := STARTUP;
  LastTemplateInfo[PLAYLIST_1].Scene_ID := 0;
  LastTemplateInfo[PLAYLIST_2].LastFieldTypeID := STARTUP;
  LastTemplateInfo[PLAYLIST_2].Scene_ID := 0;
end;

//Handler to clear all templates remotely
procedure TMainForm.ClearAllTemplatesOnAir1Click(Sender: TObject);
begin
  PlayoutInterface.ClearAllGraphics;
end;

//Handler to re-load all scenes
procedure TMainForm.ReloadAllScenes1Click(Sender: TObject);
begin
  //Load scenes
  EngineInterface.SetupInitialScenes;
  //Run startup commands
  RunStartupCommands;
  //Clear all graphics
  ClearAllTemplates;
end;

////////////////////////////////////////////////////////////////////////////////
// Handler for editing default start/end enable times for selected template
////////////////////////////////////////////////////////////////////////////////
procedure TMainForm.EditDefaultTemplateStartEndEnableTimes1Click(Sender: TObject);
var
  i: SmallInt;
  CurrentTemplateID: SmallInt;
  MyTemplateIndex: SmallInt;
  Modal: TScheduleEntryEditDlg;
  Control: Word;
  TemplateDefsRecPtr, TemplateDefsRecPtr2: ^TemplateDefsRec;
  CollectionCount: SmallInt;
  FoundRecord: Boolean;
begin
  //Get ID of current template
  CurrentTemplateID := dmMain.Query2.FieldByName('Template_ID').AsInteger;
  //Find index of template record in collection
  if (Template_Defs_Collection.Count > 0) then
  begin
    for i := 0 to Template_Defs_Collection.Count-1 do
    begin
      TemplateDefsRecPtr := Template_Defs_Collection.At(i);
      if (TemplateDefsRecPtr^.Template_ID = CurrentTemplateID) then
        MyTemplateIndex := i;
    end;
  end;
  //Get the pointer to the selected template in the collection
  TemplateDefsRecPtr := Template_Defs_Collection.At(MyTemplateIndex);
  //Setup and launch editing dialog if applicable
  try
    Modal := TScheduleEntryEditDlg.Create(Application);
    Modal.Caption := 'Template Default Enable Time Editor';
    Modal.DefaultDwellTimeLabel.Visible := TRUE;
    Modal.DefaultDwellTime.Visible := TRUE;
    Modal.ScheduleEntryStartDate.Date := TemplateDefsRecPtr^.StartEnableDateTime;
    Modal.ScheduleEntryStartTime.Time := TemplateDefsRecPtr^.StartEnableDateTime;
    Modal.ScheduleEntryEndDate.Date := TemplateDefsRecPtr^.EndEnableDateTime;
    Modal.ScheduleEntryEndTime.Time := TemplateDefsRecPtr^.EndEnableDateTime;
    Modal.DefaultDwellTime.Value := Trunc(TemplateDefsRecPtr^.Default_Dwell/1000);
    //Show the dialog
    Control := Modal.ShowModal;
  finally
    //Set new values if user didn't cancel out
    if (Control = mrOK) then
    begin
      //Update the values in the collection
      TemplateDefsRecPtr^.StartEnableDateTime := Trunc(Modal.ScheduleEntryStartDate.Date) +
          (Modal.ScheduleEntryStartTime.Time - Trunc(Modal.ScheduleEntryStartTime.Time));
      TemplateDefsRecPtr^.EndEnableDateTime := Trunc(Modal.ScheduleEntryEndDate.Date) +
          (Modal.ScheduleEntryEndTime.Time - Trunc(Modal.ScheduleEntryEndTime.Time));
      TemplateDefsRecPtr^.Default_Dwell := Modal.DefaultDwellTime.Value*1000;
      //Update the database values
      dmMain.tblTemplate_Defs.Active := FALSE;
      dmMain.tblTemplate_Defs.Active := TRUE;
      if (dmMain.tblTemplate_Defs.RecordCount > 0) then
      begin
        FoundRecord := dmMain.tblTemplate_Defs.Locate('Template_ID', CurrentTemplateID, []);
        if (FoundRecord) then
        begin
          dmMain.tblTemplate_Defs.Edit;
          dmMain.tblTemplate_Defs.FieldByName('StartEnableTime').AsDateTime := TemplateDefsRecPtr^.StartEnableDateTime;
          dmMain.tblTemplate_Defs.FieldByName('EndEnableTime').AsDateTime := TemplateDefsRecPtr^.EndEnableDateTime;
          dmMain.tblTemplate_Defs.FieldByName('Default_Dwell').AsInteger := TemplateDefsRecPtr^.Default_Dwell;
          dmMain.tblTemplate_Defs.Post;
        end;
      end;
      dmMain.tblTemplate_Defs.Active := FALSE;
    end;
    Modal.Free;
  end;
end;

//Handler for launching sponsor logo editor
procedure TMainForm.EditSponsorLogoDefinitions1Click(Sender: TObject);
var
  Modal: TSponsorLogoEditorDlg;
  Control: Word;
begin
  //Refresh & launch dialog
  dmMain.tblSponsor_Logos.Active := FALSE;
  dmMain.tblSponsor_Logos.Active := TRUE;
  Modal := TSponsorLogoEditorDlg.Create(Application);
  try
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    Modal.Free
  end;
end;

//Handler for launching team info editor
procedure TMainForm.EditTeamInformation1Click(Sender: TObject);
var
  Modal: TDBEditorDlg;
  Control: Word;
begin
  //Refresh & launch dialog
  dmMain.tblTeams.Active := FALSE;
  dmMain.tblTeams.Active := TRUE;
  Modal := TDBEditorDlg.Create(Application);
  try
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    Modal.Free
  end;
end;

//Handler for launching game phase info editor dialog
procedure TMainForm.EditGamePhaseData1Click(Sender: TObject);
var
  Modal: TGamePhaseEditorDlg;
  Control: Word;
begin
  //Refresh & launch dialog
  dmMain.tblGame_Phase_Codes.Active := FALSE;
  dmMain.tblGame_Phase_Codes.Active := TRUE;
  Modal := TGamePhaseEditorDlg.Create(Application);
  try
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    Modal.Free;
    ReloadDataFromDatabase;
    dmMain.tblGame_Phase_Codes.Active := FALSE;
  end;
end;

//Handler to launch editor for custom headers
procedure TMainForm.EditCustomHeaders1Click(Sender: TObject);
var
  Modal: TCustomHeaderEditorDlg;
  Control: Word;
begin
  //Refresh & launch dialog
  dmMain.tblCustom_Headers.Active := FALSE;
  dmMain.tblCustom_Headers.Active := TRUE;
  Modal := TCustomHeaderEditorDlg.Create(Application);
  try
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    Modal.Free;
    ReloadDataFromDatabase;
    dmMain.tblCustom_Headers.Active := FALSE;
  end;
end;

//Handler for manual game override
procedure TMainForm.GamesDBGridDblClick(Sender: TObject);
begin
  ManualGameOverride;
end;

//Function to return the game phase code given the league and the display string
function GetGamePhaseCode(League: String; Display_Period: String): SmallInt;
var
  i, OutVal: SmallInt;
  GamePhaseRecPtr: ^GamePhaseRec;
begin
  OutVal := 0;
  if (Game_Phase_Collection.Count > 0) then
  begin
    for i := 1 to Game_Phase_Collection.Count-1 do
    begin
      GamePhaseRecPtr := Game_Phase_Collection.At(i);
      if (GamePhaseRecPtr^.League = League) AND
         (GamePhaseRecPtr^.Display_Period = Display_Period) then
        OutVal := GamePhaseRecPtr^.SI_Phase_Code;
    end;
  end;
  GetGamePhaseCode := OutVal;
end;

//General procedure for manual game override
//General procedure for manual game override
procedure TMainForm.ManualGameOverride;
var
  i: SmallInt;
  CurrentPhase: SmallInt;
  Modal: TManualGameDlg;
  CategoryRecPtr: ^CategoryRec;
  GamePhaseRecPtr: ^GamePhaseRec;
  Control: Word;
  VisitorOddsID, HomeOddsID: Double;
  TempLeague: String;
begin
  if (dmMain.SportbaseQuery.RecordCount > 0) then
  begin
    Modal := TManualGameDlg.Create(Application);
    try
      //GAME DATA
      //Set initial values
      Modal.SelectedGameLabel.Caption := dmMain.SportbaseQuery.FieldByName('League').AsString + ': ' +
        dmMain.SportbaseQuery.FieldByName('VCity').AsString + ' @ ' +
        dmMain.SportbaseQuery.FieldByName('HCity').AsString;
      Modal.ManualGamesOverrideEnable.Checked := dmMain.SportbaseQuery.FieldByName('Manual').AsBoolean;
      Modal.ManualGamesOverrideEnable.Checked := dmMain.SportbaseQuery.FieldByName('Manual').AsBoolean;
      Modal.VisitorName.Text := dmMain.SportbaseQuery.FieldByName('VCity').AsString + ' ' +
        dmMain.SportbaseQuery.FieldByName('VTeam').AsString;
      Modal.HomeName.Text := dmMain.SportbaseQuery.FieldByName('HCity').AsString + ' ' +
        dmMain.SportbaseQuery.FieldByName('HTeam').AsString;
      Modal.VisitorScore.Value := dmMain.SportbaseQuery.FieldByName('VScore').AsInteger;
      Modal.HomeScore.Value := dmMain.SportbaseQuery.FieldByName('HScore').AsInteger;
      Modal.StartTime.Time :=  dmMain.SportbaseQuery.FieldByName('Start').AsDateTime;
      //Set game state
      if (dmMain.SportbaseQuery.FieldByName('State').AsString = 'Pre-Game') then
        Modal.GameState.ItemIndex := 0
      else if (dmMain.SportbaseQuery.FieldByName('State').AsString = 'In-Progress') then
        Modal.GameState.ItemIndex := 1
      else
        Modal.GameState.ItemIndex := 2;
      //Set game phase and populate dropdown
      if (Game_Phase_Collection.Count > 0) then
      begin
        for i := 0 to Game_Phase_Collection.Count-1 do
        begin
          GamePhaseRecPtr := Game_Phase_Collection.At(i);
          if (GamePhaseRecPtr^.League = dmMain.SportbaseQuery.FieldByName('League').AsString) then
          begin
            Modal.GamePhase.Items.Add(GamePhaseRecPtr^.Display_Period);
            if (dmMain.SportbaseQuery.FieldByName('Period').AsInteger = Modal.GamePhase.Items.Count) then
              CurrentPhase := Modal.GamePhase.Items.Count;
          end;
        end;
        if (CurrentPhase > 0) then Modal.GamePhase.ItemIndex := CurrentPhase-1;
      end;

      //Show the dialog
      Control := Modal.ShowModal;
    finally
      //Set new values if user didn't cancel out
      if (Control = mrOK) then
      begin
        //GAME DATA
        //Put games table into edit mode
        dmMain.SportbaseQuery.Edit;
        //Set edited fields
        dmMain.SportbaseQuery.FieldByName('Manual').AsBoolean := Modal.ManualGamesOverrideEnable.Checked;
        if (Modal.ManualGamesOverrideEnable.Checked) then
        begin
          dmMain.SportbaseQuery.FieldByName('VScore').AsInteger := Modal.VisitorScore.Value;
          dmMain.SportbaseQuery.FieldByName('HScore').AsInteger := Modal.HomeScore.Value;
          //Set game state
          Case Modal.GameState.ItemIndex of
           0: dmMain.SportbaseQuery.FieldByName('State').AsString := 'Pre-Game';
           1: dmMain.SportbaseQuery.FieldByName('State').AsString := 'In-Progress';
           2: dmMain.SportbaseQuery.FieldByName('State').AsString := 'Final';
          end;
          //Set period
          dmMain.SportbaseQuery.FieldByName('Period').AsInteger := Modal.GamePhase.ItemIndex+1;
          //Set clock values
          //If end of period, set to min, sec = 0 so that proper phase is displayed
          if (Modal.EndOfPeriod.Checked) then
          begin
            dmMain.SportbaseQuery.FieldByName('TimeMin').AsInteger := 0;
            dmMain.SportbaseQuery.FieldByName('TimeSec').AsInteger := 0;
          end
          else begin
            //Set clock values to -1 to flag manual mode; clock will not be shown
            dmMain.SportbaseQuery.FieldByName('TimeMin').AsInteger := -1;
            dmMain.SportbaseQuery.FieldByName('TimeSec').AsInteger := -1;
          end;
          //Set game start time
          dmMain.SportbaseQuery.FieldByName('Start').AsDateTime :=
            Modal.StartTime.Time - Trunc(Modal.StartTime.Time);
        end;
        //Post the edits to the games table
        dmMain.SportbaseQuery.Post;
      end;
      Modal.Free
    end;
  end;
end;

//Function to check to make sure that the current playlist mode matches all of the templates in
//the playlist
function TMainForm.CheckForInvalidTemplates(CurrentDisplayMode: SmallInt): Boolean;
var
  i: SmallInt;
  TickerRecPtr: ^TickerRec;
  OutVal: Boolean;
begin
  //Init
  OutVal := FALSE;
  if (Ticker_Collection.Count > 0) then
  begin
    //Walk the collection
    for i := 0 to Ticker_Collection.Count-1 do
    begin
      TickerRecPtr := Ticker_Collection.At(i);
      //Check display mode against template IDs; all 1-line (1 <= X < 1000) OR (X > 10000); all 2-line (1000 <= X < 10000)
      //Case CurrentDisplayMode of
        //If invalid template ID, set flag
        //1,2: if (TickerRecPtr^.Template_ID > 1000) OR (TickerRecPtr^.Template_ID < 10000) then OutVal := TRUE;
        //3,4: if (TickerRecPtr^.Template_ID < 1000) OR (TickerRecPtr^.Template_ID > 10000)then OutVal := TRUE;
        //1,2: if ((TickerRecPtr^.Template_ID >= 1000) AND (TickerRecPtr^.Template_ID < 10000)) then OutVal := TRUE;
        //3,4: if (TickerRecPtr^.Template_ID < 1000) OR (TickerRecPtr^.Template_ID >= 10000)then OutVal := TRUE;
      //end;
    end;
  end;
  CheckForInvalidTemplates := OutVal;
end;

////////////////////////////////////////////////////////////////////////////////
// REGION SELECT FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Handler for region mode select change
procedure TMainForm.RegionSelectModeClick(Sender: TObject);
begin
  Case RegionSelectMode.ItemIndex of
    //Save as global playlist
    0: begin
         RegionSelectMode.Color := clBtnFace;
         RegionSelect.Color := clBtnFace;
         RegionIDNum.Text := '0';
         RegionIDNum.Color := clBtnFace;
       end;
    //Save as specific region playlist
    1: begin
         RegionSelectMode.Color := clYellow;
         RegionSelect.Color := clYellow;
         RegionIDNum.Text := IntToStr(RegionSelect.ItemIndex+1);
         RegionIDNum.Color := clYellow;
       end;
  end;
end;
//Handler for region select change
procedure TMainForm.RegionSelectChange(Sender: TObject);
begin
  Case RegionSelectMode.ItemIndex of
    //Save as global playlist
    0: begin
         RegionSelectMode.Color := clBtnFace;
         RegionSelect.Color := clBtnFace;
         RegionIDNum.Text := '0';
         RegionIDNum.Color := clBtnFace;
       end;
    //Save as specific region playlist
    1: begin
         RegionSelectMode.Color := clYellow;
         RegionSelect.Color := clYellow;
         RegionIDNum.Text := IntToStr(RegionSelect.ItemIndex+1);
         RegionIDNum.Color := clYellow;
       end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// TEMPLATE SELECT FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Handler for mouse right-click - launches dialog to select templates; added for
//Stats Inc. Phase 2 modifications
procedure TMainForm.GamesDBGridMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Modal: TTemplateSelectDlg;
  i: SmallInt;
  CategoryRecPtr: ^CategoryRec;
  SelectedTemplatesRecPtr: ^DefaultTemplatesRec;
  QueryString: String;
  OddsOnly: SmallInt;
  QueryStr: String;
  Control: Word;
begin
  //if (EnableDefaultTemplates) then
  begin
    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
    //First, check to see if it's a league tab
    if (CategoryRecPtr^.Category_Is_Sport = TRUE) then
    begin
      //Only act on right-click
      if (Button = mbRight) then
      begin
        Modal := TTemplateSelectDlg.Create(Application);
        try
          CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
          //Now, populate the templates grid by querying the database
          dmMain.Query4.Active := FALSE;
          dmMain.Query4.SQL.Clear;
          dmMain.Query4.Filtered := FALSE;
          QueryStr := 'SELECT * FROM Default_Templates WHERE ' +
            'Category_ID = ' + IntToStr(CategoryRecPtr^.Category_ID);
          //Add to query to filter templates based on 1 or 2 line display modes
          if (TickerDisplayMode = 1) OR (TickerDisplayMode = 2) then
            //1-line ticker
            QueryStr := QueryStr + ' AND ((Template_ID < 1000) OR (Template_ID >= 10000))'
          else
            //2-line ticker
            QueryStr := QueryStr + ' AND ((Template_ID >= 1000) AND (Template_ID < 10000))';
          dmMain.Query4.SQL.Add (QueryStr);
          dmMain.Query4.Active := TRUE;
          //Populate checklistbox
          if (dmMain.Query4.RecordCount > 0) then
          begin
            //Clear the temporary templates collection
            Selected_Templates_Collection.Clear;
            Selected_Templates_Collection.Pack;
            for i := 0 to dmMain.Query4.RecordCount-1 do
            begin
              //Add template to listbox
              Modal.TemplateCheckListbox.Items.Add(dmMain.Query4.FieldByName('Template_Description').AsString);
              Modal.TemplateCheckListbox.Checked[i] := dmMain.Query4.FieldByName('Enabled').AsBoolean;
              //Add the template to the temporary collection
              GetMem (SelectedTemplatesRecPtr, SizeOf(DefaultTemplatesRec));
              With SelectedTemplatesRecPtr^ do
              begin
                //Set values for collection record
                Category_ID := dmMain.Query4.FieldByName('Category_ID').AsInteger;
                Category_Name := dmMain.Query4.FieldByName('Category_Name').AsString;
                Template_Index := dmMain.Query4.FieldByName('Template_Index').AsInteger;
                Template_ID := dmMain.Query4.FieldByName('Template_ID').AsInteger;
                Template_Description := dmMain.Query4.FieldByName('Template_Description').AsString;
                Enabled := dmMain.Query4.FieldByName('Enabled').AsBoolean;
                Selected := TRUE;
                //Insert object to collection
                Selected_Templates_Collection.Insert(SelectedTemplatesRecPtr);
                Selected_Templates_Collection.Pack;
              end;
              //Next record
              dmMain.Query4.Next;
            end;
            //Only show if there are templates to list
            Control := Modal.ShowModal;
          end;
        finally
          //Handler for selection of "OK" - Append all enabled templates
          if (Control = mrOK) then
          begin
            //Build collection for all checked templates
            if (Modal.TemplateCheckListBox.Items.Count > 0) then
            begin
              for i := 0 to Modal.TemplateCheckListBox.Items.Count-1 do
              begin
                SelectedTemplatesRecPtr := Selected_Templates_Collection.At(i);
                SelectedTemplatesRecPtr^.Selected := Modal.TemplateCheckListbox.Checked[i];
              end;
              //Call function to add all playlist entries with all default templates
              AddPlaylistEntry(0, ALLDEFAULTTEMPLATES, FIELD_1, PlaylistGrid);
            end;
          end
          //Handler for selection of "YES" - Insert all selected templates
          else if (Control = mrYes) then
          begin
            //Build collection for all checked templates
            if (Modal.TemplateCheckListBox.Items.Count > 0) then
            begin
              for i := 0 to Modal.TemplateCheckListBox.Items.Count-1 do
              begin
                SelectedTemplatesRecPtr := Selected_Templates_Collection.At(i);
                SelectedTemplatesRecPtr^.Selected := Modal.TemplateCheckListbox.Checked[i];
              end;
              //Call function to add all playlist entries with all default templates
              InsertPlaylistEntry(0, ALLDEFAULTTEMPLATES, FIELD_1, PlaylistGrid);
            end;
          end;
          Modal.Free
        end;
      end;
    end;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
// DEFAULT TEMPLATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//General procedure to clear and refresh the defasult templates grid
procedure TMainForm.RefreshDefaultTemplatesGrid;
var
  i, j: SmallInt;
  DefaultTemplatesRecPtr: ^DefaultTemplatesRec;
  CollectionCount: SmallInt;
  CurrentRow, CurrentTopRow: SmallInt;
begin
  //Get current row
  CurrentRow := DefaultTemplatesGrid.CurrentDataRow;
  CurrentTopRow := DefaultTemplatesGrid.TopRow;

  //Clear grid values
  if (DefaultTemplatesGrid.Rows > 0) then
    DefaultTemplatesGrid.DeleteRows (1, DefaultTemplatesGrid.Rows);

  //Init values
  CollectionCount := Default_Templates_Collection.Count;

  if (CollectionCount > 0) then
  begin
    DefaultTemplatesGrid.StoreData := TRUE;
    DefaultTemplatesGrid.Cols := 6;
    j := 0;
    //Populate the grid
    for i := 0 to CollectionCount-1 do
    begin
      DefaultTemplatesRecPtr := Default_Templates_Collection.At(i);
      //Add if matching category
      if (DefaultTemplatesRecPtr^.Category_ID = LeagueTab.TabIndex+1) then
      begin
        //Add row
        inc(j);
        DefaultTemplatesGrid.Rows := j;
        DefaultTemplatesGrid.Cell[1,j] := IntToStr(j); //Index
        DefaultTemplatesGrid.Cell[2,j] := DefaultTemplatesRecPtr^.Category_Name; //League
        DefaultTemplatesGrid.Cell[3,j] := DefaultTemplatesRecPtr^.Template_ID;
        if (DefaultTemplatesRecPtr^.Template_ID < 1000) OR (DefaultTemplatesRecPtr^.Template_ID > 10000) then
          DefaultTemplatesGrid.Cell[4,j] := '1-Line'
        else
          DefaultTemplatesGrid.Cell[4,j] := '2-Line';
        DefaultTemplatesGrid.Cell[5,j] := DefaultTemplatesRecPtr^.Template_Description;
        DefaultTemplatesGrid.Cell[6,j] := DefaultTemplatesRecPtr^.Enabled;
        DefaultTemplatesGrid.RowColor[j] := clWindow;
      end;
    end;
  end;
  //Refresh grid
  if (CurrentTopRow > 0) AND (j > 0) then
  begin
    if (CurrentTopRow > j) then CurrentTopRow := j;
    DefaultTemplatesGrid.TopRow := CurrentTopRow;
  end;
  if (CurrentRow > 0) AND (j > 0) then
  begin
    if (CurrentRow > j) then CurrentRow := j;
    DefaultTemplatesGrid.CurrentDataRow := CurrentRow;
    DefaultTemplatesGrid.RowSelected[CurrentRow] := TRUE;
  end;
end;

//General procedure for appending an entry to the default template list
procedure TMainForm.AddDefaultTemplateEntry(CurrentTemplateID: SmallInt);
var
  i,j,k,m: SmallInt;
  DefaultTemplatesRecPtr: ^DefaultTemplatesRec;
  SaveRow: SmallInt;
  CurrentTemplateDefs: TemplateDefsRec;
  CollectionCount: SmallInt;
  CategoryRecPtr: ^CategoryRec;
begin
  //Init
  SaveRow := DefaultTemplatesGrid.CurrentDataRow;

  //Check to see if category tab is a sport; if so, show and populate games grid
  CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);

  //Ticker
  CollectionCount := Default_Templates_Collection.Count;

  if (CollectionCount < 500) then
  begin
    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
    //First, check to make sure that there are games listed if the template requires one
    CurrentTemplateDefs := GetTemplateInformation(CurrentTemplateID);
    GetMem (DefaultTemplatesRecPtr, SizeOf(DefaultTemplatesRec));
    With DefaultTemplatesRecPtr^ do
    begin
      //Set values for collection record
      Category_ID := CategoryRecPtr^.Category_ID;
      Category_Name := CategoryRecPtr^.Category_Description;
      Template_Index := Default_Templates_Collection.Count+1;
      Template_ID := CurrentTemplateID;
      Template_Description := CurrentTemplateDefs.Template_Description;
      Enabled := TRUE;

      //Insert object to collection
      Default_Templates_Collection.Insert(DefaultTemplatesRecPtr);
      Default_Templates_Collection.Pack;
    end;

    //Refresh the Default Templates grid
    RefreshDefaultTemplatesGrid;
    //Restore current grid row
    if (SaveRow < 1) then SaveRow := 1;
    DefaultTemplatesGrid.CurrentDataRow := SaveRow;
  end;
end;

//General procedure for inserting an entry into the default template list
procedure TMainForm.InsertDefaultTemplateEntry(CurrentTemplateID: SmallInt);
var
  i,j,k,m: SmallInt;
  DefaultTemplatesRecPtr: ^DefaultTemplatesRec;
  SaveRow: SmallInt;
  CurrentTemplateDefs: TemplateDefsRec;
  CollectionCount: SmallInt;
  CategoryRecPtr: ^CategoryRec;
begin
  //Init
  SaveRow := DefaultTemplatesGrid.CurrentDataRow;

  //Check to see if category tab is a sport; if so, show and populate games grid
  CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);

  //Ticker
  CollectionCount := Default_Templates_Collection.Count;

  if (CollectionCount < 500) then
  begin
    CategoryRecPtr := Categories_Collection.At (LeagueTab.TabIndex);
    //First, check to make sure that there are games listed if the template requires one
    CurrentTemplateDefs := GetTemplateInformation(CurrentTemplateID);
    GetMem (DefaultTemplatesRecPtr, SizeOf(DefaultTemplatesRec));
    With DefaultTemplatesRecPtr^ do
    begin
      //Set values for collection record
      Category_ID := CategoryRecPtr^.Category_ID;
      Category_Name := CategoryRecPtr^.Category_Description;
      Template_Index := Default_Templates_Collection.Count+1;
      Template_ID := CurrentTemplateID;
      Template_Description := CurrentTemplateDefs.Template_Description;
      Enabled := TRUE;

      //Insert object to collection
      if (DefaultTemplatesGrid.CurrentDataRow > 0) then
      begin
        Default_Templates_Collection.AtInsert(DefaultTemplatesGrid.CurrentDataRow-1, DefaultTemplatesRecPtr);
      end
      else
        Default_Templates_Collection.Insert(DefaultTemplatesRecPtr);
      Default_Templates_Collection.Pack;
    end;

    //Refresh the Default Templates grid
    RefreshDefaultTemplatesGrid;
    //Restore current grid row
    if (SaveRow < 1) then SaveRow := 1;
    DefaultTemplatesGrid.CurrentDataRow := SaveRow;
  end;
end;

//Handler to confirm league change with operator; will clear collection
procedure TMainForm.LeagueTabChanging(Sender: TObject;
  var AllowChange: Boolean);
var
  Control: Word;
begin
  //Confirm before allowing change
  if (Default_Templates_Collection.Count > 0) then
  begin
    Control := MessageDlg('Are you sure you want to change leagues? Any changes made to the default ' +
      'templates list for this league will be lost if they have not been saved to the database.',
      mtConfirmation, [mbYes, mbNo], 0);
    if (Control = mrYes) then
    begin
      //Clear collection and allow change
      Default_Templates_Collection.Clear;
      Default_Templates_Collection.Pack;
      RefreshDefaultTemplatesGrid;
    end
    //Disallow league change
    else AllowChange := FALSE;
  end;
end;

//Handler for select all pop-up entry for Default Templates Grid
procedure TMainForm.MenuItem6Click(Sender: TObject);
var
  i: SmallInt;
  DefaultTemplatesRecPtr: ^DefaultTemplatesRec;
begin
  if (Default_Templates_Collection.Count > 0) then
  begin
    for i := 0 to Default_Templates_Collection.Count-1 do
    begin
      DefaultTemplatesRecPtr := Default_Templates_Collection.At(i);
      DefaultTemplatesGrid.RowSelected[i+1] := TRUE;
      DefaultTemplatesRecPtr^.Selected := TRUE;
    end;
  end;
end;

//Handler for de-select all pop-up entry for Default Templates Grid
procedure TMainForm.MenuItem7Click(Sender: TObject);
var
  i: SmallInt;
  DefaultTemplatesRecPtr: ^DefaultTemplatesRec;
begin
  if (Default_Templates_Collection.Count > 0) then
  begin
    for i := 0 to Default_Templates_Collection.Count-1 do
    begin
      DefaultTemplatesRecPtr := Default_Templates_Collection.At(i);
      DefaultTemplatesGrid.RowSelected[i+1] := FALSE;
      DefaultTemplatesRecPtr^.Selected := FALSE;
    end;
  end;
end;

//Handler for enable selected templates by default
procedure TMainForm.Enable2Click(Sender: TObject);
var
  i: SmallInt;
  DefaultTemplatesRecPtr: ^DefaultTemplatesRec;
begin
  if (Default_Templates_Collection.Count > 0) then
  begin
    for i := 0 to Default_Templates_Collection.Count-1 do
    begin
      DefaultTemplatesRecPtr := Default_Templates_Collection.At(i);
      if (DefaultTemplatesGrid.RowSelected[i+1] = TRUE) then
        DefaultTemplatesRecPtr^.Enabled := TRUE;
    end;
  end;
  //Refresh the grid
  RefreshDefaultTemplatesGrid;
end;

//Handler for disable selected templates by default
procedure TMainForm.DisablebyDefault1Click(Sender: TObject);
var
  i: SmallInt;
  DefaultTemplatesRecPtr: ^DefaultTemplatesRec;
begin
  if (Default_Templates_Collection.Count > 0) then
  begin
    for i := 0 to Default_Templates_Collection.Count-1 do
    begin
      DefaultTemplatesRecPtr := Default_Templates_Collection.At(i);
      if (DefaultTemplatesGrid.RowSelected[i+1] = TRUE) then
        DefaultTemplatesRecPtr^.Enabled := FALSE;
    end;
  end;
  //Refresh the grid
  RefreshDefaultTemplatesGrid;
end;

//Handler for load default templates list
procedure TMainForm.LoadTemplateListBtnClick(Sender: TObject);
var
  DefaultTemplatesRecPtr: ^DefaultTemplatesRec;
  Control: Word;
  OKToGo: Boolean;
  CollectionCount: SmallInt;
begin
  //Init
  OKToGo := TRUE;
  CollectionCount := Default_Templates_Collection.Count;
  if (CollectionCount > 0) then
  begin
    Control := MessageDlg('There are currently entries in the default templates list. ' +
      'Loading the list again will overwrite any edits. Do you want to proceed?',
      mtWarning, [mbYes, mbNo], 0);
    if (Control <> mrYes) then OKToGo := FALSE;
  end;
  //If operator confirmed, save the playlist out
  if (OkToGo) then
  try
    //Clear the collection
    Default_Templates_Collection.Clear;
    Default_Templates_Collection.Pack;

    //Query for the elements table for those that match the playlist ID
    dmMain.Query1.Close;
    dmMain.Query1.SQL.Clear;
    dmMain.Query1.SQL.Add('SELECT * FROM Default_Templates ' +
                             'WHERE Category_ID = ' + IntToStr(LeagueTab.TabIndex+1));
    dmMain.Query1.Open;
    if (dmMain.Query1.RecordCount > 0) then

    //Iterate through all records in the table
    repeat
      GetMem (DefaultTemplatesRecPtr, SizeOf(DefaultTemplatesRec));
      With DefaultTemplatesRecPtr^ do
      begin
        //Set values for collection record
        Category_ID := dmMain.Query1.FieldByName('Category_ID').AsInteger;
        Category_Name := dmMain.Query1.FieldByName('Category_Name').AsString;
        Template_Index := dmMain.Query1.FieldByName('Template_Index').AsInteger;
        Template_ID := dmMain.Query1.FieldByName('Template_ID').AsInteger;
        Template_Description := dmMain.Query1.FieldByName('Template_Description').AsString;
        Enabled := dmMain.Query1.FieldByName('Enabled').AsBoolean;
        Selected := TRUE;
        //Insert the object
        Default_Templates_Collection.Insert(DefaultTemplatesRecPtr);
        Default_Templates_Collection.Pack;
        //Go to next record in query
        dmMain.Query1.Next;
      end;
    until (dmMain.Query1.EOF = TRUE);
    //Refresh the grid
    RefreshDefaultTemplatesGrid;
  except
    MessageDlg ('Error occurred while trying to load the default templates from the database.',
                 mtError, [mbOk], 0);
  end;
end;

//Handler for save default templates list
procedure TMainForm.SaveTemplateListBtnClick(Sender: TObject);
begin
  SaveDefaultTemplatesList(LeagueTab.TabIndex+1, FALSE);
end;

//Handler for save & close default templates list
procedure TMainForm.SaveCloseTemplateListBtnClick(Sender: TObject);
begin
  SaveDefaultTemplatesList(LeagueTab.TabIndex+1, TRUE);
end;

//General procedure to save default templates collection out to database
procedure TMainForm.SaveDefaultTemplatesList(Category_ID: SmallInt; ClearCollection: Boolean);
var
  i: SmallInt;
  OKToGo: Boolean;
  DefaultTemplatesRecPtr: ^DefaultTemplatesRec;
  Control: Word;
  Save_Cursor: TCursor;
begin
  //Init
  OKToGo := TRUE;
  
  //Check for entries in collection
  if (Default_Templates_Collection.Count =0) then
  begin
    Control := MessageDlg ('No default templates have been specified', mtError, [mbOk], 0);
    Exit;
  end;

  //Confirm
  Control := MessageDlg ('Are you sure you want to overwrite the default templates currently stored ' +
                         'in the database for this league?', mtConfirmation, [mbYes, mbNo], 0);
  if (Control <> mrYes) then OKToGo := FALSE;

  if (OKToGo) then
  begin
    //Change cursor
    Save_Cursor := Screen.Cursor;
    //Show hourglass cursor
    Screen.Cursor := crHourGlass;

    //Insert all elements into Default Templates table
    try
      //Open the table
      dmMain.tblDefault_Templates.Active := TRUE;
      dmMain.tblDefault_Templates.First;
      //Delete the existing entries from the database
      While(dmMain.tblDefault_Templates.EOF = FALSE) do
      begin
        if (dmMain.tblDefault_Templates.FieldByName('Category_ID').AsString = IntToStr(Category_ID)) then
          dmMain.tblDefault_Templates.Delete
        else
          dmMain.tblDefault_Templates.Next;
      end;
      //Iterate through the collection
      for i := 0 to Default_Templates_Collection.Count-1 do
      begin
        //Point to collection object
        DefaultTemplatesRecPtr := Default_Templates_Collection.At(i);
        //Append graphic record to graphics pages table in database}
        dmMain.tblDefault_Templates.AppendRecord ([
          DefaultTemplatesRecPtr^.Category_ID,
          DefaultTemplatesRecPtr^.Category_Name,
          DefaultTemplatesRecPtr^.Template_Index,
          DefaultTemplatesRecPtr^.Template_ID,
          DefaultTemplatesRecPtr^.Template_Description,
          DefaultTemplatesRecPtr^.Enabled
        ]);
      end;
      dmMain.tblDefault_Templates.Active := FALSE;
    except
      MessageDlg ('Error occurred while trying to insert default templates element into database.',
                   mtError, [mbOk], 0);
    end;
    if (ClearCollection) then
    begin
      Default_Templates_Collection.Clear;
      Default_Templates_Collection.Pack;
    end;
    //Refresh the grid
    RefreshDefaultTemplatesGrid;
    //Restore cursor
    Screen.Cursor := Save_Cursor;
  end;
end;

//Handler for delete selected items from default templates list
procedure TMainForm.MenuItem18Click(Sender: TObject);
var
  i: SmallInt;
  DefaultTemplatesRecPtr: ^DefaultTemplatesRec;
  CollectionCount: SmallInt;
  Control: Word;
begin
  Control := MessageDlg('Are you sure you want to delete the selected records?', mtConfirmation, [mbYes, mbNo], 0);
  if (Control = mrYes) then
  begin
    CollectionCount := Default_Templates_Collection.Count;
    If ((CollectionCount > 0) AND (DefaultTemplatesGrid.Rows >= 0)) then
    begin
      for i := 0 to Default_Templates_Collection.Count-1 do
      begin
        DefaultTemplatesRecPtr := Default_Templates_Collection.At(i);
        if (DefaultTemplatesGrid.RowSelected[i+1] = TRUE) then DefaultTemplatesRecPtr^.Selected := TRUE
        else DefaultTemplatesRecPtr^.Selected := FALSE;
      end;
      i := 0;
      Repeat
        DefaultTemplatesRecPtr := Default_Templates_Collection.At(i);
        if (DefaultTemplatesRecPtr^.Selected = TRUE) then
        begin
          Default_Templates_Collection.AtDelete(i);
          Default_Templates_Collection.Pack;
        end
        else Inc(i);
      Until (i = Default_Templates_Collection.Count);
      //Refresh the grid
      RefreshDefaultTemplatesGrid;
    end;
  end;
end;

//Handler to use NCAAB Ranks
procedure TMainForm.UseRankingsforNCAABClick(Sender: TObject);
begin
  UseRankingsforNCAAB.Checked := TRUE;
  UseSeedforNCAAB.Checked := FALSE;
  UseSeedingForNCAABGames := FALSE;
  StorePrefs;
end;

//Handler to use NCAAB Seeds
procedure TMainForm.UseSeedforNCAABClick(Sender: TObject);
begin
  UseRankingsforNCAAB.Checked := FALSE;
  UseSeedforNCAAB.Checked := TRUE;
  UseSeedingForNCAABGames := TRUE;
  StorePrefs;
end;

////////////////////////////////////////////////////////////////////////////////
// REMOTE PLAYOUT MODE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Go to remote playout mode
procedure TMainForm.GoToRemotePlayoutMode;
begin
  if (PlayoutControllerInfo.Connected) and not (InRemotePlayoutMode) then
  begin
    //Set flag
    InRemotePlayoutMode := TRUE;
    //Set UI controls
    GotoRemotePlayoutModeMenu.Checked := TRUE;
    ExitRemotePlayoutModeMenu.Checked := FALSE;
    //PlaylistGridPanel.Color := clYellow;
    PlayoutPanel.Visible := TRUE;
    PlaylistModeLabel.Color := clLime;
    PlaylistModeLabel.Caption := 'AUTHORING ONLINE MODE';
    //StatusBar.Color := clYellow;
    //Set the mode on the Playout Controller
    PlayoutInterface.SetPlayoutSlaveMode(ENABLE);
    //Set the grid modes to single row
    MainForm.PlaylistGrid.RowSelectMode := rsSingle;
    MainForm.PlaylistGrid2.RowSelectMode := rsSingle;
    //Load the currently loaded Authoring tool playlist into the Playout Controller
    if (PlaylistInfo[TICKER].PlaylistID <> 0) then
      PlayoutInterface.LoadPlaylist(PlaylistInfo[TICKER].PlaylistID);
  end
  else
    MessageDlg ('Not connected to remote Playout Controller.', mtError, [mbOk], 0);
end;
//Menu handler
procedure TMainForm.GotoRemotePlayoutModeMenuClick(Sender: TObject);
begin
  GoToRemotePlayoutMode;
end;
//Button handler
procedure TMainForm.GoToRemoteModeBtnClick(Sender: TObject);
begin
  GoToRemotePlayoutMode;
end;

//Exit remote playout mode
procedure TMainForm.ExitRemotePlayoutMode;
begin
  //if (PlayoutControllerInfo.Connected) then
  if (True) then
  begin
    if (InRemotePlayoutMode) then
    begin
      //Set flag
      InRemotePlayoutMode := FALSE;
      //Set UI controls
      GotoRemotePlayoutModeMenu.Checked := FALSE;
      ExitRemotePlayoutModeMenu.Checked := TRUE;
      //PlaylistGridPanel.Color := clBtnFace;
      PlayoutPanel.Visible := FALSE;
      PlaylistModeLabel.Color := clSilver;
      PlaylistModeLabel.Caption := 'AUTHORING OFFLINE MODE';
      StatusBar.Color := clBtnFace;
      //Set the mode on the Playout Controller
      PlayoutInterface.SetPlayoutSlaveMode(FALSE);
      //Set the grid mode to multiple rows
      MainForm.PlaylistGrid.RowSelectMode := rsMulti;
    end;
  end
  else
    MessageDlg ('Not connected to remote Playout Controller.', mtError, [mbOk], 0);
end;
//Menu handler
procedure TMainForm.ExitRemotePlayoutModeMenuClick(Sender: TObject);
var
  i: SmallInt;
begin
  ExitRemotePlayoutMode;
  //Remove any remote control row colors
  if (MainForm.PlaylistGrid.Rows > 0) then
    for i := 1 to MainForm.PlaylistGrid.Rows do
      MainForm.PlaylistGrid.RowColor[i] := clWindow;
  if (MainForm.PlaylistGrid2.Rows > 0) then
    for i := 1 to MainForm.PlaylistGrid2.Rows do
      MainForm.PlaylistGrid2.RowColor[i] := clWindow;
end;
//Button handler
procedure TMainForm.ExitRemoteModeBtnClick(Sender: TObject);
var
  i: SmallInt;
begin
  ExitRemotePlayoutMode;
  //Remove any remote control row colors
  if (MainForm.PlaylistGrid.Rows > 0) then
    for i := 1 to MainForm.PlaylistGrid.Rows do
      MainForm.PlaylistGrid.RowColor[i] := clWindow;
  if (MainForm.PlaylistGrid2.Rows > 0) then
    for i := 1 to MainForm.PlaylistGrid2.Rows do
      MainForm.PlaylistGrid2.RowColor[i] := clWindow;
end;

//Manually re-connect to remote Playout Controller
procedure TMainForm.ReconnnecttoPlayoutController1Click(Sender: TObject);
begin
  //Connect to the Playout Controller for remote control if enabled
  if (PlayoutControllerInfo.Enabled = TRUE) then
  begin
    try
      EngineParameters.Enabled := TRUE;
      PlayoutInterface.PlayoutConnection.Address := PlayoutControllerInfo.IPAddress;
      PlayoutInterface.PlayoutConnection.Port := PlayoutControllerInfo.Port;
      PlayoutInterface.PlayoutConnection.Active := TRUE;
    except
      if (ErrorLoggingEnabled = True) then
      begin
        Error_Condition := True;
        Label16.Caption := 'ERROR';
        //WriteToErrorLog
        EngineInterface.WriteToErrorLog('Error occurred while trying connect to Playout Controller');
      end;
    end;
  end;
end;

//Handler for checking/unchecking auto playback enable checkbox
procedure TMainForm.AutoPlaybackEnableCheckboxClick(Sender: TObject);
begin
  if (AutoPlaybackEnableCheckbox.Checked) then
  begin
    //LoadPlaylistBtn.Visible := FALSE;
    //UnloadCurrentPlaylistBtn.Visible := FALSE;
    InRemoteAutoSequenceMode := TRUE;
    RemoveAllFieldsBtn.Visible := FALSE;
    PlayNextField1Btn.Visible := FALSE;
    PlayNextField2Btn.Visible := FALSE;
    PlayNextBothBtn.Visible := FALSE;
    PlaySelectedField1Btn.Visible := FALSE;
    PlaySelectedField2Btn.Visible := FALSE;
    PlaySelectedBothBtn.Visible := FALSE;
    TriggerTickerBtn.Visible := TRUE;
    AbortPlaylistBtn.Visible := TRUE;
    ResetTickerBtn.Visible := TRUE;
    //Set the mode on the Playout Controller
    PlayoutInterface.SetAutoPlayMode(TRUE);
    //Show status LEDs
    PlaylistRunningLED.Visible := TRUE;
    PlaylistHaltedLED.Visible := TRUE;
  end
  else begin
    //LoadPlaylistBtn.Visible := TRUE;
    //UnloadCurrentPlaylistBtn.Visible := TRUE;
    InRemoteAutoSequenceMode := FALSE;
    RemoveAllFieldsBtn.Visible := TRUE;
    PlayNextField1Btn.Visible := TRUE;
    PlayNextField2Btn.Visible := TRUE;
    PlayNextBothBtn.Visible := TRUE;
    PlaySelectedField1Btn.Visible := TRUE;
    PlaySelectedField2Btn.Visible := TRUE;
    PlaySelectedBothBtn.Visible := TRUE;
    TriggerTickerBtn.Visible := FALSE;
    AbortPlaylistBtn.Visible := FALSE;
    ResetTickerBtn.Visible := FALSE;
    //Halt the ticker in case it's running
    PlayoutInterface.PauseTicker;
    //Set the mode on the Playout Controller
    PlayoutInterface.SetAutoPlayMode(FALSE);
    //Hide status LEDs
    PlaylistRunningLED.Visible := FALSE;
    PlaylistHaltedLED.Visible := FALSE;
  end;
end;

//COMMANDS FOR LOADING/UNLOADING PLAYLISTS REMOTELY
//Launch dialog to load a specific zipper collection
procedure TMainForm.LoadPlaylistBtnClick(Sender: TObject);
begin
  LoadZipperCollection;
end;

procedure TMainForm.UnloadCurrentPlaylistBtnClick(Sender: TObject);
begin
  //Clear the ticker collection
  Ticker_Collection.Clear;
  Ticker_Collection.Pack;
  //Refresh the grids
  RefreshPlaylistGrid(FIELD_1, PlaylistGrid);
  RefreshEntryFieldsGrid(TickerDisplayMode, FIELD_1, EntryFieldsGrid);
  //Send command to playout
  PlayoutInterface.UnloadCurrentPlaylist;
  //De-bounce
  DisableRemoteControlButtons;
  DebounceTimer.Enabled := TRUE;
end;

//COMMANDS FOR AUTO-SEQUENCE ENABLE MODE
//Trigger ticker to start on Playout Controller
procedure TMainForm.TriggerTickerBtnClick(Sender: TObject);
begin
  PlayoutInterface.TriggerTicker;
end;

//Pause ticker on Playout Controller
procedure TMainForm.AbortPlaylistBtnClick(Sender: TObject);
begin
  PlayoutInterface.PauseTicker;
end;

//Reset ticker to top on Playout Controller
procedure TMainForm.ResetTickerBtnClick(Sender: TObject);
begin
  PlayoutInterface.ResetTickerToTop;
end;

//COMMANDS FOR MANUAL TRIGGERING (NOT AUTO-SEQUENCING)
//Remove all fields - sends command to Playout
procedure TMainForm.RemoveAllFieldsBtnClick(Sender: TObject);
begin
  PlayoutInterface.RemoveAllFields;
  //De-bounce
  DisableRemoteControlButtons;
  DebounceTimer.Enabled := TRUE;
end;

//Play next page in playlist
//Field 1
procedure TMainForm.PlayNextField1BtnClick(Sender: TObject);
begin
  PlayoutInterface.PlayNextPage(PLAYLIST_1);
  //De-bounce
  DisableRemoteControlButtons;
  DebounceTimer.Enabled := TRUE;
end;
//Field 2
procedure TMainForm.PlayNextField2BtnClick(Sender: TObject);
begin
  PlayoutInterface.PlayNextPage(PLAYLIST_2);
  //De-bounce
  DisableRemoteControlButtons;
  DebounceTimer.Enabled := TRUE;
end;
//Both fields
procedure TMainForm.PlayNextBothBtnClick(Sender: TObject);
begin
  PlayoutInterface.PlayNextPage(PLAYLIST_1);
  PlayoutInterface.PlayNextPage(PLAYLIST_2);
  //De-bounce
  DisableRemoteControlButtons;
  DebounceTimer.Enabled := TRUE;
end;

//Play selected page
//Field 1
procedure TMainForm.PlaySelectedField1BtnClick(Sender: TObject);
begin
  //Send command to play selected page
  if (PlaylistGrid.CurrentDataRow <= 0) then PlaylistGrid.CurrentDataRow := 1;
  PlayoutInterface.PlaySpecifiedPage(PLAYLIST_1, PlaylistGrid.CurrentDataRow-1);
  //De-bounce
  DisableRemoteControlButtons;
  DebounceTimer.Enabled := TRUE;
end;
//Field 2
procedure TMainForm.PlaySelectedField2BtnClick(Sender: TObject);
begin
  //Send command to play selected page
  if (PlaylistGrid2.CurrentDataRow <= 0) then PlaylistGrid2.CurrentDataRow := 1;
  PlayoutInterface.PlaySpecifiedPage(PLAYLIST_2, PlaylistGrid2.CurrentDataRow-1);
  //De-bounce
  DisableRemoteControlButtons;
  DebounceTimer.Enabled := TRUE;
end;
//Both fields
procedure TMainForm.PlaySelectedBothBtnClick(Sender: TObject);
begin
  //Send command to play selected page - Playlist 1
  if (PlaylistGrid.CurrentDataRow <= 0) then PlaylistGrid.CurrentDataRow := 1;
  PlayoutInterface.PlaySpecifiedPage(PLAYLIST_1, PlaylistGrid.CurrentDataRow-1);
  //Lock out update to Playlist 2 if Playlist 1 set to a Field 3 template
  if (LastTemplateInfo[PLAYLIST_1].LastFieldTypeID <> FIELD_3) then
  begin
    //Send command to play selected page - Playlist 2
    if (PlaylistGrid2.CurrentDataRow <= 0) then PlaylistGrid2.CurrentDataRow := 1;
    PlayoutInterface.PlaySpecifiedPage(PLAYLIST_2, PlaylistGrid2.CurrentDataRow-1);
  end;
  //De-bounce
  DisableRemoteControlButtons;
  DebounceTimer.Enabled := TRUE;
end;

//Function to check CTRL key state
function isCtrlDown : Boolean;
var
  State: TKeyboardState;
begin { isCtrlDown }
  GetKeyboardState(State);
  Result := ((State[VK_CONTROL] and 128)<>0);
end; { isCtrlDown }

//For function key processing
//Handler for key down - to check for Function key press
procedure TMainForm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  Flag: Boolean;
begin
  Flag := FALSE;
  if (Shift = [ssCtrl]) then Flag := TRUE;
  ProcessFunctionKey(Key, Flag);
end;
procedure TMainForm.ProcessFunctionKey (Key: Word; IsCtrlKey: Boolean);
begin
  //F1: PLAY NEXT, PLAYLIST 1
  if (Key = VK_F1) then
  begin
    PlayoutInterface.PlayNextPage(PLAYLIST_1);
  end
  //F2: PLAY SELECTED, PLAYLIST 1
  else if (Key = VK_F2) then
  begin
    //Send command to play selected page
    if (PlaylistGrid.CurrentDataRow <= 0) then PlaylistGrid.CurrentDataRow := 1;
    PlayoutInterface.PlaySpecifiedPage(PLAYLIST_1, PlaylistGrid.CurrentDataRow-1);
  end
  //F3: PLAY NEXT, PLAYLIST 2
  else if (Key = VK_F3) then
  begin
    PlayoutInterface.PlayNextPage(PLAYLIST_2);
  end
  //F4: PLAY SELECTED, PLAYLIST 2
  else if (Key = VK_F4) then
  begin
    //Send command to play selected page
    if (PlaylistGrid2.CurrentDataRow <= 0) then PlaylistGrid2.CurrentDataRow := 1;
    PlayoutInterface.PlaySpecifiedPage(PLAYLIST_2, PlaylistGrid2.CurrentDataRow-1);
  end
  //F5 or SPACEBAR = PLAY NEXT, BOTH PLAYLISTS
  else if (Key = VK_F5) or (Key = VK_SPACE) then
  begin
    PlayoutInterface.PlayNextPage(PLAYLIST_1);
    PlayoutInterface.PlayNextPage(PLAYLIST_2);
  end
  //F6: PLAY SELECTED, BOTH PLAYLISTS
  else if (Key = VK_F6) then
  begin
    //Send command to play selected page - Playlist 1
    if (PlaylistGrid.CurrentDataRow <= 0) then PlaylistGrid.CurrentDataRow := 1;
    PlayoutInterface.PlaySpecifiedPage(PLAYLIST_1, PlaylistGrid.CurrentDataRow-1);
    //Lock out update to Playlist 2 if Playlist 1 set to a Field 3 template
    if (LastTemplateInfo[PLAYLIST_1].LastFieldTypeID <> FIELD_3) then
    begin
      //Send command to play selected page - Playlist 2
      if (PlaylistGrid2.CurrentDataRow <= 0) then PlaylistGrid2.CurrentDataRow := 1;
      PlayoutInterface.PlaySpecifiedPage(PLAYLIST_2, PlaylistGrid2.CurrentDataRow-1);
    end;
  end
  //F9: CLEAR ALL FIELD 1,2,3 DATA & BACKPLATES
  else if (Key = VK_F9) then
  begin
    PlayoutInterface.RemoveAllFields;
  end
  //Handlers for CTRL keys to make selected grid entries the current on-air entries
  //CTRL-L - Left-side playlist
  else if (Key = Ord('L')) and (isCtrlDown) then
  begin
    //Send command to play selected page
    if (PlaylistGrid.CurrentDataRow <= 0) then PlaylistGrid.CurrentDataRow := 1;
    PlayoutInterface.PlaySpecifiedPage(PLAYLIST_1, PlaylistGrid.CurrentDataRow-1);
  end
  //CTRL-R - Right-side playlist
  else if (Key = Ord('R')) and (isCtrlDown) then
  begin
    //Send command to play selected page
    if (PlaylistGrid2.CurrentDataRow <= 0) then PlaylistGrid2.CurrentDataRow := 1;
    PlayoutInterface.PlaySpecifiedPage(PLAYLIST_2, PlaylistGrid2.CurrentDataRow-1);
  end
  //CTRL-B - Both playlists
  else if (Key = Ord('B')) and (isCtrlDown) then
  begin
    //Send command to play selected page
    if (PlaylistGrid.CurrentDataRow <= 0) then PlaylistGrid.CurrentDataRow := 1;
    PlayoutInterface.PlaySpecifiedPage(PLAYLIST_1, PlaylistGrid.CurrentDataRow-1);
    //Lock out update to Playlist 2 if Playlist 1 set to a Field 3 template
    if (LastTemplateInfo[PLAYLIST_1].LastFieldTypeID <> FIELD_3) then
    begin
      if (PlaylistGrid2.CurrentDataRow <= 0) then PlaylistGrid2.CurrentDataRow := 1;
      PlayoutInterface.PlaySpecifiedPage(PLAYLIST_2, PlaylistGrid2.CurrentDataRow-1);
    end;
  end
end;

//For switch de-bounce
//Enable control buttons
procedure TMainForm.EnableRemoteControlButtons;
begin
  RemoveAllFieldsBtn.Enabled := TRUE;
  LoadPlaylistBtn.Enabled := TRUE;
  UnloadCurrentPlaylistBtn.Enabled := TRUE;
  PlayNextField1Btn.Enabled := TRUE;
  PlaySelectedField1Btn.Enabled := TRUE;
  PlayNextField2Btn.Enabled := TRUE;
  PlaySelectedField2Btn.Enabled := TRUE;
  PlayNextBothBtn.Enabled := TRUE;
  PlaySelectedBothBtn.Enabled := TRUE;
end;

//Disable control buttons
procedure TMainForm.DisableRemoteControlButtons;
begin
  RemoveAllFieldsBtn.Enabled := FALSE;
  LoadPlaylistBtn.Enabled := FALSE;
  UnloadCurrentPlaylistBtn.Enabled := FALSE;
  PlayNextField1Btn.Enabled := FALSE;
  PlaySelectedField1Btn.Enabled := FALSE;
  PlayNextField2Btn.Enabled := FALSE;
  PlaySelectedField2Btn.Enabled := FALSE;
  PlayNextBothBtn.Enabled := FALSE;
  PlaySelectedBothBtn.Enabled := FALSE;
end;

//De-bounce timer event handler
procedure TMainForm.DebounceTimerTimer(Sender: TObject);
begin
  DebounceTimer.Enabled := FALSE;
  EnableRemoteControlButtons;
end;

////////////////////////////////////////////////////////////////////////////////
// TRAFFIC FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//View/edit traffic times
procedure TMainForm.ViewTrafficTimesBtnClick(Sender: TObject);
var
  Modal: TTrafficTimesEditorDlg;
  Control: Word;
begin
  //Refresh & launch dialog
  dmMain.tblTraffic_Times.Active := FALSE;
  dmMain.tblTraffic_Times.Active := TRUE;
  Modal := TTrafficTimesEditorDlg.Create(Application);
  try
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    Modal.Free
  end;
end;

//View/edit traffic incidents
procedure TMainForm.ViewTrafficIncidentsBtnClick(Sender: TObject);
var
  Modal: TTrafficIncidentsEditorDlg;
  Control: Word;
begin
  //Refresh & launch dialog
  dmMain.tblTraffic_Incidents.Active := FALSE;
  dmMain.tblTraffic_Incidents.Active := TRUE;
  Modal := TTrafficIncidentsEditorDlg.Create(Application);
  try
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    Modal.Free
  end;
end;

//Clear old traffic incidents
procedure TMainForm.ClearIncidentsBtnClick(Sender: TObject);
begin
  //Prompt for confirmation
  if (MessageDlg('This will delete all incidents older than one (1) hour. Are you sure you want to proceed?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
  with dmMain.TrafficQuery do
  begin
    Close;
    SQL.Clear;
    SQL.Add ('DELETE FROM Traffic_Incidents WHERE UpdateTime < ' + QuotedStr(DateTimeToStr(Now - 1/24)));
    ExecSQL;
    Close;
  end;
end;

//Launch dialog to edit maximum character lengths for text fields
procedure TMainForm.EditTemplateFieldLengthsClick(Sender: TObject);
var
  Modal: TTemplateFieldLengthEditorDlg;
  Control: Word;
begin
  //Refresh & launch dialog
  dmMain.tblTemplate_Defs.Active := FALSE;
  dmMain.tblTemplate_Defs.Active := TRUE;
  dmMain.tblTemplate_Fields.Active := FALSE;
  dmMain.tblTemplate_Fields.Active := TRUE;
  Modal := TTemplateFieldLengthEditorDlg.Create(Application);
  try
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    Modal.Free;
    ReloadDataFromDatabase;
    dmMain.tblTemplate_Defs.Active := FALSE;
    dmMain.tblTemplate_Fields.Active := FALSE;
  end;
end;

//Launch dialog to edit wheel definition for top image region
procedure TMainForm.Edit_Top_region_Image_WheelClick(Sender: TObject);
var
  Modal: TTopBarImageWheelEditDlg;
  Control: Word;
begin
  //Refresh & launch dialog
  dmMain.tblTop_Region_Image_Wheel.Active := FALSE;
  dmMain.tblTop_Region_Image_Wheel.Active := TRUE;
  Modal := TTopBarImageWheelEditDlg.Create(Application);
  try
    //Show the dialog
    Control := Modal.ShowModal;
    //Set initial values
  finally
    Modal.Free;
    dmMain.tblTop_Region_Image_Wheel.Active := FALSE;
  end;
end;

////////////////////////////////////////////////////////////////////////////////
//CLOCK FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
//Handler to bring clock in (go to logo + clock)
procedure TMainForm.ClockOnBtnClick(Sender: TObject);
begin
  //if (ClockIsIn = CLOCK_OUT) then
  begin
    //Command clock on local/preview Channel Box
    EngineInterface.SetClockState(CLOCK_IN);
    //Send command to Playout Controller if in remote mode
    PlayoutInterface.SetRemoteClockMode(CLOCK_IN);
  end;
end;

//Handler to take clock out (go to logo only)
procedure TMainForm.ClockOffBtnClick(Sender: TObject);
begin
  //if (ClockIsIn = CLOCK_IN) then
  begin
    //Command clock on local/preview Channel Box
    EngineInterface.SetClockState(CLOCK_OUT);
    //Send command to Playout Controller if in remote mode
    PlayoutInterface.SetRemoteClockMode(CLOCK_OUT);
  end;
end;

end.
