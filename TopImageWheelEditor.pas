unit TopImageWheelEditor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids_ts, TSGrid, StdCtrls, Buttons, ExtDlgs, Spin, Globals,
  TSDBGrid;

type
  TTopBarImageWheelEditDlg = class(TForm)
    Panel1: TPanel;
    OpenPictureDialog: TOpenPictureDialog;
    AddEntryBtn: TBitBtn;
    DeleteEntryBtn: TBitBtn;
    BitBtn3: TBitBtn;
    DwellTime: TSpinEdit;
    Label1: TLabel;
    TeamsGrid: TtsDBGrid;
    GraphicIndex: TSpinEdit;
    Label2: TLabel;
    Label3: TLabel;
    procedure AddEntryBtnClick(Sender: TObject);
    procedure DeleteEntryBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TopBarImageWheelEditDlg: TTopBarImageWheelEditDlg;

implementation

uses DataModule;

{$R *.dfm}

//Launch file selector dialog
procedure TTopBarImageWheelEditDlg.AddEntryBtnClick(Sender: TObject);
var
  RemoteFilename: String;
  RemoteFileDrive: String;
  HostDrive: String;
  FoundRecord: Boolean;
begin
  //Launch the file selector
  OpenPictureDialog.InitialDir := Top_Region_Image_Mapped_Drive + '\' + Top_Region_Image_Base_Path;
  OpenPictureDialog.Filter := 'All (*.tga;*.tif;*.png;*)|*.tga;*.tif;*.png|TGA Format (*.tga)|*.tga|TIF Format (*.tif)|*.tif|PNG Format (*.png)|*.png';
  HostDrive := Top_Region_Image_Host_Drive;

  //If a file was selected, store the full path with the drive substituted.
  if (OpenPictureDialog.Execute) then
  begin
    RemoteFilename := OpenPictureDialog.Files[0];
    //Replace backslashes with slashes for Channel Box
    RemoteFileDrive := ExtractFileDrive (RemoteFilename);
    RemoteFilename := StringReplace(RemoteFilename, RemoteFileDrive, HostDrive,[]);
    RemoteFilename := StringReplace(RemoteFilename, '\', '/', [rfReplaceAll]);
  end;

  //Check the database table for a duplicate index
  FoundRecord := dmMain.tblTop_Region_Image_Wheel.Locate('GraphicIndex', GraphicIndex.Value, []);
  if (FoundRecord) then
  begin
    MessageDlg('An image with the same Graphic Index was found in the database. The index must be unique.', mtError, [mbOk], 0)
  end
  else begin
    dmMain.tblTop_Region_Image_Wheel.Append;
    dmMain.tblTop_Region_Image_Wheel.FieldByName('GraphicIndex').AsInteger := GraphicIndex.Value;
    dmMain.tblTop_Region_Image_Wheel.FieldByName('GraphicFilePath').AsString := RemoteFileName;
    dmMain.tblTop_Region_Image_Wheel.FieldByName('DwellTime').AsInteger := DwellTime.Value;
    dmMain.tblTop_Region_Image_Wheel.Post;
  end;
end;

//Delete the selected entry from the wheel
procedure TTopBarImageWheelEditDlg.DeleteEntryBtnClick(Sender: TObject);
var
  Control: Word;
begin
  if (dmMain.tblTop_Region_Image_Wheel.RecordCount > 0) then
  begin
    Control := MessageDlg('Are you sure you want to delete this image from the display wheel?', mtError, [mbYes, mbNo], 0);
    if (Control = mrYes) then dmMain.tblTop_Region_Image_Wheel.Delete;
  end;
end;

end.
